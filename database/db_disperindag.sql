-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 06, 2017 at 02:06 
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_disperindag`
--

-- --------------------------------------------------------

--
-- Table structure for table `mst_gudang`
--

CREATE TABLE `mst_gudang` (
  `kd_gudang` int(11) NOT NULL,
  `nm_gudang` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_gudang`
--

INSERT INTO `mst_gudang` (`kd_gudang`, `nm_gudang`) VALUES
(2, 'Gudang Gas');

-- --------------------------------------------------------

--
-- Table structure for table `mst_kecamatan`
--

CREATE TABLE `mst_kecamatan` (
  `id_kecamatan` varchar(7) NOT NULL,
  `kabupaten_id` varchar(4) NOT NULL,
  `nm_kecamatan` varchar(30) NOT NULL,
  `lat` varchar(50) NOT NULL,
  `lon` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_kecamatan`
--

INSERT INTO `mst_kecamatan` (`id_kecamatan`, `kabupaten_id`, `nm_kecamatan`, `lat`, `lon`) VALUES
('3271010', '3271', ' Bogor Selatan', '', ''),
('3271020', '3271', ' Bogor Timur', '', ''),
('3271030', '3271', ' Bogor Utara', '', ''),
('3271040', '3271', ' Bogor Tengah', '', ''),
('3271050', '3271', ' Bogor Barat', '', ''),
('3271060', '3271', ' Tanah Sereal', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `mst_kelurahan`
--

CREATE TABLE `mst_kelurahan` (
  `id_kelurahan` varchar(10) NOT NULL,
  `kecamatan_id` varchar(7) NOT NULL,
  `nm_kelurahan` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_kelurahan`
--

INSERT INTO `mst_kelurahan` (`id_kelurahan`, `kecamatan_id`, `nm_kelurahan`) VALUES
('3271010001', '3271010', 'Mulyaharja'),
('3271010002', '3271010', 'Pamoyanan'),
('3271010003', '3271010', 'Ranggamekar'),
('3271010004', '3271010', 'Genteng'),
('3271010005', '3271010', 'Kertamaya'),
('3271010006', '3271010', 'Rancamaya'),
('3271010007', '3271010', 'Bojongkerta'),
('3271010008', '3271010', 'Harjasari'),
('3271010009', '3271010', 'Muarasari'),
('3271010010', '3271010', 'Pakuan'),
('3271010011', '3271010', 'Cipaku'),
('3271010012', '3271010', 'Lawanggintung'),
('3271010013', '3271010', 'Batutulis'),
('3271010014', '3271010', 'Bondongan'),
('3271010015', '3271010', 'Empang'),
('3271010016', '3271010', 'Cikaret'),
('3271020001', '3271020', 'Sindangsari'),
('3271020002', '3271020', 'Sindangrasa'),
('3271020003', '3271020', 'Tajur'),
('3271020004', '3271020', 'Katulampa'),
('3271020005', '3271020', 'Baranangsiang'),
('3271020006', '3271020', 'Sukasari'),
('3271030001', '3271030', 'Bantarjati'),
('3271030002', '3271030', 'Tegalgundil'),
('3271030003', '3271030', 'Tanahbaru'),
('3271030004', '3271030', 'Cimahpar'),
('3271030005', '3271030', 'Ciluar'),
('3271030006', '3271030', 'Cibuluh'),
('3271030007', '3271030', 'Kedunghalang'),
('3271030008', '3271030', 'Ciparigi'),
('3271040001', '3271040', 'Paledang'),
('3271040002', '3271040', 'Gudang'),
('3271040003', '3271040', 'Babakanpasar'),
('3271040004', '3271040', 'Tegallega'),
('3271040005', '3271040', 'Babakan'),
('3271040006', '3271040', 'Sempur'),
('3271040007', '3271040', 'Pabaton'),
('3271040008', '3271040', 'Cibogor'),
('3271040009', '3271040', 'Panaragan'),
('3271040010', '3271040', 'Kebonkelapa'),
('3271040011', '3271040', 'Ciwaringin'),
('3271050001', '3271050', 'Pasirmulya'),
('3271050002', '3271050', 'Pasirkuda'),
('3271050003', '3271050', 'Pasirjaya'),
('3271050004', '3271050', 'Gunungbatu'),
('3271050005', '3271050', 'Loji'),
('3271050006', '3271050', 'Menteng'),
('3271050007', '3271050', 'Cilendek Timur'),
('3271050008', '3271050', 'Cilendek Barat'),
('3271050009', '3271050', 'Sindangbarang'),
('3271050010', '3271050', 'Margajaya'),
('3271050011', '3271050', 'Balungbangjaya'),
('3271050012', '3271050', 'Situgede'),
('3271050013', '3271050', 'Bubulak'),
('3271050014', '3271050', 'Semplak'),
('3271050015', '3271050', 'Curugmekar'),
('3271050016', '3271050', 'Curug'),
('3271060001', '3271060', 'Kedungwaringin'),
('3271060002', '3271060', 'Kedungjaya'),
('3271060003', '3271060', 'Kebonpedes'),
('3271060004', '3271060', 'Tanahsareal'),
('3271060005', '3271060', 'Kedungbadak'),
('3271060006', '3271060', 'Sukaresmi'),
('3271060007', '3271060', 'Sukadamai'),
('3271060008', '3271060', 'Cibadak'),
('3271060009', '3271060', 'Kayumanis'),
('3271060010', '3271060', 'Mekarwangi'),
('3271060011', '3271060', 'Kencana');

-- --------------------------------------------------------

--
-- Table structure for table `td_distribusi_stok`
--

CREATE TABLE `td_distribusi_stok` (
  `id` int(15) NOT NULL,
  `id_bapok` varchar(30) NOT NULL,
  `tdpud` varchar(50) NOT NULL,
  `stok` int(15) NOT NULL,
  `tgl_input` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_instansi`
--

CREATE TABLE `tr_instansi` (
  `id` int(1) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nama_singkatan` varchar(12) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `kepsek` varchar(100) NOT NULL,
  `nip_kepsek` varchar(100) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `tentang_singkat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_instansi`
--

INSERT INTO `tr_instansi` (`id`, `nama`, `nama_singkatan`, `alamat`, `kepsek`, `nip_kepsek`, `logo`, `no_telp`, `email`, `tentang_singkat`) VALUES
(1, 'Touced By Peoplehurt1337', 'Disperindag', 'Jl. Dadali,Tanah Sareal,Tanah Sereal\nKota Bogor, Jawa Barat, \nIndonesia', 'Drs Test S.kom', '1234i1020', '132.png', '124810348', 'exampe@example.com', 'Dinas Perindustrian dan Perdagangan Kota Bogor merupakan Perangkat Daerah yang melaksanakan tugas urusan di Bidang Perindustrian dan Perdagangan yang memiliki Struktur Organisasi  (SOTK) sebagai berikut: Kepala Dinas; Sekretariat; Bidang Perindustrian; Bidang Sarana Komoditi Perdagangan; Bidang Promosi Kemitraan dan Perdagangan Jasa ; dan Bidang Metrologi dan Tertib Niaga.');

-- --------------------------------------------------------

--
-- Table structure for table `t_admin`
--

CREATE TABLE `t_admin` (
  `admin_id` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` text NOT NULL,
  `nama_user` varchar(50) NOT NULL,
  `level` enum('Petugas','Admin') NOT NULL,
  `nama_agent` varchar(100) NOT NULL,
  `lokasi` text NOT NULL,
  `pemilik` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_admin`
--

INSERT INTO `t_admin` (`admin_id`, `username`, `password`, `nama_user`, `level`, `nama_agent`, `lokasi`, `pemilik`) VALUES
(1, 'admin', '90b9aa7e25f80cf4f64e990b78a9fc5ebd6cecad', 'Administrator', 'Admin', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_agenda`
--

CREATE TABLE `t_agenda` (
  `id_agenda` int(11) NOT NULL,
  `judul_agenda` varchar(100) NOT NULL,
  `kategori_agenda` int(11) DEFAULT NULL,
  `isi_agenda` text,
  `publisher` int(11) DEFAULT NULL,
  `tanggal_posting` datetime DEFAULT NULL,
  `tanggal_agenda` date DEFAULT NULL,
  `cover_agenda` text,
  `publish_status` enum('Y','N') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_agenda`
--

INSERT INTO `t_agenda` (`id_agenda`, `judul_agenda`, `kategori_agenda`, `isi_agenda`, `publisher`, `tanggal_posting`, `tanggal_agenda`, `cover_agenda`, `publish_status`) VALUES
(4, 'Rapat Mendag dan Melantik', 1, '<p><br></p>', 1, '2016-08-26 07:34:09', '2016-08-30', '2.jpg', 'Y'),
(5, 'Rapat Mendag dan TTD', 1, '<h  class="excerpt"  Roboto; margin-bottom: 0px; font-weight: normal; color: rgb(37, 40, 42); font-size: 20px; line-height: 28px;">Politicians have looked weak in the face of such natural disaster, with many facing criticism from local residents for doing little more than turning up as flood tourists at the site of disasters.</h ><div class="text"  0px; padding: 0px; border: 0px; outline-width: 0px; font-size: 16px; font-family: Arial; vertical-align: baseline; color: rgb(255, 255, 255);"><p  11px; margin-bottom: 0px; padding: 1em 0px; border: 0px; outline-width: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(62, 62, 62); line-height: 24px;">Britons are normally never more comfortable than when talking about the weather, but recent extreme weather events have began to test that theory. Since December, the United Kingdom has faced a relentless assault from some of the worst winter weather on record. It began with the worst storm and tidal surges in 60 years hitting the North Sea coastline, floods that ruined Christmas for thousands across Surrey and Dorset and in January, the most exceptional period of rainfall since 1766. The deluge has transformed swathes of southern England into cold, dark lakes, destroying homes and businesses, and in some cases taking lives.</p><p  11px; margin-bottom: 0px; padding: 1em 0px; border: 0px; outline-width: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(62, 62, 62); line-height: 24px;">Politicians have looked weak in the face of such natural disaster, with many facing criticism from local residents for doing little more than turning up as “flood tourists” at the site of disasters, incapable of helping those in crisis and only there for a photo opportunity. The Environment Agency, the body responsible for combating floods and managing rivers, has also been blamed for failing to curb the disasters. But there’s an ever larger debate over the role of climate change in the current floods and storms, and it has been unremittingly hostile.</p></div>', 1, '2016-08-26 07:34:52', '0000-00-00', '21.jpg', 'Y'),
(6, 'Pidato Mendag', 1, '<h  class="excerpt"  Roboto; margin-bottom: 0px; font-weight: normal; color: rgb(37, 40, 42); font-size: 20px; line-height: 28px;">Politicians have looked weak in the face of such natural disaster, with many facing criticism from local residents for doing little more than turning up as flood tourists at the site of disasters.</h ><div class="text"  0px; padding: 0px; border: 0px; outline-width: 0px; font-size: 16px; font-family: Arial; vertical-align: baseline; color: rgb(255, 255, 255);"><p  11px; margin-bottom: 0px; padding: 1em 0px; border: 0px; outline-width: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(62, 62, 62); line-height: 24px;">Britons are normally never more comfortable than when talking about the weather, but recent extreme weather events have began to test that theory. Since December, the United Kingdom has faced a relentless assault from some of the worst winter weather on record. It began with the worst storm and tidal surges in 60 years hitting the North Sea coastline, floods that ruined Christmas for thousands across Surrey and Dorset and in January, the most exceptional period of rainfall since 1766. The deluge has transformed swathes of southern England into cold, dark lakes, destroying homes and businesses, and in some cases taking lives.</p><p  11px; margin-bottom: 0px; padding: 1em 0px; border: 0px; outline-width: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(62, 62, 62); line-height: 24px;">Politicians have looked weak in the face of such natural disaster, with many facing criticism from local residents for doing little more than turning up as “flood tourists” at the site of disasters, incapable of helping those in crisis and only there for a photo opportunity. The Environment Agency, the body responsible for combating floods and managing rivers, has also been blamed for failing to curb the disasters. But there’s an ever larger debate over the role of climate change in the current floods and storms, and it has been unremittingly hostile.</p></div>', 1, '2016-08-26 07:35:17', '2016-08-16', 'event1.jpg', 'Y'),
(8, 'Agenda Test 2', 1, '<p>test</p>', 1, '2016-12-27 06:35:45', '2016-10-28', 'b3.PNG', 'Y'),
(11, 'bu santi', 1, 'laporan mengenai harga bahan pokok', 1, '2016-12-27 08:45:12', '2016-12-20', 'pasar_kebon_kembang-.jpg', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `t_b3`
--

CREATE TABLE `t_b3` (
  `id` int(15) NOT NULL,
  `no_cas` varchar(50) NOT NULL,
  `pos_trf_hs` varchar(50) NOT NULL,
  `kecamatan` varchar(30) NOT NULL,
  `kelurahan` varchar(30) NOT NULL,
  `almt_penyimpanan` text NOT NULL,
  `uraian_brg` text NOT NULL,
  `tata_niaga_impor` varchar(30) NOT NULL,
  `kep_lain_tdk_utk_pgn` varchar(30) NOT NULL,
  `lab` varchar(30) NOT NULL,
  `satuan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_b3`
--

INSERT INTO `t_b3` (`id`, `no_cas`, `pos_trf_hs`, `kecamatan`, `kelurahan`, `almt_penyimpanan`, `uraian_brg`, `tata_niaga_impor`, `kep_lain_tdk_utk_pgn`, `lab`, `satuan`) VALUES
(0, '10043-35-3', 'Ex. 2810.00.00.00', '3271060', '3271060002', 'Cimanggu Pahlawan no.5', 'Asam Borat', 'IT-B2 / IP-B2', '1 Kg', '25 g', '0'),
(2, '1330-43-4', 'Ex.2525.00.00.00', '3271060', '3271060002', '-', 'Sodium borat alam dan pekatannya (dikalsinasi maupun tidak)', 'IT-B2 / IP-B2', '5 Kg', '25 g', 'Kg'),
(3, '7632-04-4', '2840.11.00.00', '3271060', '3271060002', '-', 'Dinatrium Tetraborat Anhidrat', 'IT-B2 / IP-B2', '5 Kg', '25 g', 'Kg'),
(4, '1303-96-4', 'Ex. 2840.19.00.00', '3271060', '3271060002', '-', 'Boraks', 'IT-B2 / IP-B2', '5 Kg', '25 g', 'Kg');

-- --------------------------------------------------------

--
-- Table structure for table `t_b3_keluar`
--

CREATE TABLE `t_b3_keluar` (
  `id` int(15) NOT NULL,
  `nm_distributor` varchar(30) NOT NULL,
  `no_cas` varchar(15) NOT NULL,
  `stok_awal` int(15) NOT NULL,
  `jml_pengeluaran` int(15) NOT NULL,
  `stok_akhir` int(15) NOT NULL,
  `nm_pengguna_akhir` varchar(30) NOT NULL,
  `no_rekomendasi` varchar(50) NOT NULL,
  `tgl_input` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_b3_keluar`
--

INSERT INTO `t_b3_keluar` (`id`, `nm_distributor`, `no_cas`, `stok_awal`, `jml_pengeluaran`, `stok_akhir`, `nm_pengguna_akhir`, `no_rekomendasi`, `tgl_input`) VALUES
(1, '1234', '10043-35-3', 60, 10, 50, 'Test', '1234567890', '2017-11-01');

-- --------------------------------------------------------

--
-- Table structure for table `t_b3_penambahan`
--

CREATE TABLE `t_b3_penambahan` (
  `id` int(15) NOT NULL,
  `no_cas` varchar(15) NOT NULL,
  `distributor` varchar(30) NOT NULL,
  `jml_penambahan` int(15) NOT NULL,
  `tgl_input` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_b3_penambahan`
--

INSERT INTO `t_b3_penambahan` (`id`, `no_cas`, `distributor`, `jml_penambahan`, `tgl_input`) VALUES
(1, '10043-35-3', '1234', 50, '2017-10-31'),
(2, '10043-35-3', '1234', 10, '2017-10-31');

-- --------------------------------------------------------

--
-- Table structure for table `t_b3_stok`
--

CREATE TABLE `t_b3_stok` (
  `id` int(15) NOT NULL,
  `no_cas` varchar(10) NOT NULL,
  `distributor` varchar(30) NOT NULL,
  `stok` int(15) NOT NULL,
  `tgl_input` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_b3_stok`
--

INSERT INTO `t_b3_stok` (`id`, `no_cas`, `distributor`, `stok`, `tgl_input`) VALUES
(1, '10043-35-3', '1234', 50, '2017-10-31');

-- --------------------------------------------------------

--
-- Table structure for table `t_bahan_pokok`
--

CREATE TABLE `t_bahan_pokok` (
  `id_bahan_pok` int(11) NOT NULL,
  `nama_bahan_pokok` varchar(100) NOT NULL,
  `harga_min` varchar(20) DEFAULT NULL,
  `harga_max` varchar(20) DEFAULT NULL,
  `tanggal_awal` date DEFAULT NULL,
  `tanggal_akhir` date DEFAULT NULL,
  `satuan` varchar(50) DEFAULT NULL,
  `foto_b_pokok` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_bahan_pokok`
--

INSERT INTO `t_bahan_pokok` (`id_bahan_pok`, `nama_bahan_pokok`, `harga_min`, `harga_max`, `tanggal_awal`, `tanggal_akhir`, `satuan`, `foto_b_pokok`) VALUES
(1, 'Beras', '8500', '9000', '2016-09-10', '2016-09-17', 'Kg', 'beras.jpg'),
(3, 'Gula Pasir', '14000', '15000', '2016-09-10', '2016-09-11', 'Kg', 'Gula_Pasir_DN1.jpg'),
(4, 'Cabai', '43000', '47000', '2016-09-10', '2016-09-17', 'Kg', 'cabe1.jpg'),
(5, 'Telur Ayam', '5500', '9000', '2016-09-10', '2016-09-17', 'Kg ', 'telur_broiler1.jpeg'),
(7, 'Tepung Terigu', '5000', '6000', '2016-09-10', '2016-09-17', 'Kg', 'tepung_terigu1.jpg'),
(8, 'Kelapa', '10000', '12000', '2016-09-02', '2016-09-10', 'Kg', 'beras_ketan_hitam.jpg'),
(9, 'Minyak Goreng', '1221', '12112', '2016-09-08', '2016-09-09', 'Kg', 'beras_pulen.jpg'),
(10, 'Kentang', '12000', '15000', '2016-09-18', '2016-09-24', 'Kg', 'kentang1.jpg'),
(11, 'Bawang', '5000', '6000', '2016-09-18', '2016-09-24', 'Kg', 'bawang_mm1.jpg'),
(12, 'Daging', '', '', '0000-00-00', '0000-00-00', 'Kg ', 'sapi_dan_ayam.jpg'),
(13, 'Susu', NULL, NULL, '2017-09-27', '2017-09-30', 'Gr/ kl', 'susu-bubuk1.jpg'),
(14, 'Jagung', NULL, NULL, '2017-09-27', '2017-09-30', 'Kg', 'jagungpipilan1.jpg'),
(15, 'Garam Beryodium', NULL, NULL, '2017-09-27', '2017-09-30', 'Bh', 'garam_halus1.jpg'),
(17, 'Kacang', NULL, NULL, '2017-09-27', '2017-09-30', 'Kg', 'kacang_kedelai1.jpg'),
(18, 'Mie Instan', NULL, NULL, '2017-09-27', '2017-09-30', 'Bks', 'mie_instan1.jpg'),
(19, 'Tomat', NULL, NULL, '2017-09-27', '2017-09-30', 'Kg', 'tomat1.jpg'),
(20, 'Kol', NULL, NULL, '2017-09-27', '2017-09-30', 'Kg', 'kol_atau_kubis1.jpg'),
(22, 'Ikan', NULL, NULL, '2017-09-27', '2017-09-30', 'Kg', 'tongkol1.jpg'),
(25, 'Ketela', NULL, NULL, '2017-09-27', '2017-09-30', 'Kg', 'ketela1.jpg'),
(26, '0', NULL, NULL, '2017-09-27', '2017-09-30', '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_bahan_pokok_berbahaya`
--

CREATE TABLE `t_bahan_pokok_berbahaya` (
  `id_bahan_pok` int(11) NOT NULL,
  `nama_bahan_pokok` varchar(100) NOT NULL,
  `harga_min` varchar(20) DEFAULT NULL,
  `harga_max` varchar(20) DEFAULT NULL,
  `tanggal_awal` date DEFAULT NULL,
  `tanggal_akhir` date DEFAULT NULL,
  `satuan` varchar(50) DEFAULT NULL,
  `foto_b_pokok` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_bahan_pokok_berbahaya`
--

INSERT INTO `t_bahan_pokok_berbahaya` (`id_bahan_pok`, `nama_bahan_pokok`, `harga_min`, `harga_max`, `tanggal_awal`, `tanggal_akhir`, `satuan`, `foto_b_pokok`) VALUES
(1, 'Boraks', NULL, NULL, NULL, NULL, 'Kg', 'boraks.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `t_bahan_pokok_sub`
--

CREATE TABLE `t_bahan_pokok_sub` (
  `id_bahan_pok_sub` int(11) NOT NULL,
  `nama_sub_bahan_pokok` varchar(100) NOT NULL,
  `harga_min_sub` varchar(20) DEFAULT NULL,
  `harga_max_sub` varchar(20) DEFAULT NULL,
  `kategori` enum('primer','sakunder','tersier','lainnya') NOT NULL DEFAULT 'primer',
  `foto_sub_b_pokok` text,
  `satuan_sub` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_bahan_pokok_sub`
--

INSERT INTO `t_bahan_pokok_sub` (`id_bahan_pok_sub`, `nama_sub_bahan_pokok`, `harga_min_sub`, `harga_max_sub`, `kategori`, `foto_sub_b_pokok`, `satuan_sub`) VALUES
(5, 'Beras Ir 64 (Medium)', '', '', 'primer', 'beras1.jpg', 'Kg'),
(7, 'Bawang Merah', '', '', 'primer', 'bawang_merah1.jpg', 'Kg'),
(8, 'Bawang Putih', '', '', 'primer', 'bawang_putih1.jpg', 'Kg'),
(9, 'Daging Sapi', '', '', 'primer', 'daging_sapi1.jpg', 'Kg'),
(10, 'Daging Ayam', '', '', 'primer', 'daging_ayam_broiler1.jpg', 'Kg'),
(11, 'Minyak Goreng', '', '', 'primer', 'minyak_goreng1.jpg', 'Kg'),
(12, 'Telur Ayam', NULL, NULL, 'sakunder', 'telur_broiler1.jpeg', 'Kg'),
(13, 'Gula Pasir Luar Negeri', NULL, NULL, 'primer', 'Gula_Pasir_LN1.jpg', 'Kg'),
(14, 'Gula Pasir Dalam Negeri', NULL, NULL, 'primer', 'Gula_Pasir_DN1.jpg', 'Kg'),
(15, 'Minyak Goreng Bimoli Botol (620ml/ btl)', NULL, NULL, 'primer', 'minyak_goreng2.jpg', '620ml/btl'),
(16, 'Minyak Goreng Tanpa Kemasan', NULL, NULL, 'primer', 'minyak_tanpa_kemasan1.jpg', 'Kg'),
(17, 'Daging Sapi Murni', NULL, NULL, 'primer', 'daging_sapi2.jpg', 'Kg'),
(18, 'Daging Ayam Broiler', NULL, NULL, 'primer', 'daging_ayam_broiler2.jpg', 'Kg'),
(19, 'Daging Ayam Kampung (per ekor)', NULL, NULL, 'primer', 'daging_ayam_kampung1.jpg', 'ekor'),
(20, 'Telur Ayam Broiler', NULL, NULL, 'primer', 'telur_broiler2.jpeg', 'Kg'),
(21, 'Telur Ayam Kampung (per butir)', NULL, NULL, 'primer', 'telur_kampung1.jpg', NULL),
(22, 'Susu Kental Manis (merk Bendera)', NULL, NULL, 'primer', 'susu_kental_bendera1.jpg', NULL),
(23, 'Susu Kental Manis (merk Indomilk)', NULL, NULL, 'primer', 'susu_kental_indomilk1.jpg', NULL),
(24, 'Susu Bubuk (merk Bendera)', NULL, NULL, 'primer', 'bubuk_bendera1.jpg', NULL),
(25, 'Susu Bubuk (merk Indomilk)', NULL, NULL, 'primer', 'bubuk_indomilk1.jpg', NULL),
(26, 'Garam Beryodium Bata', NULL, NULL, 'primer', 'garam_bata_21.jpg', NULL),
(27, 'Garam Beryodium Halus (250 gr)', NULL, NULL, 'primer', 'garam_halus1.jpg', NULL),
(28, 'Tepung Terigu Segitiga Biru (Kw. Medium)', NULL, NULL, 'primer', 'tepung_terigu1.jpg', NULL),
(29, 'Mie Instan Indomie Rasa Kari Ayam', NULL, NULL, 'primer', 'mie_instan1.jpg', NULL),
(30, 'Cabai Merah Keriting', NULL, NULL, 'primer', 'cabe_merah_keriting1.jpg', 'Kg'),
(31, 'Cabai Merah Biasa', NULL, NULL, 'primer', 'cabe_merah_biasa1.JPG', 'Kg'),
(32, 'Cabai Rawit Merah', NULL, NULL, 'primer', 'cabe1.jpg', 'Kg'),
(33, 'Cabe Rawit', NULL, NULL, 'primer', 'cabe_rawit_ijo1.jpg', NULL),
(34, 'Jagung Pipilan', NULL, NULL, 'primer', 'jagungpipilan1.jpg', NULL),
(35, 'Kacang Kedelai', NULL, NULL, 'primer', 'kacang_kedelai1.jpg', NULL),
(36, 'Kentang', NULL, NULL, 'primer', 'kentang1.jpg', NULL),
(37, 'Tomat Sayur', NULL, NULL, 'primer', 'tomat1.jpg', NULL),
(38, 'Kol', NULL, NULL, 'primer', 'kol_atau_kubis1.jpg', NULL),
(39, 'Ikan Asin Teri No. 2', NULL, NULL, 'primer', 'ikan_asin_teri_no._2_1.jpg', NULL),
(40, 'Kembung', NULL, NULL, 'primer', 'Ikan_Kembung.jpg', 'Kg'),
(41, 'Kacang Hijau', NULL, NULL, 'primer', 'manfaat-kacang-hijau-300x1661.jpg', NULL),
(42, 'Kacang Tanah', NULL, NULL, 'primer', 'kacang_tanah1.jpg', NULL),
(43, 'Ketela Pohon', NULL, NULL, 'primer', 'ketela.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_berita`
--

CREATE TABLE `t_berita` (
  `id_berita` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `isi_berita` text,
  `kategori` int(11) NOT NULL,
  `tanggal_posting` datetime NOT NULL,
  `cover_berita` text,
  `publish_status` enum('Y','N') NOT NULL,
  `publisher` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_berita`
--

INSERT INTO `t_berita` (`id_berita`, `judul`, `isi_berita`, `kategori`, `tanggal_posting`, `cover_berita`, `publish_status`, `publisher`) VALUES
(7, 'Arsip', '<p><br></p>', 1, '2016-09-26 07:57:16', 'blog-vintage-1.jpg', 'Y', 1),
(8, 'Gallery 1', '<p  11px; margin-bottom: 0px; padding: 1em 0px; border: 0px; outline-width: 0px; font-size: 16px; font-family: Arial; vertical-align: baseline; color: rgb(62, 62, 62); line-height: 24px;">For those affected by flooding however, their immediate concerns are not necessarily about the manmade changes to the earth’s atmosphere. A YouGov poll from February found that while 84% of those surveyed believed Britain was likely to experience similar extreme weather events in the next few years, only 30% thought it was connected to man-made climate change.</p><p  11px; margin-bottom: 0px; padding: 1em 0px; border: 0px; outline-width: 0px; font-size: 16px; font-family: Arial; vertical-align: baseline; color: rgb(62, 62, 62); line-height: 24px;">There is no evidence to counter the basic premise that a warmer world will lead to more intense daily and hourly rain events. When heavy rain in 2000 devastated parts of Britain, a later study found the climate change had doubled the chances of the flood occurring, said Julia Slingo.</p>', 2, '2016-08-26 11:14:53', 'image_01_(1).jpg', 'Y', 1),
(9, 'Web IKM Profile', '<p  11px; margin-bottom: 0px; padding: 1em 0px; border: 0px; outline-width: 0px; font-size: 16px; font-family: Arial; vertical-align: baseline; color: rgb(62, 62, 62); line-height: 24px;">Britons are normally never more comfortable than when talking about the weather, but recent extreme weather events have began to test that theory. Since December, the United Kingdom has faced a relentless assault from some of the worst winter weather on record. It began with the worst storm and tidal surges in 60 years hitting the North Sea coastline, floods that ruined Christmas for thousands across Surrey and Dorset and in January, the most exceptional period of rainfall since 1766. The deluge has transformed swathes of southern England into cold, dark lakes, destroying homes and businesses, and in some cases taking lives.</p><p  11px; margin-bottom: 0px; padding: 1em 0px; border: 0px; outline-width: 0px; font-size: 16px; font-family: Arial; vertical-align: baseline; color: rgb(62, 62, 62); line-height: 24px;">Politicians have looked weak in the face of such natural disaster, with many facing criticism from local residents for doing little more than turning up as “flood tourists” at the site of disasters, incapable of helping those in crisis and only there for a photo opportunity. The Environment Agency, the body responsible for combating floods and managing rivers, has also been blamed for failing to curb the disasters. But there’s an ever larger debate over the role of climate change in the current floods and storms, and it has been unremittingly hostile.</p>', 1, '2016-08-27 03:19:10', 'b.PNG', 'Y', 1),
(10, 'Danau Bogor 1', '<p  11px; margin-bottom: 0px; padding: 1em 0px; border: 0px; outline-width: 0px; font-size: 16px; font-family: Arial; vertical-align: baseline; color: rgb(62, 62, 62); line-height: 24px;">Britons are normally never more comfortable than when talking about the weather, but recent extreme weather events have began to test that theory. Since December, the United Kingdom has faced a relentless assault from some of the worst winter weather on record. It began with the worst storm and tidal surges in 60 years hitting the North Sea coastline, floods that ruined Christmas for thousands across Surrey and Dorset and in January, the most exceptional period of rainfall since 1766. The deluge has transformed swathes of southern England into cold, dark lakes, destroying homes and businesses, and in some cases taking lives.</p><p  11px; margin-bottom: 0px; padding: 1em 0px; border: 0px; outline-width: 0px; font-size: 16px; font-family: Arial; vertical-align: baseline; color: rgb(62, 62, 62); line-height: 24px;">Politicians have looked weak in the face of such natural disaster, with many facing criticism from local residents for doing little more than turning up as “flood tourists” at the site of disasters, incapable of helping those in crisis and only there for a photo opportunity. The Environment Agency, the body responsible for combating floods and managing rivers, has also been blamed for failing to curb the disasters. But there’s an ever larger debate over the role of climate change in the current floods and storms, and it has been unremittingly hostile.</p>', 2, '2016-08-27 03:40:09', 'image_03.jpg', 'Y', 1),
(11, 'Berita 1', '<p><br></p>', 1, '2016-10-25 02:23:06', 'disperidag_kantor.png', 'N', 1),
(12, 'test berita2', '<p>testj</p>', 1, '2016-12-27 06:43:07', 'a.PNG', 'N', 1),
(13, 'Pasar Tertib Ukur 2016', '<p><br></p>', 1, '2017-03-07 09:37:28', 'IMG-20170224-WA0010.jpg', 'Y', 1),
(14, 'Touch By K0D1K', '<p>Touch By K0D1K<br></p>', 1, '2017-04-05 07:52:15', '15078705_557798814419356_3337269771310045154_n.jpg', 'Y', 1),
(15, 'Touched By Peoplehurt', 'Assalamualaikum Admin :) Website Securitynya Lemah | Tolong Di ganti Username Sama Password nya | Untuk Password Sekarang Bisa Hubungi Saya di stupidcoder1933@gmail.com :) Maaf Admin | Saya cuma &nbsp;mau membantu :)&nbsp;', 2, '2017-04-05 09:16:41', '16174561_404631686542404_4815728804355100797_n.jpg', 'Y', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_distribusi_keluar`
--

CREATE TABLE `t_distribusi_keluar` (
  `id` int(15) NOT NULL,
  `id_bapok` varchar(30) NOT NULL,
  `tdpud` varchar(50) NOT NULL,
  `stok_awal` int(15) NOT NULL,
  `jml_pengeluaran` int(15) NOT NULL,
  `stok_akhir` int(15) NOT NULL,
  `tujuan` varchar(50) NOT NULL,
  `ket` text NOT NULL,
  `tgl_input` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_distribusi_keluar`
--

INSERT INTO `t_distribusi_keluar` (`id`, `id_bapok`, `tdpud`, `stok_awal`, `jml_pengeluaran`, `stok_akhir`, `tujuan`, `ket`, `tgl_input`) VALUES
(1, '1', '1234', 100, 20, 80, 'Tujuan 1', '', '2017-10-17'),
(2, '11', '1234', 100, 20, 80, 'Tujuan 1', '', '2017-10-17'),
(3, '1', '1234', 80, 30, 50, 'Tujuan 2', '', '2017-10-17'),
(4, '13', '4321', 100, 20, 80, 'Tujuan 3', '', '2017-10-17'),
(5, '3', '1342', 50, 40, 10, 'Tujuan 1', '', '2017-10-17');

-- --------------------------------------------------------

--
-- Table structure for table `t_distribusi_pengadaan`
--

CREATE TABLE `t_distribusi_pengadaan` (
  `id` int(15) NOT NULL,
  `id_bapok` varchar(30) NOT NULL,
  `tdpud` varchar(50) NOT NULL,
  `asal` varchar(50) NOT NULL,
  `jml_pengadaan` int(15) NOT NULL,
  `tgl_input` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_distribusi_pengadaan`
--

INSERT INTO `t_distribusi_pengadaan` (`id`, `id_bapok`, `tdpud`, `asal`, `jml_pengadaan`, `tgl_input`) VALUES
(1, '1', '1234', 'Asal 1', 100, '2017-10-17'),
(2, '11', '1234', 'Asal 2', 100, '2017-10-17'),
(3, '3', '1342', 'Asal 3', 50, '2017-10-17'),
(4, '13', '4321', 'Asal 4', 100, '2017-10-17');

-- --------------------------------------------------------

--
-- Table structure for table `t_distribusi_stok`
--

CREATE TABLE `t_distribusi_stok` (
  `id` int(15) NOT NULL,
  `id_bapok` varchar(30) NOT NULL,
  `tdpud` varchar(50) NOT NULL,
  `stok` int(15) NOT NULL,
  `tgl_input` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_distribusi_stok`
--

INSERT INTO `t_distribusi_stok` (`id`, `id_bapok`, `tdpud`, `stok`, `tgl_input`) VALUES
(1, '1', '1234', 50, '2017-10-17'),
(2, '11', '1234', 80, '2017-10-17'),
(3, '3', '1342', 10, '2017-10-17'),
(4, '13', '4321', 80, '2017-10-17');

-- --------------------------------------------------------

--
-- Table structure for table `t_gallery`
--

CREATE TABLE `t_gallery` (
  `id_gallery` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `kategori` int(11) NOT NULL,
  `isi` text NOT NULL,
  `publisher` int(11) NOT NULL,
  `tanggal_posting` datetime NOT NULL,
  `tanggal` date NOT NULL,
  `gambar` text NOT NULL,
  `publish_status` enum('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_gallery`
--

INSERT INTO `t_gallery` (`id_gallery`, `judul`, `kategori`, `isi`, `publisher`, `tanggal_posting`, `tanggal`, `gambar`, `publish_status`) VALUES
(1, 'Gurun Pasir 1', 2, '<p  11px; margin-bottom: 0px; padding: 1em 0px; border: 0px; outline-width: 0px; font-size: 16px; font-family: Arial; vertical-align: baseline; color: rgb(62, 62, 62); line-height: 24px;">For those affected by flooding however, their immediate concerns are not necessarily about the manmade changes to the earth’s atmosphere. A YouGov poll from February found that while 84% of those surveyed believed Britain was likely to experience similar extreme weather events in the next few years, only 30% thought it was connected to man-made climate change.</p><p  11px; margin-bottom: 0px; padding: 1em 0px; border: 0px; outline-width: 0px; font-size: 16px; font-family: Arial; vertical-align: baseline; color: rgb(62, 62, 62); line-height: 24px;">There is no evidence to counter the basic premise that a warmer world will lead to more intense daily and hourly rain events. When heavy rain in 2000 devastated parts of Britain, a later study found the climate change had doubled the chances of the flood occurring, said Julia Slingo.</p>', 1, '2016-08-26 11:17:22', '0000-00-00', 'image_01_(1).jpg', 'Y'),
(2, 'Danau Bogor 1', 2, '<p><img src="file:///C:/Users/hendrasatrya/Downloads/Compressed/New Media Magazine Template/ThemeForest_-_Pressroom_v1.3_-_Responsive_News_and_Magazine_Template_-_9066845/www/html/images/samples/100x100/image_06.jpg" alt="img"><span  rgb(62, 62, 62); font-family: Arial; font-size: 16px; line-height: 24px;">Britons are normally never more comfortable than when talking about the weather, but recent extreme weather events have began to test that theory. Since December, the United Kingdom has faced a relentless assault from some of the worst winter weather on record. It began with the worst storm and tidal surges in 60 years hitting the North Sea coastline, floods that ruined Christmas for thousands across Surrey and Dorset and in January, the most exceptional period of rainfall since 1766. The deluge has transformed swathes of southern England into cold, dark lakes, destroying homes and businesses, and in some cases taking lives.</span><br></p><p  11px; margin-bottom: 0px; padding: 1em 0px; border: 0px; outline-width: 0px; font-size: 16px; font-family: Arial; vertical-align: baseline; color: rgb(62, 62, 62); line-height: 24px;">Politicians have looked weak in the face of such natural disaster, with many facing criticism from local residents for doing little more than turning up as “flood tourists” at the site of disasters, incapable of helping those in crisis and only there for a photo opportunity. The Environment Agency, the body responsible for combating floods and managing rivers, has also been blamed for failing to curb the disasters. But there’s an ever larger debate over the role of climate change in the current floods and storms, and it has been unremittingly hostile.&nbsp; &nbsp;</p>', 1, '2016-08-26 11:20:34', '0000-00-00', 'image_03.jpg', 'Y'),
(3, 'Ruang Kerja', 1, '<p><br></p>', 1, '2016-09-26 08:02:21', '0000-00-00', 'blog-vintage-2.jpg', 'Y'),
(4, 'Kantor', 1, '<p><br></p>', 1, '2016-09-26 08:03:20', '0000-00-00', 'office-1.jpg', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `t_iupp`
--

CREATE TABLE `t_iupp` (
  `id` int(10) NOT NULL,
  `tgl_pendaftaran` date NOT NULL,
  `nm_perusahaan` varchar(50) NOT NULL,
  `nm_penanggungjwb` varchar(30) NOT NULL,
  `almt_perusahaan` text NOT NULL,
  `kecamatan` varchar(30) NOT NULL,
  `kelurahan` varchar(30) NOT NULL,
  `jenis_usaha` varchar(30) NOT NULL,
  `siup` varchar(50) NOT NULL,
  `tdp` varchar(50) NOT NULL,
  `no_iupp` varchar(50) NOT NULL,
  `tgl_iupp` date NOT NULL,
  `no_rekomendasi` varchar(50) NOT NULL,
  `no_rencana` varchar(50) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_iupp`
--

INSERT INTO `t_iupp` (`id`, `tgl_pendaftaran`, `nm_perusahaan`, `nm_penanggungjwb`, `almt_perusahaan`, `kecamatan`, `kelurahan`, `jenis_usaha`, `siup`, `tdp`, `no_iupp`, `tgl_iupp`, `no_rekomendasi`, `no_rencana`, `foto`) VALUES
(1, '2017-09-20', 'PT Test', 'Akbar', 'Bogor', '3271020', '3271020003', 'Test', '123', '1234', '1234', '2017-09-20', '1234567890', '5413435144', '3993492-natural-hd-wallpaper_(copy).jpg');

-- --------------------------------------------------------

--
-- Table structure for table `t_iupr`
--

CREATE TABLE `t_iupr` (
  `id` int(10) NOT NULL,
  `tgl_pendaftaran` date NOT NULL,
  `nm_perusahaan` varchar(50) NOT NULL,
  `nm_penanggungjwb` varchar(30) NOT NULL,
  `almt_perusahaan` text NOT NULL,
  `kecamatan` varchar(30) NOT NULL,
  `kelurahan` varchar(30) NOT NULL,
  `jenis_usaha` varchar(30) NOT NULL,
  `siup` varchar(50) NOT NULL,
  `tdp` varchar(50) NOT NULL,
  `no_iupr` varchar(50) NOT NULL,
  `tgl_iupr` date NOT NULL,
  `no_rekomendasi` varchar(50) NOT NULL,
  `masaberlaku` date NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_iupr`
--

INSERT INTO `t_iupr` (`id`, `tgl_pendaftaran`, `nm_perusahaan`, `nm_penanggungjwb`, `almt_perusahaan`, `kecamatan`, `kelurahan`, `jenis_usaha`, `siup`, `tdp`, `no_iupr`, `tgl_iupr`, `no_rekomendasi`, `masaberlaku`, `foto`) VALUES
(1, '2017-09-20', 'PT Test', 'Akbar', 'Bogor', '3271010', '3271010009', 'Test', '1234', '1234', '1234', '2017-09-20', '1234567890', '0000-00-00', 'mbuntu-7.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `t_iuts`
--

CREATE TABLE `t_iuts` (
  `id` int(10) NOT NULL,
  `nm_perusahaan` varchar(50) NOT NULL,
  `nm_penanggungjwb` varchar(30) NOT NULL,
  `almt_perusahaan` text NOT NULL,
  `kecamatan` varchar(30) NOT NULL,
  `kelurahan` varchar(30) NOT NULL,
  `npwp` varchar(30) NOT NULL,
  `no_telp` varchar(12) NOT NULL,
  `no_fax` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `keg_usaha_pokok` text NOT NULL,
  `kbli` varchar(30) NOT NULL,
  `status_kantor` enum('Kantor Tunggal','Kantor Pusat') NOT NULL,
  `siup` varchar(50) NOT NULL,
  `tdp` varchar(50) NOT NULL,
  `no_iuts` varchar(50) NOT NULL,
  `tgl_iuts` date NOT NULL,
  `no_rekomendasi` varchar(50) NOT NULL,
  `no_rencana` varchar(50) NOT NULL,
  `masaberlaku` date NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_iuts`
--

INSERT INTO `t_iuts` (`id`, `nm_perusahaan`, `nm_penanggungjwb`, `almt_perusahaan`, `kecamatan`, `kelurahan`, `npwp`, `no_telp`, `no_fax`, `email`, `keg_usaha_pokok`, `kbli`, `status_kantor`, `siup`, `tdp`, `no_iuts`, `tgl_iuts`, `no_rekomendasi`, `no_rencana`, `masaberlaku`, `foto`) VALUES
(1, 'PT Test1', 'Akbar', 'Bogor', '3271010', '3271010009', '-', '081314118157', '-', '-@gmail.com', 'Test', 'Test', 'Kantor Tunggal', '1234', '1234', '1234', '2017-09-20', '1234567890', '5413435144', '2017-09-20', 'mbuntu-21.png'),
(2, 'PT. Test2', 'Farhan', 'Cimanggu', '3271060', '3271060002', 'Test', '081313118157', '-', 'farhan@gmail.com', 'Test', 'Test', 'Kantor Tunggal', '1234', '54332', '8998', '2017-10-15', '-', '-', '2020-10-15', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_kategori_agenda`
--

CREATE TABLE `t_kategori_agenda` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(100) NOT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kategori_agenda`
--

INSERT INTO `t_kategori_agenda` (`id_kategori`, `nama_kategori`, `keterangan`) VALUES
(1, 'Rapat', ''),
(2, 'Pelantikan', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_kategori_berita`
--

CREATE TABLE `t_kategori_berita` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(100) NOT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kategori_berita`
--

INSERT INTO `t_kategori_berita` (`id_kategori`, `nama_kategori`, `keterangan`) VALUES
(1, 'Perdangangan', ''),
(2, 'Perindustrian', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_kategori_gallery`
--

CREATE TABLE `t_kategori_gallery` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(100) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kategori_gallery`
--

INSERT INTO `t_kategori_gallery` (`id_kategori`, `nama_kategori`, `keterangan`) VALUES
(1, 'Ruang Kerja', ''),
(2, 'Event', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_keluar`
--

CREATE TABLE `t_keluar` (
  `id` int(10) NOT NULL,
  `id_bapok` int(10) NOT NULL,
  `no_tdg` varchar(50) NOT NULL,
  `volume` int(10) NOT NULL,
  `dikeluarkan_ke` varchar(30) NOT NULL,
  `tgl_keluar` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_keluar`
--

INSERT INTO `t_keluar` (`id`, `id_bapok`, `no_tdg`, `volume`, `dikeluarkan_ke`, `tgl_keluar`) VALUES
(11, 4, '1234567890', 100, 'Pasar B', '2017-10-09');

-- --------------------------------------------------------

--
-- Table structure for table `t_klasifikasi`
--

CREATE TABLE `t_klasifikasi` (
  `id` int(11) NOT NULL,
  `nm_klasifikasi` varchar(30) NOT NULL,
  `no_siup` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_klasifikasi`
--

INSERT INTO `t_klasifikasi` (`id`, `nm_klasifikasi`, `no_siup`) VALUES
(3, 'TDP', '1234'),
(4, 'TDG', '1235');

-- --------------------------------------------------------

--
-- Table structure for table `t_org`
--

CREATE TABLE `t_org` (
  `id` int(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `jabatan` varchar(30) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_org`
--

INSERT INTO `t_org` (`id`, `nama`, `jabatan`, `foto`) VALUES
(1, 'People', 'Pimpinan', 'heart-png-8.png'),
(2, 'People', 'Pimpinan', '148042-200.png'),
(3, 'People', 'Pimpinan', '51904-200.png'),
(4, 'Mukidi', 'Petugas', 'adwada.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `t_pasar`
--

CREATE TABLE `t_pasar` (
  `id_pasar` int(11) NOT NULL,
  `nama_pasar` varchar(100) NOT NULL,
  `lat` varchar(100) DEFAULT NULL,
  `lon` varchar(100) DEFAULT NULL,
  `alamat` text,
  `foto` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_pasar`
--

INSERT INTO `t_pasar` (`id_pasar`, `nama_pasar`, `lat`, `lon`, `alamat`, `foto`) VALUES
(1, 'Ps. KEBON KEMBANG', '-6.592084', '106.792372', 'Jl. Dewi Sartika No.1, Cibogor, Bogor Tengah, Kota Bogor, Jawa Barat 16124', ''),
(2, 'Ps. BARU BOGOR', '-6.592084', '106.792372', 'Jl. Roda, Babakan Pasar, Bogor Tengah, Kota Bogor, Jawa Barat 16124', ''),
(3, 'Ps. JAMBU DUA', '-6.592084', '106.792372', 'Jl. Ahmad Yani No. 83, Tanah Sareal, Tanah Sareal, Kota Bogor, Jawa Barat 16124', ''),
(4, 'Ps. MERDEKA', '-6.592084', '106.792372', 'Jl. Perintis Kemerdekaan, Kebon Kalapa, Bogor Tengah, Kota Bogor, Jawa Barat 16124', ''),
(5, 'Ps. SUKASARI', '-6.592084', '106.792372', 'Jl. Sukasari, Sukasari, Bogor Timur, Kota Bogor, Jawa Barat 16124', ''),
(6, 'Ps.PADASUKA', '-6.592084', '106.792372', 'Jl. Padasuka, Gudang, Bogor Tengah, Kota Bogor, Jawa Barat 16124', ''),
(7, 'Ps. GUNUNG BATU', '-6.592084', '106.792372', 'Jl. Ishak Djuarsa, Gunungbatu, Bogor Barat, Kota Bogor, Jawa Barat 16124', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_regulasi`
--

CREATE TABLE `t_regulasi` (
  `id_regulasi` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `file` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_regulasi`
--

INSERT INTO `t_regulasi` (`id_regulasi`, `nama`, `file`) VALUES
(2, 'Regulasi 2', 'UNSD2009MAT999-1.pdf'),
(3, 'Regulasi 3', 'UNSD2009MAT999-11.pdf'),
(4, 'Regulasi 4', 'UNSD2009MAT999-12.pdf'),
(5, 'UNDANG-UNDANG REPUBLIK INDONESIA NOMOR 7 TAHUN 2014 TENTANG PERDAGANGAN', 'uu7.pdf'),
(6, 'PERMENDAG REPUBLIK INDONESIA NOMOR 90/M-DAG/PER/12/2014 TENTANG PENATAAN DAN PEMBINAAN GUDANG', 'PERMEN_KEMENDAG_Nomor_90-M-DAG-PER-12-2014_Tahun_2014_(bn1957-2014)8.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `t_sambutan`
--

CREATE TABLE `t_sambutan` (
  `id` int(10) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_sambutan`
--

INSERT INTO `t_sambutan` (`id`, `foto`, `text`) VALUES
(1, 'foto_sambutan.jpg', '<p  center;">Lorem Ipsum ist ein einfacher Demo-Text f&uuml;r die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text seit 1500, als ein unbekannter Schriftsteller eine Hand voll W&ouml;rter nahm und diese durcheinander warf um ein Musterbuch zu erstellen. Es hat nicht nur 5 Jahrhunderte &uuml;berlebt, sondern auch in Spruch in die elektronische Schriftbearbeitung geschafft (bemerke, nahezu unver&auml;ndert). Bekannt wurde es 1960, mit dem erscheinen von &quot;Letraset&quot;, welches Passagen von Lorem Ipsum enhielt, so wie Desktop Software wie &quot;Aldus PageMaker&quot; - ebenfalls mit Lorem Ipsum.</p>\r\n\r\n<p  center;">Lorem Ipsum ist ein einfacher Demo-Text f&uuml;r die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text seit 1500, als ein unbekannter Schriftsteller eine Hand voll W&ouml;rter nahm und diese durcheinander warf um ein Musterbuch zu erstellen. Es hat nicht nur 5 Jahrhunderte &uuml;berlebt, sondern auch in Spruch in die elektronische Schriftbearbeitung geschafft (bemerke, nahezu unver&auml;ndert). Bekannt wurde es 1960, mit dem erscheinen von &quot;Letraset&quot;, welches Passagen von Lorem Ipsum enhielt, so wie Desktop Software wie &quot;Aldus PageMaker&quot; - ebenfalls mit Lorem Ipsum.</p>\r\n\r\n<p  center;">&nbsp;</p>\r\n\r\n<p  center;">&nbsp;</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `t_siup`
--

CREATE TABLE `t_siup` (
  `id` int(10) NOT NULL,
  `nm_perusahaan` varchar(50) NOT NULL,
  `nm_penanggungjwb` varchar(30) NOT NULL,
  `almt_perusahaan` text NOT NULL,
  `kecamatan` varchar(30) NOT NULL,
  `kelurahan` varchar(30) NOT NULL,
  `no_telp` varchar(12) NOT NULL,
  `no_fax` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `modal_usaha` varchar(15) NOT NULL,
  `kelembagaan` enum('Distributor','Sub Distributor','Agen','Sub Agen') NOT NULL,
  `kbli` varchar(30) NOT NULL,
  `barang_dagangan` text NOT NULL,
  `no_siup` varchar(50) NOT NULL,
  `tgl_siup` date NOT NULL,
  `masaberlaku` date NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_siup`
--

INSERT INTO `t_siup` (`id`, `nm_perusahaan`, `nm_penanggungjwb`, `almt_perusahaan`, `kecamatan`, `kelurahan`, `no_telp`, `no_fax`, `email`, `modal_usaha`, `kelembagaan`, `kbli`, `barang_dagangan`, `no_siup`, `tgl_siup`, `masaberlaku`, `foto`) VALUES
(1, 'PT Suka Jaya', 'Muhammad Farhan', 'Jl Tentara Pelajar, Cimnggu Pahlawan no.5 RT 01/02', '3271060', '3271060002', '081314118157', '-', '', '500000000', 'Distributor', '-', 'Beras', '-', '2017-09-15', '2020-09-15', 'khilafah-21.jpg'),
(2, 'PT. Beras', 'Farhan', 'Cimanggu', '3271060', '3271060004', '081314118157', '-', 'farhan@gmail.com', '5000000', 'Distributor', 'Test', 'Beras', '1234', '2017-09-26', '2017-09-26', ''),
(3, 'PT. TEST', 'Farhan', 'Cimanggu Pahlawan', '3271060', '3271060002', '081314118157', '-', 'farhan@gmail.com', '500000', 'Distributor', 'Test', 'Beras', '5555', '2017-10-18', '2019-10-18', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_siupmb`
--

CREATE TABLE `t_siupmb` (
  `id` int(10) NOT NULL,
  `nm_perusahaan` varchar(50) NOT NULL,
  `almt_perusahaan` text NOT NULL,
  `kecamatan` varchar(30) NOT NULL,
  `kelurahan` varchar(30) NOT NULL,
  `siupmb` varchar(50) NOT NULL,
  `tdp` varchar(50) NOT NULL,
  `tgl_keluar` date NOT NULL,
  `tgl_berakhir` date NOT NULL,
  `ins_yg_mengeluarkan` varchar(30) NOT NULL,
  `keterangan` text NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_siupmb`
--

INSERT INTO `t_siupmb` (`id`, `nm_perusahaan`, `almt_perusahaan`, `kecamatan`, `kelurahan`, `siupmb`, `tdp`, `tgl_keluar`, `tgl_berakhir`, `ins_yg_mengeluarkan`, `keterangan`, `foto`) VALUES
(1, 'PT Test', 'Bogor', '3271010', '3271010001', '1234', '1234', '2017-09-20', '2018-09-20', 'Testst', '-', 'mbuntu-81.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `t_stok`
--

CREATE TABLE `t_stok` (
  `id` int(10) NOT NULL,
  `id_bapok` int(10) NOT NULL,
  `tdg` varchar(50) NOT NULL,
  `jml_stok` int(10) NOT NULL,
  `tgl_input` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_stok`
--

INSERT INTO `t_stok` (`id`, `id_bapok`, `tdg`, `jml_stok`, `tgl_input`) VALUES
(1, 4, '1234567890', 500, '2017-09-24'),
(2, 12, '8765', 200, '2017-09-25'),
(3, 13, '1234567890', 150, '2017-09-30'),
(5, 12, '9999', 350, '2017-10-02'),
(6, 1, '1234567890', 300, '2017-10-03');

-- --------------------------------------------------------

--
-- Table structure for table `t_stpw`
--

CREATE TABLE `t_stpw` (
  `id` int(10) NOT NULL,
  `no_pendaftaran` varchar(30) NOT NULL,
  `nm_perusahaan` varchar(50) NOT NULL,
  `nm_penanggungjwb` varchar(30) NOT NULL,
  `almt_perusahaan` text NOT NULL,
  `kecamatan` varchar(30) NOT NULL,
  `kelurahan` varchar(30) NOT NULL,
  `npwp` varchar(30) NOT NULL,
  `iuts` varchar(50) NOT NULL,
  `no_stpw` varchar(50) NOT NULL,
  `tgl_stpw` date NOT NULL,
  `no_rekomendasi` varchar(50) NOT NULL,
  `no_telp` varchar(12) NOT NULL,
  `masaberlaku` date NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_stpw`
--

INSERT INTO `t_stpw` (`id`, `no_pendaftaran`, `nm_perusahaan`, `nm_penanggungjwb`, `almt_perusahaan`, `kecamatan`, `kelurahan`, `npwp`, `iuts`, `no_stpw`, `tgl_stpw`, `no_rekomendasi`, `no_telp`, `masaberlaku`, `foto`) VALUES
(1, '12345', 'PT Test', 'Akbarud', 'Bogor', '3271040', '3271040005', '-', '1234', '1234', '2017-09-20', '1234567890', '081314118157', '2017-09-20', 'mbuntu-71.jpg'),
(2, '89', 'PT Test1', 'Farhan', 'Alamat1', '3271050', '3271050015', '1414412312', '1234', '1234-1', '2017-10-11', '', '081314118157', '2017-10-11', ''),
(3, '90', 'PT Test2', 'Farhan', 'Alamat2', '3271040', '3271040011', '1245124', '1234', '1234-2', '2017-10-11', '', '081314118157', '2017-10-11', ''),
(4, '001', 'PT. Test5', 'Farhan', 'Cimanggu', '3271060', '3271060002', '-', '8998', '4356', '2017-10-15', '', '081314118157', '2018-10-15', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_tdg`
--

CREATE TABLE `t_tdg` (
  `id` int(10) NOT NULL,
  `nm_perusahaan` varchar(50) NOT NULL,
  `nm_penanggungjwb` varchar(30) NOT NULL,
  `kecamatan` varchar(30) NOT NULL,
  `kelurahan` varchar(30) NOT NULL,
  `almt_penanggungjwb` text NOT NULL,
  `almt_gudang` text NOT NULL,
  `no_telp` varchar(30) NOT NULL,
  `no_fax` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `jenis_isi_gudang` varchar(30) NOT NULL,
  `klasifikasi_gudang` varchar(30) NOT NULL,
  `luas_gudang` varchar(30) NOT NULL,
  `kapasitas_gudang` varchar(20) NOT NULL,
  `kelengkapan_gudang` varchar(30) NOT NULL,
  `no_tdg` varchar(30) NOT NULL,
  `tgl_tdg` date NOT NULL,
  `tdp` varchar(50) NOT NULL,
  `masaberlaku` date NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tdg`
--

INSERT INTO `t_tdg` (`id`, `nm_perusahaan`, `nm_penanggungjwb`, `kecamatan`, `kelurahan`, `almt_penanggungjwb`, `almt_gudang`, `no_telp`, `no_fax`, `email`, `jenis_isi_gudang`, `klasifikasi_gudang`, `luas_gudang`, `kapasitas_gudang`, `kelengkapan_gudang`, `no_tdg`, `tgl_tdg`, `tdp`, `masaberlaku`, `longitude`, `latitude`, `foto`) VALUES
(1, 'PT. Beras', 'Farhan', '3271060', '3271060002', 'Cimanggu Pahlawan No.5', 'Cimanggu Pahlawan No.5', '081314118157', '-', 'farhan@gmail.com', 'Beras', '-', '300', '300', 'Tidak Berpendingin', '1234', '2017-10-08', '253453', '2017-10-08', '1.624123', '1.512421', 'value='),
(2, 'PT Indah', 'Farhan', '3271050', '3271050015', 'Cijahe', 'Cijahe', '081314118157', '-', 'farhan@gmail.com', 'T', 'Test', '300', '300', 'Berpendingin', '89765', '2017-10-09', '', '2017-10-09', '1.41231', '123.123', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_tdg_penambahan_stok`
--

CREATE TABLE `t_tdg_penambahan_stok` (
  `id` int(15) NOT NULL,
  `id_bapok` varchar(30) NOT NULL,
  `tdg` varchar(50) NOT NULL,
  `asal` varchar(50) NOT NULL,
  `jml_stok` int(15) NOT NULL,
  `tgl_input` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tdg_penambahan_stok`
--

INSERT INTO `t_tdg_penambahan_stok` (`id`, `id_bapok`, `tdg`, `asal`, `jml_stok`, `tgl_input`) VALUES
(1, '1', '1234', 'Pasar X', 200, '2017-10-15'),
(2, '11', '1234', 'Pasar Y', 200, '2017-10-15');

-- --------------------------------------------------------

--
-- Table structure for table `t_tdg_pengiriman`
--

CREATE TABLE `t_tdg_pengiriman` (
  `id` int(15) NOT NULL,
  `id_bapok` varchar(30) NOT NULL,
  `tdg` varchar(50) NOT NULL,
  `jml_pengiriman` int(15) NOT NULL,
  `tujuan` varchar(50) NOT NULL,
  `stok_awal` int(15) NOT NULL,
  `stok_akhir` int(15) NOT NULL,
  `ket` text NOT NULL,
  `tgl_input` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tdg_pengiriman`
--

INSERT INTO `t_tdg_pengiriman` (`id`, `id_bapok`, `tdg`, `jml_pengiriman`, `tujuan`, `stok_awal`, `stok_akhir`, `ket`, `tgl_input`) VALUES
(1, '1', '1234', 50, 'Pasar A', 200, 150, '', '2017-10-15'),
(2, '1', '1234', 30, 'Pasar B', 150, 120, '', '2017-10-15'),
(3, '11', '1234', 50, 'Pasar B', 200, 150, '', '2017-10-15');

-- --------------------------------------------------------

--
-- Table structure for table `t_tdg_stok`
--

CREATE TABLE `t_tdg_stok` (
  `id` int(15) NOT NULL,
  `id_bapok` varchar(30) NOT NULL,
  `tdg` varchar(50) NOT NULL,
  `stok` int(15) NOT NULL,
  `tgl_input` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tdg_stok`
--

INSERT INTO `t_tdg_stok` (`id`, `id_bapok`, `tdg`, `stok`, `tgl_input`) VALUES
(1, '1', '1234', 120, '2017-10-15'),
(2, '11', '1234', 150, '2017-10-15');

-- --------------------------------------------------------

--
-- Table structure for table `t_tdp`
--

CREATE TABLE `t_tdp` (
  `id` int(10) NOT NULL,
  `nm_perusahaan` varchar(50) NOT NULL,
  `nm_penanggungjwb` varchar(30) NOT NULL,
  `almt_perusahaan` text NOT NULL,
  `kecamatan` varchar(30) NOT NULL,
  `kelurahan` varchar(30) NOT NULL,
  `npwp` varchar(30) NOT NULL,
  `no_telp` varchar(12) NOT NULL,
  `no_fax` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `keg_usaha_pokok` text NOT NULL,
  `kbli` varchar(30) NOT NULL,
  `status_kantor` enum('Kantor Tunggal','Kantor Pusat') NOT NULL,
  `no_tdp` varchar(50) NOT NULL,
  `tgl_tdp` date NOT NULL,
  `masaberlaku` date NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tdp`
--

INSERT INTO `t_tdp` (`id`, `nm_perusahaan`, `nm_penanggungjwb`, `almt_perusahaan`, `kecamatan`, `kelurahan`, `npwp`, `no_telp`, `no_fax`, `email`, `keg_usaha_pokok`, `kbli`, `status_kantor`, `no_tdp`, `tgl_tdp`, `masaberlaku`, `foto`) VALUES
(1, 'PT Test', 'Akbar', 'Bekasi', '3271010', '3271010009', 'Test', '081314118157', '-', 'akbar@gmail.com', 'Test', 'Test', 'Kantor Tunggal', '1234', '2017-09-20', '2017-09-20', 'triangle_shape_dark_figure_88540_1920x10801.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `t_tdpud`
--

CREATE TABLE `t_tdpud` (
  `id` int(10) NOT NULL,
  `nm_perusahaan` varchar(50) NOT NULL,
  `nm_penanggungjwb` varchar(30) NOT NULL,
  `almt_perusahaan` text NOT NULL,
  `kecamatan` varchar(30) NOT NULL,
  `kelurahan` varchar(30) NOT NULL,
  `npwp` varchar(30) NOT NULL,
  `no_telp` varchar(12) NOT NULL,
  `no_fax` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `keg_usaha_pokok` text NOT NULL,
  `kbli` varchar(30) NOT NULL,
  `status_kantor` enum('Kantor Tunggal','Kantor Pusat') NOT NULL,
  `tdp` varchar(50) NOT NULL,
  `no_tdpud` varchar(50) NOT NULL,
  `tgl_tdpud` date NOT NULL,
  `jenis_tdpud` varchar(30) NOT NULL,
  `masaberlaku` date NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tdpud`
--

INSERT INTO `t_tdpud` (`id`, `nm_perusahaan`, `nm_penanggungjwb`, `almt_perusahaan`, `kecamatan`, `kelurahan`, `npwp`, `no_telp`, `no_fax`, `email`, `keg_usaha_pokok`, `kbli`, `status_kantor`, `tdp`, `no_tdpud`, `tgl_tdpud`, `jenis_tdpud`, `masaberlaku`, `foto`) VALUES
(1, 'PT Test', 'Akbar', 'Bogor', '3271010', '3271010015', '-', '081314118157', '-', '', 'Test', 'Test', 'Kantor Tunggal', '1234', '1234', '2017-09-20', 'Distributor', '2017-09-20', 'mbuntu-01.jpg'),
(2, 'PT. ABCD', 'Farhan', 'Cimanggu', '3271060', '3271060002', '123456789', '081314118157', '-', 'farhan@gmail.com', 'Jualan Daging', 'test', 'Kantor Tunggal', '1234', '1342', '2017-10-09', 'Agen', '2020-10-09', ''),
(3, 'PT. Perusahaan2', 'Farhan', 'Cimanggu', '3271060', '3271060002', 'Test', '081314118157', '-', 'farhan@gmail.com', 'Jualan', 'Test', 'Kantor Tunggal', '9087', '4321', '2017-10-17', 'Agen', '2017-10-17', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_transaksi_b_pokok`
--

CREATE TABLE `t_transaksi_b_pokok` (
  `id_tr_pokok` int(11) NOT NULL,
  `id_pasar` int(11) NOT NULL,
  `id_b_pokok` int(11) NOT NULL,
  `id_sub_b_pokok` int(11) NOT NULL,
  `harga` bigint(11) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_transaksi_b_pokok`
--

INSERT INTO `t_transaksi_b_pokok` (`id_tr_pokok`, `id_pasar`, `id_b_pokok`, `id_sub_b_pokok`, `harga`, `tanggal`) VALUES
(2, 1, 1, 6, 12000, '2016-10-10'),
(19, 1, 1, 5, 9500, '2017-03-07'),
(20, 1, 3, 13, 12000, '2017-03-07'),
(21, 1, 3, 14, 13000, '2017-03-07'),
(22, 1, 9, 15, 16000, '2017-03-07'),
(23, 1, 9, 16, 12500, '2017-03-07'),
(24, 1, 12, 17, 120000, '2017-03-07'),
(25, 1, 12, 18, 33000, '2017-03-07'),
(26, 1, 12, 19, 65000, '2017-03-07'),
(27, 1, 5, 20, 20000, '2017-03-07'),
(28, 1, 5, 21, 2000, '2017-03-07'),
(29, 1, 13, 22, 10000, '2017-03-07'),
(30, 1, 13, 23, 11000, '2017-03-07'),
(31, 1, 13, 24, 40000, '2017-03-07'),
(32, 1, 13, 25, 39500, '2017-03-07'),
(33, 1, 14, 34, 13000, '2017-03-07'),
(34, 1, 15, 26, 600, '2017-03-07'),
(35, 1, 15, 27, 1000, '2017-03-07'),
(36, 1, 7, 28, 8000, '2017-03-07'),
(37, 1, 17, 35, 12500, '2017-03-07'),
(38, 1, 18, 29, 2200, '2017-03-07'),
(39, 1, 4, 30, 40000, '2017-03-07'),
(40, 1, 4, 31, 40000, '2017-03-07'),
(41, 1, 4, 32, 150000, '2017-03-07'),
(42, 1, 4, 33, 50000, '2017-03-07'),
(43, 1, 10, 36, 14000, '2017-03-07'),
(44, 1, 19, 37, 8000, '2017-03-07'),
(45, 1, 20, 38, 10000, '2017-03-07'),
(46, 1, 11, 7, 38000, '2017-03-07'),
(47, 1, 11, 8, 35000, '2017-03-07'),
(48, 1, 21, 39, 80000, '2017-02-06'),
(49, 1, 22, 40, 35000, '2017-03-07'),
(50, 1, 17, 41, 14000, '2017-03-07'),
(51, 1, 17, 42, 26000, '2017-03-07'),
(52, 1, 25, 43, 3000, '2017-03-07'),
(53, 1, 22, 39, 120000, '2017-03-07'),
(54, 2, 1, 5, 9700, '2017-03-07'),
(55, 2, 3, 13, 13000, '2017-03-07'),
(56, 2, 3, 14, 14000, '2017-03-07'),
(57, 2, 9, 15, 17000, '2017-03-07'),
(58, 2, 9, 16, 14000, '2017-03-07'),
(59, 2, 12, 17, 120000, '2017-03-07'),
(60, 2, 12, 18, 32000, '2017-03-07'),
(61, 2, 12, 19, 63000, '2017-03-07'),
(62, 2, 5, 20, 19000, '2017-03-07'),
(63, 2, 5, 21, 2500, '2017-03-07'),
(64, 2, 13, 22, 10000, '2017-03-07'),
(65, 2, 13, 23, 11000, '2017-03-07'),
(66, 2, 13, 24, 39500, '2017-03-07'),
(67, 2, 13, 25, 39500, '2017-03-07'),
(68, 2, 14, 34, 13000, '2017-03-07'),
(69, 2, 15, 26, 600, '2017-03-07'),
(70, 2, 15, 27, 1000, '2017-03-07'),
(71, 2, 7, 28, 8000, '2017-03-07'),
(72, 2, 17, 35, 10000, '2017-03-07'),
(73, 2, 18, 29, 2300, '2017-03-07'),
(74, 2, 4, 30, 40000, '2017-03-07'),
(75, 2, 4, 31, 30000, '2017-03-07'),
(76, 2, 4, 32, 140000, '2017-03-07'),
(77, 2, 4, 33, 50000, '2017-03-07'),
(78, 2, 10, 36, 13000, '2017-03-07'),
(79, 2, 19, 37, 6000, '2017-03-07'),
(80, 2, 20, 38, 7000, '2017-03-07'),
(81, 2, 11, 7, 40000, '2017-03-07'),
(82, 2, 11, 8, 35000, '2017-03-07'),
(83, 2, 22, 39, 105000, '2017-03-07'),
(84, 2, 22, 40, 35000, '2017-03-07'),
(85, 2, 17, 41, 20000, '2017-03-07'),
(86, 2, 17, 42, 26000, '2017-03-07'),
(87, 2, 25, 43, 3000, '2017-03-07'),
(88, 3, 1, 5, 9600, '2017-03-07'),
(89, 3, 3, 13, 13000, '2017-03-07'),
(90, 3, 3, 14, 12000, '2017-03-07'),
(91, 3, 9, 15, 17000, '2017-03-07'),
(92, 3, 9, 16, 12000, '2017-03-07'),
(93, 3, 12, 17, 120000, '2017-03-07'),
(94, 4, 1, 5, 9600, '2017-03-07'),
(95, 4, 3, 13, 12000, '2017-03-07'),
(96, 4, 3, 14, 12500, '2017-03-07'),
(97, 3, 12, 18, 33000, '2017-03-07'),
(98, 3, 12, 19, 60000, '2017-03-07'),
(99, 4, 9, 15, 16000, '2017-03-07'),
(100, 3, 5, 20, 20000, '2017-03-07'),
(101, 4, 9, 16, 13000, '2017-03-07'),
(102, 3, 5, 21, 2000, '2017-03-07'),
(103, 4, 12, 17, 110000, '2017-03-07'),
(104, 3, 13, 22, 10000, '2017-03-07'),
(105, 3, 13, 23, 11000, '2017-03-07'),
(106, 4, 12, 18, 32000, '2017-03-07'),
(107, 3, 13, 24, 40000, '2017-03-07'),
(108, 4, 12, 19, 65000, '2017-03-07'),
(109, 4, 5, 20, 19000, '2017-03-07'),
(110, 3, 13, 25, 39500, '2017-03-07'),
(111, 4, 5, 21, 2000, '2017-03-07'),
(112, 4, 13, 22, 11000, '2017-03-07'),
(113, 4, 13, 23, 10000, '2017-03-07'),
(114, 3, 14, 34, 14000, '2017-03-07'),
(115, 4, 13, 24, 39500, '2017-03-07'),
(116, 3, 15, 26, 600, '2017-03-07'),
(117, 3, 15, 27, 1000, '2017-03-07'),
(118, 4, 13, 25, 39500, '2017-03-07'),
(119, 4, 14, 34, 14000, '2017-03-07'),
(120, 3, 7, 28, 9000, '2017-03-07'),
(121, 4, 15, 26, 600, '2017-03-07'),
(122, 3, 17, 35, 10000, '2017-03-07'),
(123, 3, 18, 29, 2500, '2017-03-07'),
(124, 4, 15, 27, 1000, '2017-03-07'),
(125, 3, 4, 30, 40000, '2017-03-07'),
(126, 3, 4, 31, 38000, '2017-03-07'),
(127, 4, 7, 28, 7500, '2017-03-07'),
(128, 3, 4, 32, 150000, '2017-03-07'),
(129, 4, 17, 35, 9000, '2017-03-07'),
(130, 3, 4, 33, 40000, '2017-03-07'),
(131, 3, 10, 36, 20000, '2017-03-07'),
(132, 4, 18, 29, 2500, '2017-03-07'),
(133, 3, 19, 37, 6000, '2017-03-07'),
(134, 4, 4, 30, 30000, '2017-03-07'),
(135, 3, 20, 38, 7000, '2017-03-07'),
(136, 3, 11, 7, 40000, '2017-03-07'),
(137, 4, 4, 31, 38000, '2017-03-07'),
(138, 3, 11, 8, 40000, '2017-03-07'),
(139, 4, 4, 32, 130000, '2017-03-07'),
(140, 3, 22, 39, 70000, '2017-03-07'),
(141, 4, 4, 33, 45000, '2017-03-07'),
(142, 3, 22, 40, 35000, '2017-03-07'),
(143, 4, 10, 36, 12000, '2017-03-07'),
(144, 3, 17, 41, 14000, '2017-03-07'),
(145, 3, 17, 42, 26000, '2017-03-07'),
(146, 4, 19, 37, 4000, '2017-03-07'),
(147, 3, 25, 43, 3000, '2017-03-07'),
(148, 4, 20, 38, 6000, '2017-03-07'),
(149, 4, 11, 7, 35000, '2017-03-07'),
(150, 4, 11, 8, 32000, '2017-03-07'),
(151, 4, 22, 39, 58000, '2017-03-07'),
(152, 4, 22, 40, 35000, '2017-03-07'),
(153, 4, 17, 41, 19000, '2017-03-07'),
(154, 4, 17, 42, 21000, '2017-03-07'),
(155, 4, 25, 43, 3000, '2017-03-07'),
(156, 6, 1, 5, 9500, '2017-03-07'),
(157, 6, 3, 13, 12500, '2017-03-07'),
(158, 6, 3, 14, 13000, '2017-03-07'),
(159, 6, 9, 15, 16500, '2017-03-07'),
(160, 6, 9, 16, 13000, '2017-03-07'),
(161, 6, 12, 17, 120000, '2017-03-07'),
(162, 6, 12, 18, 29000, '2017-03-07'),
(163, 6, 12, 19, 60000, '2017-03-07'),
(164, 6, 5, 20, 20000, '2017-03-07'),
(165, 6, 5, 21, 2200, '2017-03-07'),
(166, 6, 13, 22, 9500, '2017-03-07'),
(167, 6, 13, 23, 10000, '2017-03-07'),
(168, 6, 13, 24, 39500, '2017-03-07'),
(169, 6, 13, 25, 39500, '2017-03-07'),
(170, 6, 14, 34, 15000, '2017-03-07'),
(171, 6, 15, 26, 500, '2017-03-07'),
(172, 6, 15, 27, 1000, '2017-03-07'),
(173, 6, 7, 28, 7000, '2017-03-07'),
(174, 6, 17, 35, 8500, '2017-03-07'),
(175, 6, 18, 29, 2500, '2017-03-07'),
(176, 6, 4, 30, 32000, '2017-03-07'),
(177, 6, 4, 31, 35000, '2017-03-07'),
(178, 6, 4, 32, 150000, '2017-03-07'),
(179, 6, 4, 33, 60000, '2017-03-07'),
(180, 6, 10, 36, 14000, '2017-03-07'),
(181, 6, 19, 37, 6000, '2017-03-07'),
(182, 6, 20, 38, 8000, '2017-03-07'),
(183, 6, 11, 7, 34000, '2017-03-07'),
(184, 6, 11, 8, 40000, '2017-03-07'),
(185, 6, 22, 39, 30000, '2017-03-07'),
(186, 6, 22, 40, 30000, '2017-03-07'),
(187, 5, 1, 5, 10000, '2017-03-07'),
(188, 6, 17, 41, 20000, '2017-03-07'),
(189, 5, 3, 13, 12000, '2017-03-07'),
(190, 6, 17, 42, 22000, '2017-03-07'),
(191, 5, 3, 14, 13000, '2017-03-07'),
(192, 6, 25, 43, 3000, '2017-03-07'),
(193, 5, 9, 15, 16000, '2017-03-07'),
(194, 7, 1, 5, 9600, '2017-03-07'),
(195, 5, 9, 16, 12000, '2017-03-07'),
(196, 7, 3, 13, 12000, '2017-03-07'),
(197, 5, 12, 17, 120000, '2017-03-07'),
(198, 7, 3, 14, 13000, '2017-03-07'),
(199, 7, 9, 15, 17000, '2017-03-07'),
(200, 7, 9, 16, 13000, '2017-03-07'),
(201, 5, 12, 18, 32000, '2017-03-07'),
(202, 7, 12, 17, 120000, '2017-03-07'),
(203, 5, 12, 19, 50000, '2017-03-07'),
(204, 7, 12, 18, 35000, '2017-03-07'),
(205, 5, 5, 20, 22000, '2017-03-07'),
(206, 5, 5, 21, 2000, '2017-03-07'),
(207, 5, 13, 22, 9500, '2017-03-07'),
(208, 5, 13, 23, 10000, '2017-03-07'),
(209, 7, 12, 19, 60000, '2017-03-07'),
(210, 5, 13, 24, 39500, '2017-03-07'),
(211, 7, 5, 20, 20000, '2017-03-07'),
(212, 5, 13, 25, 39500, '2017-03-07'),
(213, 7, 5, 21, 2500, '2017-03-07'),
(214, 5, 14, 34, 15000, '2017-03-07'),
(215, 7, 13, 22, 10000, '2017-03-07'),
(216, 7, 13, 23, 11000, '2017-03-07'),
(217, 5, 15, 26, 500, '2017-03-07'),
(218, 5, 15, 27, 1000, '2017-03-07'),
(219, 7, 13, 24, 39500, '2017-03-07'),
(220, 7, 13, 25, 42500, '2017-03-07'),
(221, 7, 14, 34, 13000, '2017-03-07'),
(222, 7, 15, 26, 600, '2017-03-07'),
(223, 5, 7, 28, 7500, '2017-03-07'),
(224, 7, 15, 27, 1000, '2017-03-07'),
(225, 5, 17, 35, 9000, '2017-03-07'),
(226, 7, 7, 28, 8500, '2017-03-07'),
(227, 5, 18, 29, 2500, '2017-03-07'),
(228, 5, 4, 30, 35000, '2017-03-07'),
(229, 7, 17, 35, 10000, '2017-03-07'),
(230, 7, 18, 29, 2500, '2017-03-07'),
(231, 7, 4, 30, 40000, '2017-03-07'),
(232, 5, 4, 31, 40000, '2017-03-07'),
(233, 7, 4, 31, 30000, '2017-03-07'),
(234, 5, 4, 32, 160000, '2017-03-07'),
(235, 7, 4, 32, 150000, '2017-03-07'),
(236, 5, 4, 33, 60000, '2017-03-07'),
(237, 7, 4, 33, 50000, '2017-03-07'),
(238, 5, 10, 36, 16000, '2017-03-07'),
(239, 7, 10, 36, 13000, '2017-03-07'),
(240, 7, 19, 37, 8000, '2017-03-07'),
(241, 7, 20, 38, 8000, '2017-03-07'),
(242, 5, 19, 37, 7000, '2017-03-07'),
(243, 5, 20, 38, 6000, '2017-03-07'),
(244, 7, 11, 7, 36000, '2017-03-07'),
(245, 5, 11, 7, 52000, '2017-03-07'),
(246, 7, 11, 8, 34000, '2017-03-07'),
(247, 5, 11, 8, 40000, '2017-03-07'),
(248, 7, 22, 39, 110000, '2017-03-07'),
(249, 5, 22, 39, 50000, '2017-03-07'),
(250, 7, 22, 40, 36000, '2017-03-07'),
(251, 5, 22, 40, 35000, '2017-03-07'),
(252, 7, 17, 41, 19000, '2017-03-07'),
(253, 7, 17, 42, 26000, '2017-03-07'),
(254, 5, 17, 41, 18000, '2017-03-07'),
(255, 7, 25, 43, 3000, '2017-03-07'),
(256, 5, 17, 42, 22000, '2017-03-07'),
(257, 5, 25, 43, 4000, '2017-03-07');

-- --------------------------------------------------------

--
-- Table structure for table `t_visitor`
--

CREATE TABLE `t_visitor` (
  `id` int(11) NOT NULL,
  `location` varchar(100) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_visitor`
--

INSERT INTO `t_visitor` (`id`, `location`, `ip`, `tgl`) VALUES
(88, '/disperindag/index.php', '::1', '2017-11-06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mst_gudang`
--
ALTER TABLE `mst_gudang`
  ADD PRIMARY KEY (`kd_gudang`);

--
-- Indexes for table `mst_kecamatan`
--
ALTER TABLE `mst_kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`);

--
-- Indexes for table `mst_kelurahan`
--
ALTER TABLE `mst_kelurahan`
  ADD PRIMARY KEY (`id_kelurahan`);

--
-- Indexes for table `td_distribusi_stok`
--
ALTER TABLE `td_distribusi_stok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_instansi`
--
ALTER TABLE `tr_instansi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_admin`
--
ALTER TABLE `t_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `t_agenda`
--
ALTER TABLE `t_agenda`
  ADD PRIMARY KEY (`id_agenda`);

--
-- Indexes for table `t_b3`
--
ALTER TABLE `t_b3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_b3_keluar`
--
ALTER TABLE `t_b3_keluar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_b3_penambahan`
--
ALTER TABLE `t_b3_penambahan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_b3_stok`
--
ALTER TABLE `t_b3_stok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_bahan_pokok`
--
ALTER TABLE `t_bahan_pokok`
  ADD PRIMARY KEY (`id_bahan_pok`);

--
-- Indexes for table `t_bahan_pokok_berbahaya`
--
ALTER TABLE `t_bahan_pokok_berbahaya`
  ADD PRIMARY KEY (`id_bahan_pok`);

--
-- Indexes for table `t_bahan_pokok_sub`
--
ALTER TABLE `t_bahan_pokok_sub`
  ADD PRIMARY KEY (`id_bahan_pok_sub`);

--
-- Indexes for table `t_berita`
--
ALTER TABLE `t_berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `t_distribusi_keluar`
--
ALTER TABLE `t_distribusi_keluar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_distribusi_pengadaan`
--
ALTER TABLE `t_distribusi_pengadaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_distribusi_stok`
--
ALTER TABLE `t_distribusi_stok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_gallery`
--
ALTER TABLE `t_gallery`
  ADD PRIMARY KEY (`id_gallery`);

--
-- Indexes for table `t_iupp`
--
ALTER TABLE `t_iupp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_iupr`
--
ALTER TABLE `t_iupr`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_iuts`
--
ALTER TABLE `t_iuts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kategori_agenda`
--
ALTER TABLE `t_kategori_agenda`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `t_kategori_berita`
--
ALTER TABLE `t_kategori_berita`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `t_kategori_gallery`
--
ALTER TABLE `t_kategori_gallery`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `t_keluar`
--
ALTER TABLE `t_keluar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_klasifikasi`
--
ALTER TABLE `t_klasifikasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_org`
--
ALTER TABLE `t_org`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_pasar`
--
ALTER TABLE `t_pasar`
  ADD PRIMARY KEY (`id_pasar`);

--
-- Indexes for table `t_regulasi`
--
ALTER TABLE `t_regulasi`
  ADD PRIMARY KEY (`id_regulasi`);

--
-- Indexes for table `t_sambutan`
--
ALTER TABLE `t_sambutan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_siup`
--
ALTER TABLE `t_siup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_siupmb`
--
ALTER TABLE `t_siupmb`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_stok`
--
ALTER TABLE `t_stok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_stpw`
--
ALTER TABLE `t_stpw`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tdg`
--
ALTER TABLE `t_tdg`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tdg_penambahan_stok`
--
ALTER TABLE `t_tdg_penambahan_stok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tdg_pengiriman`
--
ALTER TABLE `t_tdg_pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tdg_stok`
--
ALTER TABLE `t_tdg_stok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tdp`
--
ALTER TABLE `t_tdp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tdpud`
--
ALTER TABLE `t_tdpud`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_transaksi_b_pokok`
--
ALTER TABLE `t_transaksi_b_pokok`
  ADD PRIMARY KEY (`id_tr_pokok`);

--
-- Indexes for table `t_visitor`
--
ALTER TABLE `t_visitor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mst_gudang`
--
ALTER TABLE `mst_gudang`
  MODIFY `kd_gudang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `td_distribusi_stok`
--
ALTER TABLE `td_distribusi_stok`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tr_instansi`
--
ALTER TABLE `tr_instansi`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_admin`
--
ALTER TABLE `t_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_agenda`
--
ALTER TABLE `t_agenda`
  MODIFY `id_agenda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `t_b3`
--
ALTER TABLE `t_b3`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `t_b3_keluar`
--
ALTER TABLE `t_b3_keluar`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_b3_penambahan`
--
ALTER TABLE `t_b3_penambahan`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_b3_stok`
--
ALTER TABLE `t_b3_stok`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_bahan_pokok`
--
ALTER TABLE `t_bahan_pokok`
  MODIFY `id_bahan_pok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `t_bahan_pokok_berbahaya`
--
ALTER TABLE `t_bahan_pokok_berbahaya`
  MODIFY `id_bahan_pok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_bahan_pokok_sub`
--
ALTER TABLE `t_bahan_pokok_sub`
  MODIFY `id_bahan_pok_sub` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `t_berita`
--
ALTER TABLE `t_berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `t_distribusi_keluar`
--
ALTER TABLE `t_distribusi_keluar`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_distribusi_pengadaan`
--
ALTER TABLE `t_distribusi_pengadaan`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `t_distribusi_stok`
--
ALTER TABLE `t_distribusi_stok`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `t_gallery`
--
ALTER TABLE `t_gallery`
  MODIFY `id_gallery` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `t_iupp`
--
ALTER TABLE `t_iupp`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_iupr`
--
ALTER TABLE `t_iupr`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_iuts`
--
ALTER TABLE `t_iuts`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_kategori_agenda`
--
ALTER TABLE `t_kategori_agenda`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_kategori_berita`
--
ALTER TABLE `t_kategori_berita`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_kategori_gallery`
--
ALTER TABLE `t_kategori_gallery`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_keluar`
--
ALTER TABLE `t_keluar`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `t_klasifikasi`
--
ALTER TABLE `t_klasifikasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `t_org`
--
ALTER TABLE `t_org`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `t_pasar`
--
ALTER TABLE `t_pasar`
  MODIFY `id_pasar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `t_regulasi`
--
ALTER TABLE `t_regulasi`
  MODIFY `id_regulasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `t_sambutan`
--
ALTER TABLE `t_sambutan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_siup`
--
ALTER TABLE `t_siup`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_siupmb`
--
ALTER TABLE `t_siupmb`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_stok`
--
ALTER TABLE `t_stok`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `t_stpw`
--
ALTER TABLE `t_stpw`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `t_tdg`
--
ALTER TABLE `t_tdg`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_tdg_penambahan_stok`
--
ALTER TABLE `t_tdg_penambahan_stok`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_tdg_pengiriman`
--
ALTER TABLE `t_tdg_pengiriman`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_tdg_stok`
--
ALTER TABLE `t_tdg_stok`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_tdp`
--
ALTER TABLE `t_tdp`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_tdpud`
--
ALTER TABLE `t_tdpud`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_transaksi_b_pokok`
--
ALTER TABLE `t_transaksi_b_pokok`
  MODIFY `id_tr_pokok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;
--
-- AUTO_INCREMENT for table `t_visitor`
--
ALTER TABLE `t_visitor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
