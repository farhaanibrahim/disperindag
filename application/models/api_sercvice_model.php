<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_sercvice_model extends CI_Model {
	function opd_sub_where_id($id){
		
		$this->db->where('opd', $id);
		$this->db->order_by('nm_sub_opd', 'asc');
		
		return $this->db->get('sub_opd');	
	}
	function klasifikasi_sub_where_id($id){
		
		$this->db->where('kd_klasifikasi', $id);
		$this->db->order_by('nm_sub_klasifikasi', 'asc');
		
		return $this->db->get('t_sub_klasifikasi');	
	}
	function klasifikasi_rinci_where_id($id){
		
		$this->db->where('kd_subklasifikasi', $id);
		$this->db->order_by('nm_klasifikasi_rinci', 'asc');
		
		return $this->db->get('t_klasifikasi_rinci');	
	}
	function klasifikasi_arsip_where_id($id){
		
		$this->db->where('kd_klasifikasi_rinci', $id);
		$this->db->order_by('nm_klasifikasi_arsip', 'asc');
		
		return $this->db->get('t_klasifikasi_arsip');	
	}
}

?>