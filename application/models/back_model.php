<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Back_model extends CI_Model {
	public function mst_kecamatan()
	{
		return $this->db->get('mst_kecamatan');
	}
	public function kelurahan($id_kecamatan)
	{
		$this->db->where('kecamatan_id',$id_kecamatan);
		return $this->db->get('mst_kelurahan');
	}
	function data_login($user,$pass){
		$this->db->where('username', $user);
		$this->db->where('password', sha1(md5($pass)));

		return $this->db->get('t_admin');
	}
	function cek_password($pass){
		$this->db->where('password', sha1(md5($pass)));

		return $this->db->get('t_admin');
	}
	function edit_pass(){
		$data = array(
			'password'      => sha1(md5($this->input->post('password'))),
		);
		$this->db->where('admin_id', $this->input->post('id_admin'));
		return $this->db->update('t_admin', $data);
	}
	function edit_pass2(){
		$data = array(
			'password'      => sha1(md5($this->input->post('new_password'))),
		);
		$this->db->where('admin_id', $this->input->post('id_admin'));
		return $this->db->update('t_admin', $data);
	}
	function update_setting_web($nama_file){
		$data = array(
			'nama'      => $this->input->post('nama'),
			'alamat'      => $this->input->post('alamat'),
			'nip_kepsek'      => $this->input->post('nip_kepsek'),
			'kepsek'      => $this->input->post('kepsek'),
			'no_telp'      => $this->input->post('no_telp'),
			'email'      => $this->input->post('email'),
			'tentang_singkat'      => $this->input->post('tentang_singkat'),
			'logo'      => $nama_file,
		);
		$this->db->where('id', 1);
		return $this->db->update('tr_instansi',$data);
	}
	//list aspek start
	function daftar_katg_agenda(){
		return $this->db->get('t_kategori_agenda');
	}
	function daftar_katg_agenda_where_id($id){
		$this->db->where('id_kategori',$id);
		return $this->db->get('t_kategori_agenda');
	}
	function tambah_katg_agenda(){
		$data = array(
			'nama_kategori'      => $this->input->post('nama_kategori'),
			'keterangan'      => $this->input->post('keterangan'),
		);
		return $this->db->insert('t_kategori_agenda',$data);
	}
	function edit_katg_agenda($id){
		$data = array(
			'nama_kategori'      => $this->input->post('nama_kategori'),
			'keterangan'      => $this->input->post('keterangan'),
		);
		$this->db->where('id_kategori', $id);
		return $this->db->update('t_kategori_agenda',$data);
	}
	function hapus_katg_agenda($id){
		$this->db->where('id_kategori', $id);
		return $this->db->delete('t_kategori_agenda');
	}
	function daftar_agenda(){
		$this->db->join('t_kategori_agenda', 't_kategori_agenda.id_kategori = t_agenda.kategori_agenda');
		$this->db->order_by('tanggal_posting','DESC');
		return $this->db->get('t_agenda');
	}
	function daftar_agenda_where_id_agenda($id_agenda){
		$this->db->join('t_kategori_agenda', 't_kategori_agenda.id_kategori = t_agenda.kategori_agenda');
		$this->db->where('id_agenda', $id_agenda);
		return $this->db->get('t_agenda');
	}
	function tambah_agenda($params){
		$data = array(
			'judul_agenda'      => $this->input->post('judul'),
			'isi_agenda'      => $this->input->post('isi_agenda'),
			'kategori_agenda'      => $this->input->post('kategori'),
			'tanggal_agenda'      => $this->input->post('tanggal_agenda'),
			'tanggal_posting'      => date("Y-m-d h:i:s"),
			'cover_agenda'      => $params['text'],
			'publisher'      => $params['publisher'],
			'publish_status'      => $this->input->post('publish_status'),
		);
		return $this->db->insert('t_agenda',$data);
	}
	function edit_agenda($params){
		$data = array(
			'judul_agenda'      => $this->input->post('judul'),
			'isi_agenda'      => $this->input->post('isi_agenda'),
			'kategori_agenda'      => $this->input->post('kategori'),
			'tanggal_agenda'      => $this->input->post('tanggal_agenda'),
			'tanggal_posting'      => date("Y-m-d h:i:s"),
			'cover_agenda'      => $params['text'],
			'publisher'      => $params['publisher'],
			'publish_status'      => $this->input->post('publish_status'),
		);
		$this->db->where('id_agenda', $this->input->post('id_agenda'));
		return $this->db->update('t_agenda',$data);
	}
	function hapus_agenda(){
		$this->db->where('id_agenda', $this->input->post('id_agenda'));
		return $this->db->delete('t_agenda');
	}
	function daftar_kategori_agenda(){
		return $this->db->get('t_kategori_agenda');
	}

	function daftar_kategori_gallery(){
		return $this->db->get('t_kategori_gallery');
	}
	function daftar_gallery(){
		$this->db->join('t_kategori_gallery', 't_kategori_gallery.id_kategori = t_gallery.kategori');
		$this->db->order_by('t_gallery.tanggal_posting','DESC');
		return $this->db->get('t_gallery');
	}
	function tambah_gallery($params){
		$data = array(
			'judul'      => $this->input->post('judul_berita'),
			'isi'      => $this->input->post('isi_berita'),
			'kategori'      => $this->input->post('kategori'),
			'tanggal_posting'      => date("Y-m-d h:i:s"),
			'gambar'      => $params['text'],
			'publish_status'      => $this->input->post('publish_status'),
			'publisher'      => $params['publisher'],
		);
		return $this->db->insert('t_gallery',$data);
	}


	function daftar_katg_berita(){
		return $this->db->get('t_kategori_berita');
	}
	function daftar_katg_berita_where_id($id){
		$this->db->where('id_kategori',$id);
		return $this->db->get('t_kategori_berita');
	}
	function tambah_katg_berita(){
		$data = array(
			'nama_kategori'      => $this->input->post('nama_kategori'),
			'keterangan'      => $this->input->post('keterangan'),
		);
		return $this->db->insert('t_kategori_berita',$data);
	}
	function edit_katg_berita($id){
		$data = array(
			'nama_kategori'      => $this->input->post('nama_kategori'),
			'keterangan'      => $this->input->post('keterangan'),
		);
		$this->db->where('id_kategori', $id);
		return $this->db->update('t_kategori_berita',$data);
	}
	function hapus_katg_berita($id){
		$this->db->where('id_kategori', $id);
		return $this->db->delete('t_kategori_berita');
	}
	function daftar_berita(){
		$this->db->join('t_kategori_berita', 't_kategori_berita.id_kategori = t_berita.kategori');
		$this->db->order_by('t_berita.tanggal_posting','DESC');
		return $this->db->get('t_berita');
	}
	function daftar_berita_where_id_berita($id_berita){
		$this->db->join('t_kategori_berita', 't_kategori_berita.id_kategori = t_berita.kategori');
		$this->db->order_by('t_berita.tanggal_posting','DESC');
		$this->db->where('id_berita',$id_berita);
		return $this->db->get('t_berita');
	}
	function daftar_kategori_berita(){
		return $this->db->get('t_kategori_berita');
	}
	function tambah_berita($params){
		$data = array(
			'judul'      => $this->input->post('judul_berita'),
			'isi_berita'      => $this->input->post('isi_berita'),
			'kategori'      => $this->input->post('kategori'),
			'tanggal_posting'      => date("Y-m-d h:i:s"),
			'cover_berita'      => $params['text'],
			'publish_status'      => $this->input->post('publish_status'),
			'publisher'      => $params['publisher'],
		);
		return $this->db->insert('t_berita',$data);
	}
	function edit_berita($params){
		$data = array(
			'judul'      => $this->input->post('judul_berita'),
			'isi_berita'      => $this->input->post('isi_berita'),
			'kategori'      => $this->input->post('kategori'),
			'tanggal_posting'      => date("Y-m-d h:i:s"),
			'cover_berita'      => $params['text'],
			'publish_status'      => $this->input->post('publish_status'),
			'publisher'      => $params['publisher'],
		);
		$this->db->where('id_berita',$this->input->post('id_berita'));
		return $this->db->update('t_berita',$data);
	}
	function hapus_berita($params){
		$this->db->where('id_berita',$this->input->post('id_berita'));
		return $this->db->delete('t_berita');
	}

	//sub bahan pokok start
	function daftar_sub_b_pokok_all(){
		$this->db->order_by('t_bahan_pokok_sub.nama_sub_bahan_pokok','ASC');
		return $this->db->get('t_bahan_pokok_sub');
	}
	function daftar_sub_b_pokok($id_bahan_pokok){
		$this->db->order_by('t_bahan_pokok_sub.id_bahan_pok_sub','DESC');
		$this->db->where('t_bahan_pokok_sub.id_bahan_pok', $id_bahan_pokok);
		return $this->db->get('t_bahan_pokok_sub');
	}
	function daftar_sub_b_pokok_where_id($id_bahan_pokok_sub){
		$this->db->where('t_bahan_pokok_sub.id_bahan_pok_sub', $id_bahan_pokok_sub);
		return $this->db->get('t_bahan_pokok_sub');
	}
	function daftar_sub_b_pokok_upto_date(){
		$this->db->limit(20);
		return $this->db->get('t_bahan_pokok_sub');
	}
	function tambah_sub_b_pokok($params){
		$data = array(
			'foto_sub_b_pokok'      => $params['text'],
			'nama_sub_bahan_pokok'      => $this->input->post('nama_sub_bahan_pokok'),
			'satuan_sub'      => $this->input->post('satuan_sub_bahan_pokok'),
			//'harga_min_sub'      => $this->input->post('harga_min'),
			//'harga_max_sub'      => $this->input->post('harga_max'),
			'kategori'      => $this->input->post('kategori'),
		);
		return $this->db->insert('t_bahan_pokok_sub',$data);
	}
	function edit_sub_b_pokok($params){
		$data = array(
			'nama_sub_bahan_pokok'      => $this->input->post('nama_sub_bahan_pokok'),
			'satuan_sub'      => $this->input->post('satuan_sub_bahan_pokok'),
			//'harga_min_sub'      => $this->input->post('harga_min'),
			//'harga_max_sub'      => $this->input->post('harga_max'),
			'kategori'      => $this->input->post('kategori'),
		);
		if($params['text'] != 'kosong'){
			if($params['text'] != NULL){
				$this->db->set('foto_sub_b_pokok', $params['text']);
			}
		}
		$this->db->where('t_bahan_pokok_sub.id_bahan_pok_sub', $params['id_bahan_pok_sub']);
		return $this->db->update('t_bahan_pokok_sub',$data);
	}
	function hapus_sub_b_pokok($params){
		$this->db->where('t_bahan_pokok_sub.id_bahan_pok_sub', $params['id_bahan_pok_sub']);
		return $this->db->delete('t_bahan_pokok_sub');
	}
	//sub bahan pokok end
	function daftar_tr_b_pokok(){
		$this->db->join('t_pasar', 't_pasar.id_pasar = t_transaksi_b_pokok.id_pasar');
		$this->db->join('t_bahan_pokok', 't_bahan_pokok.id_bahan_pok = t_transaksi_b_pokok.id_b_pokok');
		$this->db->join('t_bahan_pokok_sub', 't_bahan_pokok_sub.id_bahan_pok_sub = t_transaksi_b_pokok.id_sub_b_pokok');
		$this->db->order_by('t_transaksi_b_pokok.tanggal','DESC');
		return $this->db->get('t_transaksi_b_pokok');
	}
	function daftar_tr_b_pokok_where_min7days(){
		$this->db->join('t_pasar', 't_pasar.id_pasar = t_transaksi_b_pokok.id_pasar');
		$this->db->join('t_bahan_pokok', 't_bahan_pokok.id_bahan_pok = t_transaksi_b_pokok.id_b_pokok');
		$this->db->join('t_bahan_pokok_sub', 't_bahan_pokok_sub.id_bahan_pok_sub = t_transaksi_b_pokok.id_sub_b_pokok');
		$this->db->where('t_transaksi_b_pokok.tanggal >', date("Y-m-d", mktime(0,0,0,date("m"),date("d")-31,date("Y")) ) );
		return $this->db->get('t_transaksi_b_pokok');
	}
	function daftar_tr_b_pokok_where_min7days_batas($batas){
		$this->db->join('t_pasar', 't_pasar.id_pasar = t_transaksi_b_pokok.id_pasar');
		$this->db->join('t_bahan_pokok', 't_bahan_pokok.id_bahan_pok = t_transaksi_b_pokok.id_b_pokok');
		$this->db->join('t_bahan_pokok_sub', 't_bahan_pokok_sub.id_bahan_pok_sub = t_transaksi_b_pokok.id_sub_b_pokok');
		$this->db->where('t_transaksi_b_pokok.tanggal >', date("Y-m-d", mktime(0,0,0,date("m"),date("d")-31,date("Y")) ) );
		return $this->db->get('t_transaksi_b_pokok',8,$batas);
	}
	function daftar_tr_b_pokok_where_min7days_pasar($id_ps){
		$this->db->join('t_pasar', 't_pasar.id_pasar = t_transaksi_b_pokok.id_pasar');
		$this->db->join('t_bahan_pokok', 't_bahan_pokok.id_bahan_pok = t_transaksi_b_pokok.id_b_pokok');
		$this->db->join('t_bahan_pokok_sub', 't_bahan_pokok_sub.id_bahan_pok_sub = t_transaksi_b_pokok.id_sub_b_pokok');
		$this->db->where('t_transaksi_b_pokok.tanggal >', date("Y-m-d", mktime(0,0,0,date("m"),date("d")-31,date("Y")) ) );
		$this->db->where('t_transaksi_b_pokok.id_pasar', $id_ps);
		return $this->db->get('t_transaksi_b_pokok');
	}
	function daftar_tr_b_pokok_where_min7days_batas_pasar($id_ps,$batas){
		$this->db->join('t_pasar', 't_pasar.id_pasar = t_transaksi_b_pokok.id_pasar');
		$this->db->join('t_bahan_pokok', 't_bahan_pokok.id_bahan_pok = t_transaksi_b_pokok.id_b_pokok');
		$this->db->join('t_bahan_pokok_sub', 't_bahan_pokok_sub.id_bahan_pok_sub = t_transaksi_b_pokok.id_sub_b_pokok');
		$this->db->where('t_transaksi_b_pokok.tanggal >', date("Y-m-d", mktime(0,0,0,date("m"),date("d")-31,date("Y")) ) );
		$this->db->where('t_transaksi_b_pokok.id_pasar', $id_ps);
		return $this->db->get('t_transaksi_b_pokok',8,$batas);
	}
	function daftar_tr_b_pokok_where_id_tr($id_tr){
		$this->db->join('t_pasar', 't_pasar.id_pasar = t_transaksi_b_pokok.id_pasar');
		$this->db->join('t_bahan_pokok', 't_bahan_pokok.id_bahan_pok = t_transaksi_b_pokok.id_b_pokok');
		$this->db->join('t_bahan_pokok_sub', 't_bahan_pokok_sub.id_bahan_pok_sub = t_transaksi_b_pokok.id_sub_b_pokok');
		$this->db->order_by('t_transaksi_b_pokok.tanggal','DESC');
		$this->db->where('t_transaksi_b_pokok.id_tr_pokok', $id_tr);
		return $this->db->get('t_transaksi_b_pokok');
	}
	function daftar_tr_b_pokok_where_id_ps($id_ps){
		$this->db->join('t_pasar', 't_pasar.id_pasar = t_transaksi_b_pokok.id_pasar');
		$this->db->join('t_bahan_pokok', 't_bahan_pokok.id_bahan_pok = t_transaksi_b_pokok.id_b_pokok');
		$this->db->join('t_bahan_pokok_sub', 't_bahan_pokok_sub.id_bahan_pok_sub = t_transaksi_b_pokok.id_sub_b_pokok');
		$this->db->order_by('t_transaksi_b_pokok.tanggal','DESC');
		$this->db->where('t_transaksi_b_pokok.id_pasar', $id_ps);
		return $this->db->get('t_transaksi_b_pokok');
	}
	function daftar_tr_b_pokok_where_id_b_pokok($id_bahan_pokok){
		$this->db->join('t_pasar', 't_pasar.id_pasar = t_transaksi_b_pokok.id_pasar');
		$this->db->join('t_bahan_pokok', 't_bahan_pokok.id_bahan_pok = t_transaksi_b_pokok.id_b_pokok');
		$this->db->join('t_bahan_pokok_sub', 't_bahan_pokok_sub.id_bahan_pok_sub = t_transaksi_b_pokok.id_sub_b_pokok');
		$this->db->order_by('t_transaksi_b_pokok.tanggal','DESC');
		$this->db->where('t_transaksi_b_pokok.id_b_pokok', $id_bahan_pokok);
		return $this->db->get('t_transaksi_b_pokok');
	}
	function tambah_tr_b_pokok(){
		$data = array(
			'id_pasar'      => $this->input->post('id_pasar'),
			'id_b_pokok'      => $this->input->post('id_bahan_pokok'),
			'id_sub_b_pokok'      => $this->input->post('id_sub_bahan_pokok'),
			'tanggal'      => $this->input->post('tanggal'),
			'harga'      => $this->input->post('harga'),
		);
		return $this->db->insert('t_transaksi_b_pokok',$data);
	}
	function edit_tr_b_pokok(){
		$data = array(
			'id_pasar'      => $this->input->post('id_pasar'),
			'id_b_pokok'      => $this->input->post('id_bahan_pokok'),
			'id_sub_b_pokok'      => $this->input->post('id_sub_bahan_pokok'),
			'tanggal'      => $this->input->post('tanggal'),
			'harga'      => $this->input->post('harga'),
		);
		$this->db->where('t_transaksi_b_pokok.id_tr_pokok', $this->input->post('id_tr_pokok'));
		return $this->db->update('t_transaksi_b_pokok',$data);
	}
	function hapus_tr_b_pokok(){
		$this->db->where('t_transaksi_b_pokok.id_tr_pokok', $this->input->post('id_tr_pokok'));
		return $this->db->delete('t_transaksi_b_pokok');
	}
	function daftar_b_pokok(){
		$this->db->order_by('nama_bahan_pokok', 'ASC');
		return $this->db->get('t_bahan_pokok');
	}
	function daftar_b_pokok_berbahaya(){
		$this->db->order_by('nama_bahan_pokok', 'ASC');
		return $this->db->get('t_bahan_pokok_berbahaya');
	}
	function daftar_b_pokok_byid_bapok($id_bapok){
		$this->db->where('id_bahan_pok',$id_bapok);
		$this->db->order_by('nama_bahan_pokok', 'ASC');
		return $this->db->get('t_bahan_pokok');
	}
	function daftar_b_pokok_id_bahan_pokok($id_bahan_pokok){
		$this->db->order_by('t_bahan_pokok.id_bahan_pok','DESC');
		$this->db->where('id_bahan_pok', $id_bahan_pokok);
		return $this->db->get('t_bahan_pokok');
	}
	function daftar_b_pokok_id_pasar($id_pasar){
		$this->db->order_by('t_bahan_pokok.id_bahan_pok','DESC');
		$this->db->where('t_bahan_pokok.id_pasar', $id_pasar);
		return $this->db->get('t_bahan_pokok');
	}
	function tambah_b_pokok($params){
		$data = array(
			'nama_bahan_pokok'      => $this->input->post('nama_bahan_pokok'),
			//'harga_min'      => $this->input->post('harga_min'),
			//'harga_max'      => $this->input->post('harga_max'),
			//'tanggal_awal'      => $this->input->post('tanggal_awal'),
			//'tanggal_akhir'      => $this->input->post('tanggal_akhir'),
			'satuan'      => $this->input->post('satuan'),
			'foto_b_pokok'      => $params['text'],
		);
		return $this->db->insert('t_bahan_pokok',$data);
	}
	function tambah_b_pokok_berbahaya($params){
		$data = array(
			'nama_bahan_pokok'      => $this->input->post('nama_bahan_pokok'),
			//'harga_min'      => $this->input->post('harga_min'),
			//'harga_max'      => $this->input->post('harga_max'),
			//'tanggal_awal'      => $this->input->post('tanggal_awal'),
			//'tanggal_akhir'      => $this->input->post('tanggal_akhir'),
			'satuan'      => $this->input->post('satuan'),
			'foto_b_pokok'      => $params['text'],
		);
		return $this->db->insert('t_bahan_pokok_berbahaya',$data);
	}
	function edit_b_pokok($params){
		$data = array(
			'nama_bahan_pokok'      => $this->input->post('nama_bahan_pokok'),
			//'harga_min'      => $this->input->post('harga_min'),
			//'harga_max'      => $this->input->post('harga_max'),
			//'tanggal_awal'      => $this->input->post('tanggal_awal'),
			//'tanggal_akhir'      => $this->input->post('tanggal_akhir'),
			'satuan'      => $this->input->post('satuan'),
		);
		if($params['text'] != 'kosong'){
			if($params['text'] != NULL){
				$this->db->set('foto_b_pokok', $params['text']);
			}
		}
		$this->db->where('id_bahan_pok', $this->input->post('id_bahan_pok'));
		return $this->db->update('t_bahan_pokok',$data);
	}
	function hapus_b_pokok(){
		$this->db->where('id_bahan_pok', $this->input->post('id_bahan_pok'));
		return $this->db->delete('t_bahan_pokok');
	}
	function daftar_regulasi(){
		$this->db->order_by('t_regulasi.nama','ASC');
		return $this->db->get('t_regulasi');
	}
	function daftar_regulasi_where_id_regulasi($id_regulasi){
		$this->db->where('id_regulasi', $id_regulasi);
		return $this->db->get('t_regulasi');
	}
	function tambah_regulasi($params){
		$data = array(
			'nama'      => $this->input->post('nama'),
			'file'      => $params['text'],
		);
		return $this->db->insert('t_regulasi',$data);
	}
	function edit_regulasi($params){
		$data = array(
			'nama'      => $this->input->post('nama'),
			'file'      => $params['text'],
		);
		$this->db->where('id_regulasi', $this->input->post('id_regulasi'));
		return $this->db->update('t_regulasi',$data);
	}
	function hapus_regulasi($params){
		$this->db->where('id_regulasi', $this->input->post('id_regulasi'));
		return $this->db->delete('t_regulasi');
	}
	function daftar_pasar(){
		$this->db->order_by('t_pasar.nama_pasar','ASC');
		return $this->db->get('t_pasar');
	}
	function daftar_pasar_where_id_pasar($id_pasar){
		$this->db->order_by('t_pasar.nama_pasar','ASC');
		$this->db->where('id_pasar', $id_pasar);
		return $this->db->get('t_pasar');
	}
	function tambah_pasar($params){
		$data = array(
			'id_pasar'      => $this->input->post('id_pasar'),
			'nama_pasar'      => $this->input->post('nama_pasar'),
			'lat'      => $this->input->post('lat'),
			'lon'      => $this->input->post('lon'),
			'alamat'      => $this->input->post('alamat'),
			'foto'      => $params['text'],
		);
		return $this->db->insert('t_pasar',$data);
	}
	function edit_pasar($params){
		$data = array(
			'id_pasar'      => $this->input->post('id_pasar'),
			'nama_pasar'      => $this->input->post('nama_pasar'),
			'lat'      => $this->input->post('lat'),
			'lon'      => $this->input->post('lon'),
			'alamat'      => $this->input->post('alamat'),
			'foto'      => $params['text'],
		);
		$this->db->where('id_pasar', $this->input->post('id_pasar'));
		return $this->db->update('t_pasar',$data);
	}
	function hapus_pasar(){
		$this->db->where('id_pasar', $this->input->post('id_pasar'));
		return $this->db->delete('t_pasar');
	}

	function insert_pegunjung($data){
		$this->db->insert('t_visitor',$data);
	}
	function clear_visitor(){
		$this->db->where_not_in('tgl',date('Ymd'));
		$this->db->delete('t_visitor');
	}
	function get_visitor(){
		$this->db->where('tgl',date('Ymd'));
		$this->db->group_by('ip');
		return $this->db->get('t_visitor');
	}

	public function total_visitor()
	{
		$this->db->select("COUNT(hits)");
		return $this->db->get('t_visitor');
	}
	public function visitor_online()
	{
		//$batas_waktu = time() - 300;
		//$this->db->where('online >',$batas_waktu);
		return $this->db->get('t_visitor');
	}
	public function cek_visitor($ip,$tanggal)
	{
		$this->db->where('ip',$ip);
		$this->db->where('tgl',$tanggal);
		return $this->db->get('t_visitor');
	}

	public function update_visitor($ip,$tanggal,$set)
	{
		$this->db->where('ip',$ip);
		$this->db->where('tgl',$tanggal);
		$this->db->set($set);
		$this->db->update('t_visitor');
	}
	/* function data_tdg(){
		$this->db->join('mst_kecamatan','t_tdg.kecamatan = mst_kecamatan.id_kecamatan');
		$this->db->join('mst_kelurahan', 't_tdg.kelurahan = mst_kelurahan.id_kelurahan');
		$this->db->join('mst_gudang','t_tdg.gudang = mst_gudang.kd_gudang');
		return $this->db->get('t_tdg');
	} */
	function tambah_tdg($data){
		$this->db->insert('t_tdg',$data);
	}

	function delete_tdg($id){
		$this->db->where('id',$id);
		$this->db->delete('t_tdg');
	}
	public function mst_gudang()
	{
		return $this->db->get('mst_gudang');
	}
	function tambah_mst_gudang($data)
	{
		$this->db->insert('mst_gudang',$data);
	}

	public function data_mst_gudang_byid($kd_gudang)
	{
		$this->db->where('kd_gudang',$kd_gudang);
		return $this->db->get('mst_gudang');
	}
	function edit_mst_gudang($kd_gudang,$data){
		$this->db->where('kd_gudang',$kd_gudang);
		$this->db->update('mst_gudang',$data);
	}
	function delete_mst_gudang($kd_gudang){
		$this->db->where('kd_gudang',$kd_gudang);
		$this->db->delete('mst_gudang');
	}
	public function filter_perizinan($where,$table)
	{
		$this->db->join("mst_kecamatan", "mst_kecamatan.id_kecamatan = ".$table.".kecamatan");
		$this->db->join("mst_kelurahan", "mst_kelurahan.id_kelurahan = ".$table.".kelurahan");

    	$this->db->where($where);

		return $this->db->get($table);
	}
	public function filter_perizinan2($table)
	{
		$this->db->join("mst_kecamatan", "mst_kecamatan.id_kecamatan = ".$table.".kecamatan");
		$this->db->join("mst_kelurahan", "mst_kelurahan.id_kelurahan = ".$table.".kelurahan");

		return $this->db->get($table);
	}

	/* public function filter_tdg($gudang,$kecamatan)
	{
		$this->db->join('mst_kecamatan','t_tdg.kecamatan = mst_kecamatan.id_kecamatan');
		$this->db->join('mst_kelurahan', 't_tdg.kelurahan = mst_kelurahan.id_kelurahan');
		$this->db->join('mst_gudang','t_tdg.gudang = mst_gudang.kd_gudang');
		$this->db->where('gudang',$gudang);
		$this->db->where('kecamatan',$kecamatan);
		return $this->db->get('t_tdg');
	}

	*/

	public function klasifikasi_iu()
	{
		return $this->db->get('t_klasifikasi');
	}

	public function klasifikasi_iu_byid($id)
	{
		$this->db->where('id',$id);
		return $this->db->get('t_klasifikasi');
	}
	public function klasifikasi_iu_bynm($nm)
	{
		$this->db->where('nm_klasifikasi',$nm);
		return $this->db->get('t_klasifikasi');
	}

	public function input_klasifikasi($data)
	{
		$this->db->insert('t_klasifikasi',$data);
	}
	public function edit_klasifikasi($data,$id)
	{
		$this->db->where('id',$id);
		$this->db->update('t_klasifikasi',$data);
	}
	public function delete_klasifikasi($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('t_klasifikasi');
	}
	public function get_tdp_by_npwp($npwp)
	{
		$this->db->join('mst_kecamatan','t_tdp.kecamatan = mst_kecamatan.id_kecamatan');
		$this->db->join('mst_kelurahan', 't_tdp.kelurahan = mst_kelurahan.id_kelurahan');
		$this->db->where('npwp',$npwp);
		return $this->db->get('t_tdp');
	}
	public function sambutan()
	{
		$this->db->where('id',1);
		return $this->db->get('t_sambutan');
	}
	function update_sambutan($data)
	{
		$this->db->where('id',1);
		$this->db->update('t_sambutan',$data);
	}
	function struk_org($data)
	{
		$this->db->insert('t_org',$data);
	}

	function get_struk_org()
	{
		return $this->db->get('t_org');
	}

	function get_anggota_org($id)
	{
		$this->db->where('id',$id);
		return $this->db->get('t_org');
	}

	public function stok_bapok()
	{
		$this->db->join('t_bahan_pokok','t_stok.id_bapok = t_bahan_pokok.id_bahan_pok');
		return $this->db->get('t_stok');
	}

	public function stok_bapok_byid($id)
	{
		$this->db->where('id',$id);
		$this->db->join('t_bahan_pokok','t_stok.id_bapok = t_bahan_pokok.id_bahan_pok');
		return $this->db->get('t_stok');
	}

	public function edit_stok_bapok($id,$data)
	{
		$this->db->where('id',$id);
		$this->db->update('t_stok',$data);
	}

	public function data_dist_stok()
	{
		$this->db->join('t_bahan_pokok','t_distribusi_stok.id_bapok = t_bahan_pokok.id_bahan_pok');
		$this->db->order_by('id','DESC');
		return $this->db->get('t_distribusi_stok');
	}

	public function data_dist_pengadaan()
	{
		$this->db->join('t_bahan_pokok','t_distribusi_pengadaan.id_bapok = t_bahan_pokok.id_bahan_pok');
		$this->db->join('t_tdpud','t_distribusi_pengadaan.tdpud = t_tdpud.no_tdpud');
		$this->db->order_by('t_distribusi_pengadaan.id','DESC');
		return $this->db->get('t_distribusi_pengadaan');
	}

	public function data_dist_pengadaan_byid($id)
	{
		$this->db->where('id',$id);
		$this->db->join('t_bahan_pokok','t_distribusi_pengadaan.id_bapok = t_bahan_pokok.id_bahan_pok');
		return $this->db->get('t_distribusi_pengadaan');
	}

	public function data_dist()
	{
		$this->db->join('t_bahan_pokok','t_distribusi_keluar.id_bapok = t_bahan_pokok.id_bahan_pok');
		$this->db->order_by('id','DESC');
		return $this->db->get('t_distribusi_keluar');
	}

	public function data_dist_pdf($id_bapok,$tdpud,$tgl_min,$tgl_max)
	{
		$this->db->join('t_bahan_pokok','t_distribusi_keluar.id_bapok = t_bahan_pokok.id_bahan_pok');
		$this->db->join('t_tdpud','t_distribusi_keluar.tdpud = t_tdpud.no_tdpud');
		$this->db->join('t_distribusi_pengadaan','t_distribusi_pengadaan.id_bapok = t_distribusi_keluar.id_bapok');
		$this->db->join('t_distribusi_stok','t_distribusi_pengadaan.id_bapok = t_distribusi_stok.id_bapok');
		$this->db->where('t_distribusi_keluar.id_bapok',$id_bapok);
		$this->db->where('t_distribusi_keluar.tdpud',$tdpud);
		$this->db->where('t_distribusi_keluar.tgl_input >=',$tgl_min);
		$this->db->where('t_distribusi_keluar.tgl_input <=',$tgl_max);
		return $this->db->get('t_distribusi_keluar');
	}

	public function data_dist_bpok_pdf($id_bapok)
	{
		$this->db->join('t_bahan_pokok','t_distribusi_keluar.id_bapok = t_bahan_pokok.id_bahan_pok');
		$this->db->join('t_tdpud','t_distribusi_keluar.tdpud = t_tdpud.no_tdpud');
		$this->db->join('t_distribusi_pengadaan','t_distribusi_pengadaan.id_bapok = t_distribusi_keluar.id_bapok');
		$this->db->join('t_distribusi_stok','t_distribusi_pengadaan.id_bapok = t_distribusi_stok.id_bapok');
		$this->db->where('t_distribusi_keluar.id_bapok',$id_bapok);
		return $this->db->get('t_distribusi_keluar');
	}

	public function data_dist_pdf_all()
	{
		$this->db->join('t_bahan_pokok','t_distribusi_keluar.id_bapok = t_bahan_pokok.id_bahan_pok');
		$this->db->join('t_tdpud','t_distribusi_keluar.tdpud = t_tdpud.no_tdpud');
		$this->db->join('t_distribusi_pengadaan','t_distribusi_pengadaan.id_bapok = t_distribusi_keluar.id_bapok');
		$this->db->join('t_distribusi_stok','t_distribusi_pengadaan.id_bapok = t_distribusi_stok.id_bapok');
		//$this->db->group_by('t_distribusi_keluar.tdpud');
		//$this->db->where('t_distribusi_keluar.tgl_input >=',$tgl_min);
		return $this->db->get('t_distribusi_keluar');
	}

	public function dist_penyaluran($tdpud)
	{
		$this->db->where('tdpud',$tdpud);
		return $this->db->get('t_distribusi_keluar');
	}

	public function data_dist_jml_pengeluaran($id_bapok,$tdpud,$tgl_min,$tgl_max)
	{
		$this->db->where('t_distribusi_keluar.id_bapok',$id_bapok);
		$this->db->where('t_distribusi_keluar.tdpud',$tdpud);
		$this->db->where('t_distribusi_keluar.tgl_input >=',$tgl_min);
		$this->db->where('t_distribusi_keluar.tgl_input <=',$tgl_max);
		return $this->db->get('t_distribusi_keluar');
	}
	public function data_dist_jml_pengadaan($id_bapok,$tdpud,$tgl_min,$tgl_max)
	{
		$this->db->where('t_distribusi_pengadaan.id_bapok',$id_bapok);
		$this->db->where('t_distribusi_pengadaan.tdpud',$tdpud);
		$this->db->where('t_distribusi_pengadaan.tgl_input >=',$tgl_min);
		$this->db->where('t_distribusi_pengadaan.tgl_input <=',$tgl_max);
		return $this->db->get('t_distribusi_pengadaan');
	}

	public function data_dist_byid($id)
	{
		$this->db->where('id',$id);
		$this->db->join('t_bahan_pokok','t_distribusi.id_bapok = t_bahan_pokok.id_bahan_pok');
		return $this->db->get('t_distribusi');
	}

	public function cek_data_dist_stok($id_bapok,$tdpud)
	{
		$this->db->where('id_bapok',$id_bapok);
		$this->db->where('tdpud',$tdpud);
		return $this->db->get('t_distribusi_stok');
	}

	public function update_stok_dist($id,$calculate_stock)
	{
		$this->db->where('id',$id);
		$this->db->set('stok',$calculate_stock);
		$this->db->update('t_distribusi_stok');
	}
	public function update_stok_dist2($id_bapok,$tdpud,$calculate_stock)
	{
		$this->db->where('id_bapok',$id_bapok);
		$this->db->where('tdpud',$tdpud);
		$this->db->set('stok',$calculate_stock);
		$this->db->update('t_distribusi_stok');
	}

	public function tambah_dist($data)
	{
		$this->db->insert('t_distribusi_pengadaan',$data);
	}

	public function tambah_stok_dist($data)
	{
		$this->db->insert('t_distribusi_stok',$data);
	}

	public function tambah_dist_keluar($data)
	{
		$this->db->insert('t_distribusi_keluar',$data);
	}

	public function update_data_dist($id,$data)
	{
		$this->db->where('id',$id);
		$this->db->update('t_distribusi',$data);
	}

	public function data_siup()
	{
		//$this->db->join();
		return $this->db->get('t_siup');
	}
	public function data_siup_byid($id)
	{
		$this->db->where('id',$id);
		$this->db->join('mst_kecamatan','t_siup.kecamatan = mst_kecamatan.id_kecamatan');
		$this->db->join('mst_kelurahan','t_siup.kelurahan = mst_kelurahan.id_kelurahan');
		return $this->db->get('t_siup');
	}

	public function data_tdp()
	{
		return $this->db->get('t_tdp');
	}
	public function data_tdp_byid($id)
	{
		$this->db->where('id',$id);
		$this->db->join('mst_kecamatan','t_tdp.kecamatan = mst_kecamatan.id_kecamatan');
		$this->db->join('mst_kelurahan','t_tdp.kelurahan = mst_kelurahan.id_kelurahan');
		return $this->db->get('t_tdp');
	}

	public function data_iupr()
	{
		return $this->db->get('t_iupr');
	}
	public function data_iupr_byid($id)
	{
		$this->db->where('id',$id);
		$this->db->join('mst_kecamatan','t_iupr.kecamatan = mst_kecamatan.id_kecamatan');
		$this->db->join('mst_kelurahan','t_iupr.kelurahan = mst_kelurahan.id_kelurahan');
		return $this->db->get('t_iupr');
	}

	public function data_iupp()
	{
		return $this->db->get('t_iupp');
	}
	public function data_iupp_byid($id)
	{
		$this->db->where('id',$id);
		$this->db->join('mst_kecamatan','t_iupp.kecamatan = mst_kecamatan.id_kecamatan');
		$this->db->join('mst_kelurahan','t_iupp.kelurahan = mst_kelurahan.id_kelurahan');
		return $this->db->get('t_iupp');
	}

	public function data_iuts()
	{
		return $this->db->get('t_iuts');
	}

	public function data_stpw_per_iuts($iuts)
	{
		$SQL = "SELECT * FROM t_stpw WHERE iuts IN (SELECT no_iuts FROM t_iuts WHERE iuts=".$iuts.")";
		return $this->db->query($SQL);
	}
	public function data_iuts_byid($id)
	{
		$this->db->where('id',$id);
		$this->db->join('mst_kecamatan','t_iuts.kecamatan = mst_kecamatan.id_kecamatan');
		$this->db->join('mst_kelurahan','t_iuts.kelurahan = mst_kelurahan.id_kelurahan');
		return $this->db->get('t_iuts');
	}

	public function data_stpw()
	{
		return $this->db->get('t_stpw');
	}

	public function data_iuts_per_stpw($iuts)
	{
		$SQL = "SELECT * FROM t_iuts WHERE no_iuts IN (SELECT no_iuts FROM t_iuts WHERE no_iuts=".$iuts.")";
		return $this->db->query($SQL);
	}
	public function data_stpw_byid($id)
	{
		$this->db->where('id',$id);
		$this->db->join('mst_kecamatan','t_stpw.kecamatan = mst_kecamatan.id_kecamatan');
		$this->db->join('mst_kelurahan','t_stpw.kelurahan = mst_kelurahan.id_kelurahan');
		return $this->db->get('t_stpw');
	}

	public function data_tdg()
	{
		return $this->db->get('t_tdg');
	}
	public function data_tdg_byid($id)
	{
		$this->db->where('id',$id);
		$this->db->join('mst_kecamatan','t_tdg.kecamatan = mst_kecamatan.id_kecamatan');
		$this->db->join('mst_kelurahan','t_tdg.kelurahan = mst_kelurahan.id_kelurahan');
		return $this->db->get('t_tdg');
	}

	public function data_siupmb()
	{
		return $this->db->get('t_siupmb');
	}
	public function data_siupmb_byid($id)
	{
		$this->db->where('id',$id);
		$this->db->join('mst_kecamatan','t_siupmb.kecamatan = mst_kecamatan.id_kecamatan');
		$this->db->join('mst_kelurahan','t_siupmb.kelurahan = mst_kelurahan.id_kelurahan');
		return $this->db->get('t_siupmb');
	}

	public function data_tdpud()
	{
		return $this->db->get('t_tdpud');
	}
	public function data_tdpud_byid($id)
	{
		$this->db->where('id',$id);
		$this->db->join('mst_kecamatan','t_tdpud.kecamatan = mst_kecamatan.id_kecamatan');
		$this->db->join('mst_kelurahan','t_tdpud.kelurahan = mst_kelurahan.id_kelurahan');
		return $this->db->get('t_tdpud');
	}

	public function data_b3()
	{
		return $this->db->get('t_b3');
	}

	public function data_b3_byid($id)
	{
		$this->db->where('id',$id);
		$this->db->join('mst_kecamatan','t_b3.kecamatan = mst_kecamatan.id_kecamatan');
		$this->db->join('mst_kelurahan','t_b3.kelurahan = mst_kelurahan.id_kelurahan');
		return $this->db->get('t_b3');
	}

	public function tambah_stok_bapok($data)
	{
		$this->db->insert('t_stok',$data);
	}

	public function update_stok($id,$data)
	{
		$this->db->where('id',$id);
		$this->db->update('t_stok',$data);
	}

	public function tambah_perizinan($data,$table)
	{
		$this->db->insert($table,$data);
	}

	public function update_perizinan($data,$table,$id)
	{
		$this->db->where('id',$id);
		$this->db->set($data);
		$this->db->update($table);
	}

	public function cek_stok($bahan_pokok,$tdg)
	{
		$this->db->where('id_bapok',$bahan_pokok);
		$this->db->where('tdg',$tdg);
		$this->db->join('t_bahan_pokok','t_stok.id_bapok = t_bahan_pokok.id_bahan_pok');
		return $this->db->get('t_stok');
	}

	public function tambah_jml_stok($id_bapok,$tdg,$data)
	{
		$this->db->where('id_bapok',$id_bapok);
		$this->db->where('tdg',$tdg);
		$this->db->update('t_stok',$data);
	}

	public function insert_pengeluaran($data)
	{
		$this->db->insert('t_keluar',$data);
	}
	public function data_pengeluaran()
	{
		$this->db->join('t_bahan_pokok','t_keluar.id_bapok = t_bahan_pokok.id_bahan_pok');
		return $this->db->get('t_keluar');
	}

	public function data_pengeluaran_pdf1($tdg)
	{
		$this->db->join('t_bahan_pokok','t_keluar.id_bapok = t_bahan_pokok.id_bahan_pok');
		//$this->db->join('t_stok','t_keluar.no_tdg = t_stok.tdg');
		$this->db->where('no_tdg',$tdg);
		//$this->db->group_by('t_keluar.id_bapok');
		return $this->db->get('t_keluar');
	}

	public function data_pengeluaran_pdf2($tdg)
	{
		$this->db->select('*, SUM(t_keluar.volume) volume', FALSE);
		$this->db->join('t_bahan_pokok','t_keluar.id_bapok = t_bahan_pokok.id_bahan_pok');
		//$this->db->join('t_stok','t_keluar.no_tdg = t_stok.tdg');
		$this->db->where('no_tdg',$tdg);
		$this->db->group_by('t_keluar.id_bapok');
		return $this->db->get('t_keluar');
	}

	public function data_pengeluaran_byid($id)
	{
		$this->db->where('id',$id);
		$this->db->join('t_bahan_pokok','t_keluar.id_bapok = t_bahan_pokok.id_bahan_pok');
		return $this->db->get('t_keluar');
	}

	public function edit_pengeluaran($id,$data)
	{
		$this->db->where('id',$id);
		$this->db->update('t_keluar',$data);
	}

	public function hapus_pengeluaran($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('t_keluar');
	}
	public function data_stok_dist_byid($id)
	{
		$this->db->where('id',$id);
		$this->db->join('t_bahan_pokok','t_bahan_pokok.id_bahan_pok = t_distribusi_stok.id_bapok');
		return $this->db->get('t_distribusi_stok');
	}

	public function data_stock_dist_by_bpk_tdpud($id_bapok,$tdpud)
	{
		$this->db->where('id_bapok',$id_bapok);
		$this->db->where('tdpud',$tdpud);
		$this->db->join('t_bahan_pokok','t_bahan_pokok.id_bahan_pok = t_distribusi_stok.id_bapok');
		return $this->db->get('t_distribusi_stok');
	}
	public function update_stock_dist($id,$data)
	{
		$this->db->where('id',$id);
		$this->db->update('t_distribusi_stok',$data);
	}

	public function delete_dist_keluar($id_bapok,$tdpud)
	{
		$this->db->where('id_bapok',$id_bapok);
		$this->db->where('tdpud',$tdpud);
		$this->db->delete('t_distribusi_keluar');
	}

	public function delete_dist_keluar_byid($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('t_distribusi_keluar');
	}

	public function delete_dist_pengadaan($id_bapok,$tdpud)
	{
		$this->db->where('id_bapok',$id_bapok);
		$this->db->where('tdpud',$tdpud);
		$this->db->delete('t_distribusi_pengadaan');
	}
	public function delete_dist_pengadaan_byid($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('t_distribusi_pengadaan');
	}

	public function delete_dist_stok($id_bapok,$tdpud)
	{
		$this->db->where('id_bapok',$id_bapok);
		$this->db->where('tdpud',$tdpud);
		$this->db->delete('t_distribusi_stok');
	}
	public function data_peng_dist_byid($id)
	{
		$this->db->where('id',$id);
		$this->db->join('t_bahan_pokok','t_bahan_pokok.id_bahan_pok = t_distribusi_pengadaan.id_bapok');
		return $this->db->get('t_distribusi_pengadaan');
	}
	public function data_dist_stok_flter($id_bapok,$tdpud)
	{
		$this->db->where('id_bapok',$id_bapok);
		$this->db->where('tdpud',$tdpud);
		$this->db->join('t_bahan_pokok','t_distribusi_stok.id_bapok = t_bahan_pokok.id_bahan_pok');
		return $this->db->get('t_distribusi_stok');
	}
	public function update_dist_pengeluaran($id_bapok,$tdpud,$data)
	{
		$this->db->where('id_bapok',$id_bapok);
		$this->db->where('tdpud',$tdpud);
		$this->db->update('t_distribusi_pengadaan',$data);
	}
	public function data_dist_keluar_byid($id)
	{
		$this->db->where('id',$id);
		$this->db->join('t_bahan_pokok','t_distribusi_keluar.id_bapok = t_bahan_pokok.id_bahan_pok');
		return $this->db->get('t_distribusi_keluar');
	}
	public function edit_data_dist_keluar($id,$data)
	{
		$this->db->where('id',$id);
		$this->db->update('t_distribusi_keluar',$data);
	}

	public function get_tdg_stok()
	{
		$this->db->join('t_bahan_pokok','t_tdg_stok.id_bapok = t_bahan_pokok.id_bahan_pok');
		return $this->db->get('t_tdg_stok');
	}

	public function get_tdg_stok_byid($id)
	{
		$this->db->where('id',$id);
		$this->db->join('t_bahan_pokok','t_tdg_stok.id_bapok = t_bahan_pokok.id_bahan_pok');
		return $this->db->get('t_tdg_stok');
	}
	public function get_tdg_stok_bybapok_tdg($id_bapok,$tdg)
	{
		$this->db->where('id_bapok',$id_bapok);
		$this->db->where('tdg',$tdg);
		$this->db->join('t_bahan_pokok','t_tdg_stok.id_bapok = t_bahan_pokok.id_bahan_pok');
		return $this->db->get('t_tdg_stok');
	}

	public function get_tdg_penambahan_stok()
	{
		$this->db->join('t_bahan_pokok','t_tdg_penambahan_stok.id_bapok = t_bahan_pokok.id_bahan_pok');
		return $this->db->get('t_tdg_penambahan_stok');
	}
	public function get_tdg_penambahan_stok_byid($id)
	{
		$this->db->where('id',$id);
		$this->db->join('t_bahan_pokok','t_tdg_penambahan_stok.id_bapok = t_bahan_pokok.id_bahan_pok');
		return $this->db->get('t_tdg_penambahan_stok');
	}

	public function get_tdg_pengiriman()
	{
		$this->db->join('t_bahan_pokok','t_tdg_pengiriman.id_bapok = t_bahan_pokok.id_bahan_pok');
		return $this->db->get('t_tdg_pengiriman');
	}
	public function cek_stok_tdg($id_bapok,$tdg)
	{
		$this->db->where('id_bapok',$id_bapok);
		$this->db->where('tdg',$tdg);
		$this->db->join('t_bahan_pokok','t_tdg_stok.id_bapok = t_bahan_pokok.id_bahan_pok');
		return $this->db->get('t_tdg_stok');
	}
	public function insert_stok_tdg($data)
	{
		$this->db->insert('t_tdg_stok',$data);
	}

	public function update_stok_tdg($id_bapok,$tdg,$data)
	{
		$this->db->where('id_bapok',$id_bapok);
		$this->db->where('tdg',$tdg);
		$this->db->set($data);
		$this->db->update('t_tdg_stok');
	}

	public function insert_tdg_penambahan_stok($data)
	{
		$this->db->insert('t_tdg_penambahan_stok',$data);
	}

	public function insert_data_pengiriman_tdg($data)
	{
		$this->db->insert('t_tdg_pengiriman',$data);
	}

	public function delete_stok_tdg($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('t_tdg_stok');
	}

	public function delete_penambahan_stok_tdg($id_bapok,$tdg)
	{
		$this->db->where('id_bapok',$id_bapok);
		$this->db->where('tdg',$tdg);
		$this->db->delete('t_tdg_penambahan_stok');
	}
	public function delete_pengiriman_tdg($id_bapok,$tdg)
	{
		$this->db->where('id_bapok',$id_bapok);
		$this->db->where('tdg',$tdg);
		$this->db->delete('t_tdg_pengiriman');
	}
	public function get_data_penambahan_stok_gudang($id)
	{
		$this->db->where('id',$id);
		$this->db->join('t_bahan_pokok','t_tdg_penambahan_stok.id_bapok = t_bahan_pokok.id_bahan_pok');
		return $this->db->get('t_tdg_penambahan_stok');
	}

	public function update_tambah_stok_gudang($id,$data)
	{
		$this->db->where('id',$id);
		$this->db->set($data);
		$this->db->update('t_tdg_penambahan_stok');
	}

	public function delete_tdg_penambahan_stok($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('t_tdg_penambahan_stok');
	}
	public function get_data_pengiriman_tdg($id)
	{
		$this->db->where('id',$id);
		$this->db->join('t_bahan_pokok','t_tdg_pengiriman.id_bapok = t_bahan_pokok.id_bahan_pok');
		return $this->db->get('t_tdg_pengiriman');
	}

	public function delete_tdg_pengiriman($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('t_tdg_pengiriman');
	}
	public function data_tdg_pengiriman_pdf($tgl_min,$tgl_max)
	{
		$this->db->join('t_tdg','t_tdg_pengiriman.tdg = t_tdg.no_tdg');
		$this->db->join('t_bahan_pokok','t_tdg_pengiriman.id_bapok = t_bahan_pokok.id_bahan_pok');
		$this->db->where('tgl_input >=',$tgl_min);
		$this->db->where('tgl_input <=',$tgl_max);
		return $this->db->get('t_tdg_pengiriman');

	}

	public function data_tdg_pengiriman_pdf2($tdg)
	{
		$this->db->join('t_tdg','t_tdg_pengiriman.tdg = t_tdg.no_tdg');
		$this->db->join('t_bahan_pokok','t_tdg_pengiriman.id_bapok = t_bahan_pokok.id_bahan_pok');
		$this->db->where('t_tdg_pengiriman.tdg',$tdg);
		//$SQL = "SELECT * FROM t_tdg_pengiriman WHERE id_bapok IN (SELECT id_bapok FROM t_tdg_stok WHERE tdg='".$tdg."')";
		//$this->db->group_by('tdg');
		return $this->db->get('t_tdg_pengiriman');
	}

	public function data_tdg_pengiriman_pdf3($tdg)
	{
		$this->db->join('t_tdg','t_tdg_pengiriman.tdg = t_tdg.no_tdg');
		$this->db->join('t_bahan_pokok','t_tdg_pengiriman.id_bapok = t_bahan_pokok.id_bahan_pok');
		$this->db->where("tujuan IN (SELECT tujuan FROM t_tdg_pengiriman WHERE tdg='".$tdg."')");
		$this->db->where('t_tdg_pengiriman.tdg',$tdg);
		//$this->db->group_by('tujuan');
		//$SQL = "SELECT * FROM t_tdg_pengiriman WHERE id_bapok IN (SELECT id_bapok FROM t_tdg_stok WHERE tdg='".$tdg."')";
		return $this->db->get('t_tdg_pengiriman');
	}
	public function get_bpok_berbahaya()
	{
		$this->db->order_by('id_bahan_pok','ASC');
		return $this->db->get('t_bahan_pokok_berbahaya');
	}
	public function get_stok_b3()
	{
		$this->db->join('t_b3','t_b3_stok.no_cas = t_b3.no_cas');
		return $this->db->get('t_b3_stok');
	}

	public function get_stok_b3_byid($id)
	{
		$this->db->where('id_stok',$id);
		return $this->db->get('t_b3_stok');
	}

	public function get_stok_b3_by_nocas($no_cas,$distributor)
	{
		$this->db->where('t_b3_stok.no_cas',$no_cas);
		$this->db->where('t_b3_stok.distributor',$distributor);
		$this->db->join('t_b3','t_b3_stok.no_cas = t_b3.no_cas');
		return $this->db->get('t_b3_stok');
	}

	public function cek_stok_b3($no_cas,$distributor)
	{
		$this->db->where('no_cas',$no_cas);
		$this->db->where('distributor',$distributor);
		return $this->db->get('t_b3_stok');
	}
	public function insert_stok_b3($data)
	{
		$this->db->insert('t_b3_stok',$data);
	}
	public function update_stok_b3($no_cas,$distributor,$update_stok)
	{
		$this->db->where('no_cas',$no_cas);
		$this->db->where('distributor',$distributor);
		$this->db->set($update_stok);
		$this->db->update('t_b3_stok');
	}
	public function insert_history_penambahan_stok_b3($data)
	{
		$this->db->insert('t_b3_penambahan',$data);
	}

	public function get_history_penambahan_stok_b3()
	{
		$this->db->join('t_b3','t_b3_penambahan.no_cas = t_b3.no_cas');
		return $this->db->get('t_b3_penambahan');
	}

	public function delete_penambahan_b3($id)
	{
		$this->db->where('id_penambahan',$id);
		$this->db->delete('t_b3_penambahan');
	}
	public function get_data_b3_keluar()
	{
		$this->db->join('t_tdp','t_tdp.no_tdp = t_b3_keluar.nm_distributor');
		$this->db->join('t_b3','t_b3_keluar.no_cas = t_b3.no_cas');
		return $this->db->get('t_b3_keluar');
	}
	public function insert_b3_keluar($data)
	{
		$this->db->insert('t_b3_keluar',$data);
	}

	public function delete_data_stok_b3($id)
	{
		$this->db->where('id_stok',$id);
		$this->db->delete('t_b3_stok');
	}
	public function delete_data_penambahan_b3($no_cas,$dist)
	{
		$this->db->where('no_cas',$no_cas);
		$this->db->where('distributor',$dist);
		$this->db->delete('t_b3_penambahan');
	}
	public function delete_data_keluar_b3($no_cas,$dist)
	{
		$this->db->where('no_cas',$no_cas);
		$this->db->where('nm_distributor',$dist);
		$this->db->delete('t_b3_keluar');
	}
	public function get_penambahan_b3_byid($id)
	{
		$this->db->where('id_penambahan',$id);
		return $this->db->get('t_b3_penambahan');
	}
	public function get_keluar_b3_byid($id)
	{
		$this->db->where('id_keluar',$id);
		return $this->db->get('t_b3_keluar');
	}

	public function delete_keluar_b3($id)
	{
		$this->db->where('id_keluar',$id);
		$this->db->delete('t_b3_keluar');
	}
}
?>
