<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front_model extends CI_Model {
	function data_tr_instansi(){
		$this->db->where('id', 1);	
		return $this->db->get('tr_instansi');	
	}
	
	function daftar_kategori_berita(){
		return $this->db->get('t_kategori_berita');	
	}
	function daftar_kategori_berita_where_id($id){
		$this->db->where('id_kategori',$id);
		return $this->db->get('t_kategori_berita');	
	}
	function daftar_berita_all(){
		$this->db->join('t_kategori_berita', 't_kategori_berita.id_kategori = t_berita.kategori');
		$this->db->where('t_berita.publish_status','Y');	
		return $this->db->get('t_berita');	
	}
	function daftar_berita_where_katg_all($katg){
		$this->db->join('t_kategori_berita', 't_kategori_berita.id_kategori = t_berita.kategori');
		$this->db->where('t_berita.publish_status','Y');	
		$this->db->where('t_berita.kategori',$katg);	
		return $this->db->get('t_berita');	
	}
	function daftar_berita(){
		$this->db->join('t_kategori_berita', 't_kategori_berita.id_kategori = t_berita.kategori');
		$this->db->limit(4); 
		$this->db->where('t_berita.cover_berita !=', "");
		$this->db->where('t_berita.cover_berita !=', "tidak ada cover");
		$this->db->order_by('t_berita.id_berita','DESC');	
		return $this->db->get('t_berita');	
	}
	function daftar_berita2(){
		$this->db->join('t_kategori_berita', 't_kategori_berita.id_kategori = t_berita.kategori');
		$this->db->limit(4); 
		$this->db->where('t_berita.cover_berita !=', "");
		$this->db->where('t_berita.cover_berita !=', "tidak ada cover");
		$this->db->order_by('t_berita.tanggal_posting','RANDOM');	
		return $this->db->get('t_berita');	
	}
	function daftar_berita_limit_pagination($batas){	
		$this->db->join('t_kategori_berita', 't_kategori_berita.id_kategori = t_berita.kategori');
		$this->db->order_by("id_berita", "desc"); 
		$this->db->where('t_berita.publish_status','Y');
		return $this->db->get('t_berita',4,$batas);
	}
	function daftar_berita_where_katg_limit_pagination($katg,$batas){	
		$this->db->join('t_kategori_berita', 't_kategori_berita.id_kategori = t_berita.kategori');
		$this->db->order_by("id_berita", "desc"); 
		$this->db->where('t_berita.publish_status','Y');
		$this->db->where('t_berita.kategori',$katg);
		return $this->db->get('t_berita',4,$batas);
	}
	function daftar_berita_where_id($id_berita){
		$this->db->join('t_kategori_berita', 't_kategori_berita.id_kategori = t_berita.kategori');
		$this->db->where('t_berita.id_berita', $id_berita);
		return $this->db->get('t_berita');	
	}
	function daftar_gallery(){
		$this->db->join('t_kategori_gallery', 't_kategori_gallery.id_kategori = t_gallery.kategori');
		$this->db->limit(4); 
		$this->db->where('t_gallery.gambar !=', "");
		$this->db->where('t_gallery.gambar !=', "tidak ada cover");
		$this->db->order_by('t_gallery.tanggal_posting','DESC');	
		return $this->db->get('t_gallery');	
	}
	function daftar_agenda(){	
		$this->db->where('t_agenda.cover_agenda !=', "");
		$this->db->where('t_agenda.cover_agenda !=', "tidak ada cover");
		$this->db->join('t_kategori_agenda', 't_kategori_agenda.id_kategori = t_agenda.kategori_agenda');
		$this->db->order_by('t_agenda.tanggal_posting','DESC');	
		return $this->db->get('t_agenda');	
	}
	function daftar_agenda_where_katg($katg){
		$this->db->join('t_kategori_agenda', 't_kategori_agenda.id_kategori = t_agenda.kategori_agenda');
		$this->db->where('t_agenda.publish_status','Y');	
		$this->db->where('t_agenda.kategori_agenda',$katg);
		return $this->db->get('t_agenda');	
	}
	function daftar_agenda_where_primary($id_primary){	
		$this->db->where('t_agenda.cover_agenda !=', "");
		$this->db->where('t_agenda.cover_agenda !=', "tidak ada cover");
		$this->db->where('t_agenda.id_agenda', $id_primary);
		$this->db->join('t_kategori_agenda', 't_kategori_agenda.id_kategori = t_agenda.kategori_agenda');
		$this->db->order_by('t_agenda.tanggal_posting','DESC');	
		return $this->db->get('t_agenda');	
	}
	function daftar_agenda_limit_pagination($batas){	
		$this->db->join('t_kategori_agenda', 't_kategori_agenda.id_kategori = t_agenda.kategori_agenda');
		$this->db->order_by("id_agenda", "desc");
		$this->db->where('t_agenda.publish_status','Y');
		return $this->db->get('t_agenda',4,$batas);
	}
	function daftar_agenda_limit_pagination_where_ktg($batas,$id_katg){	
		$this->db->join('t_kategori_agenda', 't_kategori_agenda.id_kategori = t_agenda.kategori_agenda');
		$this->db->order_by("id_agenda", "desc"); 
		$this->db->where('t_agenda.publish_status','Y');
		$this->db->where('t_agenda.kategori_agenda',$id_katg);
		return $this->db->get('t_agenda',4,$batas);
	}
	function daftar_agenda_all(){
		$this->db->join('t_kategori_agenda', 't_kategori_agenda.id_kategori = t_agenda.kategori_agenda');
		$this->db->where('t_agenda.publish_status','Y');	
		return $this->db->get('t_agenda');	
	}
	function daftar_kategori_agenda(){
		return $this->db->get('t_kategori_agenda');	
	}
	
	function daftar_tr_pasar_limit_pagination($batas){
		$this->db->join('t_pasar', 't_pasar.id_pasar = t_transaksi_b_pokok.id_pasar');
		$this->db->join('t_bahan_pokok', 't_bahan_pokok.id_bahan_pok = t_transaksi_b_pokok.id_b_pokok');
		$this->db->join('t_bahan_pokok_sub', 't_bahan_pokok_sub.id_bahan_pok_sub = t_transaksi_b_pokok.id_sub_b_pokok');
		return $this->db->get('t_transaksi_b_pokok',8,$batas);
	}
	function daftar_b_pokok_limit_pagination($batas){	
		$this->db->order_by("id_bahan_pok", "desc"); 
		return $this->db->get('t_bahan_pokok',8,$batas);
	}
	function daftar_b_pokok_limit_pagination_id_pasar($batas){
		$this->db->order_by("id_bahan_pok", "desc");
		return $this->db->get('t_bahan_pokok',8,$batas);
	}
	
	function data_login($id_login){
		$this->db->where('admin_id', $id_login);	
		
		return $this->db->get('t_admin');	
	}
	
	function daftar_regulasi_all(){
		return $this->db->get('t_regulasi');	
	}
}

?>