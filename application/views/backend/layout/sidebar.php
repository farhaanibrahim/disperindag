 <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="<?php echo base_url(); ?>"></a>
                        <a href="#" class=""></a>
                    </li>
                    <li class="xn-title"></li>
                    <li>
                        <a href="<?php echo site_url('backend') ?>"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>
                    </li>
					<li class="xn-openable">
                        <a href="#"><span class="fa fa-clock-o"></span> <span class="xn-text">Agenda</span></a>
						<ul>
                            <li><a href="<?php echo site_url('backend/agenda') ?>"><span class="fa fa-list"></span>Daftar Agenda</a></li>
							<li><a href="<?php echo site_url('backend/katg_agenda') ?>"><span class="fa fa-list"></span>Daftar Kategori Agenda</a></li>
                            <li><a href="<?php echo site_url('backend/tambah_agenda') ?>"><span class="fa fa-plus"></span>Tambah Agenda</a></li>
                            <li><a href="<?php echo site_url('backend/tambah_katg_agenda') ?>"><span class="fa fa-plus"></span>Tambah Kategori Agenda</a></li>
                        </ul>
                    </li>
					<li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text"></span> <span class="xn-text">Berita</span></a>
						<ul>
                            <li><a href="<?php echo site_url('backend/berita') ?>"><span class="fa fa-list"></span>Daftar Berita</a></li>
							<li><a href="<?php echo site_url('backend/katg_berita') ?>"><span class="fa fa-list"></span>Daftar Kategori Berita</a></li>
                            <li><a href="<?php echo site_url('backend/tambah_berita') ?>"><span class="fa fa-plus"></span>Tambah Berita</a></li>
                            <li><a href="<?php echo site_url('backend/tambah_katg_berita') ?>"><span class="fa fa-plus"></span>Tambah Kategori Berita</a></li>
                        </ul>
                    </li>
					<?php /*
					<li class="xn-openable">
                        <a href="#"><span class="fa fa-image"></span> <span class="xn-text">Gallery</span></a>
						<ul>
                            <li><a href="<?php echo site_url('backend/gallery') ?>"><span class="fa fa-list"></span>Daftar Gallery</a></li>
							<li><a href="<?php echo site_url('backend/kategori_gallery') ?>"><span class="fa fa-list"></span>Daftar Kategori Gallery</a></li>
                            <li><a href="<?php echo site_url('backend/tambah_gallery') ?>"><span class="fa fa-plus"></span>Tambah Gallery</a></li>
                            <li><a href="<?php echo site_url('backend/tambah_kategori_gallery') ?>"><span class="fa fa-plus"></span>Tambah Kategori Gallery</a></li>
                        </ul>
                    </li>
					*/ ?>
					<li class="xn-openable">
                        <a href="#"><span class="fa fa-list"></span> <span class="xn-text">Regulasi</span></a>
						<ul>
                            <li><a href="<?php echo site_url('backend/regulasi') ?>"><span class="fa fa-list"></span>Daftar Regulasi</a></li>
                            <li><a href="<?php echo site_url('backend/tambah_regulasi') ?>"><span class="fa fa-plus"></span>Tambah Regulasi</a></li>
                        </ul>
                    </li>
					<li class="xn-openable">
                        <a href="#"><span class="fa fa-archive"></span> <span class="xn-text">Bahan Pokok</span></a>
						<ul>
                            <li><a href="<?php echo site_url('backend/pasar') ?>"><span class="fa fa-list"></span>Pasar</a></li>
                            <li><a href="<?php echo site_url('backend/bahan_pokok') ?>"><span class="fa fa-list"></span>Bahan Pokok</a></li>
                            <li><a href="<?php echo site_url('backend/sub_bahan_pokok') ?>"><span class="fa fa-list"></span>Sub Bahan Pokok</a></li>
                            <!--
                            <li><a href="<?php echo site_url('backend/bahan_pokok_berbahaya') ?>"><span class="fa fa-list"></span>Bahan Pokok Berbahaya</a></li>
                            -->
                            <li><a href="<?php echo site_url('backend/transaksi_b_pokok') ?>"><span class="fa fa-list"></span>Transakasi Bahan Pokok</a></li>
                            <!--<li><a href="<?php echo site_url('backend/stok_bahan_pokok') ?>"><span class="fa fa-list"></span>Stok Bahan Pokok</a></li>-->
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-archive"></span> <span class="xn-text">Data Perizinan</span></a>
                        <ul>
                            <li><a href="<?php echo site_url('backend/tdp'); ?>"><span class="fa fa-list"></span>Daftar Data Perizinan</a></li>
                            <li><a href="<?php echo site_url('backend/tambah_tdp'); ?>"><span class="fa fa-plus"></span>Tambah Data Perizinan</a></li>
                            <!--<li><a href="<?php echo site_url('backend/tambah_klasifikasi'); ?>"><span class="fa fa-plus"></span>Tambah Klasifikasi</a></li> -->
                        </ul>
                    </li>
					<!--
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-archive"></span> <span class="xn-text">Data Pengeluaran</span></a>
                        <ul>
                            <li><a href="<?php echo site_url('backend/data_pengeluaran') ?>"><span class="fa fa-list"></span>Data Pengeluaran</a></li>
                            <li><a href="<?php echo site_url('backend/pengeluaran_bapok'); ?>"><span class="fa fa-list"></span>Tambah Pengeluaran</a></li>
                        </ul>
                    </li>
					-->
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-archive"></span> <span class="xn-text">Data Gudang</span></a>
                        <ul>
                            <li><a href="<?php echo site_url('backend/stok_gudang') ?>"><span class="fa fa-list"></span>Stok Gudang</a></li>
                            <li><a href="<?php echo site_url('backend/tambah_data_stok_tdg'); ?>"><span class="fa fa-list"></span>Tambah Stok Gudang</a></li>
                            <li><a href="<?php echo site_url('backend/tambah_data_pengiriman_tdg'); ?>"><span class="fa fa-list"></span>Tambah Data Pengiriman Gudang</a></li>
                        </ul>
                    </li>

                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-archive"></span> <span class="xn-text">Data Distributor</span></a>
                        <ul>
                            <li><a href="<?php echo site_url('backend/data_dist') ?>"><span class="fa fa-list"></span>Data Distributor</a></li>
                            <li><a href="<?php echo site_url('backend/tambah_data_dist'); ?>"><span class="fa fa-list"></span>Tambah Stok Distributor</a></li>
                            <li><a href="<?php echo site_url('backend/tambah_data_dist2'); ?>"><span class="fa fa-list"></span>Tambah Distribusi</a></li>
                        </ul>
                    </li>

                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-archive"></span> <span class="xn-text">Data B3</span></a>
                        <ul>
                            <li><a href="<?php echo site_url('backend/data_b3') ?>"><span class="fa fa-list"></span>Data B3</a></li>
                            <li><a href="<?php echo site_url('backend/tambah_stok_b3'); ?>"><span class="fa fa-list"></span>Tambah Stok B3</a></li>
                            <li><a href="<?php echo site_url('backend/tambah_data_pengguna_akhir'); ?>"><span class="fa fa-list"></span>Tambah Data Pengguna Akhir</a></li>
                        </ul>
                    </li>
                    <!--
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-archive"></span> <span class="xn-text">Data Master</span></a>
                        <ul>
                            <li><a href="<?php echo site_url('backend/mst_gudang') ?>"><span class="fa fa-list"></span>Data Gudang</a></li>
                        </ul>
                    </li>
                    -->

					<li class="xn-openable">
                        <a href="#"><span class="fa fa-gears"></span> <span class="xn-text">Pengaturan</span></a>
						<ul>
                            <?php //<li><a href="#"><span class="fa fa-list"></span>Pengguna</a></li> ?>
                            <li><a href="<?php echo site_url('backend/pengaturan_password') ?>"><span class="fa fa-lock"></span>Pengaturan Password</a></li>
                            <li><a href="<?php echo site_url('backend/setting_web') ?>"><span class="fa fa-list"></span>Aplikasi</a></li>
							<li><a href="<?php echo site_url('backend/setting_sambutan') ?>"><span class="fa fa-file-text"></span>Sambutan</a></li>
							<li><a href="<?php echo site_url('backend/setting_struk_org') ?>"><span class="fa fa-file-text"></span>Struktur Organisasi</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
