<!-- -------------- Header  -------------- -->
    <header class="navbar navbar-fixed-top bg-dark">
        <div class="navbar-logo-wrapper">
            <a class="navbar-logo-text" href="<?php echo base_url(); ?>">
                <b>Disperindag</b>
            </a>
            <span id="sidebar_left_toggle" class="ad ad-lines"></span>
        </div>
        <ul class="nav navbar-nav navbar-left">
            <li class="dropdown dropdown-fuse hidden-xs">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
					Konfigurasi Web
					<i class="fa fa-gears"></i>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Profile Aplikasi</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Ubah Password</a></li>
                </ul>
            </li>
            
        </ul>
        <form class="navbar-form navbar-left search-form square" role="search">
            <div class="input-group add-on">

                <input type="text" class="form-control" placeholder="Search..." onfocus="this.placeholder=''"
                       onblur="this.placeholder='Search...'">

                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>

            </div>
        </form>
        <ul class="nav navbar-nav navbar-right">
            
            <li class="dropdown dropdown-fuse">
                <a href="#" class="dropdown-toggle fw600" data-toggle="dropdown">
                    <span class="hidden-xs hidden-sm"><name><?php echo $login->nama_user; ?></name> </span>
                    <span class="fa fa-caret-down hidden-xs mr15"></span>
                </a>
                <ul class="dropdown-menu list-group keep-dropdown w250" role="menu">
                    
                    <li class="dropdown-footer text-center">
                        <a href="<?php echo site_url('backend/logout') ?>" class="btn btn-primary btn-sm btn-bordered">
                            <span class="fa fa-power-off pr5"></span> Logout </a>
                    </li>
                </ul>
            </li>
            
        </ul>
    </header>
    <!-- -------------- /Header  -------------- -->