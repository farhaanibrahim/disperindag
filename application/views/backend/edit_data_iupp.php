<?php foreach ($iupp->result() as $iupp) {} ?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title><?php echo $title; ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo base_url('upload/132.png') ?>" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('aset/backend') ?>/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
           <?php include_once 'layout/sidebar.php'; ?>
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
					<li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                    <!-- END TOGGLE NAVIGATION -->                    
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">                
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->                
                
                <div class="page-title">                    
                    <h2><span class="fa fa-file-text"></span> Edit IUPP</h2>
                </div>                   
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
							
                        <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('edit_perizinan/edit_perizinan_ac'); ?>">
                          <input type="hidden" name="table" value="t_iupp">
                          <input type="hidden" name="id" value="<?php echo $iupp->id; ?>">
                          <input type="hidden" name="foto_lama" value="<?php echo $iupp->foto; ?>">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Form Edit IUPP</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row">

                                        <div class="col-md-6">

                                            <div class="form-group">
                                               <label class="col-md-3 control-label">Tanggal Pendaftaran</label>
                                               <div class="col-md-9">                                            
                                                  <div class="input-group">
                                                     <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                     <input type="date" name="tgl_pendaftaran" placeholder="Tanggal Pendaftaran" class="form-control" value="<?php echo $iupp->tgl_pendaftaran; ?>" required/>
                                                 </div>                                            
                                             </div>
                                         </div>

                                         <div class="form-group">
                                            <label class="col-md-3 control-label">Nama Perusahaan</label>
                                            <div class="col-md-9">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" name="nm_perusahaan" placeholder="Nama Perusahaan" class="form-control" value="<?php echo $iupp->nm_perusahaan; ?>" required/>
                                                </div>                                            
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Nama Penanggung jawab</label>
                                            <div class="col-md-9">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" name="nm_penanggungjwb" placeholder="Penanggung jawab" class="form-control" value="<?php echo $iupp->nm_penanggungjwb; ?>" required/>
                                                </div>                                            
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Alamat Perusahaan</label>
                                            <div class="col-md-9">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <textarea class="form-control" name="almt_perusahaan"><?php echo $iupp->almt_perusahaan; ?></textarea>
                                                </div>                                            
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Kecamatan</label>
                                            <div class="col-md-9">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <select class="form-control" name="kecamatan" id="kecamatan" required>
                                                        <option value="<?php echo $iupp->kecamatan; ?>"><?php echo $iupp->nm_kecamatan ?></option>
                                                        <?php foreach ($kecamatan->result() as $row4): ?>
                                                            <option value="<?php echo $row4->id_kecamatan; ?>"><?php echo $row4->nm_kecamatan; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>                                            
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Kelurahan</label>
                                            <div class="col-md-9">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <select class="form-control" name="kelurahan" id="kelurahan" required>
                                                    <option value="<?php echo $iupp->kelurahan;; ?>"><?php echo $iupp->nm_kelurahan; ?></option>
                                                    </select>
                                                </div>                                            
                                            </div>
                                        </div>

                                        <div class="form-group">
                                           <label class="col-md-3 control-label">Jenis Usaha</label>
                                           <div class="col-md-9">                                            
                                              <div class="input-group">
                                                 <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                 <input type="text" name="jenis_usaha" placeholder="KBLI" value="<?php echo $iupp->jenis_usaha; ?>" class="form-control" required/>
                                             </div>                                            
                                         </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">SIUP</label>
                                        <div class="col-md-9">                                            
                                          <div class="input-group">
                                             <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                             <input type="text" name="siup" placeholder="SIUP" value="<?php echo $iupp->siup; ?>" class="form-control" required/>
                                         </div>                                            
                                     </div>
                                    </div>

                                    <div class="form-group">
                                   <label class="col-md-3 control-label">TDP</label>
                                   <div class="col-md-9">                                            
                                      <div class="input-group">
                                         <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                         <input type="text" name="tdp" placeholder="TDP" value="<?php echo $iupp->tdp; ?>" class="form-control" required/>
                                     </div>                                            
                                 </div>
                             </div>

                                         <div class="form-group">
                                            <label class="col-md-3 control-label">No. IUPP </label>
                                            <div class="col-md-9">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" name="no_sk" placeholder="No. SK" value="<?php echo $iupp->no_iupp; ?>" class="form-control" required/>
                                                </div>                                            
                                            </div>
                                        </div> 

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Tanggal IUPP </label>
                                            <div class="col-md-9">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="date" name="tgl_sk" placeholder="Tanggal SK" value="<?php echo $iupp->tgl_iupp; ?>" class="form-control" required/>
                                                </div>                                            
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">No. Rekomendasi </label>
                                            <div class="col-md-9">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" name="no_rekomendasi" value="<?php echo $iupp->no_rekomendasi; ?>" placeholder="No. Rekomandasi" class="form-control" required/>
                                                </div>                                            
                                            </div>
                                        </div> 

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">No. yang direncanakan </label>
                                            <div class="col-md-9">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" name="no_rencana" value="<?php echo $iupp->no_rencana; ?>" placeholder="Nomor yang direncanakan" class="form-control" required/>
                                                </div>                                            
                                            </div>
                                        </div>

                                        <div class="form-group">
                                         <label class="col-md-3 control-label">Foto</label>
                                         <div class="col-md-9">                                            
                                          <input type="file" name="userfile" />          
                                          </div>
                                      </div>

                                      <div class="form-group">
                                         <div class="col-md-3 control-label"></div>
                                         <div class="col-md-9">
                                          <button class="btn btn-primary" name="btnIupp">Submit</button>
                                          <button type="reset" class="btn btn-default">Clear Form</button>  
                                      </div>
                                    </div>

                                </div>

                                    </div>
                                </div>
                            </div>
                         </form>
                        </div>
                    </div>
                
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo site_url('backend/logout') ?>" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                 
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap.min.js"></script>              
        <!-- END PLUGINS -->

        <!-- THIS PAGE PLUGINS -->
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/icheck/icheck.min.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>  
		
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap-datepicker.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap-file-input.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap-select.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/tagsinput/jquery.tagsinput.min.js"></script>  
		
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/summernote/summernote.js"></script>  
        <!-- END PAGE PLUGINS -->         

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins.js"></script>        
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/actions.js"></script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->         
    </body>
</html>

<script type="text/javascript">
    $("#kecamatan").change(function(){
        var id_kecamatan = $("#kecamatan option:selected").val();
        $.ajax({
            url: "<?php echo site_url('backend/kelurahan')?>",
            type: "POST",
            data    : "id_kecamatan="+id_kecamatan,
            success : function (msg) {
                document.getElementById("kelurahan").disabled = false;
                $("#kelurahan").html(msg);
                //$("#kelurahan").css("color","black");
            }
        });
    });
</script>






