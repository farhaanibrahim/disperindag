<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title><?php echo $title; ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo base_url('upload/132.png') ?>" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('aset/backend') ?>/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
           <?php include_once 'layout/sidebar.php'; ?>
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
					<li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                    <!-- END TOGGLE NAVIGATION -->                    
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">                
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->                
                
                <div class="page-title">                    
                    <h2><span class="fa fa-file-text"></span> <?php echo $title2; ?></h2>
					<a href="<?php echo site_url("backend/tambah_data_dist"); ?>" class="btn btn-sm btn-info pull-right"><span class="fa fa-plus"></span>Tambah Data Pengadaan</a>
                    <a href="<?php echo site_url("backend/tambah_data_dist2"); ?>" class="btn btn-sm btn-primary pull-right"><span class="fa fa-plus"></span>Tambah Data Distributor</a>
					<br>
					<?php if ($this->session->flashdata('insert')): ?>
                        <div class="alert alert-success" style="" id="info_sukses">
                            <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                            </button>
                            <p class="text-small"><span class="fa fa-check"></span> <?php echo $this->session->flashdata('insert'); ?> </p>
                        </div>
                    <?php endif ?>
                    <?php if ($this->session->flashdata('update')): ?>
                        <div class="alert alert-info" style="" id="info_sukses">
                            <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                            </button>
                            <p class="text-small"><span class="fa fa-check"></span> <?php echo $this->session->flashdata('update'); ?> </p>
                        </div>
                    <?php endif ?>

                    <?php if ($this->session->flashdata('delete')): ?>
                        <div class="alert alert-info" style="" id="info_sukses">
                            <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                            </button>
                            <p class="text-small"><span class="fa fa-check"></span> <?php echo $this->session->flashdata('delete'); ?> </p>
                        </div>
                    <?php endif ?>
                </div>
				
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><?php echo $title2; ?></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-6">
                                        <strong><u>Tabel Stok</u></strong>
                                        <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Bahan Pokok</th>
                                                <th>TDPUD</th>
                                                <th>Stok</th>
                                                <th>Tanggal Input</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($stok->result() as $stok){ ?>
                                            <tr>
                                                <td><?php echo $stok->id; ?></td>
                                                <td><?php echo $stok->nama_bahan_pokok; ?></td>
                                                <td><?php echo $stok->tdpud; ?></td>
                                                <td><?php echo $stok->stok; ?></td>
                                                <td><?php echo $stok->tgl_input; ?></td>
                                                <td align="center">
                                                    <a href="<?php echo site_url("backend/edit_data_dist_stok/$stok->id") ?>" class="btn btn-sm btn-warning"><span class="fa fa-pencil"></span></a>
                                                    <a href="<?php echo site_url("backend/hapus_data_dist_stok/$stok->id") ?>" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure wants to delete?')" ><span class="fa fa-trash-o"></span></a>
                                                </td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                    </div>
                                    <div class="col-md-6">
                                        <strong><u>Tabel Pengadaan</u></strong>
                                        <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Bahan Pokok</th>
                                                <th>TDPUD</th>
                                                <th>Jenis</th>
                                                <th>Asal</th>
                                                <th>Jumlah Pengadaan</th>
                                                <th>Tanggal Input</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($pengadaan->result() as $dist_pengadaan){ ?>
                                            <tr>
                                                <td><?php echo $dist_pengadaan->id; ?></td>
                                                <td><?php echo $dist_pengadaan->nama_bahan_pokok; ?></td>
                                                <td><?php echo $dist_pengadaan->tdpud; ?></td>
                                                <td><?php echo $dist_pengadaan->jenis_tdpud; ?></td>
                                                <td><?php echo $dist_pengadaan->asal; ?></td>
                                                <td><?php echo $dist_pengadaan->jml_pengadaan." ".$dist_pengadaan->satuan; ?></td>
                                                <td><?php echo $dist_pengadaan->tgl_input; ?></td>
                                                <td align="center">
                                                    <!--
                                                    <a href="<?php echo site_url("backend/edit_data_dist_pengadaan/$dist_pengadaan->id") ?>" class="btn btn-sm btn-warning"><span class="fa fa-pencil"></span></a> -->
                                                    <a href="<?php echo site_url("backend/hapus_data_dist_pengadaan/$dist_pengadaan->id") ?>" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure wants to delete?')" ><span class="fa fa-trash-o"></span></a>
                                                </td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Daftar Distribusi</h3>
                                </div>
                                <div class="panel-body">
                                <div class="col-md-4">
                                    <form action="<?php echo base_url('backend/filter_distribusi'); ?>" method="post">
                                        <div class="form-group">
                                            <h4>Filter Berdasarkan TDPUD</h4>

                                            <div class="col-md-6">
                                                <label>TDPUD</label>
                                                <input type="text" name="tdpud" class="form-control" required>

                                                <label>Jenis Barang</label>
                                                <select class="form-control" name="id_bapok" required>
                                                    <option>-- Bapok --</option>
                                                    <?php foreach ($bhn_pokok->result() as $bapok): ?>
                                                        <option value="<?php echo $bapok->id_bahan_pok; ?>"><?php echo $bapok->nama_bahan_pokok; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>

                                            <div class="col-md-6">
                                                <label>Tanggal</label>
                                                <input type="date" name="tgl_min" class="form-control" required>
                                                s/d
                                                <input type="date" name="tgl_max" class="form-control" required>
                                            </div>

                                            <div class="col-md-3">
                                                <br>
                                                <input type="submit" name="btnPDF" value="PDF" class="btn btn-success">
                                                <!--<input type="submit" name="btnEXCEL" value="Excel" class="btn btn-warning">-->
                                            </div>

                                        </div>

                                    </form>
                                </div>
                                <div class="col-md-4">
                                    <form action="<?php echo base_url('backend/filt_dist_bpok'); ?>" method="post">
                                        <div class="form-group">
                                        <h4>Filter Berdasarkan Bahan Pokok</h4>

                                        <div class="col-md-6">

                                                <label>Jenis Barang</label>
                                                <select class="form-control" name="id_bapok" required>
                                                    <option>-- Bapok --</option>
                                                    <?php foreach ($bhn_pokok->result() as $bapok2): ?>
                                                        <option value="<?php echo $bapok2->id_bahan_pok; ?>"><?php echo $bapok2->nama_bahan_pokok; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>

                                            <div class="col-md-3">
                                                <br>
                                                <input type="submit" name="btnPDF" value="PDF" class="btn btn-success">
                                                <!--<input type="submit" name="btnEXCEL" value="Excel" class="btn btn-warning">-->
                                            </div>
                                    </div>

                                    </form>
                                </div>
                                <div class="col-md-4">
                                    <a href="<?php echo base_url('backend/filter_distribusi2'); ?>" class="btn btn-primary">Report All</a>
                                </div>
                                    

                                    <div class="col-md-12">
                                        <hr>
                                    </div>

                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Bahan Pokok</th>
                                                <th>TDPUD</th>
                                                <th>Tujuan</th>
                                                <th>Stok Awal</th>
                                                <th>Jumlah Pengeluaran</th>
                                                <th>Stok Akhir</th>
                                                <th>Keterangan</th>
                                                <th>Tanggal Input</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($distribusi->result() as $dist){ ?>
                                            <tr>
                                                <td><?php echo $dist->id; ?></td>
                                                <td><?php echo $dist->nama_bahan_pokok; ?></td>
                                                <td><?php echo $dist->tdpud; ?></td>
                                                <td><?php echo $dist->tujuan; ?></td>
                                                <td><?php echo $dist->stok_awal." ".$dist->satuan; ?></td>
                                                <td><?php echo $dist->jml_pengeluaran." ".$dist->satuan; ?></td>
                                                <td><?php echo $dist->stok_akhir." ".$dist->satuan; ?></td>
                                                <td><?php echo $dist->ket; ?></td>
                                                <td><?php echo $dist->tgl_input; ?></td>
                                                <td align="center">
                                                <!--
                                                    <a href="<?php echo site_url("backend/edit_data_dist_pengeluaran/$dist->id") ?>" class="btn btn-sm btn-warning"><span class="fa fa-pencil"></span></a>
                                                    -->
                                                    <a href="<?php echo site_url("backend/hapus_data_dist_pengeluaran/$dist->id") ?>" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure wants to delete?')"><span class="fa fa-trash-o"></span></a>
                                                </td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo site_url('backend/logout') ?>" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                 
        
	<!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap.min.js"></script>              
        <!-- END PLUGINS -->

        <!-- THIS PAGE PLUGINS -->
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/icheck/icheck.min.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>  
        <!-- END PAGE PLUGINS -->         

        <!-- START TEMPLATE -->      
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/actions.js"></script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->         
    </body>
</html>



