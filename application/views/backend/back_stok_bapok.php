<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title><?php echo $title; ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo base_url('upload/132.png') ?>" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('aset/backend') ?>/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
           <?php include_once 'layout/sidebar.php'; ?>
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
					<li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                    <!-- END TOGGLE NAVIGATION -->                    
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">                
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->                
                
                <div class="page-title">                    
                    <h2><span class="fa fa-file-text"></span> Stok Bahan Pokok</h2>
                    <a href="<?php echo site_url("backend/pengeluaran_bapok"); ?>" class="btn btn-sm btn-info pull-right"><span class="fa fa-plus"></span>Pengeluaran Bahan Pokok</a>

					<a href="<?php echo site_url("backend/tambah_stok_bapok"); ?>" class="btn btn-sm btn-primary pull-right"><span class="fa fa-plus"></span>Tambah Stok Bahan Pokok</a>
					<br>
					<?php if ($this->session->flashdata('update')): ?>
                        <div class="alert alert-dismissible alert-success">
                          <button type="button" class="close" data-dismiss="alert">&times;</button>
                          <strong><?php echo $this->session->flashdata('update'); ?></strong>
                        </div>
                    <?php endif ?>
                </div>
				
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Daftar Stok Bahan Pokok</h3>
                                </div>
                                <div class="panel-body">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Bahan Pokok</th>
                                                <th>TDG</th>
                                                <th>Stok</th>
                                                <th>Tanggal Input</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
											<?php foreach($stok->result() as $row){ ?>
                                            <tr>
                                                <td><?php echo $row->id; ?></td>
                                                <td><?php echo $row->nama_bahan_pokok; ?></td>
                                                <td><?php echo $row->tdg; ?></td>
												<td><?php echo $row->jml_stok." ".$row->satuan; ?></td>
                                                <td><?php echo $row->tgl_input; ?></td>
												<td align="center">
													<a href="<?php echo site_url("backend/edit_stok_bapok/$row->id") ?>" class="btn btn-sm btn-warning">
                                                        <span class="fa fa-pencil"></span>
                                                    </a>

													<a href="<?php echo site_url("backend/hapus_stok_bapok/$row->id") ?>" onclick="return confirm('Apakah anda yakin ingin menghapus data?')" class="btn btn-sm btn-danger"><span class="fa fa-trash-o"></span></a>
												</td>
                                            </tr>
											<?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo site_url('backend/logout') ?>" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                 
        
	<!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap.min.js"></script>              
        <!-- END PLUGINS -->

        <!-- THIS PAGE PLUGINS -->
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/icheck/icheck.min.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>  
        <!-- END PAGE PLUGINS -->         

        <!-- START TEMPLATE -->      
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/actions.js"></script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->         
    </body>
</html>






