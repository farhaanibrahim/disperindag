<!DOCTYPE html>
<html>
<head>
	<title>Preview</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" href="<?php echo base_url('upload'); ?>/logo_bogor.png" />
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css'); ?>/bootstrap.min.css">
</head>
<body>
<style type="text/css">
	.header {
		text-align: center;
	}
</style>
<!-- CSS goes in the document HEAD or added to your external stylesheet -->
<style type="text/css">
table.gridtable {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
	width: 100%;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
}
</style>
<div class="container">
<div class="row">
	<div class="col-md-12">
		<div class="header">
			<h2>Data Pengeluaran Bahan Pokok</h2>
		</div>
		<br>

		<table class="gridtable">
				<thead>
					<tr>
						<th>No.</th>
						<th>Bahan Pokok</th>
						<th>TDG</th>
						<th>DIkeluarkan ke</th>
						<th>Dikeluarkan Sebanyak</th>
					</tr>
				</thead>
				<tbody>
					<?php $no=1; ?>
					<?php foreach ($keluar1->result() as $keluar1): ?>
						<?php $jml_keluar = $jml_keluar+$keluar1->volume; ?>
						<tr>
							<td><?php echo $no; ?></td>
							<td><?php echo $keluar1->nama_bahan_pokok; ?></td>
							<td><?php echo $keluar1->no_tdg; ?></td>
							<td><?php echo $keluar1->dikeluarkan_ke; ?></td>
							<td><?php echo $keluar1->volume." ".$keluar1->satuan; ?></td>
						</tr>
						<?php $no++; ?>
					<?php endforeach ?>
				</tbody>
			</table>
			
			<br><br>

			<table class="gridtable">
				<thead>
					<tr>
						<th>No.</th>
						<th>Bahan Pokok</th>
						<th>Total Pengeluaran</th>
					</tr>
				</thead>
				<tbody>
					<?php $no2 = 1; $jml_keluar=0; ?>
					<?php foreach ($keluar2->result() as $keluar2): ?>
						<?php $jml_keluar=$jml_keluar+$keluar2->volume; ?>
						<tr>
							<td><?php echo $no2; ?></td>
							<td><?php echo $keluar2->nama_bahan_pokok; ?></td>
							<td><?php echo $keluar2->volume." ".$keluar2->satuan; ?></td>
						</tr>
						<?php $no2++; ?>		
					<?php endforeach ?>
				</tbody>
			</table>
	</div>
</div>
</div>
</body>
</html>