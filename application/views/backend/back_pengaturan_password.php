<!DOCTYPE html>
<?php foreach($instansi->result() as $is_row); ?>
<?php foreach($data_login->result() as $is_row2); ?>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title><?php echo $title; ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo base_url('upload/132.png') ?>" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('aset/backend') ?>/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
           <?php include_once 'layout/sidebar.php'; ?>
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
					<li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                    <!-- END TOGGLE NAVIGATION -->                    
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">                
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->                
                
                <div class="page-title">                    
                    <h2><span class="fa fa-file-text"></span> <?php echo $title2; ?></h2>
                </div>                 
				
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
					
                    <div class="row">
                        <div class="col-md-12">
							
							<div class="alert alert-success" style="display:none" id="alert1">
								<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
								</button>
								<p class="text-small">Berhasil.. Data Password sudah terupdate.. </p>
							</div>
							
							<div class="alert alert-danger" style="display:none" id="alert2">
								<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
								</button>
								<p class="text-small">Maaf.. Password yang anda ketikan Saat ini tidak benar, pastikan anda mengetik password anda saat ini dengan benar. </p>
							</div>
							
							<div class="alert alert-danger" style="display:none" id="alert3">
								<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
								</button>
								<p class="text-small">Maaf.. Password Baru yang anda ketikan tidak sama. </p>
							</div>
							
							<div class="alert alert-danger" style="display:none" id="alert0">
								<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
								</button>
								<p class="text-small">Maaf.. Input Gagal, saat ini sedang terjadi gangguang, silahkan coba beberapa saat lagi. </p>
							</div>
							
							<form class="form-horizontal" enctype="multipart/form-data" method="post" id="form_input" action="<?php echo site_url('backend/pengaturan_password_ac'); ?>">
							<input type="hidden" name="id_admin" value="<?php echo $is_row2->admin_id; ?>" />
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><?php echo $title2; ?></h3>
                                </div>
                                <div class="panel-body">
									<div class="row">
                                        
                                        <div class="col-md-8">
											
											<input name="logo_lama" class="form-control" id="logo_lama" placeholder="" type="hidden" value="<?php echo $is_row->logo; ?>" />
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Password Lama</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
														<input name="old_password" class="form-control" placeholder="Ketik Password anda saat ini" type="password" required />
                                                    </div>                                            
                                                </div>
                                            </div>
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Password Baru</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
														<input name="new_password" class="form-control" placeholder="Ketik Password baru anda saat ini" type="password" required />
													</div>                                            
                                                </div>
                                            </div>
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Ketik Ulang Password Baru</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
														<input name="re_new_password" class="form-control" placeholder="Ketik Ulang Password baru anda saat ini" type="password" required />
													</div>                                            
                                                </div>
                                            </div>
											
											
										</div>
										
                                    </div>
								</div>
								<div class="panel-footer">                                    
                                    <button class="btn btn-primary pull-right">Submit</button>
                                </div>
                            </div>
							</form>
                        </div>
                    </div>
                
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo site_url('backend/logout') ?>" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                 
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap.min.js"></script>              
        <!-- END PLUGINS -->

        <!-- THIS PAGE PLUGINS -->
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/icheck/icheck.min.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>  
		
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap-datepicker.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap-file-input.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap-select.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/tagsinput/jquery.tagsinput.min.js"></script>  
		
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/summernote/summernote.js"></script>  
        <!-- END PAGE PLUGINS -->         

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins.js"></script>        
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/actions.js"></script>        
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/jquery.form.js"></script>        
        <script>
			$(document).ready(function() {
				var options2 = { 
					beforeSend: function() {
						$("#alert1").hide();
						$("#alert2").hide();	
						$("#alert3").hide();
						$("#alert0").hide();
					},
					success: function() {
					},
					complete: function(response) {
						$("#prog_bar").hide();
						if(response.responseText == '1'){
							$("#alert1").show();
						}else if(response.responseText == '2'){
							$("#alert2").show();
						}else if(response.responseText == '3'){
							$("#alert3").show();
						}else{
							$("#alert0").show();
						}
					},
					error: function(){
						//$("#form_edit_ads #message").html("<font color='red'> ERROR: unable to upload files</font>");
					}
				}; 
				$("#form_input").ajaxForm(options2);
			});
		</script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->         
    </body>
</html>