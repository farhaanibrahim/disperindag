<?php foreach ($get->result() as $title) {} ?>
<!DOCTYPE html>
<html>
<head>
	<title>Preview</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" href="<?php echo base_url('upload'); ?>/logo_bogor.png" />
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css'); ?>/bootstrap.min.css">
</head>
<body>
	<style type="text/css">
		.header {
			text-align: center;
		}
	</style>
	<!-- CSS goes in the document HEAD or added to your external stylesheet -->
	<style type="text/css">
		table.gridtable {
			font-family: verdana,arial,sans-serif;
			font-size:11px;
			color:#333333;
			border-width: 1px;
			border-color: #666666;
			border-collapse: collapse;
			width: 100%;
		}
		table.gridtable th {
			border-width: 1px;
			padding: 8px;
			border-style: solid;
			border-color: #666666;
			background-color: #dedede;
		}
		table.gridtable td {
			border-width: 1px;
			padding: 8px;
			border-style: solid;
			border-color: #666666;
			background-color: #ffffff;
		}
	</style>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="header">
					<p>
						DINAS PERINDUSTRIAN DAN PERDAGANGAN KOTA BOGOR
						<br>
						LAPORAN DISTRIBUSI BARANG KEBUTUHAN POKOK <?php echo $jenis_barang; ?><br>
					</p>
				</div>
				<br>
				<table border="0">
					<tr>
						<td>Bahan Pokok</td>
						<td>:</td>
						<td><?php echo $jenis_barang; ?></td>
					</tr>
				</table>

				<table class="gridtable">
					<thead>
						<tr>
							<th>TDPUD</th>
							<th>Dikirim ke</th>
							<th>Stok Awal</th>
							<th>Jumlah Penyaluran</th>
							<th>Stok Akhir</th>
							<th>Tanggal</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($get->result() as $row): ?>
							<tr>
								<td><?php echo $row->tdpud ?></td>
								<td><?php echo $row->tujuan ?></td>
								<td><?php echo $row->stok_awal." ".$row->satuan ?></td>
								<td><?php echo $row->jml_pengeluaran." ".$row->satuan ?></td>
								<td><?php echo $row->stok_akhir." ".$row->satuan ?></td>
								<td><?php echo $row->tgl_input ?></td>
							</tr>
						<?php endforeach ?>					
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>	