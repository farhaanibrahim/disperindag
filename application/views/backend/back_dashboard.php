<?php foreach($data_login->result() as $admin){} ?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title><?php echo $title; ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo base_url('upload/132.png') ?>" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('aset/backend') ?>/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
           <?php include_once 'layout/sidebar.php'; ?>
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
					<li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                    <!-- END TOGGLE NAVIGATION -->                    
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">                
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->                
                
                <div class="page-title">                    
                    <h2><span class="fa fa-smile-o"></span> Halo <?php echo $admin->nama_user; ?>, Selamat Datang di Halaman Administrator</h2>
                </div>                   
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-3">

                            <div class="widget widget-success widget-item-icon">
                                <div class="widget-item-left">
                                    <span class="fa fa-calendar"></span>
                                </div>
                                <div class="widget-data">
                                    <div class="widget-int num-count"></div>
                                    <div class="widget-title">Agenda</div>
                                    <div class="widget-subtitle">Menu Agenda terdapat pada icon berikut</div>
                                </div>
								<div class="widget-buttons widget-c2">
                                    <div class="col">
                                        <a href="<?php echo site_url('backend/agenda') ?>"><span class="fa fa-list"></span></a>
                                    </div>
                                    <div class="col">
                                        <a href="<?php echo site_url('backend/tambah_agenda') ?>"><span class="fa fa-pencil"></span></a>
                                    </div>
                                </div>
                                <div class="widget-controls">                                
                                </div>                            
                            </div>

                        </div>
						<div class="col-md-3">

                            <div class="widget widget-info widget-item-icon">
                                <div class="widget-item-left">
                                    <span class="fa fa-file-text"></span>
                                </div>
                                <div class="widget-data">
                                    <div class="widget-int num-count"></div>
                                    <div class="widget-title">Berita</div>
                                    <div class="widget-subtitle">Menu Berita terdapat pada icon berikut</div>
                                </div>
								<div class="widget-buttons widget-c2">
                                    <div class="col">
                                        <a href="<?php echo site_url('backend/berita') ?>"><span class="fa fa-list"></span></a>
                                    </div>
                                    <div class="col">
                                        <a href="<?php echo site_url('backend/tambah_berita') ?>"><span class="fa fa-pencil"></span></a>
                                    </div>
                                </div>
                                <div class="widget-controls">                                
                                </div>                            
                            </div>

                        </div>
						<div class="col-md-3">

                            <div class="widget widget-danger widget-item-icon">
                                <div class="widget-item-left">
                                    <span class="fa fa-list"></span>
                                </div>
                                <div class="widget-data">
                                    <div class="widget-int num-count"></div>
                                    <div class="widget-title">Regulasi</div>
                                    <div class="widget-subtitle">Menu Regulasi terdapat pada icon berikut</div>
                                </div>
								<div class="widget-buttons widget-c2">
                                    <div class="col">
                                        <a href="<?php echo site_url('backend/regulasi') ?>"><span class="fa fa-list"></span></a>
                                    </div>
                                    <div class="col">
                                        <a href="<?php echo site_url('backend/tambah_regulasi') ?>"><span class="fa fa-pencil"></span></a>
                                    </div>
                                </div>
                                <div class="widget-controls">                                
                                </div>                            
                            </div>

                        </div>
						<div class="col-md-3">

                            <div class="widget widget-warning widget-item-icon">
                                <div class="widget-item-left">
                                    <span class="fa fa-archive"></span>
                                </div>
                                <div class="widget-data">
                                    <div class="widget-int num-count"></div>
                                    <div class="widget-title">Bahan Pokok</div>
                                    <div class="widget-subtitle">Menu Bahan Pokok terdapat pada icon berikut</div>
                                </div>
								<div class="widget-buttons widget-c2">
                                    <div class="col">
                                        <a href="<?php echo site_url('backend/bahan_pokok') ?>"><span class="fa fa-list"></span></a>
                                    </div>
                                    <div class="col">
                                        <a href="<?php echo site_url('backend/tambah_b_pokok') ?>"><span class="fa fa-pencil"></span></a>
                                    </div>
                                </div>
                                <div class="widget-controls">                                
                                </div>                            
                            </div>

                        </div>
						<div class="col-md-3">

                            <div class="widget widget-primary widget-item-icon">
                                <div class="widget-item-left">
                                    <span class="fa fa-archive"></span>
                                </div>
                                <div class="widget-data">
                                    <div class="widget-int num-count"></div>
                                    <div class="widget-title">Pasar</div>
                                    <div class="widget-subtitle">Menu Pasar terdapat pada icon berikut</div>
                                </div>
								<div class="widget-buttons widget-c2">
                                    <div class="col">
                                        <a href="<?php echo site_url('backend/pasar') ?>"><span class="fa fa-list"></span></a>
                                    </div>
                                    <div class="col">
                                        <a href="<?php echo site_url('backend/tambah_pasar') ?>"><span class="fa fa-pencil"></span></a>
                                    </div>
                                </div>
                                <div class="widget-controls">                                
                                </div>                            
                            </div>

                        </div>
						
                    </div>
                
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Yakin anda akan log out?</p>                    
                        <p>Tekan No jika melanjutkan. Tekan Yes untuk keluar.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo site_url('backend/logout') ?>" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                 
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->

        <!-- THIS PAGE PLUGINS -->

        <!-- END PAGE PLUGINS -->         

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins.js"></script>        
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/actions.js"></script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->         
    </body>
</html>






