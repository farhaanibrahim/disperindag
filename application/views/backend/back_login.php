<?php foreach($instansi->result() as $web){} ?>
<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>        
        <!-- META SECTION -->
        <title><?php echo $title; ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo base_url('upload/132.png') ?>" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('aset/backend') ?>/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->                                    
    </head>
    <body>
        
        <div class="login-container">
        
            <div class="login-box animated fadeInDown">
                <div class="login-logo"></div>
                <div class="login-body">
                    <div class="login-title"><strong>Welcome</strong>, Please login</div>
                    <form action="<?php echo site_url('backend/login') ?>" class="form-horizontal" method="post" id="form">
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="text" name="username" class="form-control" placeholder="Username" autofocus/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="password" name="password" class="form-control" placeholder="Password"/>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="col-md-12">
							<div class="progress progress-large progress-striped active" id="progresbar" style="display:none;">
								<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">Loading!!...</div>
							</div> 
							<div class="alert alert-danger" id="alert2" style="display:none;" role="alert">
                                <strong>Maaf!</strong> Username atau Password Salah, Mohon periksa dan silahkan login kembali .
                            </div>
                            <div class="alert alert-success" id="alert1" style="display:none;" role="alert">
                                <strong>Well done!</strong> You successfully read this important alert message.
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <?php //<a href="#" class="btn btn-link btn-block">Forgot your password?</a> ?>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-info btn-block">Log In</button>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="login-footer">
                    <div class="pull-left">
                        &copy; 2016 <?php echo $web->nama; ?>
                    </div>
                    <div class="pull-right">
                       
                    </div>
                </div>
            </div>
            
        </div>
        
    </body>
	<script src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery.min.js"></script>
	<script src="<?php echo base_url('aset/backend') ?>/js/jquery.form.js"></script>
	<script>
		$(document).ready(function(){
			var options2 = { 
                beforeSend: function() {
					$("#alert2").hide();
					$("#alert1").hide();
					$("#progresbar").fadeIn(500);
                },
                success: function() {
					$("#progresbar").fadeOut(500);
                },
                complete: function(response) {
                    if(response.responseText == '0'){
						$("#alert2").show();
					}else{
						$("#alert1").show();
						window.location = "<?php echo site_url('backend'); ?>";
					}
                },
                error: function(){
                    $("#form_edit_ads #message").html("<font color='red'> ERROR: unable to upload files</font>");
                }
            }; 
             $("#form").ajaxForm(options2);
		});
	</script>
</html>