<!DOCTYPE html>
<html>
<head>
	<title>Preview</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" href="<?php echo base_url('upload'); ?>/logo_bogor.png" />
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css'); ?>/bootstrap.min.css">
</head>
<body>
<style type="text/css">
	.header {
		text-align: center;
	}
</style>
<!-- CSS goes in the document HEAD or added to your external stylesheet -->
<style type="text/css">
table.gridtable {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
}
</style>
<div class="container">
<div class="row">
	<div class="col-md-12">
		<div class="header">
			<?php if ($table == 't_siup'): ?>
				<?php echo "SIUP"; ?>
			<?php endif ?>
			<?php if ($table == 't_tdp'): ?>
				<?php echo "TDP"; ?>
			<?php endif ?>
			<?php if ($table == 't_iupr'): ?>
				<?php echo "IUPR"; ?>
			<?php endif ?>
			<?php if ($table == 't_iupp'): ?>
				<?php echo "IUPP"; ?>
			<?php endif ?>
			<?php if ($table == 't_iuts'): ?>
				<?php echo "IUTS"; ?>
			<?php endif ?>
			<?php if ($table == 't_stpw'): ?>
				<?php echo "STPW"; ?>
			<?php endif ?>
			<?php if ($table == 't_tdg'): ?>
				<div style="text-align: right;">
					<p>LAMPIRAN IV <br>
					PERATURAN MENTERI PERDAGANGAN REPUBLIK INDONESIA <br>
					NOMOR 90/M-DAG/PER/12/2014 <br>
					TENTANG <br>
					PENATAAN DAN PEMBINAAN GUDANG</p>
				</div>
				<hr>
				<?php echo "REKAPITULASI PENERBITAN TDG"; ?>
			<?php endif ?>
			<?php if ($table == 't_siupmb'): ?>
				<?php echo "SIUP-MB"; ?>
			<?php endif ?>
			<?php if ($table == 't_tdpud'): ?>
				<?php echo "TDPUD"; ?>
			<?php endif ?>
			<?php if ($table == 't_b3'): ?>
				<p style="text-align: left;">
					LAMPIRAN I <br>
					PERATURAN MENTERI PERDAGANGAN REPUBLIK INDONESIA <br>
					NOMOR 75/M-DAG/PER/10/2014 <br>
					TENTANG <br>
					PERUBAHAN KEDUA ATAS PERATURAN MENTERI PERDAGANGAN NOMOR <br>
					44/M-DAG/PER/9/2009 TENTANG PENGADAAN, DISTRIBUSI DAN PENGAWASAN <br>
					BAHAN BERBAHAYA
				</p>
				<p><b>JENIS BAHAN BERBAHAYA YANG DIBATASI IMPOR, DISTRIBUSI DAN PENGAWASANNYA</b></p>
			<?php endif ?>
		</div>
		<br>

		<?php if ($table == 't_siup'): ?>
			<table class="gridtable">
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama Perusahaan</th>
						<th>Nama Penanggung Jawab</th>
						<th>No. Telp & Fax</th>
						<th>Modal Usaha</th>
						<th>Kelembagaan</th>
						<th>KBLI</th>
						<th>No. SIUP & Tanggal SIUP</th>
						<th>Masa Berlaku</th>
					</tr>
				</thead>
				<tbody>
					<?php $no=1; ?>
					<?php foreach ($perizinan->result() as $siup): ?>
						<tr>
							<td><?php echo $no; ?></td>
							<td><?php echo $siup->nm_perusahaan; ?></td>
							<td><?php echo $siup->nm_penanggungjwb; ?></td>
							<td><?php echo $siup->no_telp." & ".$siup->no_fax;; ?></td>
							<td><?php echo $siup->modal_usaha; ?></td>
							<td><?php echo $siup->kelembagaan; ?></td>
							<td><?php echo $siup->kbli; ?></td>
							<td><?php echo $siup->no_siup." & ".$siup->tgl_siup; ?></td>
							<td><?php echo $siup->masaberlaku; ?></td>
						</tr>
						<?php $no++; ?>
					<?php endforeach ?>
				</tbody>
			</table>
		<?php endif ?>

		<?php if ($table == 't_tdp'): ?>
			<table class="gridtable">
				<thead>
					<tr>
					<th>No.</th>
					<th>Nama Perusahaan</th>
					<th>Nama Penanggung Jawab</th>
					<th>Alamat Perusahaan</th>
					<th>NPWP</th>
					<th>No. Telp & Fax</th>
					<th>Kegiatan Usaha Pokok</th>
					<th>KBLI</th>
					<th>Status Kantor</th>
					<th>No. TDP & Tanggal TDP</th>
					<th>Masa Berlaku</th>
				</tr>
				</thead>
				<tbody>
					<?php $no2=1; ?>
					<?php foreach ($perizinan->result() as $tdp): ?>
						<tr>
							<td><?php echo $no2; ?></td>
							<td><?php echo $tdp->nm_perusahaan; ?></td>
							<td><?php echo $tdp->nm_penanggungjwb; ?></td>
							<td><?php echo $tdp->almt_perusahaan; ?></td>
							<td><?php echo $tdp->npwp; ?></td>
							<td><?php echo $tdp->no_telp." & ".$tdp->no_fax; ?></td>
							<td><?php echo $tdp->keg_usaha_pokok; ?></td>
							<td><?php echo $tdp->kbli; ?></td>
							<td><?php echo $tdp->status_kantor; ?></td>
							<td><?php echo $tdp->no_tdp." & ".$tdp->tgl_tdp; ?></td>
							<td><?php echo $tdp->masaberlaku; ?></td>
						</tr>
						<?php $no2++; ?>
					<?php endforeach ?> 
				</tbody>
			</table>
		<?php endif ?>

		<?php if ($table == 't_iupr'): ?>
			<table class="gridtable">
				<thead>
					<tr>
						<th>No.</th>
						<th>Tanggal Pendaftaran</th>
						<th>Nama Perusahaan</th>
						<th>Nama Penanggung Jawab</th>
						<th>Alamat Perusahaan</th>
						<th>Jenis Usaha</th>
						<th>SIUP</th>
						<th>TDP</th>
						<th>No. IUPR & Tanggal IUPR</th>
					</tr>
				</thead>
				<tbody>
					<?php $no3=1; ?>
					<?php foreach ($perizinan->result() as $iupr): ?>
						<tr>
							<td><?php echo $no3; ?></td>
							<td><?php echo $iupr->tgl_pendaftaran ?></td>
							<td><?php echo $iupr->nm_perusahaan ?></td>
							<td><?php echo $iupr->nm_penanggungjwb ?></td>
							<td><?php echo $iupr->almt_perusahaan ?></td>
							<td><?php echo $iupr->jenis_usaha ?></td>
							<td><?php echo $iupr->siup ?></td>
							<td><?php echo $iupr->tdp ?></td>
							<td><?php echo $iupr->no_iupr." & ".$iupr->tgl_iupr ?></td>
						</tr>
						<?php $no3++; ?>
					<?php endforeach ?>
				</tbody>
			</table>
		<?php endif ?>
			
		<?php if ($table == 't_iupp'): ?>
			<table class="gridtable">
				<thead>
					<tr>
						<th>No.</th>
						<th>Tanggal Pendaftaran</th>
						<th>Nama Perusahaan</th>
						<th>Nama Penanggung Jawab</th>
						<th>Alamat Perusahaan</th>
						<th>Jenis Usaha</th>
						<th>SIUP</th>
						<th>TDP</th>
						<th>No. IUPP & Tanggal IUPP</th>
					</tr>	
				</thead>
				<tbody>
					<?php $no4=1; ?>
					<?php foreach ($perizinan->result() as $iupp): ?>
						<tr>
							<td><?php echo $no4; ?></td>
							<td><?php echo $iupp->tgl_pendaftaran ?></td>
							<td><?php echo $iupp->nm_perusahaan ?></td>
							<td><?php echo $iupp->nm_penanggungjwb ?></td>
							<td><?php echo $iupp->almt_perusahaan ?></td>
							<td><?php echo $iupp->jenis_usaha ?></td>
							<td><?php echo $iupp->siup ?></td>
							<td><?php echo $iupp->tdp ?></td>
							<td><?php echo $iupp->no_iupp." & ".$iupp->tgl_iupp ?></td>
						</tr>
						<?php $no4++; ?>
					<?php endforeach ?>
				</tbody>
			</table>
		<?php endif ?>

		<?php if ($table == 't_iuts'): ?>
			<table class="gridtable">
			<thead>
				<tr>
					<th>No.</th>
					<th>Nama Perusahaan Pemberi</th>
					<th>Nama Penanggung Jawab</th>
					<th>Alamat Perusahaan</th>
					<th>NPWP</th>
					<th>No. Telp & Fax</th>
					<th>Kegiatan Usaha Pokok</th>
					<th>KBLI</th>
					<th>STatus Kantor</th>
					<th>SIUP</th>
					<th>TDP</th>
					<th>No. IUTS dan Tanggal IUTS</th>
					<th>Masa Berlaku</th>
					<th>STPW</th>
				</tr>
			</thead>
			<tbody>
				<?php $no5 = 1; ?>
				<?php foreach ($perizinan->result() as $iuts): ?>
					<tr>
						<td><?php echo $no5; ?></td>
						<td><?php echo $iuts->nm_perusahaan ?></td> 
						<td><?php echo $iuts->nm_penanggungjwb ?></td>
						<td><?php echo $iuts->almt_perusahaan ?></td>
						<td><?php echo $iuts->npwp ?></td>
						<td><?php echo $iuts->no_telp." & ".$iuts->no_fax ?></td>
						<td><?php echo $iuts->keg_usaha_pokok ?></td>
						<td><?php echo $iuts->kbli ?></td>
						<td><?php echo $iuts->status_kantor ?></td>
						<td><?php echo $iuts->siup ?></td>
						<td><?php echo $iuts->tdp ?></td>
						<td><?php echo $iuts->no_iuts." & ".$iuts->tgl_iuts ?></td>
						<td><?php echo $iuts->masaberlaku ?></td>
						<td>
							<table class="gridtable">
								<thead>
									<tr>
										<th>Nama Perusahaan</th>
										<th>Alamat Perusahaan</th>
										<th>Nama Penanggung Jawab</th>
										<th>STPW</th>
									</tr>
								</thead>
								<tbody>
									<?php $data_stpw = $stpw->data_stpw_per_iuts($iuts->no_iuts); ?>
									<?php foreach ($data_stpw->result() as $data_stpw): ?>
										<tr>
											<td><?php echo $data_stpw->nm_perusahaan ?></td>
											<td><?php echo $data_stpw->almt_perusahaan ?></td>
											<td><?php echo $data_stpw->nm_penanggungjwb ?></td>
											<td><?php echo $data_stpw->no_stpw ?></td>
										</tr>
									<?php endforeach ?>
								</tbody>
							</table>
						</td>
					</tr>
					<?php $no5++; ?>
				<?php endforeach ?>
			</tbody>
			</table>
		<?php endif ?>

		<?php if ($table == 't_stpw'): ?>
			<table class="gridtable">
				<thead>
					<tr>
						<th>No.</th>
						<th>No. Pendaftaran</th>
						<th>Nama Perusahaan Penerima</th>
						<th>Nama Penanggung Jawab</th>
						<th>Alamat Perusahaan</th>
						<th>NPWP</th>
						<th>IUTS</th>
						<th>No. STPW & Tanggal STPW</th>
						<th>No. Telp</th>
						<th>Masa Berlaku</th>
						<th>IUTS</th>
					</tr>
				</thead>
				<tbody>
					<?php $no6=1; ?>
					<?php foreach ($perizinan->result() as $stpw): ?>
						<tr>
							<td><?php echo $no6; ?></td>
							<td><?php echo $stpw->no_pendaftaran ?></td>
							<td><?php echo $stpw->nm_perusahaan ?></td>
							<td><?php echo $stpw->nm_penanggungjwb ?></td>
							<td><?php echo $stpw->almt_perusahaan ?></td>
							<td><?php echo $stpw->npwp ?></td>
							<td><?php echo $stpw->iuts ?></td>
							<td><?php echo $stpw->no_stpw." & ".$stpw->tgl_stpw ?></td>
							<td><?php echo $stpw->no_telp ?></td>
							<td><?php echo $stpw->masaberlaku ?></td>
							<td>
								<?php $data_iuts = $iuts->data_iuts_per_stpw($stpw->iuts); ?>
								<ul>
									<?php foreach ($data_iuts->result() as $data_iuts): ?>
										<li><?php echo "Nama Perusahaan : ".$data_iuts->nm_perusahaan; ?></li>
										<li><?php echo "No. IUTS : ".$data_iuts->no_iuts; ?></li>
									<?php endforeach ?>
								</ul>
							</td>
						</tr>
						<?php $no6++; ?>
					<?php endforeach ?>
				</tbody>
			</table>
		<?php endif ?>

		<?php if ($table == 't_tdg'): ?>
			<table class="gridtable">
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama Pemilik/Penangung Jawab Gudang</th>
						<th>No. TDG & Tanggal TDG</th>
						<th>Alamat Gudang</th>
						<th>Telp/Fax/Email</th>
						<th>Titik Kordinat</th>
						<th>Luas (m2)</th>
						<th>Kapasitas (m3/ton)</th>
						<th>Kelengkapan Gudang</th>
					</tr>
				</thead>
				<tbody>
					<?php $no7=1; ?>
					<?php foreach ($perizinan->result() as $tdg): ?>
						<tr>
							<td><?php echo $no7; ?></td>
							<td><?php echo $tdg->nm_penanggungjwb ?></td>
							<td><?php echo $tdg->no_tdg."&".$tdg->tgl_tdg ?></td>
							<td><?php echo $tdg->almt_gudang ?></td>
							<td><?php echo $tdg->no_telp."/".$tdg->no_fax."/".$tdg->email ?></td>
							<td><?php echo $tdg->longitude." - ".$tdg->latitude ?></td>
							<td><?php echo $tdg->luas_gudang ?></td>
							<td><?php echo $tdg->kapasitas_gudang ?></td>
							<td><?php echo $tdg->kelengkapan_gudang ?></td>
						</tr>
						<?php $no7++; ?>
					<?php endforeach ?>
				</tbody>
			</table>

			<br><br><br>
			<div style="text-align: right;">
				<p>
					........, ........................... 20.......... <br>
					Kepala PTSP/Dinas ........................... <br>
					Kabupaten/Kota ..............................
					<br><br><br><br>
					__________________________________ <br>
					NIP. .............................................
				</p>
			</div>
			
			<br><br><br>

			<div style="text-align: right;">
				<p>
					MENTERI PERDAGANGAN <br>
					REPUBLIK INDONESIA,
					<br><br><br><br>
					RAHMAT GOBEL
				</p>
			</div>
		<?php endif ?>

		<?php if ($table == 't_siupmb'): ?>
			<table class="gridtable">
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama Perusahaan</th>
						<th>Alamat Perusahaan</th>
						<th>SIUP-MB</th>
						<th>TDP</th>
						<th>Tanggal Keluar</th>
						<th>Tanggal Berakhir</th>
						<th>Insansi Yang Mengeluarkan</th>
						<th>Keterangan</th>
					</tr>
				</thead>
				<tbody>
					<?php $no8 = 1; ?>
					<?php foreach ($perizinan->result() as $siupmb): ?>
						<tr>
							<td><?php echo $no8; ?></td>
							<td><?php echo $siupmb->nm_perusahaan ?></td>
							<td><?php echo $siupmb->almt_perusahaan ?></td>
							<td><?php echo $siupmb->siupmb ?></td>
							<td><?php echo $siupmb->tdp ?></td>
							<td><?php echo $siupmb->tgl_keluar ?></td>
							<td><?php echo $siupmb->tgl_berakhir ?></td>
							<td><?php echo $siupmb->ins_yg_mengeluarkan ?></td>
							<td><?php echo $siupmb->keterangan ?></td>
						</tr>
						<?php $no8++; ?>
					<?php endforeach ?>
				</tbody>
			</table>
		<?php endif ?>

		<?php if ($table == 't_tdpud'): ?>
			<table class="gridtable">
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama Perusahaan</th>
						<th>Nama Penanggung Jawab</th>
						<th>Alamat Perusahaan</th>
						<th>NPWP</th>
						<th>No. Telp & Fax</th>
						<th>Kegiatan Usaha Pokok</th>
						<th>KBLI</th>
						<th>Status Kantor</th>
						<th>TDP</th>
						<th>No. TDPUD & Tanggal TDPUD</th>
						<th>Masa Berlaku</th>
					</tr>
				</thead>
				<tbody>
					<?php $no9=1; ?>
					<?php foreach ($perizinan->result() as $tdpud): ?>
						<tr>
							<td><?php echo $no9 ?></td>
							<td><?php echo $tdpud->nm_perusahaan ?></td>
							<td><?php echo $tdpud->nm_penanggungjwb ?></td>
							<td><?php echo $tdpud->almt_perusahaan ?></td>
							<td><?php echo $tdpud->npwp ?></td>
							<td><?php echo $tdpud->no_telp." & ".$tdpud->no_fax ?></td>
							<td><?php echo $tdpud->keg_usaha_pokok ?></td>
							<td><?php echo $tdpud->kbli ?></td>
							<td><?php echo $tdpud->status_kantor ?></td>
							<td><?php echo $tdpud->tdp ?></td>
							<td><?php echo $tdpud->no_tdpud." & ".$tdpud->tgl_tdpud ?></td>
							<td><?php echo $tdpud->masaberlaku ?></td>
						</tr>
						<?php $no9++; ?>
					<?php endforeach ?>
				</tbody>
			</table>
		<?php endif ?>

		<?php if ($table == 't_b3'): ?>
			<table class="gridtable">
				<thead>
					<tr>
						<th>NO.</th>
						<th>NO. CAS</th>
						<th>POS TARIF/HS</th>
						<th>URAIAN BARANG</th>
						<th>TATA NIAGA IMPOR</th>
						<th>KEPERLUAN LAIN TIDAK UNTUK PANGAN</th>
						<th>LABORATORIUM / PENELITIAN</th>
					</tr>
				</thead>
				<tbody>
					<?php $no=1; ?>
					<?php foreach ($perizinan->result() as $b3): ?>
						<tr>
							<td><?php echo $no; ?></td>
							<td><?php echo $b3->no_cas; ?></td>
							<td><?php echo $b3->pos_trf_hs; ?></td>
							<td><?php echo $b3->uraian_brg; ?></td>
							<td><?php echo $b3->tata_niaga_impor; ?></td>
							<td><?php echo $b3->kep_lain_tdk_utk_pgn; ?></td>
							<td><?php echo $b3->lab; ?></td>
							<?php $no++; ?>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		<?php endif ?>
		
	</div>
</div>
</div>
</body>
</html>