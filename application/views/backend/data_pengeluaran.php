<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title><?php echo $title; ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo base_url('upload/132.png') ?>" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('aset/backend') ?>/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
           <?php include_once 'layout/sidebar.php'; ?>
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
					<li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                    <!-- END TOGGLE NAVIGATION -->                    
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">                
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->                
                
                <div class="page-title">                    
                    <h2><span class="fa fa-file-text"></span> Data Pengeluaran Bahan Pokok</h2>
                </div>                   
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
							
							<form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('backend/export_pengeluaran'); ?>">
							<div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Data Pengeluaran Bahan Pokok</h3>
                                </div>
                                <div class="panel-body">
									<div class="row">

                                        <?php if ($this->session->flashdata('upload')): ?>
                                            <div class="alert alert-success" style="" id="info_sukses">
                                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                                                </button>
                                                <p class="text-small"><span class="fa fa-check"></span> <?php echo $this->session->flashdata('upload'); ?> </p>
                                            </div>
                                        <?php endif ?>
                                        <?php if ($this->session->flashdata('sukses')): ?>
                                            <div class="alert alert-success" style="" id="info_sukses">
                                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                                                </button>
                                                <p class="text-small"><span class="fa fa-check"></span> <?php echo $this->session->flashdata('upload'); ?> </p>
                                            </div>
                                        <?php endif ?>
                                        <?php if ($this->session->flashdata('error')): ?>
                                            <div class="alert alert-danger" style="" id="info_sukses">
                                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                                                </button>
                                                <p class="text-small"><span class="fa fa-check"></span> <?php echo $this->session->flashdata('error'); ?> </p>
                                            </div>
                                        <?php endif ?>

                                        <?php if ($this->session->flashdata('delete')): ?>
                                            <div class="alert alert-warning" style="" id="info_sukses">
                                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                                                </button>
                                                <p class="text-small"><span class="fa fa-check"></span> <?php echo $this->session->flashdata('delete'); ?> </p>
                                            </div>
                                        <?php endif ?>
                                        
                                        <div class="col-md-12">
                                            <form target="_blank" action="<?php echo base_url('backend/export_pengeluaran'); ?>" method="post">
                                                <div class="form-group">
                                                    <h4>Filter</h4>
                                                    <div class="col-md-3">
                                                        <label>TDG</label>
                                                        <input type="text" name="tdg" class="form-control">
                                                    </div>

                                                    <div class="col-md-3">
                                                        <br>
                                                        <input type="submit" name="btnPDF" value="PDF" class="btn btn-success">
                                                        <input type="submit" name="btnEXCEL" value="Excel" class="btn btn-warning">
                                                    </div>

                                                </div>
                                                <div class="col-md-12">
                                                    <input type="submit" name="btnFilter" class="btn btn-primary">
                                                </div>
                                            </form>

                                            <div class="col-md-12">
                                                <div class="row">
                                                    <hr>
                                                </div>
                                            </div>
                                            <br>
                                            <strong>Tabel pengeluaran bahan pokok</strong>
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>ID Bapok</th>
                                                        <th>Nama Bapok</th>
                                                        <th>Volume</th>
                                                        <th>DIkeluarkan ke</th>
                                                        <th>Tanggal</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($pengeluaran->result() as $row): ?>
                                                        <tr>
                                                            <td><?php echo $row->id_bapok; ?></td>
                                                            <td><?php echo $row->nama_bahan_pokok; ?></td>
                                                            <td><?php echo $row->volume; ?></td>
                                                            <td><?php echo $row->dikeluarkan_ke; ?></td>
                                                            <td><?php echo $row->tgl_keluar; ?></td>
                                                            <td>
                                                                <a href="<?php echo base_url('backend/edit_pengeluaran'); ?>/<?php echo $row->id; ?>" class="btn btn-warning"><span class="fa fa-pencil"></span></a>

                                                                <a href="<?php echo base_url('backend/hapus_pengeluaran'); ?>/<?php echo $row->id; ?>" class="btn btn-danger" onclick="return confirm('Are you sure wants to delete?')"><span class="fa fa-trash-o"></span></a>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach ?>
                                                </tbody>
                                            </table>

                                        </div>
                                        
                                    </div>
								</div>
								<div class="panel-footer">
                                    <button class="btn btn-default">Clear Form</button>                                    
                                    <button class="btn btn-primary pull-right">Submit</button>
                                </div>
                            </div>
							</form>
                        </div>
                    </div>
                
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo site_url('backend/logout') ?>" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                 
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap.min.js"></script>              
        <!-- END PLUGINS -->

        <!-- THIS PAGE PLUGINS -->
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/icheck/icheck.min.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>  
		
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap-datepicker.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap-file-input.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap-select.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/tagsinput/jquery.tagsinput.min.js"></script>  
		
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/summernote/summernote.js"></script>  
        <!-- END PAGE PLUGINS -->         

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins.js"></script>        
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/actions.js"></script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->         
    </body>
</html>






