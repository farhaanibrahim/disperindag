<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<?php if ($table == 't_siup'): ?>
			<table>
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama Perusahaan</th>
						<th>Nama Penanggung Jawab</th>
						<th>No. Telp & Fax</th>
						<th>Modal Usaha</th>
						<th>Kelembagaan</th>
						<th>KBLI</th>
						<th>No. SIUP & Tanggal SIUP</th>
						<th>Masa Berlaku</th>
					</tr>
				</thead>
				<tbody>
					<?php $no=1; ?>
					<?php foreach ($perizinan->result() as $siup): ?>
						<tr>
							<td><?php echo $no; ?></td>
							<td><?php echo $siup->nm_perusahaan; ?></td>
							<td><?php echo $siup->nm_penanggungjwb; ?></td>
							<td><?php echo $siup->no_telp." & ".$siup->no_fax;; ?></td>
							<td><?php echo $siup->modal_usaha; ?></td>
							<td><?php echo $siup->kelembagaan; ?></td>
							<td><?php echo $siup->kbli; ?></td>
							<td><?php echo $siup->no_siup." & ".$siup->tgl_siup; ?></td>
							<td><?php echo $siup->masaberlaku; ?></td>
						</tr>
						<?php $no++; ?>
					<?php endforeach ?>
				</tbody>
			</table>
		<?php endif ?>

		<?php if ($table == 't_tdp'): ?>
			<table>
				<thead>
					<tr>
					<th>No.</th>
					<th>Nama Perusahaan</th>
					<th>Nama Penanggung Jawab</th>
					<th>Alamat Perusahaan</th>
					<th>NPWP</th>
					<th>No. Telp & Fax</th>
					<th>Kegiatan Usaha Pokok</th>
					<th>KBLI</th>
					<th>Status Kantor</th>
					<th>No. TDP & Tanggal TDP</th>
					<th>Masa Berlaku</th>
				</tr>
				</thead>
				<tbody>
					<?php $no2=1; ?>
					<?php foreach ($perizinan->result() as $tdp): ?>
						<tr>
							<td><?php echo $no2; ?></td>
							<td><?php echo $tdp->nm_perusahaan; ?></td>
							<td><?php echo $tdp->nm_penanggungjwb; ?></td>
							<td><?php echo $tdp->almt_perusahaan; ?></td>
							<td><?php echo $tdp->npwp; ?></td>
							<td><?php echo $tdp->no_telp." & ".$tdp->no_fax; ?></td>
							<td><?php echo $tdp->keg_usaha_pokok; ?></td>
							<td><?php echo $tdp->kbli; ?></td>
							<td><?php echo $tdp->status_kantor; ?></td>
							<td><?php echo $tdp->no_tdp." & ".$tdp->tgl_tdp; ?></td>
							<td><?php echo $tdp->masaberlaku; ?></td>
						</tr>
						<?php $no2++; ?>
					<?php endforeach ?> 
				</tbody>
			</table>
		<?php endif ?>

		<?php if ($table == 't_iupr'): ?>
			<table>
				<thead>
					<tr>
						<th>No.</th>
						<th>Tanggal Pendaftaran</th>
						<th>Nama Perusahaan</th>
						<th>Nama Penanggung Jawab</th>
						<th>Alamat Perusahaan</th>
						<th>Jenis Usaha</th>
						<th>SIUP</th>
						<th>TDP</th>
						<th>No. IUPR & Tanggal IUPR</th>
					</tr>
				</thead>
				<tbody>
					<?php $no3=1; ?>
					<?php foreach ($perizinan->result() as $iupr): ?>
						<tr>
							<td><?php echo $no3; ?></td>
							<td><?php echo $iupr->tgl_pendaftaran ?></td>
							<td><?php echo $iupr->nm_perusahaan ?></td>
							<td><?php echo $iupr->nm_penanggungjwb ?></td>
							<td><?php echo $iupr->almt_perusahaan ?></td>
							<td><?php echo $iupr->jenis_usaha ?></td>
							<td><?php echo $iupr->siup ?></td>
							<td><?php echo $iupr->tdp ?></td>
							<td><?php echo $iupr->no_iupr." & ".$iupr->tgl_iupr ?></td>
						</tr>
						<?php $no3++; ?>
					<?php endforeach ?>
				</tbody>
			</table>
		<?php endif ?>
			
		<?php if ($table == 't_iupp'): ?>
			<table >
				<thead>
					<tr>
						<th>No.</th>
						<th>Tanggal Pendaftaran</th>
						<th>Nama Perusahaan</th>
						<th>Nama Penanggung Jawab</th>
						<th>Alamat Perusahaan</th>
						<th>Jenis Usaha</th>
						<th>SIUP</th>
						<th>TDP</th>
						<th>No. IUPP & Tanggal IUPP</th>
					</tr>	
				</thead>
				<tbody>
					<?php $no4=1; ?>
					<?php foreach ($perizinan->result() as $iupp): ?>
						<tr>
							<td><?php echo $no4; ?></td>
							<td><?php echo $iupp->tgl_pendaftaran ?></td>
							<td><?php echo $iupp->nm_perusahaan ?></td>
							<td><?php echo $iupp->nm_penanggungjwb ?></td>
							<td><?php echo $iupp->almt_perusahaan ?></td>
							<td><?php echo $iupp->jenis_usaha ?></td>
							<td><?php echo $iupp->siup ?></td>
							<td><?php echo $iupp->tdp ?></td>
							<td><?php echo $iupp->no_iupp." & ".$iupp->tgl_iupp ?></td>
						</tr>
						<?php $no4++; ?>
					<?php endforeach ?>
				</tbody>
			</table>
		<?php endif ?>

		<?php if ($table == 't_iuts'): ?>
			<table>
			<thead>
				<tr>
					<th>No.</th>
					<th>Nama Perusahaan Pemberi</th>
					<th>Nama Penanggung Jawab</th>
					<th>Alamat Perusahaan</th>
					<th>NPWP</th>
					<th>No. Telp & Fax</th>
					<th>Kegiatan Usaha Pokok</th>
					<th>KBLI</th>
					<th>STatus Kantor</th>
					<th>SIUP</th>
					<th>TDP</th>
					<th>No. IUTS dan Tanggal IUTS</th>
					<th>Masa Berlaku</th>
				</tr>
			</thead>
			<tbody>
				<?php $no5 = 1; ?>
				<?php foreach ($perizinan->result() as $iuts): ?>
					<tr>
						<td><?php echo $no5; ?></td>
						<td><?php echo $iuts->nm_perusahaan ?></td> 
						<td><?php echo $iuts->nm_penanggungjwb ?></td>
						<td><?php echo $iuts->almt_perusahaan ?></td>
						<td><?php echo $iuts->npwp ?></td>
						<td><?php echo $iuts->no_telp." & ".$iuts->no_fax ?></td>
						<td><?php echo $iuts->keg_usaha_pokok ?></td>
						<td><?php echo $iuts->kbli ?></td>
						<td><?php echo $iuts->status_kantor ?></td>
						<td><?php echo $iuts->siup ?></td>
						<td><?php echo $iuts->tdp ?></td>
						<td><?php echo $iuts->no_iuts." & ".$iuts->tgl_iuts ?></td>
						<td><?php echo $iuts->masaberlaku ?></td>
					</tr>
					<?php $no5++; ?>
				<?php endforeach ?>
			</tbody>
			</table>
		<?php endif ?>

		<?php if ($table == 't_stpw'): ?>
			<table>
				<thead>
					<tr>
						<th>No.</th>
						<th>No. Pendaftaran</th>
						<th>Nama Perusahaan Penerima</th>
						<th>Nama Penanggung Jawab</th>
						<th>Alamat Perusahaan</th>
						<th>NPWP</th>
						<th>IUTS</th>
						<th>No. STPW & Tanggal STPW</th>
						<th>No. Telp</th>
						<th>Masa Berlaku</th>
					</tr>
				</thead>
				<tbody>
					<?php $no6=1; ?>
					<?php foreach ($perizinan->result() as $stpw): ?>
						<tr>
							<td><?php echo $no6; ?></td>
							<td><?php echo $stpw->no_pendaftaran ?></td>
							<td><?php echo $stpw->nm_perusahaan ?></td>
							<td><?php echo $stpw->nm_penanggungjwb ?></td>
							<td><?php echo $stpw->almt_perusahaan ?></td>
							<td><?php echo $stpw->npwp ?></td>
							<td><?php echo $stpw->iuts ?></td>
							<td><?php echo $stpw->no_stpw." & ".$stpw->tgl_stpw ?></td>
							<td><?php echo $stpw->no_telp ?></td>
							<td><?php echo $stpw->masaberlaku ?></td>
						</tr>
						<?php $no6++; ?>
					<?php endforeach ?>
				</tbody>
			</table>
		<?php endif ?>

		<?php if ($table == 't_tdg'): ?>
			<table>
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama Perusahaan</th>
						<th>Nama Penanggung Jawab</th>
						<th>Alamat Perusahaan</th>
						<th>Alamat Penanggung Jawab</th>
						<th>Lokasi Gudang</th>
						<th>Luas Gudang</th>
						<th>Jenis Isi Gudang</th>
						<th>Klasifikasi Gudang</th>
						<th>No. TDG</th>
						<th>TDP</th>
						<th>Status</th>
						<th>Masa Berlaku</th>
					</tr>
				</thead>
				<tbody>
					<?php $no7=1; ?>
					<?php foreach ($perizinan->result() as $tdg): ?>
						<tr>
							<td><?php echo $no7; ?></td>
							<td><?php echo $tdg->nm_perusahaan ?></td>
							<td><?php echo $tdg->nm_penanggungjwb ?></td>
							<td><?php echo $tdg->almt_perusahaan ?></td>
							<td><?php echo $tdg->almt_penanggungjwb ?></td>
							<td><?php echo $tdg->lokasi_gudang ?></td>
							<td><?php echo $tdg->luas_gudang ?></td>
							<td><?php echo $tdg->jenis_isi_gudang ?></td>
							<td><?php echo $tdg->klasifikasi_gudang ?></td>
							<td><?php echo $tdg->no_tdg ?></td>
							<td><?php echo $tdg->tdp ?></td>
							<td><?php echo $tdg->status ?></td>
							<td><?php echo $tdg->masaberlaku ?></td>
						</tr>
						<?php $no7++; ?>
					<?php endforeach ?>
				</tbody>
			</table>
		<?php endif ?>

		<?php if ($table == 't_siupmb'): ?>
			<table>
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama Perusahaan</th>
						<th>Alamat Perusahaan</th>
						<th>SIUP-MB</th>
						<th>TDP</th>
						<th>Tanggal Keluar</th>
						<th>Tanggal Berakhir</th>
						<th>Insansi Yang Mengeluarkan</th>
						<th>Keterangan</th>
					</tr>
				</thead>
				<tbody>
					<?php $no8 = 1; ?>
					<?php foreach ($perizinan->result() as $siupmb): ?>
						<tr>
							<td><?php echo $no8; ?></td>
							<td><?php echo $siupmb->nm_perusahaan ?></td>
							<td><?php echo $siupmb->almt_perusahaan ?></td>
							<td><?php echo $siupmb->siupmb ?></td>
							<td><?php echo $siupmb->tdp ?></td>
							<td><?php echo $siupmb->tgl_keluar ?></td>
							<td><?php echo $siupmb->tgl_berakhir ?></td>
							<td><?php echo $siupmb->ins_yg_mengeluarkan ?></td>
							<td><?php echo $siupmb->keterangan ?></td>
						</tr>
						<?php $no8++; ?>
					<?php endforeach ?>
				</tbody>
			</table>
		<?php endif ?>

		<?php if ($table == 't_tdpud'): ?>
			<table>
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama Perusahaan</th>
						<th>Nama Penanggung Jawab</th>
						<th>Alamat Perusahaan</th>
						<th>NPWP</th>
						<th>No. Telp & Fax</th>
						<th>Kegiatan Usaha Pokok</th>
						<th>KBLI</th>
						<th>Status Kantor</th>
						<th>TDP</th>
						<th>No. TDPUD & Tanggal TDPUD</th>
						<th>Masa Berlaku</th>
					</tr>
				</thead>
				<tbody>
					<?php $no9=1; ?>
					<?php foreach ($perizinan->result() as $tdpud): ?>
						<tr>
							<td><?php echo $no9 ?></td>
							<td><?php echo $tdpud->nm_perusahaan ?></td>
							<td><?php echo $tdpud->nm_penanggungjwb ?></td>
							<td><?php echo $tdpud->almt_perusahaan ?></td>
							<td><?php echo $tdpud->npwp ?></td>
							<td><?php echo $tdpud->no_telp." & ".$tdpud->no_fax ?></td>
							<td><?php echo $tdpud->keg_usaha_pokok ?></td>
							<td><?php echo $tdpud->kbli ?></td>
							<td><?php echo $tdpud->status_kantor ?></td>
							<td><?php echo $tdpud->tdp ?></td>
							<td><?php echo $tdpud->no_tdpud." & ".$tdpud->tgl_tdpud ?></td>
							<td><?php echo $tdpud->masaberlaku ?></td>
						</tr>
						<?php $no9++; ?>
					<?php endforeach ?>
				</tbody>
			</table>
		<?php endif ?>

		<?php if ($table == 't_b3'): ?>
			<table >
				<tr>
					<th>No.</th>
					<th>Nama Perusahaan</th>
					<th>B3</th>
					<th>Nama Penanggung Jawab</th>
					<th>Alamat Perusahaan</th>
					<th>No. SK</th>
					<th>Tanggal SK</th>
					<th>NPWP</th>
					<th>KBLI</th>
					<th>Gol. Usaha</th>
					<th>Kelembagaan</th>
					<th>Barang Jasa Dagangan Utama</th>
					<th>Status Kantor</th>
					<th>Masa Berlalaku</th>
					<th>Stok</th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
			</table>
		<?php endif ?>