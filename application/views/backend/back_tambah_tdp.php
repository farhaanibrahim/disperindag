<!DOCTYPE html>
<html lang="en">
<head>        
    <!-- META SECTION -->
    <title><?php echo $title; ?></title>            
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="<?php echo base_url('upload/132.png') ?>" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->        
    <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('aset/backend') ?>/css/theme-default.css"/>
    <!-- EOF CSS INCLUDE -->
</head>
<body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">

       <?php include_once 'layout/sidebar.php'; ?>

       <!-- PAGE CONTENT -->
       <div class="page-content">

        <!-- START X-NAVIGATION VERTICAL -->
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
            <!-- TOGGLE NAVIGATION -->
            <li class="xn-icon-button">
                <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
            </li>
            <li class="xn-icon-button pull-right">
                <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
            </li> 
            <!-- END SIGN OUT -->
            <!-- END TOGGLE NAVIGATION -->                    
        </ul>
        <!-- END X-NAVIGATION VERTICAL -->                     

        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">                
            <li class="active">Dashboard</li>
        </ul>
        <!-- END BREADCRUMB -->                

        <div class="page-title">                    
            <h2><span class="fa fa-file-text"></span> Tambah Perizinan</h2>
        </div>

        <?php if (isset($error)): ?>
            <div class="alert alert-danger">
                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                </button>
                <p class="text-small"> <?php echo $error; ?></p>
            </div>          
        <?php endif ?>                   

        <!-- PAGE CONTENT WRAPPER -->
        <div class="page-content-wrap">

            <div class="row">
                <div class="col-md-12">
                   <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Tambah Perizinan</h3>
                    </div>
                    <div class="panel-body">
                       <div class="row">

                          <br><br>
                          <ul class="nav nav-tabs">
                             <li class="active"><a href="#siup" data-toggle="tab" aria-expanded="true">SIUP</a></li>
                             <li class=""><a href="#tdp" data-toggle="tab" aria-expanded="false">TDP</a></li>
                             <li class=""><a href="#iupr" data-toggle="tab" aria-expanded="false">IUPR</a></li>
                             <li class=""><a href="#iupp" data-toggle="tab" aria-expanded="false">IUPP</a></li>
                             <li class=""><a href="#iuts" data-toggle="tab" aria-expanded="false">IUTS</a></li>
                             <li class=""><a href="#stpw" data-toggle="tab" aria-expanded="false">STPW</a></li>
                             <li class=""><a href="#tdg" data-toggle="tab" aria-expanded="false">TDG</a></li>
                             <li class=""><a href="#siupmb" data-toggle="tab" aria-expanded="false">SIUP-MB</a></li>
                             <li class=""><a href="#tdpud" data-toggle="tab" aria-expanded="false">TDPUD</a></li>
                             <li class=""><a href="#b3" data-toggle="tab" aria-expanded="false">B3</a></li>
                         </ul>
                       <div id="myTabContent" class="tab-content">
                         <br><br>
                         <div class="tab-pane fade active in" id="siup">
                            <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('perizinan/tambah_perizinan'); ?>">
                              <input type="hidden" name="table" value="t_siup">
                              <div class="col-md-6">

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Nama Perusahaan</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" name="nm_perusahaan" placeholder="Nama Perusahaan" class="form-control" required/>
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Nama Penanggung jawab</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" name="nm_penanggungjwb" placeholder="Penanggung jawab" class="form-control" required/>
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Alamat Perusahaan</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <textarea class="form-control" name="almt_perusahaan"></textarea>
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Kecamatan</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <select class="form-control" name="kecamatan" id="kecamatan" required>
                                                <option>-- Kecamatan --</option>
                                                <?php foreach ($kecamatan->result() as $row): ?>
                                                    <option value="<?php echo $row->id_kecamatan; ?>"><?php echo $row->nm_kecamatan; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Kelurahan</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <select class="form-control" name="kelurahan" id="kelurahan" required disabled>

                                            </select>
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">No. Telp</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="phone" name="no_telp" id="no_telp" class="form-control" placeholder="No. Telp">
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">No. Fax</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" name="no_fax" id="no_fax" class="form-control" placeholder="No. Fax">
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Email</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                                        </div>                                            
                                    </div>
                                </div>
                                
                            </div>

                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Modal Usaha</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" name="modal_usaha" id="modal_usaha" class="form-control" placeholder="Modal Usaha">
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Kelembagaan</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <select class="form-control" name="kelembagaan">
                                                <option>--  Kelembagaan --</option>
                                                <option value="Distributor">Distributor</option>
                                                <option value="Sub Distributor">Sub Distributor</option>
                                                <option value="Agen">Agen</option>
                                                <option value="Sub Agen">Sub Agen</option>
                                            </select>
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">KBLI</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" name="kbli" placeholder="KBLI" class="form-control" required/>
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Barang Dagangan</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <textarea class="form-control" name="barang_dagangan" required></textarea>
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">No. SIUP </label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" name="no_sk" placeholder="No. SK" class="form-control" required/>
                                        </div>                                            
                                    </div>
                                </div> 

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tanggal SIUP </label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="date" name="tgl_sk" placeholder="Tanggal SK" class="form-control" required/>
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Masaberlaku</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input type="date" name="masaberlaku" placeholder="Masa Berlaku" class="form-control" required/>
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Foto</label>
                                    <div class="col-md-9">                                            
                                        <input type="file" name="userfile" />          
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3 control-label"></div>
                                    <div class="col-md-9">
                                       <button class="btn btn-primary" name="btnSiup">Submit</button>
                                       <button type="reset" class="btn btn-default">Clear Form</button>  
                                   </div>
                               </div>

                           </div>
                       </form>
                   </div>

                   <div class="tab-pane fade" id="tdp">
                    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('perizinan/tambah_perizinan'); ?>">
                      <input type="hidden" name="table" value="t_tdp">
                      <div class="col-md-6">

                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama Perusahaan</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" name="nm_perusahaan" placeholder="Nama Perusahaan" class="form-control" required/>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama Penanggung jawab</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" name="nm_penanggungjwb" placeholder="Penanggung jawab" class="form-control" required/>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Alamat Perusahaan</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <textarea class="form-control" name="almt_perusahaan"></textarea>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Kecamatan</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <select class="form-control" name="kecamatan" id="kecamatan2" required>
                                        <option>-- Kecamatan --</option>
                                        <?php foreach ($kecamatan->result() as $row2): ?>
                                            <option value="<?php echo $row2->id_kecamatan; ?>"><?php echo $row2->nm_kecamatan; ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Kelurahan</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <select class="form-control" name="kelurahan" id="kelurahan2" required disabled>

                                    </select>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">NPWP</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" name="npwp" placeholder="NPWP" class="form-control" required/>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">No. Telp</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="phone" name="no_telp" id="no_telp" class="form-control" placeholder="No. Telp">
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">No. Fax</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" name="no_fax" id="no_fax" class="form-control" placeholder="No. Fax">
                                </div>                                            
                            </div>
                        </div>


                    </div>


                    <div class="col-md-6">

                        <div class="form-group">
                            <label class="col-md-3 control-label">Email</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                                </div>                                            
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 control-label">Kegiatan Usaha Pokok</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <textarea class="form-control" name="keg_usaha_pokok" required></textarea>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">KBLI</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" name="kbli" placeholder="KBLI" class="form-control" required/>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Status Kantor</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <select class="form-control" name="status_kantor">
                                        <option>--  Status Kantor --</option>
                                        <option value="Kantor Tunggal">Kantor Tunggal</option>
                                        <option value="Kantor Pusat">Kantor Pusat</option>
                                    </select>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">No. TDP </label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" name="no_sk" placeholder="No. SK" class="form-control" required/>
                                </div>                                            
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="col-md-3 control-label">Tanggal TDP </label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="date" name="tgl_sk" placeholder="Tanggal SK" class="form-control" required/>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Masaberlaku</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                    <input type="date" name="masaberlaku" placeholder="Masa Berlaku" class="form-control" required/>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Foto</label>
                            <div class="col-md-9">                                            
                                <input type="file" name="userfile" />          
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3 control-label"></div>
                            <div class="col-md-9">
                               <button class="btn btn-primary" name="btnTdp">Submit</button>
                               <button type="reset" class="btn btn-default">Clear Form</button>  
                           </div>
                       </div>
                   </div>
               </form>
           </div>

           <div class="tab-pane fade" id="iupr">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('perizinan/tambah_perizinan'); ?>">
              <input type="hidden" name="table" value="t_iupr">
              <div class="col-md-6">

                <div class="form-group">
                 <label class="col-md-3 control-label">Tanggal Pendaftaran</label>
                 <div class="col-md-9">                                            
                  <div class="input-group">
                   <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                   <input type="date" name="tgl_pendaftaran" placeholder="Tanggal Pendaftaran" class="form-control" required/>
               </div>                                            
           </div>
       </div>

       <div class="form-group">
        <label class="col-md-3 control-label">Nama Perusahaan</label>
        <div class="col-md-9">                                            
            <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                <input type="text" name="nm_perusahaan" placeholder="Nama Perusahaan" class="form-control" required/>
            </div>                                            
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Nama Penanggung jawab</label>
        <div class="col-md-9">                                            
            <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                <input type="text" name="nm_penanggungjwb" placeholder="Penanggung jawab" class="form-control" required/>
            </div>                                            
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Alamat Perusahaan</label>
        <div class="col-md-9">                                            
            <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                <textarea class="form-control" name="almt_perusahaan"></textarea>
            </div>                                            
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Kecamatan</label>
        <div class="col-md-9">                                            
            <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                <select class="form-control" name="kecamatan" id="kecamatan3" required>
                    <option>-- Kecamatan --</option>
                    <?php foreach ($kecamatan->result() as $row3): ?>
                        <option value="<?php echo $row3->id_kecamatan; ?>"><?php echo $row3->nm_kecamatan; ?></option>
                    <?php endforeach ?>
                </select>
            </div>                                            
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Kelurahan</label>
        <div class="col-md-9">                                            
            <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                <select class="form-control" name="kelurahan" id="kelurahan3" required disabled>

                </select>
            </div>                                            
        </div>
    </div>

</div>
<div class="col-md-6">

    <div class="form-group">
     <label class="col-md-3 control-label">Jenis Usaha</label>
     <div class="col-md-9">                                            
      <div class="input-group">
       <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
       <input type="text" name="jenis_usaha" placeholder="KBLI" class="form-control" required/>
   </div>                                            
</div>
</div>

<div class="form-group">
   <label class="col-md-3 control-label">SIUP</label>
   <div class="col-md-9">                                            
      <div class="input-group">
         <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
         <input type="text" name="siup" placeholder="SIUP" class="form-control" required/>
     </div>                                            
 </div>
</div>

<div class="form-group">
   <label class="col-md-3 control-label">TDP</label>
   <div class="col-md-9">                                            
      <div class="input-group">
         <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
         <input type="text" name="tdp" placeholder="TDP" class="form-control" required/>
     </div>                                            
 </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">No. IUPR </label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="text" name="no_sk" placeholder="No. SK" class="form-control" required/>
        </div>                                            
    </div>
</div> 

<div class="form-group">
    <label class="col-md-3 control-label">Tanggal IUPR </label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="date" name="tgl_sk" placeholder="Tanggal SK" class="form-control" required/>
        </div>                                            
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">No. Rekomendasi </label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="text" name="no_rekomendasi" placeholder="No. Rekomendasi" class="form-control" required/>
        </div>                                            
    </div>
</div>

<div class="form-group">
 <label class="col-md-3 control-label">Foto</label>
 <div class="col-md-9">                                            
  <input type="file" name="userfile" />          
</div>
</div>

<div class="form-group">
 <div class="col-md-3 control-label"></div>
 <div class="col-md-9">
  <button class="btn btn-primary" name="btnIupr">Submit</button>
  <button type="reset" class="btn btn-default">Clear Form</button>  
</div>
</div>

</div>
</form>
</div>

<div class="tab-pane fade" id="iupp">
    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('perizinan/tambah_perizinan'); ?>">
      <input type="hidden" name="table" value="t_iupp">
      <div class="col-md-6">

        <div class="form-group">
         <label class="col-md-3 control-label">Tanggal Pendaftaran</label>
         <div class="col-md-9">                                            
          <div class="input-group">
           <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
           <input type="date" name="tgl_pendaftaran" placeholder="Tanggal Pendaftaran" class="form-control" required/>
       </div>                                            
   </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">Nama Perusahaan</label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="text" name="nm_perusahaan" placeholder="Nama Perusahaan" class="form-control" required/>
        </div>                                            
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">Nama Penanggung jawab</label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="text" name="nm_penanggungjwb" placeholder="Penanggung jawab" class="form-control" required/>
        </div>                                            
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">Alamat Perusahaan</label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <textarea class="form-control" name="almt_perusahaan"></textarea>
        </div>                                            
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">Kecamatan</label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <select class="form-control" name="kecamatan" id="kecamatan4" required>
                <option>-- Kecamatan --</option>
                <?php foreach ($kecamatan->result() as $row4): ?>
                    <option value="<?php echo $row4->id_kecamatan; ?>"><?php echo $row4->nm_kecamatan; ?></option>
                <?php endforeach ?>
            </select>
        </div>                                            
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">Kelurahan</label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <select class="form-control" name="kelurahan" id="kelurahan4" required disabled>

            </select>
        </div>                                            
    </div>
</div>

<div class="form-group">
       <label class="col-md-3 control-label">Jenis Usaha</label>
       <div class="col-md-9">                                            
          <div class="input-group">
             <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
             <input type="text" name="jenis_usaha" placeholder="KBLI" class="form-control" required/>
         </div>                                            
     </div>
 </div>

</div>
<div class="col-md-6">

    <div class="form-group">
        <label class="col-md-3 control-label">SIUP</label>
        <div class="col-md-9">                                            
          <div class="input-group">
             <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
             <input type="text" name="siup" placeholder="SIUP" class="form-control" required/>
         </div>                                            
     </div>
 </div>

<div class="form-group">
   <label class="col-md-3 control-label">TDP</label>
   <div class="col-md-9">                                            
      <div class="input-group">
         <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
         <input type="text" name="tdp" placeholder="TDP" class="form-control" required/>
     </div>                                            
 </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">No. IUPP </label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="text" name="no_sk" placeholder="No. SK" class="form-control" required/>
        </div>                                            
    </div>
</div> 

<div class="form-group">
    <label class="col-md-3 control-label">Tanggal IUPP </label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="date" name="tgl_sk" placeholder="Tanggal SK" class="form-control" required/>
        </div>                                            
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">No. Rekomendasi </label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="text" name="no_rekomendasi" placeholder="No. Rekomandasi" class="form-control" required/>
        </div>                                            
    </div>
</div> 

<div class="form-group">
    <label class="col-md-3 control-label">No. yang direncanakan </label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="text" name="no_rencana" placeholder="Nomor yang direncanakan" class="form-control" required/>
        </div>                                            
    </div>
</div>

<div class="form-group">
 <label class="col-md-3 control-label">Foto</label>
 <div class="col-md-9">                                            
  <input type="file" name="userfile" />          
</div>
</div>

<div class="form-group">
 <div class="col-md-3 control-label"></div>
 <div class="col-md-9">
  <button class="btn btn-primary" name="btnIupp">Submit</button>
  <button type="reset" class="btn btn-default">Clear Form</button>  
</div>
</div>

</div>
</form>
</div>

<div class="tab-pane fade" id="iuts">
    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('perizinan/tambah_perizinan'); ?>">
      <input type="hidden" name="table" value="t_iuts">
      <div class="col-md-6">

        <div class="form-group">
            <label class="col-md-3 control-label">Nama Perusahaan Pemberi</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="nm_perusahaan" placeholder="Nama Perusahaan" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Nama Penanggung jawab</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="nm_penanggungjwb" placeholder="Penanggung jawab" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Alamat Perusahaan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="almt_perusahaan"></textarea>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kecamatan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kecamatan" id="kecamatan5" required>
                        <option>-- Kecamatan --</option>
                        <?php foreach ($kecamatan->result() as $row5): ?>
                            <option value="<?php echo $row5->id_kecamatan; ?>"><?php echo $row5->nm_kecamatan; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kelurahan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kelurahan" id="kelurahan5" required disabled>

                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">NPWP</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="npwp" placeholder="NPWP" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">No. Telp</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="phone" name="no_telp" id="no_telp" class="form-control" placeholder="No. Telp">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">No. Fax</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="no_fax" id="no_fax" class="form-control" placeholder="No. Fax">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Email</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                </div>                                            
            </div>
        </div>


        <div class="form-group">
            <label class="col-md-3 control-label">Kegiatan Usaha Pokok</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="keg_usaha_pokok" required></textarea>
                </div>                                            
            </div>
        </div>

    </div>
    <div class="col-md-6">

        <div class="form-group">
            <label class="col-md-3 control-label">KBLI</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="kbli" placeholder="KBLI" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Status Kantor</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="status_kantor">
                        <option>--  Status Kantor --</option>
                        <option value="Kantor Tunggal">Kantor Tunggal</option>
                        <option value="Kantor Pusat">Kantor Pusat</option>
                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">SIUP</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="siup" id="siup" class="form-control" placeholder="SIUP">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
         <label class="col-md-3 control-label">TDP</label>
         <div class="col-md-9">                                            
          <div class="input-group">
           <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
           <input type="text" name="tdp" placeholder="TDP" class="form-control" required/>
       </div>                                            
   </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">No. IUTS </label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="text" name="no_sk" placeholder="No. SK" class="form-control" required/>
        </div>                                            
    </div>
</div> 

<div class="form-group">
    <label class="col-md-3 control-label">Tanggal IUTS </label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="date" name="tgl_sk" placeholder="Tanggal SK" class="form-control" required/>
        </div>                                            
    </div>
</div>


<div class="form-group">
    <label class="col-md-3 control-label">No. Rekomendasi </label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="text" name="no_rekomendasi" placeholder="No. Rekomandasi" class="form-control" required/>
        </div>                                            
    </div>
</div> 

<div class="form-group">
    <label class="col-md-3 control-label">No. yang direncanakan </label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="text" name="no_rencana" placeholder="Nomor yang direncanakan" class="form-control" required/>
        </div>                                            
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">Masaberlaku</label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
            <input type="date" name="masaberlaku" placeholder="Masa Berlaku" class="form-control" required/>
        </div>                                            
    </div>
</div>


<div class="form-group">
    <label class="col-md-3 control-label">Foto</label>
    <div class="col-md-9">                                            
        <input type="file" name="userfile" />          
    </div>
</div>

<div class="form-group">
    <div class="col-md-3 control-label"></div>
    <div class="col-md-9">
       <button class="btn btn-primary" name="btnIuts">Submit</button>
       <button type="reset" class="btn btn-default">Clear Form</button>  
   </div>
</div>

</div>
</form>
</div>

<div class="tab-pane fade" id="stpw">
    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('perizinan/tambah_perizinan'); ?>">
      <input type="hidden" name="table" value="t_stpw">
      <div class="col-md-6">
        <div class="form-group">
            <label class="col-md-3 control-label">No. Pendaftaran</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="no_pendaftaran" placeholder="No. Pendaftaran" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Nama Perusahaan Penerima</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="nm_perusahaan" placeholder="Nama Perusahaan" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Nama Penanggung jawab</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="nm_penanggungjwb" placeholder="Penanggung jawab" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Alamat Perusahaan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="almt_perusahaan"></textarea>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kecamatan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kecamatan" id="kecamatan6" required>
                        <option>-- Kecamatan --</option>
                        <?php foreach ($kecamatan->result() as $row6): ?>
                            <option value="<?php echo $row6->id_kecamatan; ?>"><?php echo $row6->nm_kecamatan; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kelurahan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kelurahan" id="kelurahan6" required disabled>

                    </select>
                </div>                                            
            </div>
        </div>

    </div>
    <div class="col-md-6">

        <div class="form-group">
            <label class="col-md-3 control-label">NPWP</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="npwp" placeholder="NPWP" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">IUTS</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="iuts" id="iuts" class="form-control" placeholder="IUTS">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">No. STPW </label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="no_sk" placeholder="No. SK" class="form-control" required/>
                </div>                                            
            </div>
        </div> 

        <div class="form-group">
            <label class="col-md-3 control-label">Tanggal STPW </label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="date" name="tgl_sk" placeholder="Tanggal SK" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">No. Rekomendasi </label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="no_rekomendasi" placeholder="No. Rekomendasi" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">No. Telp</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="phone" name="no_telp" id="no_telp" class="form-control" placeholder="No. Telp">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Masaberlaku</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                    <input type="date" name="masaberlaku" placeholder="Masa Berlaku" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Foto</label>
            <div class="col-md-9">                                            
                <input type="file" name="userfile" />          
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-3 control-label"></div>
            <div class="col-md-9">
               <button class="btn btn-primary" name="btnStpw">Submit</button>
               <button type="reset" class="btn btn-default">Clear Form</button>  
           </div>
       </div>

   </div>
</form>
</div>


<div class="tab-pane fade" id="tdg">
    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('perizinan/tambah_perizinan'); ?>">
      <input type="hidden" name="table" value="t_tdg">
      <div class="col-md-6">

        <div class="form-group">
            <label class="col-md-3 control-label">Nama Perusahaan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="nm_perusahaan" placeholder="Nama Perusahaan" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Nama Penanggung jawab</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="nm_penanggungjwb" placeholder="Penanggung jawab" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kecamatan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kecamatan" id="kecamatan7" required>
                        <option>-- Kecamatan --</option>
                        <?php foreach ($kecamatan->result() as $row7): ?>
                            <option value="<?php echo $row7->id_kecamatan; ?>"><?php echo $row7->nm_kecamatan; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kelurahan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kelurahan" id="kelurahan7" required disabled>

                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Alamat Penanggung Jawab</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="almt_penanggungjwb"></textarea>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Alamat Gudang</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="almt_gudang"></textarea>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">No. Telp</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="no_telp" class="form-control" placeholder="No. Telp">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">No. Fax</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="no_fax" class="form-control" placeholder="No. Fax">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Email</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="email" class="form-control" placeholder="Email">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Jenis Isi Gudang</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="jenis_isi_gudang" id="jenis_isi_gudang" class="form-control" placeholder="Jenis Isi Gudang">
                </div>                                            
            </div>
        </div>

    </div>
    <div class="col-md-6">

        <div class="form-group">
            <label class="col-md-3 control-label">Klasifikasi Gudang</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="klasifikasi_gudang" id="klasifikasi_gudang" class="form-control" placeholder="Klasifikasi Gudang">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Luas Gudang</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="luas_gudang" id="ls_gudang" class="form-control" placeholder="Jenis Isi Gudang">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kapasitas Gudang</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="kapasitas_gudang" id="jenis_isi_gudang" class="form-control" placeholder="Kapasitas Gudang">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kelengkapan Gudang</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <input type="radio" name="kelengkapan_gudang" value="Berpendingin">Berpendingin <br>
                    <input type="radio" name="kelengkapan_gudang" value="Tidak Berpendingin">Tidak Berpendingin
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">No. TDG </label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="no_tdg" placeholder="No. TDG" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Tanggal TDG </label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="date" name="tgl_tdg" placeholder="No. TDG" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">TDP </label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="tdp" placeholder="TDP" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Masaberlaku</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                    <input type="date" name="masaberlaku" placeholder="Masa Berlaku" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Titik Kordinat</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="longitude" id="klasifikasi_gudang" class="form-control" placeholder="Longitude">
                    <input type="text" name="latitude" id="klasifikasi_gudang" class="form-control" placeholder="Latitude">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Foto</label>
            <div class="col-md-9">                                            
                <input type="file" name="userfile" />          
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-3 control-label"></div>
            <div class="col-md-9">
             <button class="btn btn-primary" name="btnTdg">Submit</button>
             <button type="reset" class="btn btn-default">Clear Form</button>  
         </div>
     </div>

 </div>
</form>
</div>

<div class="tab-pane fade" id="siupmb">
    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('perizinan/tambah_perizinan'); ?>">
       <input type="hidden" name="table" value="t_siupmb">
       <div class="col-md-6">

        <div class="form-group">
            <label class="col-md-3 control-label">Nama Perusahaan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="nm_perusahaan" placeholder="Nama Perusahaan" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Alamat Perusahaan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="almt_perusahaan"></textarea>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kecamatan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kecamatan" id="kecamatan8" required>
                        <option>-- Kecamatan --</option>
                        <?php foreach ($kecamatan->result() as $row8): ?>
                            <option value="<?php echo $row8->id_kecamatan; ?>"><?php echo $row8->nm_kecamatan; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kelurahan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kelurahan" id="kelurahan8" required disabled>

                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">SIUP-MB</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="siupmb" id="siupmb" class="form-control" placeholder="SIUP-MB">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">TDP</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="tdp" id="tdp" class="form-control" placeholder="SIUP-MB">
                </div>                                            
            </div>
        </div>

    </div>
    <div class="col-md-6">

        <div class="form-group">
            <label class="col-md-3 control-label">Tanggal Keluar </label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="date" name="tgl_keluar" placeholder="Tanggal Keluar" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Tanggal Berakhir </label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="date" name="tgl_berakhir" placeholder="Tanggal Berakhir" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Instansi yang Mengeluarkan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="ins_yg_mengeluarkan" placeholder="" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        

        <div class="form-group">
            <label class="col-md-3 control-label">Keterangan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="keterangan" required></textarea>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Foto</label>
            <div class="col-md-9">                                            
                <input type="file" name="userfile" />          
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-3 control-label"></div>
            <div class="col-md-9">
               <button class="btn btn-primary" name="btnSiupmb">Submit</button>
               <button type="reset" class="btn btn-default">Clear Form</button>  
           </div>
       </div>

   </div>
</form>
</div>

<div class="tab-pane fade" id="tdpud">
    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('perizinan/tambah_perizinan'); ?>">
      <input type="hidden" name="table" value="t_tdpud">
      <div class="col-md-6">

        <div class="form-group">
            <label class="col-md-3 control-label">Nama Perusahaan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="nm_perusahaan" placeholder="Nama Perusahaan" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Nama Penanggung jawab</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="nm_penanggungjwb" placeholder="Penanggung jawab" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Alamat Perusahaan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="almt_perusahaan"></textarea>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kecamatan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kecamatan" id="kecamatan9" required>
                        <option>-- Kecamatan --</option>
                        <?php foreach ($kecamatan->result() as $row9): ?>
                            <option value="<?php echo $row9->id_kecamatan; ?>"><?php echo $row9->nm_kecamatan; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kelurahan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kelurahan" id="kelurahan9" required disabled>

                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">NPWP</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="npwp" placeholder="NPWP" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">No. Telp</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="phone" name="no_telp" id="no_telp" class="form-control" placeholder="No. Telp">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">No. Fax</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="no_fax" id="no_fax" class="form-control" placeholder="No. Fax">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Email</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                </div>                                            
            </div>
        </div>

    </div>

    <div class="col-md-6">

        <div class="form-group">
            <label class="col-md-3 control-label">Kegiatan Usaha Pokok</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="keg_usaha_pokok" required></textarea>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">KBLI</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="kbli" placeholder="KBLI" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Status Kantor</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="status_kantor">
                        <option>--  Status Kantor --</option>
                        <option value="Kantor Tunggal">Kantor Tunggal</option>
                        <option value="Kantor Pusat">Kantor Pusat</option>
                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">TDP</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="tdp" placeholder="TDP" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">No. TDPUD </label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="no_sk" placeholder="No. SK" class="form-control" required/>
                </div>                                            
            </div>
        </div> 

        <div class="form-group">
            <label class="col-md-3 control-label">Tanggal TDPUD </label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="date" name="tgl_sk" placeholder="Tanggal SK" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Jenis TDPUD</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="jenis_tdpud">
                        <option>--  Jenis --</option>
                        <option value="Distributor">Distributor</option>
                        <option value="Sub Distributor">Sub Distributor</option>
                        <option value="Agen">Agen</option>
                        <option value="Sub Agen">Sub Agen</option>
                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Masaberlaku</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                    <input type="date" name="masaberlaku" placeholder="Masa Berlaku" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Foto</label>
            <div class="col-md-9">                                            
                <input type="file" name="userfile" />          
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-3 control-label"></div>
            <div class="col-md-9">
             <button class="btn btn-primary" name="btnTdpud">Submit</button>
             <button type="reset" class="btn btn-default">Clear Form</button>  
         </div>
     </div>
 </div>
</form>
</div>

<div class="tab-pane fade" id="b3">
    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('perizinan/tambah_perizinan'); ?>">
      <input type="hidden" name="table" value="t_b3">
      <div class="col-md-6">

        <div class="form-group">
            <label class="col-md-3 control-label">No. CAS</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="no_cas" placeholder="No. Cas" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">POS TARIF/HS</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="pos_trf_hs" class="form-control" placeholder="Izin Penyimpanan B3">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kecamatan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kecamatan" id="kecamatan10" required>
                        <option>-- Kecamatan --</option>
                        <?php foreach ($kecamatan->result() as $row10): ?>
                            <option value="<?php echo $row10->id_kecamatan; ?>"><?php echo $row10->nm_kecamatan; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kelurahan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kelurahan" id="kelurahan10" required disabled>

                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Alamat Penyimpanan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="almt_penyimpanan"></textarea>
                </div>                                            
            </div>
        </div>

        


    </div>
    <div class="col-md-6">

        <div class="form-group">
            <label class="col-md-3 control-label">Uraian Barang</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="uraian_brg"></textarea>
                </div>                                            
            </div>
        </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Tata Niaga Impor</label>
        <div class="col-md-9">                                            
            <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                <input type="text" name="tata_niaga_impor" placeholder="Tata Niaga Impor" class="form-control" required/>
            </div>                                            
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Keperluan diluar pangan</label>
        <div class="col-md-9">                                            
            <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                <input type="text" name="kep_lain_tdk_utk_pgn" placeholder="Keperluan diluar pangan" class="form-control" required/>
            </div>                                            
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Laboratorium / Penelitian</label>
        <div class="col-md-9">                                            
            <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                <input type="text" name="lab" placeholder="Laboratorium / Penelitian" class="form-control" required/>
            </div>                                            
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Satuan</label>
        <div class="col-md-9">                                            
            <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                <input type="text" name="satuan" placeholder="Satuan" class="form-control" required/>
            </div>                                            
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-3 control-label"></div>
        <div class="col-md-9">
           <button class="btn btn-primary" name="btnB3">Submit</button>
           <button type="reset" class="btn btn-default">Clear Form</button>  
       </div>
   </div>

</div>
</form>

</div>

</div>



</div>
</div>
<div class="panel-footer">

</div>
</div>
</div>
</div>

</div>
<!-- END PAGE CONTENT WRAPPER -->                
</div>            
<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->

<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
            <div class="mb-content">
                <p>Are you sure you want to log out?</p>                    
                <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <a href="<?php echo site_url('backend/logout') ?>" class="btn btn-success btn-lg">Yes</a>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MESSAGE BOX-->

<!-- START PRELOADS -->
<audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
<audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
<!-- END PRELOADS -->                 

<!-- START SCRIPTS -->
<!-- START PLUGINS -->
<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap.min.js"></script>              
<!-- END PLUGINS -->

<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/icheck/icheck.min.js"></script>  
<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>  

<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap-datepicker.js"></script>  
<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap-file-input.js"></script>  
<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap-select.js"></script>  
<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/tagsinput/jquery.tagsinput.min.js"></script>  

<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/summernote/summernote.js"></script>  
<!-- END PAGE PLUGINS -->         

<!-- START TEMPLATE -->
<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins.js"></script>        
<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/actions.js"></script>        
<!-- END TEMPLATE -->
<!-- END SCRIPTS -->         
</body>
</html>
<script type="text/javascript">
    $("#kecamatan").change(function(){
        var id_kecamatan = $("#kecamatan option:selected").val();
        $.ajax({
            url: "<?php echo site_url('backend/kelurahan')?>",
            type: "POST",
            data    : "id_kecamatan="+id_kecamatan,
            success : function (msg) {
                document.getElementById("kelurahan").disabled = false;
                $("#kelurahan").html(msg);
                //$("#kelurahan").css("color","black");
            }
        });
    });

    $("#kecamatan2").change(function(){
        var id_kecamatan = $("#kecamatan2 option:selected").val();
        $.ajax({
            url: "<?php echo site_url('backend/kelurahan')?>",
            type: "POST",
            data    : "id_kecamatan="+id_kecamatan,
            success : function (msg) {
                document.getElementById("kelurahan2").disabled = false;
                $("#kelurahan2").html(msg);
                //$("#kelurahan").css("color","black");
            }
        });
    });

    $("#kecamatan3").change(function(){
        var id_kecamatan = $("#kecamatan3 option:selected").val();
        $.ajax({
            url: "<?php echo site_url('backend/kelurahan')?>",
            type: "POST",
            data    : "id_kecamatan="+id_kecamatan,
            success : function (msg) {
                document.getElementById("kelurahan3").disabled = false;
                $("#kelurahan3").html(msg);
                //$("#kelurahan").css("color","black");
            }
        });
    });

    $("#kecamatan4").change(function(){
        var id_kecamatan = $("#kecamatan4 option:selected").val();
        $.ajax({
            url: "<?php echo site_url('backend/kelurahan')?>",
            type: "POST",
            data    : "id_kecamatan="+id_kecamatan,
            success : function (msg) {
                document.getElementById("kelurahan4").disabled = false;
                $("#kelurahan4").html(msg);
                //$("#kelurahan").css("color","black");
            }
        });
    });

    $("#kecamatan5").change(function(){
        var id_kecamatan = $("#kecamatan5 option:selected").val();
        $.ajax({
            url: "<?php echo site_url('backend/kelurahan')?>",
            type: "POST",
            data    : "id_kecamatan="+id_kecamatan,
            success : function (msg) {
                document.getElementById("kelurahan5").disabled = false;
                $("#kelurahan5").html(msg);
                //$("#kelurahan").css("color","black");
            }
        });
    });

    $("#kecamatan6").change(function(){
        var id_kecamatan = $("#kecamatan6 option:selected").val();
        $.ajax({
            url: "<?php echo site_url('backend/kelurahan')?>",
            type: "POST",
            data    : "id_kecamatan="+id_kecamatan,
            success : function (msg) {
                document.getElementById("kelurahan6").disabled = false;
                $("#kelurahan6").html(msg);
                //$("#kelurahan").css("color","black");
            }
        });
    });

    $("#kecamatan7").change(function(){
        var id_kecamatan = $("#kecamatan7 option:selected").val();
        $.ajax({
            url: "<?php echo site_url('backend/kelurahan')?>",
            type: "POST",
            data    : "id_kecamatan="+id_kecamatan,
            success : function (msg) {
                document.getElementById("kelurahan7").disabled = false;
                $("#kelurahan7").html(msg);
                //$("#kelurahan").css("color","black");
            }
        });
    });

    $("#kecamatan8").change(function(){
        var id_kecamatan = $("#kecamatan8 option:selected").val();
        $.ajax({
            url: "<?php echo site_url('backend/kelurahan')?>",
            type: "POST",
            data    : "id_kecamatan="+id_kecamatan,
            success : function (msg) {
                document.getElementById("kelurahan8").disabled = false;
                $("#kelurahan8").html(msg);
                //$("#kelurahan").css("color","black");
            }
        });
    });

    $("#kecamatan9").change(function(){
        var id_kecamatan = $("#kecamatan9 option:selected").val();
        $.ajax({
            url: "<?php echo site_url('backend/kelurahan')?>",
            type: "POST",
            data    : "id_kecamatan="+id_kecamatan,
            success : function (msg) {
                document.getElementById("kelurahan9").disabled = false;
                $("#kelurahan9").html(msg);
                //$("#kelurahan").css("color","black");
            }
        });
    });

    $("#kecamatan10").change(function(){
        var id_kecamatan = $("#kecamatan10 option:selected").val();
        $.ajax({
            url: "<?php echo site_url('backend/kelurahan')?>",
            type: "POST",
            data    : "id_kecamatan="+id_kecamatan,
            success : function (msg) {
                document.getElementById("kelurahan10").disabled = false;
                $("#kelurahan10").html(msg);
                //$("#kelurahan").css("color","black");
            }
        });
    });

    function tambah_form() {
     var idf = document.getElementById("idf").value;
     var stre;
     stre="<p id='srow" + idf + "'><input type='text' size='40' name='stpw[]' class='form-control' placeholder='' /> <a href='#' style=\"color:#3399FD;\" onclick='hapusElemen(\"#srow" + idf + "\"); return false;'>Hapus</a></p>";
     $("#text_form").append(stre);
     idf = (idf-1) + 2;
     document.getElementById("idf").value = idf;
   }
   function hapusElemen(idf) {
     $(idf).remove();
   }
    
</script>

