<?php foreach ($get->result() as $title) {} ?>
<!DOCTYPE html>
<html>
<head>
	<title>Preview</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" href="<?php echo base_url('upload'); ?>/logo_bogor.png" />
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css'); ?>/bootstrap.min.css">
</head>
<body>
	<style type="text/css">
		.header {
			text-align: center;
		}
	</style>
	<!-- CSS goes in the document HEAD or added to your external stylesheet -->
	<style type="text/css">
		table.gridtable {
			font-family: verdana,arial,sans-serif;
			font-size:11px;
			color:#333333;
			border-width: 1px;
			border-color: #666666;
			border-collapse: collapse;
			width: 100%;
		}
		table.gridtable th {
			border-width: 1px;
			padding: 8px;
			border-style: solid;
			border-color: #666666;
			background-color: #dedede;
		}
		table.gridtable td {
			border-width: 1px;
			padding: 8px;
			border-style: solid;
			border-color: #666666;
			background-color: #ffffff;
		}
	</style>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="header">
					<p>
						(KOP PERUSAHAAN)
						<hr>
						LAPORAN DISTRIBUSI BARANG KEBUTUHAN POKOK <br>
						BULAN <?php //echo $bulan_min." - ".$bulan_max; ?> TAHUN <?php echo $year; ?>
					</p>
				</div>
				<br>

				<table class="gridtable">
					<thead>
						<tr>
							<th>No.</th>
							<th>Nama Perusahaan</th>
							<th>No. TDPUD</th>
							<th>Jenis</th>
							<th>Penyaluran</th>
						</tr>
					</thead>
					<tbody>
					<?php $no = 1; ?>
						<?php foreach ($get->result() as $dist): ?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?php echo $dist->nm_perusahaan; ?></td>
								<td><?php echo $dist->no_tdpud; ?></td>
								<td><?php echo $dist->jenis_tdpud; ?></td>
								<td>
									<?php 
									$penyaluran = $sub_query->dist_penyaluran($dist->no_tdpud); 
									
									?>
									<ul>
										<?php foreach ($penyaluran->result() as $penyaluran): ?>
											<li><?php echo $penyaluran->tujuan; ?></li>	
										<?php endforeach ?>
									</ul>
								</td>
								<?php $no++; ?>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>	