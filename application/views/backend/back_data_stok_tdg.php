<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title><?php echo $title; ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo base_url('upload/132.png') ?>" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('aset/backend') ?>/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
           <?php include_once 'layout/sidebar.php'; ?>
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
					<li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                    <!-- END TOGGLE NAVIGATION -->                    
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">                
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->                
                
                <div class="page-title">                    
                    <h2><span class="fa fa-file-text"></span> <?php echo $title2; ?></h2>
					<a href="<?php echo site_url("backend/tambah_data_stok_tdg"); ?>" class="btn btn-sm btn-info pull-right"><span class="fa fa-plus"></span>Tambah Stok Gudang</a>
                    <a href="<?php echo site_url("backend/tambah_data_pengiriman_tdg"); ?>" class="btn btn-sm btn-primary pull-right"><span class="fa fa-plus"></span>Tambah Data Pengiriman</a>
					<br>
					<?php if ($this->session->flashdata('insert')): ?>
                        <div class="alert alert-success" style="" id="info_sukses">
                            <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                            </button>
                            <p class="text-small"><span class="fa fa-check"></span> <?php echo $this->session->flashdata('insert'); ?> </p>
                        </div>
                    <?php endif ?>
                    <?php if ($this->session->flashdata('update')): ?>
                        <div class="alert alert-info" style="" id="info_sukses">
                            <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                            </button>
                            <p class="text-small"><span class="fa fa-check"></span> <?php echo $this->session->flashdata('update'); ?> </p>
                        </div>
                    <?php endif ?>

                    <?php if ($this->session->flashdata('delete')): ?>
                        <div class="alert alert-info" style="" id="info_sukses">
                            <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                            </button>
                            <p class="text-small"><span class="fa fa-check"></span> <?php echo $this->session->flashdata('delete'); ?> </p>
                        </div>
                    <?php endif ?>

                    <?php if ($this->session->flashdata('failed')): ?>
                        <div class="alert alert-danger" style="" id="info_sukses">
                            <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                            </button>
                            <p class="text-small"><span class="fa fa-check"></span> <?php echo $this->session->flashdata('failed'); ?> </p>
                        </div>
                    <?php endif ?>
                </div>
				
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Data</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-6">
                                        <strong><u>Tabel Stok</u></strong>
                                        <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Bahan Pokok</th>
                                                <th>No. TDG</th>
                                                <th>Stok</th>
                                                <th>Tanggal Input</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($stok_gudang->result() as $stok_gudang){ ?>
                                            <tr>
                                                <td><?php echo $stok_gudang->id; ?></td>
                                                <td><?php echo $stok_gudang->nama_bahan_pokok; ?></td>
                                                <td><?php echo $stok_gudang->tdg; ?></td>
                                                <td><?php echo $stok_gudang->stok." ".$stok_gudang->satuan; ?></td>
                                                <td><?php echo $stok_gudang->tgl_input; ?></td>
                                                <td align="center">
                                                    <a href="<?php echo site_url("backend/edit_stok_gudang/$stok_gudang->id") ?>" class="btn btn-sm btn-warning"><span class="fa fa-pencil"></span></a>
                                                    
                                                    <a href="<?php echo site_url("backend/hapus_data_gudang/$stok_gudang->id") ?>" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure wants to delete?')" ><span class="fa fa-trash-o"></span></a>
                                                </td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                    </div>
                                    <div class="col-md-6">
                                        <strong><u>Tabel History Penambahan Stok</u></strong>
                                        <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Bahan Pokok</th>
                                                <th>No. TDG</th>
                                                <th>Asal</th>
                                                <th>Jumlah Pengadaan</th>
                                                <th>Tanggal Input</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($penambahan_stok->result() as $penambahan_stok){ ?>
                                            <tr>
                                                <td><?php echo $penambahan_stok->id; ?></td>
                                                <td><?php echo $penambahan_stok->nama_bahan_pokok; ?></td>
                                                <td><?php echo $penambahan_stok->tdg; ?></td>
                                                <td><?php echo $penambahan_stok->asal; ?></td>
                                                <td><?php echo $penambahan_stok->jml_stok." ".$penambahan_stok->satuan; ?></td>
                                                <td><?php echo $penambahan_stok->tgl_input; ?></td>
                                                <td align="center">
                                                    <!--
                                                    <a href="<?php echo site_url("backend/edit_tambah_stok_gudang/$penambahan_stok->id") ?>" class="btn btn-sm btn-warning"><span class="fa fa-pencil"></span></a>
                                                    -->
                                                    <a href="<?php echo site_url("backend/hapus_tambah_stok_gudang/$penambahan_stok->id") ?>" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure wants to delete?')" ><span class="fa fa-trash-o"></span></a>
                                                </td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Daftar Pengiriman</h3>
                                </div>
                                <div class="panel-body">

                                    <div class="col-md-6">
                                        <form action="<?php echo base_url('backend/filter_gudang'); ?>" method="post">
                                        <div class="form-group">
                                            <h4>Report</h4>

                                            <div class="col-md-4">
                                                <label>Tanggal</label>
                                                <input type="date" name="tgl_min" class="form-control" required>
                                                s/d
                                                <input type="date" name="tgl_max" class="form-control" required>
                                            </div>

                                            <div class="col-md-3">
                                                <br>
                                                <input type="submit" name="btnPDF" value="PDF" class="btn btn-success">
                                                <!--<input type="submit" name="btnEXCEL" value="Excel" class="btn btn-warning">-->
                                            </div>

                                        </div>

                                    </form>
                                    </div>

                                    <div class="col-md-6">
                                        <form action="<?php echo base_url('backend/filter_gudang2'); ?>" method="post">
                                        <div class="form-group">
                                            <h4>Report Berdasarkan Gudang</h4>
                                            <div class="col-md-4">
                                                <label>No. TDG</label>
                                                <input type="text" name="tdg" class="form-control" required>
                                            </div>

                                            <div class="col-md-3">
                                                <br>
                                                <input type="submit" name="btnPDF" value="PDF" class="btn btn-success">
                                                <!--<input type="submit" name="btnEXCEL" value="Excel" class="btn btn-warning">-->
                                            </div>

                                        </div>

                                    </form>
                                    </div>

                                    <div class="col-md-12">
                                        <hr>
                                    </div>

                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Bahan Pokok</th>
                                                <th>No. TDG</th>
                                                <th>Tujuan</th>
                                                <th>Stok Awal</th>
                                                <th>Jumlah Pengeluaran</th>
                                                <th>Stok Akhir</th>
                                                <th>Keterangan</th>
                                                <th>Tanggal Input</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($pengiriman_gudang->result() as $pengiriman_gudang){ ?>
                                            <tr>
                                                <td><?php echo $pengiriman_gudang->id; ?></td>
                                                <td><?php echo $pengiriman_gudang->nama_bahan_pokok; ?></td>
                                                <td><?php echo $pengiriman_gudang->tdg; ?></td>
                                                <td><?php echo $pengiriman_gudang->tujuan; ?></td>
                                                <td><?php echo $pengiriman_gudang->stok_awal." ".$pengiriman_gudang->satuan; ?></td>
                                                <td><?php echo $pengiriman_gudang->jml_pengiriman." ".$pengiriman_gudang->satuan; ?></td>
                                                <td><?php echo $pengiriman_gudang->stok_akhir." ".$pengiriman_gudang->satuan; ?></td>
                                                <td><?php echo $pengiriman_gudang->ket; ?></td>
                                                <td><?php echo $pengiriman_gudang->tgl_input; ?></td>
                                                <td align="center">
                                                    <!--
                                                    <a href="<?php echo site_url("backend/edit_data_tdg_pengiriman/$pengiriman_gudang->id") ?>" class="btn btn-sm btn-warning"><span class="fa fa-pencil"></span></a>
                                                    -->
                                                    <a href="<?php echo site_url("backend/hapus_data_tdg_pengiriman/$pengiriman_gudang->id") ?>" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure wants to delete?')"><span class="fa fa-trash-o"></span></a>
                                                </td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo site_url('backend/logout') ?>" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                 
        
	<!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap.min.js"></script>              
        <!-- END PLUGINS -->

        <!-- THIS PAGE PLUGINS -->
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/icheck/icheck.min.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>  
        <!-- END PAGE PLUGINS -->         

        <!-- START TEMPLATE -->      
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/actions.js"></script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->         
    </body>
</html>



