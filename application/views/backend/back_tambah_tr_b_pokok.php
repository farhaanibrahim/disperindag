<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title><?php echo $title; ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo base_url('upload/132.png') ?>" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('aset/backend') ?>/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
           <?php include_once 'layout/sidebar.php'; ?>
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
					<li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                    <!-- END TOGGLE NAVIGATION -->                    
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">                
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->                
                
                <div class="page-title">                    
                    <h2><span class="fa fa-file-text"></span> <?php echo $title2; ?></h2>
                </div>                   
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
							
							<form id="jvalidate" class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('backend/tambah_transaksi_b_pokok_ac'); ?>">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><?php echo $title2; ?></h3>
                                </div>
                                <div class="panel-body">
									<div class="row">
                                        
                                        <div class="col-md-6">
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Asal Pasar</label>
                                                <div class="col-md-9">       
													<select name="id_pasar" id="id_pasar" class="form-control select" required>
														<option value="">Pilih Asal Pasar</option>
														<?php foreach($pasar->result() as $res): ?>
														<option value="<?php echo $res->id_pasar; ?>"><?php echo $res->nama_pasar; ?></option>
														<?php endforeach; ?>
													</select>                                     
                                                </div>
                                            </div>

											<div class="form-group">
                                                <label class="col-md-3 control-label">Bahan Pokok</label>
                                                <div class="col-md-9">       
													<select name="id_bahan_pokok" id="id_bahan_pokok" class="form-control select" required>
														<option value="">Pilih Bahan Pokok</option>
														<?php foreach($b_pokok->result() as $res1): ?>
														<option value="<?php echo $res1->id_bahan_pok; ?>"><?php echo $res1->nama_bahan_pokok; ?></option>
														<?php endforeach; ?>
													</select>                                     
                                                </div>
                                            </div>
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Sub Bahan Pokok</label>
                                                <div class="col-md-9">       
													<select name="id_sub_bahan_pokok" id="id_sub_bahan_pokok" class="form-control select" required>
														<option value="">Pilih Sub Bahan Pokok</option>
														<?php foreach($b_pokok_sub->result() as $res3): ?>
														<option value="<?php echo $res3->id_bahan_pok_sub; ?>"><?php echo $res3->nama_sub_bahan_pokok; ?></option>
														<?php endforeach; ?>
													</select>                                     
                                                </div>
                                            </div>
											
                                        </div>
										
                                        <div class="col-md-6">
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Harga</label>
                                                <div class="col-md-9">                                            
                                                    <input type="number" name="harga" placeholder="Harga Bahan Pokok" class="form-control" required/>                                            
                                                </div>
                                            </div>
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Tanggal</label>
                                                <div class="col-md-9">                                            
                                                    <input type="text" name="tanggal" placeholder="Tanggal" class="form-control datepicker	" required/>                                       
                                                </div>
                                            </div>
											
                                        </div>
                                        
                                    </div>
								</div>
								<div class="panel-footer">
                                    <button class="btn btn-default" type="reset">Clear Form</button>                                    
                                    <button class="btn btn-primary pull-right">Submit</button>
                                </div>
                            </div>
							</form>
                        </div>
                    </div>
                
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo site_url('backend/logout') ?>" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                 
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap.min.js"></script>              
        <!-- END PLUGINS -->

        <!-- THIS PAGE PLUGINS -->
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/icheck/icheck.min.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>  
		
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap-datepicker.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap-file-input.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap-select.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/tagsinput/jquery.tagsinput.min.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery-validation/jquery.validate.js"></script>  
		
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/summernote/summernote.js"></script>  
        <!-- END PAGE PLUGINS -->         

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins.js"></script>        
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/actions.js"></script>        
        <script type="text/javascript">
			var jvalidate = $("#jvalidate").validate({
                ignore: [],
                rules: {}
            });
		</script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->         
    </body>
</html>






