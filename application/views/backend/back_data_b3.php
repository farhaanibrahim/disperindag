<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- META SECTION -->
        <title><?php echo $title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="icon" href="<?php echo base_url('upload/132.png') ?>" type="image/x-icon" />
        <!-- END META SECTION -->

        <!-- CSS INCLUDE -->
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('aset/backend') ?>/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">

           <?php include_once 'layout/sidebar.php'; ?>

            <!-- PAGE CONTENT -->
            <div class="page-content">

                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
					<li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>
                    </li>
                    <!-- END SIGN OUT -->
                    <!-- END TOGGLE NAVIGATION -->
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->

                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->

                <div class="page-title">
                    <h2><span class="fa fa-file-text"></span> <?php echo $title2; ?></h2>
					<a href="<?php echo site_url("backend/tambah_stok_b3"); ?>" class="btn btn-sm btn-info pull-right"><span class="fa fa-plus"></span>Tambah Stok B3</a>
                    <a href="<?php echo site_url("backend/tambah_data_pengguna_akhir"); ?>" class="btn btn-sm btn-primary pull-right"><span class="fa fa-plus"></span>Tambah Data Pengguna Akhir</a>
					<br>
					<?php if ($this->session->flashdata('insert')): ?>
                        <div class="alert alert-success" style="" id="info_sukses">
                            <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                            </button>
                            <p class="text-small"><span class="fa fa-check"></span> <?php echo $this->session->flashdata('insert'); ?> </p>
                        </div>
                    <?php endif ?>
                    <?php if ($this->session->flashdata('update')): ?>
                        <div class="alert alert-info" style="" id="info_sukses">
                            <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                            </button>
                            <p class="text-small"><span class="fa fa-check"></span> <?php echo $this->session->flashdata('update'); ?> </p>
                        </div>
                    <?php endif ?>
                    <?php if ($this->session->flashdata('failed')): ?>
                        <div class="alert alert-warning" style="" id="info_sukses">
                            <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                            </button>
                            <p class="text-small"><span class="fa fa-check"></span> <?php echo $this->session->flashdata('failed'); ?> </p>
                        </div>
                    <?php endif ?>
                    <?php if ($this->session->flashdata('delete')): ?>
                        <div class="alert alert-info" style="" id="info_sukses">
                            <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                            </button>
                            <p class="text-small"><span class="fa fa-check"></span> <?php echo $this->session->flashdata('delete'); ?> </p>
                        </div>
                    <?php endif ?>
                </div>

                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">

                    <div class="row">
                        <div class="col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><?php echo $title2; ?></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-6">
                                        <strong><u>Tabel Stok B3</u></strong>
                                        <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Distributor</th>
                                                <th>Nama Barang</th>
                                                <th>Stok</th>
                                                <th>Tanggal Input</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 1; ?>
                                            <?php foreach ($stok->result() as $row): ?>
                                                <tr>
                                                    <td><?php echo $no; ?></td>
                                                    <td><?php echo $row->distributor; ?></td>
                                                    <td><?php echo $row->uraian_brg; ?></td>
                                                    <td><?php echo $row->stok." ".$row->satuan; ?></td>
                                                    <td><?php echo $row->tgl_input; ?></td>
                                                    <td><a href="<?php echo base_url('backend/hapus_stok_b3')."/".$row->id_stok; ?>" class="btn btn-danger" onclick="return confirm('Are you sure wants to delete?')">Delete</a></td>
                                                </tr>
                                                <?php $no++; ?>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>
                                    </div>
                                    <div class="col-md-6">
                                      <strong> <u>History Penambahan Stok B3</u> </strong>
                                        <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Barang</th>
                                                <th>Distributor</th>
                                                <th>Jumlah Penambahan</th>
                                                <th>Tanggal Input</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no2 = 1; ?>
                                            <?php foreach ($hist_penambahan_stok->result() as $row2): ?>
                                                <tr>
                                                    <td><?php echo $no2 ?></td>
                                                    <td><?php echo $row2->uraian_brg; ?></td>
                                                    <td><?php echo $row2->distributor; ?></td>
                                                    <td><?php echo $row2->jml_penambahan." ".$row->satuan; ?></td>
                                                    <td><?php echo $row2->tgl_input; ?></td>
                                                    <td><a href="<?php echo base_url('backend/hapus_penambahan_b3')."/".$row2->id_penambahan; ?>" class="btn btn-danger" onclick="return confirm('Are you sure wants to delete?')">Delete</a></td>
                                                </tr>
                                                <?php $no2++; ?>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Daftar Pengeluaran B3</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <a href="#" class="btn btn-primary pull-right">PDF</a>
                                        <br><br><br>
                                    </div>

                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Distributor</th>
                                                <th>Nama Barang</th>
                                                <th>Stok Awal</th>
                                                <th>Jumlah Pengeluaran</th>
                                                <th>Stok Akhir</th>
                                                <th>Nama Pengguna akhir</th>
                                                <th>No. Rekomendasi</th>
                                                <th>Tanggal Input</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $no3 = 1; ?>
                                        <?php foreach ($b3_pengeluaran->result() as $row3): ?>
                                            <tr>
                                                <td><?php echo $no3; ?></td>
                                                <td><?php echo $row3->nm_perusahaan; ?></td>
                                                <td><?php echo $row3->uraian_brg; ?></td>
                                                <td><?php echo $row3->stok_awal; ?></td>
                                                <td><?php echo $row3->jml_pengeluaran; ?></td>
                                                <td><?php echo $row3->stok_akhir; ?></td>
                                                <td><?php echo $row3->nm_pengguna_akhir; ?></td>
                                                <td><?php echo $row3->no_rekomendasi; ?></td>
                                                <td><?php echo $row3->tgl_input; ?></td>
                                                <td><a href="<?php echo base_url('backend/hapus_data_b3_keluar')."/".$row3->id_keluar; ?>" class="btn btn-danger" onclick="return confirm('Are you sure wants to delete?')">Delete</a></td>
                                            </tr>
                                        <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- END PAGE CONTENT WRAPPER -->
            </div>
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo site_url('backend/logout') ?>" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->

	<!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap.min.js"></script>
        <!-- END PLUGINS -->

        <!-- THIS PAGE PLUGINS -->
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/icheck/icheck.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <!-- END PAGE PLUGINS -->

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/actions.js"></script>
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->
    </body>
</html>
