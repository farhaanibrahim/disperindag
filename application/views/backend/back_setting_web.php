<!DOCTYPE html>
<?php foreach($instansi->result() as $is_row); ?>
<?php foreach($data_login->result() as $is_row2); ?>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title><?php echo $title; ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo base_url('upload/132.png') ?>" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('aset/backend') ?>/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
           <?php include_once 'layout/sidebar.php'; ?>
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
					<li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                    <!-- END TOGGLE NAVIGATION -->                    
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">                
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->                
                
                <div class="page-title">                    
                    <h2><span class="fa fa-file-text"></span> <?php echo $title2; ?></h2>
                </div>                 
				
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
					
                    <div class="row">
                        <div class="col-md-12">
							
							<?php if($this->session->flashdata('setting_web') == 'sukses'){ ?>
							<div class="alert alert-success" style="" id="info_sukses">
								<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
								</button>
								<p class="text-small">Berhasil.. Aplikasi berhasil di update. </p>
							</div>
							<?php }else if($this->session->flashdata('setting_web') == 'gagal'){ ?>
							<div class="alert alert-danger" style="" id="info_sukses">
								<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
								</button>
								<p class="text-small">Gagal.. Aplikasi gagal di update. </p>
							</div>
							<?php } ?>
							
							<form class="form-horizontal" enctype="multipart/form-data" method="post" id="form_input" action="<?php echo site_url('backend/setting_web_ac'); ?>">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><?php echo $title2; ?></h3>
                                </div>
                                <div class="panel-body">
									<div class="row">
                                        
                                        <div class="col-md-6">
											
											<input name="logo_lama" class="form-control" id="logo_lama" placeholder="" type="hidden" value="<?php echo $is_row->logo; ?>" />
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Judul Website</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <input name="nama" class="form-control" id="nama" placeholder="" type="text" value="<?php echo $is_row->nama; ?>" />
                                                    </div>                                            
                                                </div>
                                            </div>
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Kepala Bagian</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
														<input name="kepsek" class="form-control" id="kepsek" placeholder="" type="text" value="<?php echo $is_row->kepsek; ?>" />
													</div>                                            
                                                </div>
                                            </div>
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">NIP Kepala Bagian</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
														<input name="nip_kepsek" class="form-control" id="nip_kepsek" placeholder="" type="text" value="<?php echo $is_row->nip_kepsek; ?>"/>
													</div>                                            
                                                </div>
                                            </div>
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">No Telp</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
														<input name="no_telp" class="form-control" id="no_telp" placeholder="" type="text" value="<?php echo $is_row->no_telp; ?>" />
													</div>                                            
                                                </div>
                                            </div>
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Email</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
														<input name="email" class="form-control" id="email" placeholder="" type="email" value="<?php echo $is_row->email; ?>" />
													</div>                                            
                                                </div>
                                            </div>
											
										</div>
										<div class="col-md-6">
											
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Tentang Singkat</label>
                                                <div class="col-md-9">                                            
                                                    <textarea name="tentang_singkat" id="tentang_singkat" rows="5" class="form-control"><?php echo $is_row->tentang_singkat; ?></textarea>
                                                </div>
                                            </div>
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Alamat</label>
                                                <div class="col-md-9">                                            
                                                    <textarea name="alamat" id="alamat" rows="3" class="form-control" ><?php echo $is_row->alamat; ?></textarea>                                            
                                                </div>
                                            </div>
											
										</div>
										
                                    </div>
								</div>
								<div class="panel-footer">
                                    <button class="btn btn-default">Clear Form</button>                                    
                                    <button class="btn btn-primary pull-right">Submit</button>
                                </div>
                            </div>
							</form>
                        </div>
                    </div>
                
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo site_url('backend/logout') ?>" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                 
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap.min.js"></script>              
        <!-- END PLUGINS -->

        <!-- THIS PAGE PLUGINS -->
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/icheck/icheck.min.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>  
		
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap-datepicker.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap-file-input.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap-select.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/tagsinput/jquery.tagsinput.min.js"></script>  
		
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/summernote/summernote.js"></script>  
        <!-- END PAGE PLUGINS -->         

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins.js"></script>        
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/actions.js"></script>        
        <script>
			var options2 = { 
				beforeSend: function() {
					$("#info_sukses").hide();
					$("#info_gagal").hide();	
					$("#info_nomor").hide();	
					$("#prog_bar").show();	
				},
				success: function() {
				},
				complete: function(response) {
					$("#prog_bar").hide();
					if(response.responseText == '1'){
						$("#info_sukses").show();
						window.location="<?php echo site_url('backend/setting_web') ?>";
					}else{
						$("#info_gagal").show();
					}
				},
				error: function(){
					//$("#form_edit_ads #message").html("<font color='red'> ERROR: unable to upload files</font>");
				}
			}; 
			$("#form_input").ajaxForm(options2);
		</script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->         
    </body>
</html>