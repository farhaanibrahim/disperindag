<?php foreach ($stok->result() as $row) {} ?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title><?php echo $title; ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo base_url('upload/132.png') ?>" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('aset/backend') ?>/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
           <?php include_once 'layout/sidebar.php'; ?>
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
					<li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                    <!-- END TOGGLE NAVIGATION -->                    
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">                
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->                
                
                <div class="page-title">                    
                    <h2><span class="fa fa-file-text"></span> <?php echo $title2; ?></h2>
                    <?php if ($this->session->flashdata('insert')): ?>
                        <div class="alert alert-success" style="" id="info_sukses">
                            <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                            </button>
                            <p class="text-small"><span class="fa fa-check"></span> <?php echo $this->session->flashdata('insert'); ?> </p>
                        </div>
                    <?php endif ?>
                </div>                   
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
							
							<form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('backend/edit_stok_gudang_ac'); ?>">
                            <input type="hidden" name="id" value="<?php echo $row->id; ?>">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><?php echo $title2; ?></h3>
                                </div>
                                <div class="panel-body">
									<div class="row">
                                        
                                        <div class="col-md-6">
											
											<div class="form-group">
                                                <label class="col-md-3 control-label">Bahan Pokok</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <select name="id_bapok" class="form-control" disabled>
                                                            <option value="<?php echo $row->id_bapok ?>"><?php echo $row->nama_bahan_pokok ?></option>
                                                            <?php foreach ($bhn_pokok->result() as $bpok): ?>
                                                                <option value="<?php echo $bpok->id_bahan_pok; ?>"><?php echo $bpok->nama_bahan_pokok; ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                        <input type="hidden" name="id_bapok2" value="<?php echo $row->id_bapok ?>">
                                                    </div>                                            
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">No. TDG</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <input type="text" name="tdg" placeholder="No. TDG" class="form-control" value="<?php echo $row->tdg ?>" required disabled/>
                                                        <input type="hidden" name="tdg2" value="<?php echo $row->tdg ?>">
                                                    </div>                                            
                                                </div>
                                            </div>

											<div class="form-group">
                                                <label class="col-md-3 control-label">Stok</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <input type="text" name="stok" placeholder="Jumlah Pengadaan" class="form-control" value="<?php echo $row->stok ?>" required/>
                                                    </div>                                            
                                                </div>
                                            </div>

                                        </div>
										
                                        <div class="col-md-6">
											
                                        </div>
                                    </div>
								</div>
								<div class="panel-footer">
                                    <button class="btn btn-default">Clear Form</button>                                    
                                    <button class="btn btn-primary pull-right">Edit</button>
                                </div>
                            </div>
							</form>
                        </div>
                    </div>
                
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo site_url('backend/logout') ?>" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                 
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap.min.js"></script>              
        <!-- END PLUGINS -->

        <!-- THIS PAGE PLUGINS -->
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/icheck/icheck.min.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>  
		
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap-datepicker.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap-file-input.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap-select.js"></script>  
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/tagsinput/jquery.tagsinput.min.js"></script>  
		
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/summernote/summernote.js"></script>  
        <!-- END PAGE PLUGINS -->         

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins.js"></script>        
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/actions.js"></script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->         
    </body>
</html>



<script language="javascript">
   function tambah_form() {
     var idf = document.getElementById("idf").value;
     var stre;
     stre="<p id='srow" + idf + "'><input type='text' size='40' name='dikirim_ke[]' class='form-control' placeholder='' /> <a href='#' style=\"color:#3399FD;\" onclick='hapusElemen(\"#srow" + idf + "\"); return false;'>Hapus</a></p>";
     $("#text_form").append(stre);
     idf = (idf-1) + 2;
     document.getElementById("idf").value = idf;
   }
   function hapusElemen(idf) {
     $(idf).remove();
   }
</script>


