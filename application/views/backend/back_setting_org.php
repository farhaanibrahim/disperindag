<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title><?php echo $title; ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo base_url('upload/132.png') ?>" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('aset/backend') ?>/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
           <?php include_once 'layout/sidebar.php'; ?>
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
					<li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                    <!-- END TOGGLE NAVIGATION -->                    
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">                
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->                
                
                <div class="page-title">                    
                    <h2><span class="fa fa-file-text"></span> Setting Struktur Organisasi</h2>
					<br>
					<?php if($this->session->flashdata('inserted')){ ?>
					<div class="alert alert-success" style="" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small"><span class="fa fa-check"></span> <?php echo $this->session->flashdata('inserted'); ?> </p>
					</div>
					<?php }else if($this->session->flashdata('upload_failed')){ ?>
					<div class="alert alert-danger" style="" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small"> <?php echo $this->session->flashdata('upload_failed'); ?> </p>
					</div>
					<?php
					}
					?>
                </div>
				
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Form</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-6">
										<form action="<?php echo base_url('backend/setting_struk_org_ac'); ?>" method="post" enctype='multipart/form-data'>
											<div class="form-group">
												<lable>Nama</lable>
												<input type="text" name="nama" class="form-control">
											</div>
											
											<div class="form-group">
												<lable>Jabatan</lable>
												<input type="text" name="jabatan" class="form-control">
											</div>
											
											<div class="form-group">
												<lable>Foto</lable>
												<input type="file" name="foto">
											</div>
											
											<input type="submit" name="btnSubmit" class="btn btn-primary" value="Ok">
										</form>
									</div>
									<div class="col-md-12">
										<hr><br>
										
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>No. </th>
													<th>Foto</th>
													<th>Nama</th>
													<th>Jabatan</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<?php
												$no = 1;
												foreach($people->result() as $row)
												{
													?>
													<tr>
														<td><?php echo $no; ?></td>
														<td><img class="img img-thumbnail" src="<?php echo base_url('upload'); ?>/<?php echo $row->foto; ?>" style="width:150px;"></td>
														<td><?php echo $row->nama; ?></td>
														<td><?php echo $row->jabatan; ?></td>
														<td><a href="<?php echo base_url('backend/edit_struk_org'); ?>/<?php echo $row->id; ?>" class="btn btn-primary">Edit</a><a href="#" class="btn btn-warning">Delete</a></td>
													</tr>
													<?php
													$no++;
												}
												?>
											</tbody>
										</table>
									</div>
                                </div>
                            </div>

                        </div>
                    </div>
                
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo site_url('backend/logout') ?>" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- MESSAGE BOX DELETE-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-delete">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span><strong>Delete</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want delete?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to delete current data.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo site_url('backend/hapus_tdp'); ?>/<?php echo $row->id; ?>" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                 
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap.min.js"></script>              
        <!-- END PLUGINS -->

        <!-- THIS PAGE PLUGINS -->
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>  
        <!-- END PAGE PLUGINS -->         

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins.js"></script>        
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/actions.js"></script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->         
    </body>
</html>






