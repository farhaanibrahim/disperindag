<?php foreach ($pengiriman->result() as $title) {} ?>
<!DOCTYPE html>
<html>
<head>
	<title>Preview</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" href="<?php echo base_url('upload'); ?>/logo_bogor.png" />
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css'); ?>/bootstrap.min.css">
</head>
<body>
	<style type="text/css">
		.header {
			text-align: center;
		}
	</style>
	<!-- CSS goes in the document HEAD or added to your external stylesheet -->
	<style type="text/css">
		table.gridtable {
			font-family: verdana,arial,sans-serif;
			font-size:11px;
			color:#333333;
			border-width: 1px;
			border-color: #666666;
			border-collapse: collapse;
			width: 100%;
		}
		table.gridtable th {
			border-width: 1px;
			padding: 8px;
			border-style: solid;
			border-color: #666666;
			background-color: #dedede;
		}
		table.gridtable td {
			border-width: 1px;
			padding: 8px;
			border-style: solid;
			border-color: #666666;
			background-color: #ffffff;
		}
	</style>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="header">
					<p>
						LAPORAN PENGIRIMAN BARANG KEBUTUHAN POKOK <br>
						BULAN <?php echo $bulan_min." - ".$bulan_max; ?> TAHUN <?php echo $tahun; ?>
					</p>
				</div>
				<br>

				<table class="gridtable">
					<thead>
						<tr>
							<th>No.</th>
							<th>Nama Gudang</th>
							<th>Bahan Pokok</th>
							<th>Stok Awal</th>
							<th>Jumlah Pengiriman</th>
							<th>Stok Akhir</th>
							<th>Dikirim ke</th>
							<th>Pada Tanggal</th>
						</tr>
					</thead>
					<tbody>
						<?php $no = 1; ?>
						<?php foreach ($pengiriman->result() as $pengiriman): ?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?php echo $pengiriman->nm_perusahaan ?></td>
								<td><?php echo $pengiriman->nama_bahan_pokok ?></td>
								<td><?php echo $pengiriman->stok_awal." ".$pengiriman->satuan ?></td>
								<td><?php echo $pengiriman->jml_pengiriman." ".$pengiriman->satuan;  ?></td>
								<td><?php echo $pengiriman->stok_akhir." ".$pengiriman->satuan ?></td>
								<td><?php echo $pengiriman->tujuan ?></td>
								<td><?php echo $pengiriman->tgl_input ?></td>
								<?php $no++; ?>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>	