<?php foreach ($data->result() as $title) {} ?>
<!DOCTYPE html>
<html>
<head>
	<title>Preview</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" href="<?php echo base_url('upload'); ?>/logo_bogor.png" />
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css'); ?>/bootstrap.min.css">
</head>
<body>
	<style type="text/css">
		.header {
			text-align: center;
		}
	</style>
	<!-- CSS goes in the document HEAD or added to your external stylesheet -->
	<style type="text/css">
		table.gridtable {
			font-family: verdana,arial,sans-serif;
			font-size:11px;
			color:#333333;
			border-width: 1px;
			border-color: #666666;
			border-collapse: collapse;
			width: 100%;
		}
		table.gridtable th {
			border-width: 1px;
			padding: 8px;
			border-style: solid;
			border-color: #666666;
			background-color: #dedede;
		}
		table.gridtable td {
			border-width: 1px;
			padding: 8px;
			border-style: solid;
			border-color: #666666;
			background-color: #ffffff;
		}
	</style>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="header">
					<p>
						(KOP)
						<hr>
						LAPORAN PENGIRIMAN BARANG KEBUTUHAN POKOK <br>
					</p>
				</div>
				<br>
				<table border="0">
					<tr>
						<td>Nama Gudang</td>
						<td>:</td>
						<td><?php echo $title->nm_perusahaan; ?></td>
					</tr>
					<tr>
						<td>Alamat Gudang</td>
						<td>:</td>
						<td><?php echo $title->almt_gudang; ?></td>
					</tr>
					<tr>
						<td>No. TDG</td>
						<td>:</td>
						<td><?php echo $title->no_tdg; ?></td>
					</tr>
				</table>

				<table class="gridtable">
					<thead>
						<tr>
							<th>No. </th>
							<th>Bahan Pokok</th>
							<th>Total Pengiriman</th>
							<th>Tujuan Pengiriman</th>
							<th>Pada tanggal</th>
						</tr>
					</thead>
					<tbody>
						<?php $no = 1; ?>
						<?php foreach ($data->result() as $tdg): ?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?php echo $tdg->nama_bahan_pokok; ?></td>
								<td><?php echo $tdg->jml_pengiriman." ".$tdg->satuan; ?></td>
								<td><?php echo $tdg->tujuan; ?>
									<!--
									<?php $sub = $sub_query->data_tdg_pengiriman_pdf3($title->tdg); ?>
									<ul>
										<?php foreach ($sub->result() as $sub): ?>
											<li><?php echo $sub->tujuan."  sebanyak ".$sub->jml_pengiriman." ".$sub->satuan; ?></li>
										<?php endforeach ?>
									</ul>
									-->
								</td>
								<td><?php echo $tdg->tgl_input; ?></td>
							</tr>
							<?php $no++; ?>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>	