<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title><?php echo $title; ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo base_url('upload/132.png') ?>" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('aset/backend') ?>/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
           <?php include_once 'layout/sidebar.php'; ?>
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
					<li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                    <!-- END TOGGLE NAVIGATION -->                    
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">                
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->                
                
                <div class="page-title">                    
                    <h2><span class="fa fa-file-text"></span> Tanda Daftar Perusahaan</h2>
					<a href="<?php echo site_url('backend/tambah_tdp'); ?>" class="btn btn-sm btn-primary pull-right"><span class="fa fa-plus"></span>Tambah Perusahaan</a>
					<br>
					<?php if($this->session->flashdata('tdp') == 'tambah_sukses'){ ?>
					<div class="alert alert-success" style="" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small"><span class="fa fa-check"></span> Berhasil.. Data Perusahaan telah selesai ditambah.. </p>
					</div>
					<?php }else if($this->session->flashdata('tdp') == 'edit_sukses'){ ?>
					<div class="alert alert-success" style="" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small"><span class="fa fa-check"></span> Berhasil.. Data Perusahaan telah selesai diedit.. </p>
					</div>
					<?php }else if($this->session->flashdata('tdp') == 'hapus_sukses'){ ?>
					<div class="alert alert-success" style="" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small"><span class="fa fa-check"></span> Berhasil.. Data Perusahaan telah selesai dihapus.. </p>
					</div>
					<?php }else if($this->session->flashdata('tdp') == 'error_upload'){ ?>
					<div class="alert alert-warning" style="" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small"><span class="fa fa-danger"></span> Maaf.. Input dibatalkan, <?php echo $this->session->flashdata('regulasi_error'); ?> </p>
					</div>
					<?php }else if($this->session->flashdata('tdp') == 'input_gagal'){ ?>
					<div class="alert alert-danger" style="" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small"> Maaf.. saat ini sedang mengalami gangguang, silahkan coba beberapa saat lagi.. </p>
					</div>
					<?php } ?>
                </div>
				
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Daftar Perusahaan</h3>
                                </div>
                                <div class="panel-body">
                                    <form action="<?php echo base_url('backend/tdp_filter'); ?>" method="post">
                                        <div class="form-group">
                                            <h4>Filter</h4>
                                            <div class="col-md-3">
                                                <label>Kecamatan</label>
                                                <select class="form-control" name="kecamatan" id="kecamatan">
                                                    <option value="-">Kecamatan</option>
                                                    <?php foreach ($kecamatan->result() as $kecamatan): ?>
                                                        <option value="<?php echo $kecamatan->id_kecamatan; ?>"><?php echo $kecamatan->nm_kecamatan; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>

                                            <div class="col-md-3">
                                                <label>Kelurahan</label>
                                                <select class="form-control" name="kelurahan" id="kelurahan">
                                                    <option value="-">-- Kelurahan --</option>
                                                </select>
                                            </div>

                                            <div class="col-md-3">
                                                <label>Izin Usaha</label>
                                                <select class="form-control" name="izin_usaha">
                                                    <option value="-">-- Izin Usaha --</option>
                                                    <?php foreach ($izin_usaha->result() as $iu): ?>
                                                        <option value="<?php echo $iu->nm_klasifikasi; ?>"><?php echo $iu->nm_klasifikasi; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
											
											<div class="col-md-3">
												<br>
												<input type="submit" name="btnPDF" value="PDF" class="btn btn-success">
												<input type="submit" name="btnEXCEL" value="Excel" class="btn btn-warning">
											</div>

                                        </div>
                                        <div class="col-md-12">
                                            <input type="submit" name="btnFilter" class="btn btn-primary">
                                        </div>
                                    </form>

                                    <div class="col-md-12">
                                        <div class="row">
                                            <hr>
                                        </div>
                                    </div>

                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Foto</th>
                                                <th>Nama Perusahaan</th>
                                                <th>Nama Penanggung Jawab</th>
                                                <th>Kelurahan</th>
                                                <th>Alamat Perusahaan</th>
                                                <th>Kelembagaan</th>
                                                <th>Kegiatan Usaha (KBLI)</th>
                                                <th>Ijin Usaha Yang Dimiliki</th>
                                                <th>Barang Jasa Dagangan Utama</th>
                                                <th>Masaberlaku</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php $no=1; ?>
                                        <?php foreach ($tdp->result() as $row): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td align="center"><img class="img img-thumbnail" src="<?php echo base_url('upload'); ?>/<?php echo $row->foto; ?>" style="width: 80%"></td>
                                                <td><?php echo $row->nm_perusahaan; ?></td>
                                                <td><?php echo $row->nm_penanggungjwb; ?></td>
                                                <td><?php echo $row->nm_kelurahan; ?></td>
                                                <td><?php echo $row->almt_perusahaan; ?></td>
                                                <td><?php echo $row->kelembagaan; ?></td>
                                                <td><?php echo $row->kbli; ?></td>
                                                <td><?php echo $row->izin_usaha; ?></td>
                                                <td><?php echo $row->brg_jasa_dag_utama; ?></td>
                                                <td><?php echo $row->masaberlaku; ?></td>
                                                <td>
                                                    <a href="<?php echo base_url('backend/edit_tdp'); ?>/<?php echo $row->id; ?>" class="btn btn-warning">Edit</a>
                                                    <a href="<?php echo site_url('backend/hapus_tdp'); ?>/<?php echo $row->id; ?>" class="btn btn-danger" onclick="return confirm('Apakah anda yakin ingin menghapus?')">Delete</a>
                                                </td>
                                            </tr>
                                            <?php $no++; ?>
                                        <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo site_url('backend/logout') ?>" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- MESSAGE BOX DELETE-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-delete">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span><strong>Delete</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want delete?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to delete current data.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo site_url('backend/hapus_tdp'); ?>/<?php echo $row->id; ?>" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                 
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap.min.js"></script>              
        <!-- END PLUGINS -->

        <!-- THIS PAGE PLUGINS -->
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>  
        <!-- END PAGE PLUGINS -->         

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins.js"></script>        
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/actions.js"></script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->         
    </body>
</html>

<script type="text/javascript">
    $("#kecamatan").change(function(){
        var id_kecamatan = $("#kecamatan option:selected").val();
        $.ajax({
            url: "<?php echo site_url('backend/kelurahan')?>",
            type: "POST",
            data    : "id_kecamatan="+id_kecamatan,
            success : function (msg) {
                document.getElementById("kelurahan").disabled = false;
                $("#kelurahan").html(msg);
                //$("#kelurahan").css("color","black");
            }
        });
    });
</script>




