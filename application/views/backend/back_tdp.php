<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title><?php echo $title; ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo base_url('upload/132.png') ?>" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('aset/backend') ?>/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
           <?php include_once 'layout/sidebar.php'; ?>
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
					<li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                    <!-- END TOGGLE NAVIGATION -->                    
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">                
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->                
                
                <div class="page-title">                    
                    <h2><span class="fa fa-file-text"></span> Daftar Perizinan</h2>
					<a href="<?php echo site_url('backend/tambah_tdp'); ?>" class="btn btn-sm btn-primary pull-right"><span class="fa fa-plus"></span>Tambah Perizinan</a>
					<br>
					<?php if($this->session->flashdata('tdp') == 'tambah_sukses'){ ?>
					<div class="alert alert-success" style="" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small"><span class="fa fa-check"></span> Berhasil.. Data Perusahaan telah selesai ditambah.. </p>
					</div>
					<?php }else if($this->session->flashdata('tdp') == 'edit_sukses'){ ?>
					<div class="alert alert-success" style="" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small"><span class="fa fa-check"></span> Berhasil.. Data Perusahaan telah selesai diedit.. </p>
					</div>
					<?php }else if($this->session->flashdata('tdp') == 'hapus_sukses'){ ?>
					<div class="alert alert-success" style="" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small"><span class="fa fa-check"></span> Berhasil.. Data Perusahaan telah selesai dihapus.. </p>
					</div>
					<?php }else if($this->session->flashdata('tdp') == 'error_upload'){ ?>
					<div class="alert alert-warning" style="" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small"><span class="fa fa-danger"></span> Maaf.. Input dibatalkan, <?php echo $this->session->flashdata('regulasi_error'); ?> </p>
					</div>
					<?php }else if($this->session->flashdata('tdp') == 'input_gagal'){ ?>
					<div class="alert alert-danger" style="" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small"> Maaf.. saat ini sedang mengalami gangguang, silahkan coba beberapa saat lagi.. </p>
					</div>
					<?php } ?>
					
                    <?php if ($this->session->flashdata('update')): ?>
                        <div class="alert alert-info" style="" id="info_sukses">
                            <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                            </button>
                            <p class="text-small"> <?php echo $this->session->flashdata('update'); ?> </p>
                        </div>
                    <?php endif ?>

					<?php
					if($this->session->flashdata('empty')){
						?>
						<div class="alert alert-warning" style="" id="info_sukses">
							<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
							</button>
							<p class="text-small"> <?php echo $this->session->flashdata('empty'); ?> </p>
						</div>
						<?php
					}
					?>
                </div>
				
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Data Perizinan</h3>
                                </div>
                                <div class="panel-body">
                                <form action="<?php echo base_url('backend/filter_perizinan'); ?>" method="post">
                                            <div class="form-group">
                                                <h4>Filter</h4>
                                                <div class="col-md-3">
                                                    <label>Kecamatan</label>
                                                    <select class="form-control" name="kecamatan" id="kecamatan">
                                                        <option value="-">Kecamatan</option>
                                                        <?php foreach ($kecamatan->result() as $kcmtn): ?>
                                                            <option value="<?php echo $kcmtn->id_kecamatan; ?>"><?php echo $kcmtn->nm_kecamatan; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>

                                                <div class="col-md-3">
                                                    <label>Kelurahan</label>
                                                    <select class="form-control" name="kelurahan" id="kelurahan">
                                                        <option value="-">-- Kelurahan --</option>
                                                    </select>
                                                </div>

                                                <div class="col-md-3">
                                                    <label>Izin Usaha</label>
                                                    <select class="form-control" name="izin_usaha" required>
                                                        <option value="-">-- Izin Usaha --</option>
                                                        <option value="t_siup">SIUP</option>
                                                        <option value="t_tdp">TDP</option>
                                                        <option value="t_iupr">IUPR</option>
                                                        <option value="t_iupp">IUPP</option>
                                                        <option value="t_iuts">IUTS</option>
                                                        <option value="t_stpw">STPW</option>
                                                        <option value="t_tdg">TDG</option>
                                                        <option value="t_siupmb">SIUP-MB</option>
                                                        <option value="t_tdpud">TDPUD</option>
                                                        <option value="t_b3">B3</option>
                                                    </select>
                                                </div>

                                                <div class="col-md-3">
                                                    <br>
                                                    <input type="submit" name="btnPDF" value="PDF" class="btn btn-success">
                                                    <input type="submit" name="btnEXCEL" value="Excel" class="btn btn-warning">
                                                </div>

                                            </div>
                                            <div class="col-md-12">
                                                <input type="submit" name="btnFilter" class="btn btn-primary">
                                            </div>
                                        </form>

                                        <div class="col-md-12">
                                            <div class="row">
                                                <hr>
                                            </div>
                                        </div>
                                <br><br>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#siup" data-toggle="tab" aria-expanded="true">SIUP</a></li>
                                        <li class=""><a href="#tdp" data-toggle="tab" aria-expanded="false">TDP</a></li>
                                        <li class=""><a href="#iupr" data-toggle="tab" aria-expanded="false">IUPR</a></li>
                                        <li class=""><a href="#iupp" data-toggle="tab" aria-expanded="false">IUPP</a></li>
                                        <li class=""><a href="#iuts" data-toggle="tab" aria-expanded="false">IUTS</a></li>
                                        <li class=""><a href="#stpw" data-toggle="tab" aria-expanded="false">STPW</a></li>
                                        <li class=""><a href="#tdg" data-toggle="tab" aria-expanded="false">TDG</a></li>
                                        <li class=""><a href="#siupmb" data-toggle="tab" aria-expanded="false">SIUP-MB</a></li>
                                        <li class=""><a href="#tdpud" data-toggle="tab" aria-expanded="false">TDPUD</a></li>
                                        <li class=""><a href="#b3" data-toggle="tab" aria-expanded="false">B3</a></li>
                                    </ul>
                                    <div id="myTabContent" class="tab-content">
                                      <div class="tab-pane fade active in" id="siup">
                                        <br><br>
                                        <!--
                                        <form action="<?php echo base_url('backend/tdp_filter'); ?>" method="post">
                                            <div class="form-group">
                                                <h4>Filter</h4>
                                                <div class="col-md-3">
                                                    <label>Kecamatan</label>
                                                    <select class="form-control" name="kecamatan" id="kecamatan">
                                                        <option value="-">Kecamatan</option>
                                                        <?php foreach ($kecamatan->result() as $kecamatan): ?>
                                                            <option value="<?php echo $kecamatan->id_kecamatan; ?>"><?php echo $kecamatan->nm_kecamatan; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>

                                                <div class="col-md-3">
                                                    <label>Kelurahan</label>
                                                    <select class="form-control" name="kelurahan" id="kelurahan">
                                                        <option value="-">-- Kelurahan --</option>
                                                    </select>
                                                </div>

                                                <div class="col-md-3">
                                                    <label>Izin Usaha</label>
                                                    <select class="form-control" name="izin_usaha">
                                                        <option value="-">-- Izin Usaha --</option>
                                                        <?php foreach ($izin_usaha->result() as $iu): ?>
                                                            <option value="<?php echo $iu->nm_klasifikasi; ?>"><?php echo $iu->nm_klasifikasi; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>

                                                <div class="col-md-3">
                                                    <br>
                                                    <input type="submit" name="btnPDF" value="PDF" class="btn btn-success">
                                                    <input type="submit" name="btnEXCEL" value="Excel" class="btn btn-warning">
                                                </div>

                                            </div>
                                            <div class="col-md-12">
                                                <input type="submit" name="btnFilter" class="btn btn-primary">
                                            </div>
                                        </form>

                                        <div class="col-md-12">
                                            <div class="row">
                                                <hr>
                                            </div>
                                        </div>
                                        -->
                                        <table class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Foto</th>
                                                    <th>Nama Perusahaan</th>
                                                    <th>Nama Penanggung Jawab</th>
                                                    <th>No. Telp &</th>
                                                    <th>Modal Usaha</th>
                                                    <th>Kelembagaan</th>
                                                    <th>KBLI</th>
                                                    <th>No. SIUP & Tanggal SIUP</th>
                                                    <th>Masa Berlaku</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $no=1; ?>
                                                <?php foreach ($siup->result() as $siup): ?>
                                                    <tr>
                                                        <td><?php echo $no; ?></td>
                                                        <td><img class="img img-thumbnail" src="<?php echo base_url('upload'); ?>/<?php echo $siup->foto ?>" style="width: 60%;"></td>
                                                        <td><?php echo $siup->nm_perusahaan; ?></td>
                                                        <td><?php echo $siup->nm_penanggungjwb; ?></td>
                                                        <td><?php echo $siup->no_telp." & ".$siup->no_fax;; ?></td>
                                                        <td><?php echo $siup->modal_usaha; ?></td>
                                                        <td><?php echo $siup->kelembagaan; ?></td>
                                                        <td><?php echo $siup->kbli; ?></td>
                                                        <td><?php echo $siup->no_siup." & ".$siup->tgl_siup; ?></td>
                                                        <td><?php echo $siup->masaberlaku; ?></td>
                                                        <td>
                                                            <a href="<?php echo base_url('edit_perizinan/edit_siup'); ?>/<?php echo $siup->id; ?>" class="btn btn-warning"><span class="fa fa-pencil"></span></a>
                                                            <a href="<?php echo base_url('edit_perizinan'); ?>" class="btn btn-danger" onclick="return confirm('Are you sure wants to delete?')"><span class="fa fa-trash-o"></span></a>
                                                        </td>
                                                    </tr>
                                                    <?php $no++; ?>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>

                                      </div>
                                      <div class="tab-pane fade" id="tdp">
                                         
                                        <br><br>
                                        <table class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Foto</th>
                                                    <th>Nama Perusahaan</th>
                                                    <th>Nama Penanggung Jawab</th>
                                                    <th>Alamat Perusahaan</th>
                                                    <th>NPWP</th>
                                                    <th>No. Telp & Fax</th>
                                                    <th>Kegiatan Usaha Pokok</th>
                                                    <th>KBLI</th>
                                                    <th>Status Kantor</th>
                                                    <th>No. TDP & Tanggal TDP</th>
                                                    <th>Masa Berlaku</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $no2=1; ?>
                                                <?php foreach ($tdp->result() as $tdp): ?>
                                                    <tr>
                                                        <td><?php echo $no2; ?></td>
                                                        <td><img class="img img-thumbnail" src="<?php echo base_url('upload'); ?>/<?php echo $tdp->foto ?>" style="width: 60%;"></td>
                                                        <td><?php echo $tdp->nm_perusahaan; ?></td>
                                                        <td><?php echo $tdp->nm_penanggungjwb; ?></td>
                                                        <td><?php echo $tdp->almt_perusahaan; ?></td>
                                                        <td><?php echo $tdp->npwp; ?></td>
                                                        <td><?php echo $tdp->no_telp." & ".$tdp->no_fax; ?></td>
                                                        <td><?php echo $tdp->keg_usaha_pokok; ?></td>
                                                        <td><?php echo $tdp->kbli; ?></td>
                                                        <td><?php echo $tdp->status_kantor; ?></td>
                                                        <td><?php echo $tdp->no_tdp." & ".$tdp->tgl_tdp; ?></td>
                                                        <td><?php echo $tdp->masaberlaku; ?></td>
                                                        <td>
                                                            <a href="<?php echo base_url('edit_perizinan/edit_tdp'); ?>/<?php echo $tdp->id; ?>" class="btn btn-warning"><span class="fa fa-pencil"></span></a>
                                                            <a href="<?php echo base_url('edit_perizinan'); ?>" class="btn btn-danger" onclick="return confirm('Are you sure wants to delete?')"><span class="fa fa-trash-o"></span></a>
                                                        </td>
                                                    </tr>
                                                    <?php $no2++; ?>
                                                <?php endforeach ?> 
                                            </tbody>
                                        </table>

                                      </div>

                                      <div class="tab-pane fade" id="iupr">
                                        
                                        <br><br>
                                        <table class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Foto</th>
                                                    <th>Tanggal Pendaftaran</th>
                                                    <th>Nama Perusahaan</th>
                                                    <th>Nama Penanggung Jawab</th>
                                                    <th>Alamat Perusahaan</th>
                                                    <th>Jenis Usaha</th>
                                                    <th>SIUP</th>
                                                    <th>TDP</th>
                                                    <th>No. IUPR & Tanggal IUPR</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $no3=1; ?>
                                                <?php foreach ($iupr->result() as $iupr): ?>
                                                    <tr>
                                                        <td><?php echo $no3; ?></td>
                                                        <td><img class="img img-thumbnail" src="<?php echo base_url('upload'); ?>/<?php echo $iupr->foto ?>" style="width: 60%;"></td>
                                                        <td><?php echo $iupr->tgl_pendaftaran ?></td>
                                                        <td><?php echo $iupr->nm_perusahaan ?></td>
                                                        <td><?php echo $iupr->nm_penanggungjwb ?></td>
                                                        <td><?php echo $iupr->almt_perusahaan ?></td>
                                                        <td><?php echo $iupr->jenis_usaha ?></td>
                                                        <td><?php echo $iupr->siup ?></td>
                                                        <td><?php echo $iupr->tdp ?></td>
                                                        <td><?php echo $iupr->no_iupr." & ".$iupr->tgl_iupr ?></td>
                                                        <td>
                                                            <a href="<?php echo base_url('edit_perizinan/edit_iupr'); ?>/<?php echo $iupr->id; ?>" class="btn btn-warning"><span class="fa fa-pencil"></span></a>
                                                            <a href="<?php echo base_url('edit_perizinan'); ?>" class="btn btn-danger" onclick="return confirm('Are you sure wants to delete?')"><span class="fa fa-trash-o"></span></a>
                                                        </td>
                                                    </tr>
                                                    <?php $no3++; ?>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>

                                      </div>

                                      <div class="tab-pane fade" id="iupp">
                                        
                                        <br><br>
                                        <table class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Foto</th>
                                                    <th>Tanggal Pendaftaran</th>
                                                    <th>Nama Perusahaan</th>
                                                    <th>Nama Penanggung Jawab</th>
                                                    <th>Alamat Perusahaan</th>
                                                    <th>Jenis Usaha</th>
                                                    <th>SIUP</th>
                                                    <th>TDP</th>
                                                    <th>No. IUPP & Tanggal IUPP</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $no4=1; ?>
                                                <?php foreach ($iupp->result() as $iupp): ?>
                                                    <tr>
                                                        <td><?php echo $no4; ?></td>
                                                        <td><img class="img img-thumbnail" src="<?php echo base_url('upload'); ?>/<?php echo $iupp->foto ?>" style="width: 60%;"></td>
                                                        <td><?php echo $iupp->tgl_pendaftaran ?></td>
                                                        <td><?php echo $iupp->nm_perusahaan ?></td>
                                                        <td><?php echo $iupp->nm_penanggungjwb ?></td>
                                                        <td><?php echo $iupp->almt_perusahaan ?></td>
                                                        <td><?php echo $iupp->jenis_usaha ?></td>
                                                        <td><?php echo $iupp->siup ?></td>
                                                        <td><?php echo $iupp->tdp ?></td>
                                                        <td><?php echo $iupp->no_iupp." & ".$iupp->tgl_iupp ?></td>
                                                        <td>
                                                            <a href="<?php echo base_url('edit_perizinan/edit_iupp'); ?>/<?php echo $iupp->id; ?>" class="btn btn-warning"><span class="fa fa-pencil"></span></a>
                                                            <a href="<?php echo base_url('edit_perizinan'); ?>" class="btn btn-danger" onclick="return confirm('Are you sure wants to delete?')"><span class="fa fa-trash-o"></span></a>
                                                        </td>
                                                    </tr>
                                                    <?php $no4++; ?>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>

                                      </div>

                                      <div class="tab-pane fade" id="iuts">
                                        
                                        <br><br>
                                        <table class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Foto</th>
                                                    <th>Nama Perusahaan Pemberi</th>
                                                    <th>Nama Penanggung Jawab</th>
                                                    <th>Alamat Perusahaan</th>
                                                    <th>NPWP</th>
                                                    <th>No. Telp & Fax</th>
                                                    <th>Kegiatan Usaha Pokok</th>
                                                    <th>KBLI</th>
                                                    <th>STatus Kantor</th>
                                                    <th>SIUP</th>
                                                    <th>TDP</th>
                                                    <th>No. IUTS dan Tanggal IUTS</th>
                                                    <th>Masa Berlaku</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $no5 = 1; ?>
                                                <?php foreach ($iuts->result() as $iuts): ?>
                                                    <tr>
                                                        <td><?php echo $no5; ?></td>
                                                        <td><img class="img img-thumbnail" src="<?php echo base_url('upload'); ?>/<?php echo $iuts->foto ?>" style="width: 60%;"></td>
                                                        <td><?php echo $iuts->nm_perusahaan ?></td> 
                                                        <td><?php echo $iuts->nm_penanggungjwb ?></td>
                                                        <td><?php echo $iuts->almt_perusahaan ?></td>
                                                        <td><?php echo $iuts->npwp ?></td>
                                                        <td><?php echo $iuts->no_telp." & ".$iuts->no_fax ?></td>
                                                        <td><?php echo $iuts->keg_usaha_pokok ?></td>
                                                        <td><?php echo $iuts->kbli ?></td>
                                                        <td><?php echo $iuts->status_kantor ?></td>
                                                        <td><?php echo $iuts->siup ?></td>
                                                        <td><?php echo $iuts->tdp ?></td>
                                                        <td><?php echo $iuts->no_iuts." & ".$iuts->tgl_iuts ?></td>
                                                        <td><?php echo $iuts->masaberlaku ?></td>
                                                        <td>
                                                            <a href="<?php echo base_url('edit_perizinan/edit_iuts'); ?>/<?php echo $iuts->id; ?>" class="btn btn-warning"><span class="fa fa-pencil"></span></a>
                                                            <a href="<?php echo base_url('edit_perizinan'); ?>" class="btn btn-danger" onclick="return confirm('Are you sure wants to delete?')"><span class="fa fa-trash-o"></span></a>
                                                        </td>
                                                    </tr>
                                                    <?php $no5++; ?>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>

                                      </div>

                                      <div class="tab-pane fade" id="stpw">
                                        
                                        <br><br>
                                        <table class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Foto</th>
                                                    <th>No. Pendaftaran</th>
                                                    <th>Nama Perusahaan Penerima</th>
                                                    <th>Nama Penanggung Jawab</th>
                                                    <th>Alamat Perusahaan</th>
                                                    <th>NPWP</th>
                                                    <th>IUTS</th>
                                                    <th>No. STPW & Tanggal STPW</th>
                                                    <th>No. Telp</th>
                                                    <th>Masa Berlaku</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $no6=1; ?>
                                                <?php foreach ($stpw->result() as $stpw): ?>
                                                    <tr>
                                                        <td><?php echo $no6; ?></td>
                                                        <td><img class="img img-thumbnail" src="<?php echo base_url('upload'); ?>/<?php echo $stpw->foto ?>" style="width: 60%;"></td>
                                                        <td><?php echo $stpw->no_pendaftaran ?></td>
                                                        <td><?php echo $stpw->nm_perusahaan ?></td>
                                                        <td><?php echo $stpw->nm_penanggungjwb ?></td>
                                                        <td><?php echo $stpw->almt_perusahaan ?></td>
                                                        <td><?php echo $stpw->npwp ?></td>
                                                        <td><?php echo $stpw->iuts ?></td>
                                                        <td><?php echo $stpw->no_stpw." & ".$stpw->tgl_stpw ?></td>
                                                        <td><?php echo $stpw->no_telp ?></td>
                                                        <td><?php echo $stpw->masaberlaku ?></td>
                                                        <td>
                                                            <a href="<?php echo base_url('edit_perizinan/edit_stpw'); ?>/<?php echo $stpw->id; ?>" class="btn btn-warning"><span class="fa fa-pencil"></span></a>
                                                            <a href="<?php echo base_url('edit_perizinan'); ?>" class="btn btn-danger" onclick="return confirm('Are you sure wants to delete?')"><span class="fa fa-trash-o"></span></a>
                                                        </td>
                                                    </tr>
                                                    <?php $no6++; ?>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>

                                      </div>

                                      <div class="tab-pane fade" id="tdg">
                                        
                                        <br><br>
                                        <table class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Foto</th>
                                                    <th>Nama Gudang</th>
                                                    <th>Nama Penanggung Jawab</th>
                                                    <th>Alamat Penanggung Jawab</th>
                                                    <th>No. TDG & Tanggal TDG</th>
                                                    <th>Alamat Gudang</th>
                                                    <th>No. Telp/Fax/Email</th>
                                                    <th>Titik Kordinat</th>
                                                    <th>Luas Gudang (m2)</th>
                                                    <th>Kapasitas (m3/ton)</th>
                                                    <th>Kelengkapan Gudang</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $no7=1; ?>
                                                <?php foreach ($tdg->result() as $tdg): ?>
                                                    <tr>
                                                        <td><?php echo $no7; ?></td>
                                                        <td><img class="img img-thumbnail" src="<?php echo base_url('upload'); ?>/<?php echo $tdg->foto ?>" style="width: 60%;"></td>
                                                        <td><?php echo $tdg->nm_perusahaan ?></td>
                                                        <td><?php echo $tdg->nm_penanggungjwb ?></td>
                                                        <td><?php echo $tdg->almt_penanggungjwb ?></td>
                                                        <td><?php echo $tdg->no_tdg."&".$tdg->tgl_tdg ?></td>
                                                        <td><?php echo $tdg->almt_gudang ?></td>
                                                        <td><?php echo $tdg->no_telp."/".$tdg->no_fax."/".$tdg->email ?></td>
                                                        <td></td>
                                                        <td><?php echo $tdg->luas_gudang ?></td>
                                                        <td><?php echo $tdg->kapasitas_gudang ?></td>
                                                        <td><?php echo $tdg->kelengkapan_gudang ?></td>
                                                        <td>
                                                            <a href="<?php echo base_url('edit_perizinan/edit_tdg'); ?>/<?php echo $tdg->id; ?>" class="btn btn-warning"><span class="fa fa-pencil"></span></a>
                                                            <a href="<?php echo base_url('edit_perizinan'); ?>" class="btn btn-danger" onclick="return confirm('Are you sure wants to delete?')"><span class="fa fa-trash-o"></span></a>
                                                        </td>
                                                    </tr>
                                                    <?php $no7++; ?>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>

                                      </div>

                                      <div class="tab-pane fade" id="siupmb">
                                        
                                        <br><br>
                                        <table class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Foto</th>
                                                    <th>Nama Perusahaan</th>
                                                    <th>Alamat Perusahaan</th>
                                                    <th>SIUP-MB</th>
                                                    <th>TDP</th>
                                                    <th>Tanggal Keluar</th>
                                                    <th>Tanggal Berakhir</th>
                                                    <th>Insansi Yang Mengeluarkan</th>
                                                    <th>Keterangan</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $no8 = 1; ?>
                                                <?php foreach ($siupmb->result() as $siupmb): ?>
                                                    <tr>
                                                        <td><?php echo $no8; ?></td>
                                                        <td><img class="img img-thumbnail" src="<?php echo base_url('upload'); ?>/<?php echo $siupmb->foto ?>" style="width: 60%;"></td>
                                                        <td><?php echo $siupmb->nm_perusahaan ?></td>
                                                        <td><?php echo $siupmb->almt_perusahaan ?></td>
                                                        <td><?php echo $siupmb->siupmb ?></td>
                                                        <td><?php echo $siupmb->tdp ?></td>
                                                        <td><?php echo $siupmb->tgl_keluar ?></td>
                                                        <td><?php echo $siupmb->tgl_berakhir ?></td>
                                                        <td><?php echo $siupmb->ins_yg_mengeluarkan ?></td>
                                                        <td><?php echo $siupmb->keterangan ?></td>
                                                        <td>
                                                            <a href="<?php echo base_url('edit_perizinan/edit_siupmb'); ?>/<?php echo $siupmb->id; ?>" class="btn btn-warning"><span class="fa fa-pencil"></span></a>
                                                            <a href="<?php echo base_url('edit_perizinan'); ?>" class="btn btn-danger" onclick="return confirm('Are you sure wants to delete?')"><span class="fa fa-trash-o"></span></a>
                                                        </td>
                                                    </tr>
                                                    <?php $no8++; ?>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>

                                      </div>

                                      <div class="tab-pane fade" id="tdpud">
                                        
                                        <br><br>
                                        <table class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Foto</th>
                                                    <th>Nama Perusahaan</th>
                                                    <th>Nama Penanggung Jawab</th>
                                                    <th>Alamat Perusahaan</th>
                                                    <th>NPWP</th>
                                                    <th>No. Telp & Fax</th>
                                                    <th>Kegiatan Usaha Pokok</th>
                                                    <th>KBLI</th>
                                                    <th>Status Kantor</th>
                                                    <th>TDP</th>
                                                    <th>No. TDPUD & Tanggal TDPUD</th>
                                                    <th>Jenis</th>
                                                    <th>Masa Berlaku</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $no9=1; ?>
                                                <?php foreach ($tdpud->result() as $tdpud): ?>
                                                    <tr>
                                                        <td><?php echo $no9 ?></td>
                                                        <td><img class="img img-thumbnail" src="<?php echo base_url('upload'); ?>/<?php echo $tdpud->foto ?>" style="width: 60%;"></td>
                                                        <td><?php echo $tdpud->nm_perusahaan ?></td>
                                                        <td><?php echo $tdpud->nm_penanggungjwb ?></td>
                                                        <td><?php echo $tdpud->almt_perusahaan ?></td>
                                                        <td><?php echo $tdpud->npwp ?></td>
                                                        <td><?php echo $tdpud->no_telp." & ".$tdpud->no_fax ?></td>
                                                        <td><?php echo $tdpud->keg_usaha_pokok ?></td>
                                                        <td><?php echo $tdpud->kbli ?></td>
                                                        <td><?php echo $tdpud->status_kantor ?></td>
                                                        <td><?php echo $tdpud->tdp ?></td>
                                                        <td><?php echo $tdpud->no_tdpud." & ".$tdpud->tgl_tdpud ?></td>
                                                        <td><?php echo $tdpud->jenis_tdpud; ?></td>
                                                        <td><?php echo $tdpud->masaberlaku ?></td>
                                                        <td>
                                                            <a href="<?php echo base_url('edit_perizinan/edit_tdpud'); ?>/<?php echo $tdpud->id; ?>" class="btn btn-warning"><span class="fa fa-pencil"></span></a>
                                                            <a href="<?php echo base_url('edit_perizinan'); ?>" class="btn btn-danger" onclick="return confirm('Are you sure wants to delete?')"><span class="fa fa-trash-o"></span></a>
                                                        </td>
                                                    </tr>
                                                    <?php $no9++; ?>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>

                                      </div>

                                      <div class="tab-pane fade" id="b3">
                                        
                                        <br><br>
                                        <table class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th>NO.</th>
                                                    <th>NO. CAS</th>
                                                    <th>POS TARIF/HS</th>
                                                    <th>URAIAN BARANG</th>
                                                    <th>TATA NIAGA IMPOR</th>
                                                    <th>KEPERLUAN LAIN TIDAK UNTUK PANGAN</th>
                                                    <th>LABORATORIUM / PENELITIAN</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            	<?php $no10 = 1; ?>
                                                <?php foreach ($b3->result() as $b3): ?>
                                                	<tr>
                                                		<td><?php echo $no10; ?></td>
                                                		<td><?php echo $b3->no_cas; ?></td>
                                                		<td><?php echo $b3->pos_trf_hs; ?></td>
                                                		<td><?php echo $b3->uraian_brg; ?></td>
                                                		<td><?php echo $b3->tata_niaga_impor; ?></td>
                                                		<td><?php echo $b3->kep_lain_tdk_utk_pgn; ?></td>
                                                		<td><?php echo $b3->lab; ?></td>
                                                		<td>
                                                            <a href="<?php echo base_url('edit_perizinan/edit_b3'); ?>/<?php echo $b3->id; ?>" class="btn btn-warning"><span class="fa fa-pencil"></span></a>
                                                            <a href="<?php echo base_url('edit_perizinan/hapus_b3'); ?>/<?php echo $b3->id ?>" class="btn btn-danger" onclick="return confirm('Are you sure wants to delete?')"><span class="fa fa-trash-o"></span></a>      
                                                        </td>
                                                	</tr>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>

                                      </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo site_url('backend/logout') ?>" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                 
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap.min.js"></script>              
        <!-- END PLUGINS -->

        <!-- THIS PAGE PLUGINS -->
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>  
        <!-- END PAGE PLUGINS -->         

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins.js"></script>        
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/actions.js"></script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->         
    </body>
</html>



<script type="text/javascript">
    $("#kecamatan").change(function(){
        var id_kecamatan = $("#kecamatan option:selected").val();
        $.ajax({
            url: "<?php echo site_url('backend/kelurahan')?>",
            type: "POST",
            data    : "id_kecamatan="+id_kecamatan,
            success : function (msg) {
                document.getElementById("kelurahan").disabled = false;
                $("#kelurahan").html(msg);
                //$("#kelurahan").css("color","black");
            }
        });
    });
</script>


