<?php foreach($pasar_id->result() as $row){}?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Bahan Pokok - Disperindag</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
       <link rel="shortcut icon" href="<?php echo base_url('aset/front') ?>/images/favicon.ico">
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('aset/backend') ?>/css/theme-forest.css"/>
        <!-- EOF CSS INCLUDE -->                                      
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container page-navigation-top">            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal">
                    <li class="xn-logo">
                        <a href="<?php echo site_url('front'); ?>">SIGABAKO</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
					
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo site_url('front'); ?>">Home</a></li>
                    <li><a href="<?php echo site_url('front/bahan_pokok/'); ?>">Bahan Pokok</a></li>
                    <li class="active">Transaksi Bahan Pokok di <?php echo $row->nama_pasar; ?></li>
                </ul>
                <!-- END BREADCRUMB -->                
                
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-list"></span> Daftar Transaksi Bahan Pokok(<?php echo $row->nama_pasar; ?>)  <small><?php echo $sub_b_pokok->num_rows();?> Bahan Pokok</small></h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    
                    <div class="row">
						<div class="col-md-3">

                            <!-- LINKED LIST GROUP-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Daftar Pasar </h3>
                                </div>
                                <div class="panel-body">
                                    <div class="list-group border-bottom">
										<a href="<?php echo site_url("front/bahan_pokok/"); ?>" class="list-group-item"><i class="fa fa-list"></i>Semua Pasar</a>
										<?php foreach($pasar->result() as $ps){ ?>
                                        <a href="<?php echo site_url("front/bahan_pokok_pasar/$ps->id_pasar"); ?>" class="list-group-item"><i class="fa fa-list"></i><?php echo $ps->nama_pasar; ?></a>
										<?php } ?>
                                    </div>                              
                                </div>
                            </div>
                        </div>
						<div class="col-md-9">

                            <!-- LINKED LIST GROUP-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Daftar Transaksi Bahan Pokok di (<?php echo $row->nama_pasar; ?>)</h3>
                                </div>
                                <div class="panel-body">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Asal Pasar</th>
                                                <th>Bahan Pokok</th>
                                                <th>Harga</th>
                                                <th>Tanggal</th>
                                                <th>Satuan</th>
                                                <th>Kategori</th>
                                                <th>Foto</th>
                                            </tr>
                                        </thead>
                                        <tbody>
											<?php foreach($sub_b_pokok->result() as $row){ ?>
                                            <tr>
                                                <td><?php echo $row->id_tr_pokok ?></td>
                                                <td><?php echo $row->nama_pasar ?></td>
                                                <td><?php echo $row->nama_sub_bahan_pokok ?></td>
                                                <td>Rp.<?php echo number_format( $row->harga, 0 , ',' , '.' ); ?></td>
                                                <td><?php echo $row->tanggal; ?></td>
                                                <td><?php echo $row->satuan ?></td>
                                                <td><?php echo $row->kategori ?></td>
                                                <td>
													<?php if($row->foto_sub_b_pokok != NULL){ ?>
													<img src="<?php echo base_url("upload/sub_bahan_pokok/$row->foto_sub_b_pokok") ?>" style="width:50px;height:25px;" />
													<?php }else{ echo "Belum di Upload"; }?>
												</td>
                                            </tr>
											<?php }?>
                                        </tbody>
                                    </table>                             
                                </div>
                            </div>
                            <!-- END LINKED LIST GROUP-->                        

                        </div>
					</div>
                </div>
                <!-- END PAGE CONTENT WRAPPER --> 

				
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="pages-login.html" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->
		
		 <!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->

        <!-- THIS PAGE PLUGINS -->
		<script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>  
        <!-- END PAGE PLUGINS -->           

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins.js"></script>        
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/actions.js"></script>        
        <!-- END TEMPLATE -->
		
    <!-- END SCRIPTS -->         
    </body>
</html>






