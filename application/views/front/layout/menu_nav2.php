<div class="menu_container style_6 clearfix" style="background-color:#ffba00; border-bottom:4px solid white;">
				<nav>
				<ul class="sf-menu" >
					<li class="" style="border:none; background-color:#ffba00;">
						<a href="<?php echo site_url(); ?>" title="Home">
							Beranda
						</a>
					</li>
					<li class="" style=" border:none; background-color:#ffba00;">
						<a href="<?php echo site_url('front/about') ?>" title="Pages">
							Tentang Kami
						</a>
					</li>
					<li class="" style=" border:none; background-color:#ffba00;">
						<a href="<?php echo site_url('front/bahan_pokok') ?>" title="Pages">
							Bahan Pokok
						</a>
					</li>
					<li class="submenu mega_menu_parent" style="border:none; background-color:#ffba00;">
						<a href="#" title="Menu Berita">
							Berita
						</a>
						<ul>
							<li class="submenu">
								<a href="blog.html" title="Recent Reviews">
									Terbaru
								</a>
								<ul class="mega_menu blog">
									<?php foreach($berita->result() as $br): ?>
									<li class="post">
										<a href="post_review.html" title="New Painkiller Rekindles Addiction Concerns">
											<span class="icon"><span>8.7</span></span>
											<img src='<?php echo base_url("upload/berita/$br->cover_berita") ?>' alt='img'>
										</a>
										<h5><a href="post_review.html" title="<?php echo $br->judul; ?>"><?php echo $br->judul; ?></a></h5>
										<ul class="post_details simple">
											<li class="category"><a title="<?php echo $br->nama_kategori; ?>" href="category_health.html"><?php echo $br->nama_kategori; ?></a></li>
											<li class="date">
												<?php echo $br->tanggal_posting; ?>
											</li>
										</ul>
									</li>
									<?php endforeach; ?>
								</ul>
							</li>
						</ul>
					</li>
					<li class="submenu mega_menu_parent" style="border:none; background-color:#ffba00;">
						<a href="#" title="Menu Agenda">
							Agenda
						</a>
						<ul>
							<li class="submenu">
								<a href="blog.html" title="Most Read">
									Terbaru
								</a>
								<div class="mega_menu row">
									<div class="column column_1_2">
										<ul class="blog small">
											<?php foreach($agenda->result() as $br): ?>
											<li class="post">
												<a href="post_review.html" title="New Painkiller Rekindles Addiction Concerns">
													<span class="icon"><span>8.7</span></span>
													<img src='<?php echo base_url("upload/agenda/$br->cover_agenda") ?>' alt='img' style="width:200px;">
												</a>
												<h5><a href="post_review.html" title="<?php echo $br->judul_agenda; ?>"><?php echo $br->judul_agenda; ?></a></h5>
												<ul class="post_details simple">
													<li class="category"><a title="<?php echo $br->nama_kategori; ?>" href="category_health.html"><?php echo $br->nama_kategori; ?></a></li>
													<li class="date">
														<?php echo $br->tanggal_posting; ?>
													</li>
												</ul>
											</li>
											<?php endforeach; ?>
										</ul>
									</div>
									<div class="column column_1_2">
										<ul class="blog small">
											<?php foreach($agenda->result() as $br): ?>
											<li class="post">
												<a href="post_review.html" title="New Painkiller Rekindles Addiction Concerns">
													<span class="icon"><span>8.7</span></span>
													<img src='<?php echo base_url("upload/agenda/$br->cover_agenda") ?>' alt='img' style="width:200px;">
												</a>
												<h5><a href="post_review.html" title="<?php echo $br->judul_agenda; ?>"><?php echo $br->judul_agenda; ?></a></h5>
												<ul class="post_details simple">
													<li class="category"><a title="<?php echo $br->nama_kategori; ?>" href="category_health.html"><?php echo $br->nama_kategori; ?></a></li>
													<li class="date">
														<?php echo $br->tanggal_posting; ?>
													</li>
												</ul>
											</li>
											<?php endforeach; ?>
										</ul>
									</div>
								</div>
							</li>
						</ul>
					</li>
					<li class="" style="border:none; background-color:#ffba00;">
						<a href="<?php echo site_url(); ?>" title="Home">
							Regulasi
						</a>
					</li>
					<li class="" style="border:none; background-color:#ffba00;">
						<a href="<?php echo site_url('front/contact') ?>" title="Contact">
							Kontak
						</a>
					</li>
					<li class="" style="float:right; margin:0; padding:0; border:none; background-color:#ffba00;">
						<a href="<?php echo site_url(); ?>" title="Home">
							Login
						</a>
					</li>
				</ul>
				</nav>
				<div class="mobile_menu_container">
					<a href="#" class="mobile-menu-switch">
						<span class="line"></span>
						<span class="line"></span>
						<span class="line"></span>
					</a>
					<div class="mobile-menu-divider"></div>
					<nav>
					<ul class="mobile-menu">
						<li class="submenu selected">
							<a href="home.html" title="Home">
								Home
							</a>
							<ul>
								<li class="selected">
									<a href="home.html" title="Home Style 1">
										Home Style 1
									</a>
								</li>
								<li>
									<a href="home_2.html" title="Home Style 2">
										Home Style 2
									</a>
								</li>
								<li>
									<a href="home_3.html" title="Home Style 3">
										Home Style 3
									</a>
								</li>
								<li>
									<a href="home_4.html" title="Home Style 3">
										Home Style 4
									</a>
								</li>
								<li>
									<a href="home_5.html" title="Home Style 5">
										Home Style 5
									</a>
								</li>
								<li>
									<a href="home_6.html" title="Home Style 6">
										Home Style 6
									</a>
								</li>
								<li>
									<a href="home_7.html" title="Home Style 7">
										Home Style 7
									</a>
								</li>
							</ul>
						</li>
						<li class="submenu">
							<a href="about.html" title="Pages">
								Pages
							</a>
							<ul>
								<li>
									<a href="about.html" title="About Style 1">
										About Style 1
									</a>
								</li>
								<li>
									<a href="about_2.html" title="About Style 2">
										About Style 2
									</a>
								</li>
								<li>
									<a href="default.html" title="Default">
										Default
									</a>
								</li>
								<li>
									<a href="404.html" title="404 Not Found">
										404 Not Found
									</a>
								</li>
							</ul>
						</li>
						<li class="submenu">
							<a href="post.html" title="Post Formats">
								Post Formats
							</a>
							<ul>
								<li>
									<a href="post.html" title="Post Default">
										Post Default
									</a>
								</li>
								<li>
									<a href="post_gallery.html" title="Post Gallery">
										Post Gallery
									</a>
								</li>
								<li>
									<a href="post_small_image.html" title="Post Small Image">
										Post Small Image
									</a>
								</li>
								<li>
									<a href="post_video.html" title="Post Video YouTube">
										Post Video Youtube
									</a>
								</li>
								<li>
									<a href="post_video_2.html" title="Post Video Vimeo">
										Post Video Vimeo
									</a>
								</li>
								<li>
									<a href="post_soundcloud.html" title="Post Soundcloud">
										Post Soundcloud
									</a>
								</li>
								<li>
									<a href="post_review.html" title="Post Review Style 1">
										Post Review Style 1
									</a>
								</li>
								<li>
									<a href="post_review_2.html" title="Post Review Style 2">
										Post Review Style 2
									</a>
								</li>
								<li>
									<a href="post_quote.html" title="Post Quote Style 1">
										Post Quote Style 1
									</a>
								</li>
								<li>
									<a href="post_quote_2.html" title="Post Quote Style 2">
										Post Quote Style 2
									</a>
								</li>
							</ul>
						</li>
						<li class="submenu">
							<a href="blog.html" title="Blog">
								Blog
							</a>
							<ul>
								<li>
									<a href="blog_small_slider.html" title="Blog Small Slider">
										Blog Small Slider
									</a>
								</li>
								<li class="submenu">
									<a href="blog.html" title="Blog 1 column">
										Blog 1 Column
									</a>
									<ul>
										<li>
											<a href="blog.html" title="Blog With Right Sidebar">
												Blog With Right Sidebar
											</a>
										</li>
										<li>
											<a href="blog_left_sidebar.html" title="Blog With Left Sidebar">
												Blog With Left Sidebar
											</a>
										</li>
									</ul>
								</li>
								<li class="submenu">
									<a href="blog_2_columns.html" title="Blog 2 columns">
										Blog 2 Columns
									</a>
									<ul>
										<li>
											<a href="blog_2_columns.html" title="Right Sidebar">
												Right Sidebar
											</a>
										</li>
										<li>
											<a href="blog_2_columns_left_sidebar.html" title="Left Sidebar">
												Left Sidebar
											</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="blog_3_columns.html" title="Blog 3 Columns">
										Blog 3 Columns
									</a>
								</li>
								<li>
									<a href="search.html?s=Maecenas+Mauris" title="Search Page Template">
										Search Page Template
									</a>
								</li>
							</ul>
						</li>
						<li class="submenu">
							<a href="authors.html" title="Authors">
								Authors
							</a>
							<ul>
								<li>
									<a href="authors.html" title="Authors List">
										Authors List
									</a>
								</li>
								<li>
									<a href="author.html" title="Author Single">
										Author Single
									</a>
								</li>
							</ul>
						</li>
						<li class="submenu">
							<a href="category_health.html" title="Categories">
								Categories
							</a>
							<ul>
								<li>
									<a href="category_health.html" title="Health">
										Health
									</a>
								</li>
								<li>
									<a href="category_science.html" title="Science">
										Science
									</a>
								</li>
								<li>
									<a href="category_sports.html" title="Sports">
										Sports
									</a>
								</li>
								<li>
									<a href="category_world.html" title="World">
										World
									</a>
								</li>
								<li>
									<a href="category_lifestyle.html" title="Lifestyle">
										Lifestyle
									</a>
								</li>
							</ul>
						</li>
						<li class="">
							<a href="<?php echo site_url('front/contact') ?>" title="Contact">
								Contact
							</a>
						</li>
					</ul>
					</nav>
				</div>
			</div>	