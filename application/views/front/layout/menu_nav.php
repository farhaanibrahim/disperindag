<div class="col-md-12">
		<div class="row">
			<img src="<?php echo base_url('upload/banner3.jpg') ?>" style="width:100%; height:160px;" />
		</div>
	</div>
	<div class="col-md-12">
		<div class="row">
			<nav class="navbar navbar-default">
			  <div class="container-fluid">

			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      <ul class="nav navbar-nav">
			        <li class="active"><a href="<?php echo base_url(); ?>">Beranda <span class="sr-only">(current)</span></a></li>
			        <li class="dropdown">
						<a href="#" title="Pages" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
							Tentang Kami
						</a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" title="Perdagangan">Perdagangan</a></li>
							<li><a href="#" title="Perindustrian">Perindustrian</a></li>
							<li><a href="#" title="Metrologi">Metrologi</a></li>
						</ul>
					</li>
			        <li class="dropdown">
			   			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Profil <span class="caret"></span></a>
			   			<ul class="dropdown-menu" role="menu">
			   				<li><a href="<?php echo base_url('front/sambutan'); ?>">Sambutan</a></li>
			   				<li><a href="<?php echo base_url('front/struktur_organisasi'); ?>">Struktur Organisasi</a></li>
			   			</ul>
			   		</li>
			        <li><a href="<?php echo base_url('front/bahan_pokok'); ?>">Bahan Pokok</a></li>
			   		<li class="dropdown">
			   			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Data Perizinan<span class="caret"></span></a>
			   			<ul class="dropdown-menu" role="menu">
			   				<li><a href="<?php echo base_url('front/tdp'); ?>">Daftar Perizinan</a></li>
			   				<li><a href="<?php echo base_url('front/reg_tdp'); ?>">Regisrasi Perizinan</a></li>
			   				<!--
			   				<li><a href="<?php echo base_url('front/edit_tdp'); ?>">Edit Data Perizinan</a></li>
			   				-->
			   			</ul>
			   		</li>     
			        <li><a href="<?php echo base_url('front/event_kalender'); ?>">Kalender Event</a></li>
			        <li><a href="<?php echo base_url('front/berita'); ?>">Berita</a></li>
			        <li><a href="<?php echo base_url('front/agenda'); ?>">Agenda</a></li>
			        <li><a href="<?php echo base_url('front/regulasi'); ?>">Regulasi</a></li>
			        <li><a href="<?php echo base_url('front/contact'); ?>">Kontak</a></li>
			      </ul>
			      
			      <ul class="nav navbar-nav navbar-right">
			        <li><a href="<?php echo base_url('backend'); ?>">Login</a></li>
			      </ul>
			    </div>
			  </div>
			</nav>
		</div>
	</div>