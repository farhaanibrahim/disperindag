<div class="footer_container">
				<div class="footer clearfix">
					<div class="row">
						<div class="column column_1_2">
							<h4 class="box_header">Tentang <?php echo $ins->nama; ?></h4>
							<p class="padding_top_bottom_25"><?php echo $ins->tentang_singkat; ?></p>
							<div class="row">
								<div class="column column_1_2">
									<h5><?php echo $ins->nama; ?>.</h5>
									<p>
										<?php echo $ins->alamat; ?>
									</p>
								</div>
								<div class="column column_1_2">
									<h5>Phone</h5>
									<p>
										Phone: <?php echo $ins->no_telp; ?><br>
									</p>
								</div>
							</div>
							<h4 class="box_header page_margin_top">Sosial Media</h4>
							<ul class="social_icons dark page_margin_top clearfix">
								<li>
									<a target="_blank" title="" href="#" class="social_icon facebook">
										&nbsp;
									</a>
								</li>
								<li>
									<a target="_blank" title="" href="#" class="social_icon twitter">
										&nbsp;
									</a>
								</li>
								<li>
									<a title="" href="#" class="social_icon mail">
										&nbsp;
									</a>
								</li>
								<li>
									<a title="" href="#" class="social_icon skype">
										&nbsp;
									</a>
								</li>
								<li>
									<a title="" href="#" class="social_icon envato">
										&nbsp;
									</a>
								</li>
								<li>
									<a title="" href="#" class="social_icon instagram">
										&nbsp;
									</a>
								</li>
								<li>
									<a title="" href="#" class="social_icon pinterest">
										&nbsp;
									</a>
								</li>
							</ul>
						</div>
						<div class="column column_1_2">
							<h4 class="box_header">Berita Lama</h4>
							<div class="vertical_carousel_container clearfix">
								<ul class="blog small vertical_carousel autoplay-1 scroll-1 navigation-1 easing-easeInOutQuint duration-750">
									<?php foreach($berita->result() as $berita_row){ ?>
									<li class="post">
										<a href="post_gallery.html" title="<?php $berita_row->judul; ?>" >
											<span class="icon small gallery"></span>
											<img src='<?php echo base_url("upload/berita/$berita_row->cover_berita") ?>' alt='img' style="width:100px; height:90px;">
										</a>
										<div class="post_content">
											<h5>
												<a href="post_gallery.html" title="Study Linking Illnes and Salt Leaves Researchers Doubtful"><?php echo $berita_row->judul; ?></a>
											</h5>
											<ul class="post_details simple">
												<li class="category"><a href="<?php echo site_url("front/berita_where_kategori/$berita_row->id_kategori"); ?>" title="<?php echo $berita_row->nama_kategori; ?>"><?php echo $berita_row->nama_kategori; ?></a></li>
												<li class="date">
													<?php echo $berita_row->tanggal_posting; ?>
												</li>
											</ul>
										</div>
									</li>
									<?php } ?>
								</ul>
							</div>
						</div>
					</div>
					<div class="row page_margin_top_section">
						<div class="column column_3_4">
							<ul class="footer_menu">
								<li>
									<h4><a href="" title="World">Perdagangan</a></h4>
								</li>
								<li>
									<h4><a href="" title="Health">Perindustrian</a></h4>
								</li>
							</ul>
						</div>
						<div class="column column_1_4">
							<a class="scroll_top" href="#top" title="Scroll to top">Top</a>
						</div>
					</div>
					<div class="row copyright_row">
						<div class="column column_2_3">
							© Copyright <a href="<?php echo base_url() ?>" title="QuanticaLabs" target="_blank"> - Dinas Perdagangan Kota Bogor Template </a>. 
						</div>
						<div class="column column_1_3">
							<ul class="footer_menu">
								<li>
									<h6><a href="#" title="About">Tentang Kami</a></h6>
								</li>
								<li>
									<h6><a href="<?php echo site_url('front/contact') ?>" title="Contact Us">Kontak</a></h6>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		