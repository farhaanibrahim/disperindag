<?php foreach($instansi->result() as $ins){} ?>
<!DOCTYPE html>
<html>
	<head>
		<title>Dinas Perdagangan Kota Bogor</title>
		<!--meta-->
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.2" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="Dinas Perdagangan, Dinas Perdagangan Kota Bogor" />
		<meta name="description" content="Dinas Perdagangan Kota Bogor" />
		<!--style-->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/DataTables') ?>/media/css/jquery.dataTables.min.css">
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/reset.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/superfish.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/prettyPhoto.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/jquery.qtip.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/menu_styles.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/animations.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/responsive.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/odometer-theme-default.css">
		
		
		<!--<link rel="stylesheet" type="text/css" href="style/dark_skin.css">-->
		<!--<link rel="stylesheet" type="text/css" href="style/high_contrast_skin.css">-->
		<link rel="shortcut icon" href="<?php echo base_url('aset/front') ?>/images/favicon.ico">
	</head>
	<!--<body class="image_1">
	<body class="image_1 overlay">
	<body class="image_2">
	<body class="image_2 overlay">
	<body class="image_3">
	<body class="image_3 overlay">
	<body class="image_4">
	<body class="image_4 overlay">
	<body class="image_5">
	<body class="image_5 overlay">
	<body class="pattern_1">
	<body class="pattern_2">
	<body class="pattern_3">
	<body class="pattern_4">
	<body class="pattern_5">
	<body class="pattern_6">
	<body class="pattern_7">
	<body class="pattern_8">
	<body class="pattern_9">
	<body class="pattern_10">-->
	<body>
		<div class="site_container">
			<!--<div class="header_top_bar_container style_2 clearfix">
			<div class="header_top_bar_container style_2 border clearfix">
			<div class="header_top_bar_container style_3 clearfix">
			<div class="header_top_bar_container style_3 border clearfix">
			<div class="header_top_bar_container style_4 clearfix">
			<div class="header_top_bar_container style_4 border clearfix">
			<div class="header_top_bar_container style_5 clearfix">
			<div class="header_top_bar_container style_5 border clearfix"> -->
			<?php //include_once "layout/hearder2.php"; ?>
			<!-- <div class="menu_container style_2 clearfix">
			<div class="menu_container style_3 clearfix">
			<div class="menu_container style_... clearfix">
			<div class="menu_container style_10 clearfix">
			<div class="menu_container sticky clearfix">-->
			<?php include_once "layout/menu_nav.php"; ?>
			<!-- slider -->
			
			<div class="page">
				<?php include_once "layout/hearder.php"; ?>
				<div class="page_header clearfix page_margin_top">
					<div class="page_header_left">
						<h1 class="page_title">About Us</h1>
					</div>
					<div class="page_header_right">
						<ul class="bread_crumb">
							<li>
								<a title="Home" href="home.html">
									Home
								</a>
							</li>
							<li class="separator icon_small_arrow right_gray">
								&nbsp;
							</li>
							<li>
								Pages
							</li>
							<li class="separator icon_small_arrow right_gray">
								&nbsp;
							</li>
							<li>
								About
							</li>
						</ul>
					</div>
				</div>
				<div class="page_layout clearfix">
					<div class="divider_block clearfix">
						<hr class="divider first">
						<hr class="divider subheader_arrow">
						<hr class="divider last">
					</div>
					<div class="row page_margin_top">
						<div class="column column_1_2">
							<img class="responsive pr_preload" src='<?php echo base_url() ?>/upload/disperidag_kantor.png' alt='img'>
						</div>
						<div class="column column_1_2">
							<h1 class="about_title">Tentang</h1>
							<h2 class="about_subtitle">Dinas Perdagangan Kota Bogor</h2>
							<h3 class="page_margin_top">Dinas Perindustrian dan Perdagangan Kota Bogor merupakan Perangkat Daerah yang melaksanakan tugas urusan di Bidang Perindustrian dan Perdagangan yang memiliki Struktur Organisasi Tenaga Kerja (SOTK)sebagai berikut: Kepala Dinas, Sekretariat, Bidang Perindustrian, Bidang Perdagangan dan Bidang Metrologi..</h3>
						</div>
					</div>
					<div class="row page_margin_top">
						<div class="column column_1_1">
							<p class="text padding_top_0 page_margin_top">
							Sekretariat membawahi Sub Bagian Umum dan Kepegawaian, Sub Bagian Keuangan serta Sub Bagian Perencanaan dan Pelaporan. Bidang Perindustrian membawahi Seksi Industri Agro dan Hasil Hutan, Seksi Industri Logam, Mesin, Elektronika dan Aneka, serta Seksi Industri Kimia. Bidang Perdagangan membawahi Seksi Perdagangan Dalam Negeri, Seksi Perdagangan Luar Negeri, dan Seksi Perlindungan Konsumen. Bidang Metrologi membawahi Seksi Ukur, Arus, Panjang, Volume dan Barang Dalam Keadaan Terbungkus (BDKT), Seksi Massa dan Timbangan, serta Seksi Penyuluhan dan Pengawasan Kemetrologian. 
							</p>
						</div>
					</div>
					
					<style>
						.text{color:black; line-height:20px; }
						.text ol{margin:0 20px;}
						.text ol li{color:black; line-height:20px; margin:10px 0; list-style-type: square;}
					</style>
					<div class="row page_margin_top_section">
						
						<div class="column column_1_2">
							<ul class="accordion medium clearfix">
								<li>
									<div id="accordion-tujuan">
										<h4>Tujuan Strategi dan Arah Kebijakan</h4>
									</div>
									<ul>
										<li class="item_content clearfix">
											<div class="text" style="color:black; margin:10px 0;">
												<ol>
													<li>Meningkatnya sarana industri rumah tangga, kecil dan menengah</li>
													<li>Meningkatnya usaha industri rumah tangga, kecil dan menengah</li>
													<li>Meningkatnya produksi industri rumah tangga, kecil dan menengah</li>
													<li>Terpantaunya sistem distribusi barang dan jaringan pemasaran</li>
													<li>Meningkatnya nilai ekspor daerah</li>
													<li>Terbangunnya opini publik melalui gerakan perlindungan konsumen dengan melibatkan peran aktif seluruh stakeholder serta terselenggaranya perlindungan konsumen dan pelaku usaha</li>
													<li>Terwujudnya tertib ukur</li>
													<li>Adanya kepastian kebenaran pengukuran</li>
													<li>Meningkatnya pengetahuan dan wawasan pelaku usaha dan masyarakat kemetrologian.</li>
												</ol>
											</div>
										</li>
									</ul>
								</li>
								<li>
									<div id="accordion-sasaran">
										<h4>Sasaran Strategi dan Arah Kebijakan</h4>
									</div>
									<ul>
										<li class="item_content clearfix">
											<div class="text" style="color:black; margin:10px 0;">
												<ol>
													<li>Terpenuhinya aspek legalitas industri rumah tangga, kecil dan menengah</li>
													<li>Meningkatnya iklim usaha industri yang kondusif bagi industri rumah tangga, kecil dan menengah</li>
													<li>Meningkatnya sumber daya manusia, mutu dan standarisasi produk bagi industri rumah tangga, kecil dan menengah</li>
													<li>Terwujudnya informasi harga dan informasi distribus barang</li>
													<li>Meluasnya jaringan pemasaran ekspor</li>
													<li>Menumbuhkan tanggungjawab pelaku usaha dalam penyediaan barang/jasa serta meningkatnya kecerdasan konsumen</li>
													<li>Berkurangnya kecurangan UTTP pelaku usaha pada masyarakat</li>
													<li>Meningkatnya kepercayaan masyarakat dalam penggunaan UTTP</li>
													<li>Tersebarnya informasi tentang kemetrologian.</li>
												</ol>
											</div>
										</li>
									</ul>
								</li>
								<li>
									<div id="accordion-strategi">
										<h4>Strategi Strategi dan Arah Kebijakan</h4>
									</div>
									<ul>
										<li class="item_content clearfix">
											<div class="text" style="color:black; margin:10px 0;">
												<ol>
													<li>Meningkatkan pembinaan terhadap pelaku usaha khususnya bagi IKM agar dapat menciptakan lapangan usaha baru sehingga mendorong pertumbuhan ekonomi dibidang perindustrian dan perdagangan</li>
													<li>Menarik minat investor lokal dan investor asing dengan memperluas jaringan pemsaran dan promosi</li>
													<li>Memasyarakatkan, menerapkan dan mengembangkan teknologi terhadap para IKM.</li>
												</ol>
											</div>
										</li>
									</ul>
								</li>
							</ul>
						</div>
						<div class="column column_1_2">
							<ul class="accordion medium clearfix">
								<li>
									<div id="accordion-tupoksi">
										<h4>TUGAS POKOK DAN FUNGSI</h4>
									</div>
									<ul>
										<li class="item_content clearfix">
											<div class="text">
												<h5>Tugas Pokok</h5>
												<p>Melaksanakan sebagian urusan di bidang Perindustrian dan Perdagangan.</p>
											</div>
										</li>
										<li class="item_content clearfix">
											<div class="text">
												<h5 style="margin:0; padding:0;">Fungsi</h5>
												<ol>
													<li>Perumusan kebijakan teknis dibidang Perindustrian dan Perdagangan.</li>
													<li>Penyelenggaraan urusan pemerintahan dan pelayanan umum di bidang Perindustrian dan Perdagangan.</li>
													<li>Pembinaan dan pelaksanaan tugas di bidang Perindustrian dan Perdagangan.</li>
													<li>Pelaksanan tugas lain yang diberikan oleh Walikota sesuai tugas dan fungsinya.</li>
												</ol>
											</div>
										</li>
									</ul>
								</li>
								<li>
									<div id="accordion-cras-rutrum">
										<h4>Visi</h4>
									</div>
									<ul>
										<li class="item_content clearfix">
											<div class="text">
												Mengembangkan Potensi Industri Kecil Menengah, Perdagangan yang bernilai ekonomi tinggi dan berdaya saing
											</div>
										</li>
									</ul>
								</li>
								<li>
									<div id="accordion-donec-fermentum">
										<h4>Misi</h4>
									</div>
									<ul>
										<li class="item_content clearfix">
											<div class="text">
												<ol>
													<li>Meningkatkan kemampuan Manajerial, Skill dan Proses Produksi Industri Kecil dan Menengah.</li>
													<li>Membuka dan memberi kesempatan seluas luasnya untuk pemasaran produk dan kenyamanan berusaha.</li>
													<li>Meningkatkan kesadaran akan pentingnya penegakan perlindungan konsumen.</li>
													<li>Mengembangkan kemetrologian dan standarisasi alat ukur.</li>
												</ol>
											</div>
										</li>
									</ul>
								</li>
								
							</ul>
						</div>
					</div>
				</div>
			</div>
			
			<?php include "layout/footer.php"; ?>
		</div>
		<div class="background_overlay"></div>
		<!--js-->
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.ba-bbq.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-ui-1.11.1.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.carouFredSel-6.2.1-packed.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.touchSwipe.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.transit.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.sliderControl.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.timeago.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.hint.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.prettyPhoto.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.qtip.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.blockUI.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/main.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/odometer.min.js"></script>
		
		
		<script src="<?php echo base_url('aset/highcharts') ?>/js/highcharts.js"></script>
		<script src="<?php echo base_url('aset/highcharts') ?>/js/modules/exporting.js"></script>
		
		<script src="<?php echo base_url('aset/DataTables') ?>/media/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url('aset/DataTables') ?>/media/js/dataTables.bootstrap.js"></script>
		
		<script type="text/javascript">
		
		</script>
		
	</body>
</html>