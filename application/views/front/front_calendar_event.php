<?php foreach($instansi->result() as $ins){} ?>
<!DOCTYPE html>
<html>
	<head>
		<title>DISPERINDAG</title>
		<!--meta-->
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.2" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="Kementrian Perdagangan, Kementrian Perdagangan Kota Bogor" />
		<meta name="description" content="Kementrian Perdagangan Kota Bogor" />
		<!--style-->
		<link rel="stylesheet" href="<?php echo base_url('aset/bootstrap/css'); ?>/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/DataTables') ?>/media/css/jquery.dataTables.min.css">
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/reset.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/superfish.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/prettyPhoto.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/jquery.qtip.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/menu_styles.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/animations.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/responsive.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/odometer-theme-default.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/fullcalendar') ?>/fullcalendar.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/fullcalendar') ?>/fullcalendar.print.css" media='print' >
		
		<style>

			#calendar {
				max-width: 900px;
				margin: 0 auto;
				color:black;
				height:auto;
			}

		</style>

		<!--<link rel="stylesheet" type="text/css" href="style/dark_skin.css">-->
		<!--<link rel="stylesheet" type="text/css" href="style/high_contrast_skin.css">-->
		<link rel="shortcut icon" href="<?php echo base_url('aset/front') ?>/images/favicon.ico">
	</head>
	<body class="image_3 overlay">
		<div class="site_container">
			<?php include_once "layout/menu_nav.php"; ?>
			<!-- slider -->
			
			<div class="page" style="margin-bottom:0; padding-bottom:0;">
				<div class="page_layout clearfix">
					<div class="row page_margin_top" style="margin:0;">
						
						<div class="column column_1_1">
							<div id="calendar"></div>
							<Br>
							<Br>
							<Br>
						</div>
					</div>
				</div>
			</div>
			
			<?php include "layout/footer.php"; ?>
		</div>
		<div class="background_overlay"></div>
		<!--js-->
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.ba-bbq.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-ui-1.11.1.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.carouFredSel-6.2.1-packed.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.touchSwipe.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.transit.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.sliderControl.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.timeago.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.hint.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.prettyPhoto.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.qtip.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.blockUI.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/main.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/odometer.min.js"></script>
		
		
		<script src="<?php echo base_url('aset/highcharts') ?>/js/highcharts.js"></script>
		<script src="<?php echo base_url('aset/highcharts') ?>/js/modules/exporting.js"></script>
		
		<script src="<?php echo base_url('aset/DataTables') ?>/media/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url('aset/DataTables') ?>/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/fullcalendar') ?>/lib/moment.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/fullcalendar') ?>/fullcalendar.min.js"></script>
		<script type="text/javascript">
		$(document).ready(function() {
			//calendar event start
			<?php

			$url = "http://www.kemendag.go.id/addon/api/website_api/calendar_event";
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL ,$url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY:9b026676adc83abca0b2628e965f7d137691f34c'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "start_date='2016-10-01&end_date=2016-10-10");

			$output_cal = curl_exec($ch);
			$output_cal = str_replace('M	', '', $output_cal);
			//echo $output_cal;
			$obj_cal = json_decode($output_cal);
			
			curl_close($ch);
			
			?>
			//calendar event end
			$('#calendar').fullCalendar({
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,basicWeek,basicDay'
				},
				defaultDate: '<?php echo date('Y-m-d') ?>',
				editable: true,
				eventLimit: true, // allow "more" link when too many events
				events: [
					<?php if($obj_cal->{'status'} == 'success'){ ?>
						<?php foreach($obj_cal->{'message'} as $cal){ ?>
						{
							title: "<?php echo $cal->title; ?>",
							start: "<?php echo $cal->date_start; ?>",
							end: "<?php echo $cal->date_finish; ?>",
						},
						<?php
						}
					}?>
				]
			});
			
		});

		</script>
		
	</body>
</html>