<?php foreach($instansi->result() as $ins){} ?>
<!DOCTYPE html>
<html>
	<head>
		<title>DISPERINDAG</title>
		<!--meta-->
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.2" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="Kementrian Perdagangan, Kementrian Perdagangan Kota Bogor" />
		<meta name="description" content="Kementrian Perdagangan Kota Bogor" />
		<!--style-->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/DataTables') ?>/media/css/jquery.dataTables.min.css">
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/reset.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/superfish.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/prettyPhoto.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/jquery.qtip.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/menu_styles.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/animations.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/responsive.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/odometer-theme-default.css">
		
		
		<!--<link rel="stylesheet" type="text/css" href="style/dark_skin.css">-->
		<!--<link rel="stylesheet" type="text/css" href="style/high_contrast_skin.css">-->
		<link rel="shortcut icon" href="<?php echo base_url('aset/front') ?>/images/favicon.ico">
	</head>
	<!--<body class="image_1">
	<body class="image_1 overlay">
	<body class="image_2">
	<body class="image_2 overlay">
	<body class="image_3">
	<body class="image_3 overlay">
	<body class="image_4">
	<body class="image_4 overlay">
	<body class="image_5">
	<body class="image_5 overlay">
	<body class="pattern_1">
	<body class="pattern_2">
	<body class="pattern_3">
	<body class="pattern_4">
	<body class="pattern_5">
	<body class="pattern_6">
	<body class="pattern_7">
	<body class="pattern_8">
	<body class="pattern_9">
	<body class="pattern_10">-->
	<body class="image_3 overlay">
		<div class="site_container">
			<!--<div class="header_top_bar_container style_2 clearfix">
			<div class="header_top_bar_container style_2 border clearfix">
			<div class="header_top_bar_container style_3 clearfix">
			<div class="header_top_bar_container style_3 border clearfix">
			<div class="header_top_bar_container style_4 clearfix">
			<div class="header_top_bar_container style_4 border clearfix">
			<div class="header_top_bar_container style_5 clearfix">
			<div class="header_top_bar_container style_5 border clearfix"> -->
			
			<!--<div class="header_container small">
			<div class="header_container style_2">
			<div class="header_container style_2 small">
			<div class="header_container style_3">
			<div class="header_container style_3 small">-->
			<?php //include_once "layout/hearder2.php"; ?>
			<!-- <div class="menu_container style_2 clearfix">
			<div class="menu_container style_3 clearfix">
			<div class="menu_container style_... clearfix">
			<div class="menu_container style_10 clearfix">
			<div class="menu_container sticky clearfix">-->
			<?php include_once "layout/menu_nav.php"; ?>
			<!-- slider -->
			
			<div class="page" style="margin-bottom:0; padding-bottom:0;">
				<div class="page_layout clearfix">
					<?php include_once "layout/hearder.php"; ?>
					<div class="row page_margin_top" style="margin:0;">
						
						<div class="column column_2_3">
							<ul class="small_slider id-small_slider">
								<?php foreach($berita->result() as $berita_row){ ?>
								<li class="slide">
									<a href="post.html" title="Nuclear Fusion Closer to Becoming a Reality">
										<img src='<?php echo base_url("upload/berita/$berita_row->cover_berita") ?>' width="100%" style="height:350px;" alt='img'>
									</a>
									<div class="slider_content_box">
										<ul class="post_details simple">
											<li class="category"><a href="category_health.html" title="<?php echo $berita_row->judul; ?>"><?php echo $berita_row->nama_kategori; ?></a></li>
											<li class="date">
												<?php echo $berita_row->tanggal_posting; ?>
											</li>
										</ul>
										<h2><a href="post.html" title="<?php echo $berita_row->judul; ?>"><?php echo $berita_row->judul; ?></a></h2>
										<p class="clearfix">
											<?php echo substr(strip_tags($berita_row->isi_berita), 0, 120)."..."; ?>
										</p>
									</div>
								</li>
								<?php } ?>
							</ul>
							<div id="small_slider" class='slider_posts_list_container small'>
							</div>
							<div class="row page_margin_top_section">
								<?php /*<h4 class="box_header">Top Posts</h4> ?>
								<div class="tabs no_scroll margin_top_10 clearfix">
									<ul class="tabs_navigation small clearfix">
										<li>
											<a href="#bahan_pokok" title="Most Recent">
												Bahan Pokok Kota Bogor
											</a>
										</li>
									</ul>
									<div id="bahan_pokok" align="center"><br>
										<?php #<div id="container3" style="width:auto; height: 400px; margin:auto"></div> 
									</div>
								</div>
								*/?>
							</div>
							<div class="column column_1_1" style="margin:0; padding:0;">
							<div class="header_top_bar_container style_2 border clearfix" style="margin:0; padding:0;">
								<div class="header_top_bar">
									
									<!--<ul class="social_icons dark clearfix"><-->
									<ul class="social_icons colors clearfix">
										
										
									</ul>
									<div class="latest_news_scrolling_list_container">
										<ul>
											<li class="category">INFO HARGA BOGOR</li>
											<li class="left"><a href="#"></a></li>
											<li class="right"><a href="#"></a></li>
											<li class="posts">
												<ul class="latest_news_scrolling_list">
													<?php if($tr_b_pokok->num_rows() > 0){ ?>
														<?php foreach($tr_b_pokok->result() as $up){ ?>
														<li><a href="<?php echo site_url("front/bahan_pokok_sub/$up->id_bahan_pok") ?>" title=""><?php echo $up->nama_sub_bahan_pokok ?><?php echo ' Rp.'.number_format( $up->harga, 0 , ',' , '.' ).'/'.$up->satuan; ?></a></li>
														<?php } ?>
													<?php }else{ ?>
														<li><a>Info Bahan Pokok Belum Tersedia</a></li>
													<?php } ?>
												</ul>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<?php if($obj->{'status'} == 'success'){ ?>
						<div class="column column_1_1" style="margin:0; padding:0;">
							<div class="header_top_bar_container style_2 border clearfix" style="margin:0; padding:0;">
								<div class="header_top_bar">
									
									<div class="latest_news_scrolling_list_container">
										<ul>
											<li class="category">INFO HARGA NASIONAL</li>
											<li class="left"><a href="#"></a></li>
											<li class="right"><a href="#"></a></li>
											<li class="posts">
												<ul class="latest_news_scrolling_list">
													<?php foreach($obj->{'message'} as $m){ ?>
														<li><a><?php echo $m->nama_komoditi.' '.$m->ukuran.' '.$m->harga ?></a></li>
													<?php } ?>
												</ul>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<?php } ?>
						<?php if($obj2->{'status'} == 'success'){ ?>
						<div class="column column_1_1" style="margin:0; padding:0;">
							<div class="header_top_bar_container style_2 border clearfix" style="margin:0; padding:0;">
								<div class="header_top_bar">
									
									<!--<ul class="social_icons dark clearfix">
									<ul class="social_icons colors clearfix">-->
									
									<div class="latest_news_scrolling_list_container">
										<ul>
											<li class="category">NILAI TUKAR RUPIAH</li>
											<li class="left"><a href="#"></a></li>
											<li class="right"><a href="#"></a></li>
											<li class="posts">
												<ul class="latest_news_scrolling_list">
													<?php foreach($obj2->{'message'} as $m2){ ?>
														<li><a><?php echo $m2->kurs.', Jual : '.$m2->nilai_jual.', Beli : '.$m2->nilai_beli;  ?></a></li>
													<?php } ?>
												</ul>
											</li>
										</ul>
									</div>
									
								</div>
							</div>
						</div>
						<?php } ?>

							<h4 class="box_header">Berita</h4>
							<div class="horizontal_carousel_container page_margin_top">
								<ul class="blog horizontal_carousel autoplay-1 visible-4 scroll-1 navigation-1 easing-easeInOutQuint duration-750">
									<?php foreach($berita->result() as $berita_row){ ?>
									<li class="post">
										<a href="<?php echo site_url("front/berita_details/$berita_row->id_berita"); ?>">
											<img src='<?php echo base_url("upload/berita/$berita_row->cover_berita") ?>' alt='img' style="max-height:150px;">
										</a>
										<h5><a href="post.html" title="High Altitudes May Aid Weight Control"><?php echo $berita_row->judul; ?></a></h5>
										<ul class="post_details simple">
											<li class="category"><a href="category_health.html" title="<?php echo $berita_row->nama_kategori; ?>"><?php echo $berita_row->nama_kategori; ?></a></li>
											<li class="date">
												<?php echo $berita_row->tanggal_posting; ?>
											</li>
										</ul>
									</li>
									<?php } ?>
								</ul>
							</div>
							<br>
							<br>
							<br>
							<h4 class="box_header">Agenda</h4>
							<div class="horizontal_carousel_container page_margin_top">
								<ul class="blog horizontal_carousel autoplay-1 visible-4 scroll-1 navigation-1 easing-easeInOutQuint duration-750">
									<ul class="blog horizontal_carousel autoplay-1 visible-4 scroll-1 navigation-1 easing-easeInOutQuint duration-750">
										<?php foreach($agenda->result() as $ag){ ?>
										<li class="post">
											<a href="#" title="New Painkiller Rekindles Addiction Concerns">
												<img src='<?php echo base_url("upload/agenda/$ag->cover_agenda") ?>' alt='img' style="max-height:150px;">
											</a>
											<h5><a href="post.html" title="New Painkiller Rekindles Addiction Concerns"><?php echo $ag->judul_agenda; ?></a></h5>
											<ul class="post_details simple">
												<li class="category"><a href="category_health.html" title="<?php echo $ag->nama_kategori; ?>"><?php echo $ag->nama_kategori; ?></a></li>
												<li class="date">
													<?php echo $ag->tanggal_posting; ?>
												</li>
											</ul>
										</li>
										<?php } ?>
									</ul>
								</ul>
							</div>
						</div>
						<div class="column column_1_3">
							<ul class="blog horizontal_carousel autoplay-1 scroll-1 visible-3 navigation-1 easing-easeInOutQuint duration-750" >
								<li class="post" style="margin:0; padding:0;">
									<a href="post.html" title="New Painkiller Rekindles Addiction Concerns">
										<img src='<?php echo base_url('upload/ikm') ?>/a.png' alt='img' height="200" width="300">
									</a>
									<h5 style="margin:0;"><b><i><a style="color:red;" href="post.html" title="New Painkiller Rekindles Addiction Concerns">www.ikm.kotabogor.go.id</a></i></b></h5>
									
								</li>
								<li class="post" style="margin:0; padding:0;">
									<a href="post.html" title="New Painkiller Rekindles Addiction Concerns">
										<img src='<?php echo base_url('upload/ikm') ?>/b.png' alt='img' height="200" width="300">
									</a>
									<h5 style="margin:0;"><b><i><a style="color:red;" href="post.html" title="New Painkiller Rekindles Addiction Concerns">www.ikm.kotabogor.go.id</a></i></b></h5>
									
								</li>
							</ul>
							<br>
							<ul class="accordion medium clearfix">
								<?php foreach($pasar->result() as $ps){ ?>
								<li>
									<div id="accordion-cras-rutrumaprimer<?php echo $ps->id_pasar ?>">
										<h4><?php echo $ps->nama_pasar; ?></h4>
									</div>
									<ul>
										<li class="item_content clearfix">
										
											<p style="margin:0; padding:0;">
												<?php if($ps->foto != ""): ?>
												<img src="<?php echo base_url("upload/pasar/$ps->foto") ?>" width="100%" />
												<?php endif; ?>
											</p>
											<p style="margin:0; padding:0;">
												<?php echo $ps->alamat; ?>
											</p>
										</li>
									</ul>
								</li>
								<?php } ?>											
							</ul>
							
							<h4 class="box_header page_margin_top_section">Link Web</h4><br>
							<ul class="blog small clearfix">
								<li class="post" style="margin:0; padding:0;">
									<a href="kemendag.go.id" title="Kemendag">
										<img src='<?php echo base_url('') ?>/upload/sponsor/kemendag.jpg' alt='img'>
									</a>
									
								</li>
								<li class="post" style="margin:0; padding:0;">
									<a href="ews.kemendag.go.id" title="EWS Kemendag">
										<img src='<?php echo base_url('') ?>/upload/sponsor/ews_kemendag.png' alt='img'>
									</a>
									
								</li>
								<li class="post" style="margin:0; padding:0;">
									<a href="kemenprin.go.id" title="Kemenprin">
										<img src='<?php echo base_url('') ?>/upload/sponsor/kemenprin.png' alt='img'>
									</a>
									
								</li>
								<li class="post" style="margin:0; padding:0;">
									<a href="kotabogor.go.id" title="Kota Bogor">
										<img src='<?php echo base_url('') ?>/upload/sponsor/kotabogorweb.png' alt='img'>
									</a>
									
								</li>
								<li class="post" style="margin:0; padding:0;">
									<a href="kotabogor.go.id" title="Jawa Barat">
										<img src='<?php echo base_url('') ?>/upload/sponsor/web_jabar.png' alt='img'>
									</a>
									
								</li>
							</ul>
							
						</div>
					</div>
				</div>
			</div>
			
			<div class="page" style="margin-top:0; padding-top:0;">
				<div class="page_layout page_margin_top clearfix">
					<div class="row">
						<div class="column column_2_3">
							
							
						</div>
						<div class="column column_1_3">

						</div>
						<div class="row">
						<?php /*
						<div class="column column_1_1"><br>
							<h4 class="box_header">Galleri</h4>
							<div class="horizontal_carousel_container page_margin_top">
								<ul class="blog horizontal_carousel autoplay-1 visible-4 scroll-1 navigation-1 easing-easeInOutQuint duration-750">
									<?php foreach($gallery->result() as $gal): ?>
									<li class="post">
										<a href="post.html" title="High Altitudes May Aid Weight Control">
											<img src='<?php echo base_url("upload/gallery/$gal->gambar") ?>' alt='img' style="max-height:150px;">
										</a>
										<h5><a href="post.html" title="High Altitudes May Aid Weight Control"><?php echo $gal->judul; ?></a></h5>
										<ul class="post_details simple">
											<li class="category"><a href="#" title="<?php echo $gal->nama_kategori; ?>"><?php echo $gal->nama_kategori; ?></a></li>
											<li class="date">
												<?php echo $gal->tanggal_posting; ?>
											</li>
										</ul>
									</li>
									<?php endforeach; ?>
								</ul>
							</div>
						</div>
						*/ ?>
						</div>
					</div>
				</div>
			</div>
			<?php include "layout/footer.php"; ?>
		</div>
		<div class="background_overlay"></div>
		<!--js-->
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.ba-bbq.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-ui-1.11.1.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.carouFredSel-6.2.1-packed.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.touchSwipe.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.transit.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.sliderControl.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.timeago.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.hint.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.prettyPhoto.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.qtip.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.blockUI.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/main.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/odometer.min.js"></script>
		
		
		<script src="<?php echo base_url('aset/highcharts') ?>/js/highcharts.js"></script>
		<script src="<?php echo base_url('aset/highcharts') ?>/js/modules/exporting.js"></script>
		
		<script src="<?php echo base_url('aset/DataTables') ?>/media/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url('aset/DataTables') ?>/media/js/dataTables.bootstrap.js"></script>
		
		<script type="text/javascript">
		$(function () {
			$('#container').highcharts({
				title: {
					text: 'Bahan Pokok Nasional - <?php echo date('F, d Y'); ?>',
					x: -20 //center
				},
				subtitle: {
					text: 'Source: Sumber Test',
					x: -20
				},
				xAxis: {
					categories: ['Beras Medium', 'Gula Pasir', 'Minyak Goreng Curah', 'Tepung Terigu', 'Kedelai Impor', 'Kedelai Lokal',
						'Daging Sapi', 'Daging Ayam Broiler', 'Telur Ayam Ras', 'Cabe Merah Keriting', 'Cabe Merah Biasa', 'Bawang Merah', 'Beras Medium']
				},
				yAxis: {
					title: {
						text: 'Rupiah/Kg (Rp/Kg)'
					},
					plotLines: [{
						value: 0,
						width: 1,
						color: '#808080'
					}]
				},
				tooltip: {
					valueSuffix: 'Kg'
				},
				legend: {
					layout: 'horizontal',
					align: 'center',
					verticalAlign: 'bottom',
					borderWidth: 0
				},
				series: [ {
					name: 'Nasional',
					data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5, 30]
				}]
			});
			$('#container3').highcharts({
				title: {
					text: 'Bahan Pokok Kota Bogor - <?php echo date('F, d Y'); ?>',
					x: -20 //center
				},
				subtitle: {
					text: 'Source: Sumber Test',
					x: -20
				},
				xAxis: {
					categories: [<?php 
						foreach($b_pokok_sub->result() as $sb){ 
							echo "'".$sb->nama_sub_bahan_pokok."',";
						}
						?>]
				},
				yAxis: {
					title: {
						text: 'Rupiah/Kg (Rp/Kg)'
					},
					plotLines: [{
						value: 0,
						width: 1,
						color: '#808080'
					}]
				},
				tooltip: {
					valueSuffix: ' Rupiah/Kg'
				},
				legend: {
					layout: 'horizontal',
					align: 'center',
					verticalAlign: 'bottom',
					borderWidth: 0
				},
				series: [
					{
					name: 'Bahan Pokok Kota Bogor',
					data: [
							<?php
							foreach($b_pokok_sub->result() as $sb_res){ 
							$total = 0;
							$ht_rata = 0;
								foreach($tr_b_pokok->result() as $tr_res){ 
									if( $tr_res->id_sub_b_pokok == $sb_res->id_bahan_pok_sub ){
										$total = $total + $tr_res->harga;
										$ht_rata = $ht_rata+1;
									}
								}
								
								//echo $hasil;
								if($total > 0){
									$hasil = $total / $ht_rata;
									//echo number_format($hasil, 0, ',' , '.').',';
									echo $hasil.',';
								}else{
									echo $total.',';
								}
							}
							?>
						]
					},
				]
			});
			$('#container2').highcharts({
				title: {
					text: 'Nilai Tukar - <?php echo date('F, d Y'); ?>',
					x: -20 //center
				},
				subtitle: {
					text: 'Source: Sumber Test',
					x: -20
				},
				xAxis: {
					categories: ['AUD','BND','CAD','CHF','CNY','DKK','EUR','GBP','HKD','JPY','KRY','KWD', 'LAK','MYR','NOK','NZD','PGK','PHP','SAR']
				},
				yAxis: {
					title: {
						text: ''
					},
					plotLines: [{
						value: 0,
						width: 1,
						color: '#808080'
					}]
				},
				tooltip: {
					valueSuffix: ''
				},
				legend: {
					layout: 'horizontal',
					align: 'center',
					verticalAlign: 'bottom',
					borderWidth: 0
				},
				series: [{
					name: 'Kurs Beli',
					data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6, 12, 32, 33, 12, 43, 11, 23]
				}, {
					name: 'Kurs',
					data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5, 12, 32, 33, 12, 43, 11, 32]
				}]
			});
			$('#data_tabel').dataTable({
				"bLengthChange": false,
				"searching": false,
				"paging": false,
			});
			$( "#tabss #primer #accordion" ).accordion();
			$( "#sekunder #accordion2" ).accordion();
			$( "#tersier #accordion3" ).accordion();
			$( "#lainnya #accordion4" ).accordion();
		});
		</script>
		
	</body>
</html>