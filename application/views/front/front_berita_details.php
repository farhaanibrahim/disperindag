<?php foreach($instansi->result() as $ins){} ?>
<?php foreach($berita_details->result() as $res){} ?>
<!DOCTYPE html>
<html>
	<head>
		<title>Dinas Perdagangan Kota Bogor</title>
		<!--meta-->
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.2" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="Dinas Perdagangan, Dinas Perdagangan Kota Bogor" />
		<meta name="description" content="Dinas Perdagangan Kota Bogor" />
		<!--style-->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/DataTables') ?>/media/css/jquery.dataTables.min.css">
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/reset.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/superfish.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/prettyPhoto.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/jquery.qtip.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/menu_styles.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/animations.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/responsive.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/odometer-theme-default.css">
		
		
		<!--<link rel="stylesheet" type="text/css" href="style/dark_skin.css">-->
		<!--<link rel="stylesheet" type="text/css" href="style/high_contrast_skin.css">-->
		<link rel="shortcut icon" href="<?php echo base_url('aset/front') ?>/images/favicon.ico">
	</head>
	<!--<body class="image_1">
	<body class="image_1 overlay">
	<body class="image_2">
	<body class="image_2 overlay">
	<body class="image_3">
	<body class="image_3 overlay">
	<body class="image_4">
	<body class="image_4 overlay">
	<body class="image_5">
	<body class="image_5 overlay">
	<body class="pattern_1">
	<body class="pattern_2">
	<body class="pattern_3">
	<body class="pattern_4">
	<body class="pattern_5">
	<body class="pattern_6">
	<body class="pattern_7">
	<body class="pattern_8">
	<body class="pattern_9">
	<body class="pattern_10">-->
	<body>
		<div class="site_container">
			<!--<div class="header_top_bar_container style_2 clearfix">
			<div class="header_top_bar_container style_2 border clearfix">
			<div class="header_top_bar_container style_3 clearfix">
			<div class="header_top_bar_container style_3 border clearfix">
			<div class="header_top_bar_container style_4 clearfix">
			<div class="header_top_bar_container style_4 border clearfix">
			<div class="header_top_bar_container style_5 clearfix">
			<div class="header_top_bar_container style_5 border clearfix"> -->
			<?php include_once "layout/hearder2.php"; ?>
			<!-- <div class="menu_container style_2 clearfix">
			<div class="menu_container style_3 clearfix">
			<div class="menu_container style_... clearfix">
			<div class="menu_container style_10 clearfix">
			<div class="menu_container sticky clearfix">-->
			<?php include_once "layout/menu_nav.php"; ?>
			<!-- slider -->
			
			<div class="page">
				<?php //include_once "layout/hearder.php"; ?>
				<div class="page_header clearfix page_margin_top">
					<div class="page_header_left">
						<h1 class="page_title">Berita Terbaru</h1>
					</div>
					<div class="page_header_right">
						<ul class="bread_crumb">
							<li>
								<a title="Home" href="<?php echo base_url(); ?>">
									Home
								</a>
							</li>
							<li class="separator icon_small_arrow right_gray">
								&nbsp;
							</li>
							<li>
								<a title="Berita" href="<?php echo base_url('front/berita_details'); ?>">
									Berita
								</a>
							</li>
							<li class="separator icon_small_arrow right_gray">
								&nbsp;
							</li>
							<li>
								Detail Berita
							</li>
						</ul>
					</div>
				</div>
				<div class="page_layout page_margin_top clearfix">
					<div class="row page_margin_top">
						<div class="column column_1_1">
							<div class="horizontal_carousel_container small">
								<ul class="blog horizontal_carousel autoplay-1 scroll-1 visible-3 navigation-1 easing-easeInOutQuint duration-750">
									<?php foreach($berita->result() as $ber){ ?>
									<li class="post">
										<a href="<?php echo site_url("front/berita_details/$ber->id_berita"); ?>" title="<?php echo $ber->judul ?>">
											<img src='<?php echo base_url("upload/berita/$ber->cover_berita") ?>' alt='img' style="max-height:150px;">
										</a>
										<h5><a href="post.html" title="New Painkiller Rekindles Addiction Concerns"><?php echo $ber->judul ?></a></h5>
										<ul class="post_details simple">
											<li class="category"><a href="category_health.html" title="HEALTH"><?php echo $ber->nama_kategori ?></a></li>
											<li class="date">
												<?php echo $ber->tanggal_posting; ?>
											</li>
										</ul>
									</li>
									<?php } ?>
								</ul>
							</div>
						</div>
					</div>
					<hr class="divider page_margin_top">
					<div class="row page_margin_top">
						<div class="column column_2_3">
							<div class="row">
								<div class="post single small_image">
									<h1 class="post_title">
										<?php echo $res->judul ?>
									</h1>
									<ul class="post_details clearfix">
										<li class="detail category">In <a href="#" title="<?php echo $res->nama_kategori ?>"><?php echo $res->nama_kategori ?></a></li>
										<li class="detail date"><?php echo $res->tanggal_posting ?></li>
									</ul>
									<div class="post_content page_margin_top_section clearfix">
										<div class="content_box">
											<div class="post_image_box">
												<a href="<?php echo base_url("upload/berita/$res->cover_berita") ?>" class="post_image prettyPhoto" title="">
													<img src='<?php echo base_url("upload/berita/$res->cover_berita") ?>' alt='img'>
												</a>
											</div>
											<div class=""><?php echo $res->isi_berita ?></div>
											
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="column column_1_3">
							<h4 class="box_header page_margin_top_section">Berita Lainnya</h4>
							<div class="vertical_carousel_container clearfix">
								<ul class="blog small vertical_carousel autoplay-1 scroll-1 navigation-1 easing-easeInOutQuint duration-750">
									<?php foreach($berita->result() as $ber){ ?>
									<li class="post">
										<a href="#" title="<?php echo $ber->judul; ?>">
											<span class="icon small gallery"></span>
											<img src='<?php echo base_url("upload/berita/$ber->cover_berita") ?>' alt='img' style="max-width:100px; max-height:150px;">
										</a>
										<div class="post_content">
											<h5>
												<a href="post_gallery.html" title="Study Linking Illnes and Salt Leaves Researchers Doubtful"><?php echo substr(strip_tags($ber->judul), 0, 50).".."; ?></a>
											</h5>
											<ul class="post_details simple">
												<li class="category"><a href="<?php echo site_url("front/berita_where_kategori/$ber->id_kategori"); ?>" title="<?php echo $ber->nama_kategori; ?>"><?php echo $ber->nama_kategori; ?></a></li>
												<li class="date">
													<?php echo $ber->tanggal_posting; ?>
												</li>
											</ul>
										</div>
									</li>
									<?php } ?>
								</ul>
							</div>
							<h4 class="box_header page_margin_top_section">Kategori Berita</h4>
							<ul class="taxonomies columns clearfix page_margin_top">
								<?php foreach($katg_berita->result() as $ktg): ?>
								<li>
									<a href="<?php echo site_url("front/berita_where_kategori/$ktg->id_kategori"); ?>" title="<?php echo $ktg->nama_kategori ?>"><?php echo $ktg->nama_kategori ?></a>
								</li>
								<?php endforeach; ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			
			<?php include "layout/footer.php"; ?>
		</div>
		<div class="background_overlay"></div>
		<!--js-->
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.ba-bbq.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-ui-1.11.1.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.carouFredSel-6.2.1-packed.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.touchSwipe.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.transit.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.sliderControl.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.timeago.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.hint.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.prettyPhoto.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.qtip.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.blockUI.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/main.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/odometer.min.js"></script>
		
		
		<script src="<?php echo base_url('aset/highcharts') ?>/js/highcharts.js"></script>
		<script src="<?php echo base_url('aset/highcharts') ?>/js/modules/exporting.js"></script>
		
		<script src="<?php echo base_url('aset/DataTables') ?>/media/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url('aset/DataTables') ?>/media/js/dataTables.bootstrap.js"></script>
		
		<script type="text/javascript">
		
		</script>
		
	</body>
</html>