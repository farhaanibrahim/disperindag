<?php 
foreach($instansi->result() as $ins){} 
if(isset($katg)){
	foreach($katg->result() as $katg_row){} 
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Dinas Perdagangan Kota Bogor</title>
		<!--meta-->
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.2" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="Dinas Perdagangan, Dinas Perdagangan Kota Bogor" />
		<meta name="description" content="Dinas Perdagangan Kota Bogor" />
		<!--style-->
		<link rel="stylesheet" href="<?php echo base_url('aset/bootstrap/css'); ?>/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/DataTables') ?>/media/css/jquery.dataTables.min.css">
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/reset.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/superfish.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/prettyPhoto.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/jquery.qtip.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/menu_styles.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/animations.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/responsive.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/odometer-theme-default.css">
		
		
		<!--<link rel="stylesheet" type="text/css" href="style/dark_skin.css">-->
		<!--<link rel="stylesheet" type="text/css" href="style/high_contrast_skin.css">-->
		<link rel="shortcut icon" href="<?php echo base_url('aset/front') ?>/images/favicon.ico">
	</head>
	<!--<body class="image_1">
	<body class="image_1 overlay">
	<body class="image_2">
	<body class="image_2 overlay">
	<body class="image_3">
	<body class="image_3 overlay">
	<body class="image_4">
	<body class="image_4 overlay">
	<body class="image_5">
	<body class="image_5 overlay">
	<body class="pattern_1">
	<body class="pattern_2">
	<body class="pattern_3">
	<body class="pattern_4">
	<body class="pattern_5">
	<body class="pattern_6">
	<body class="pattern_7">
	<body class="pattern_8">
	<body class="pattern_9">
	<body class="pattern_10">-->
	<body>
		<div class="site_container">
			<?php include_once "layout/menu_nav.php"; ?>
			<!-- slider -->
			
			<div class="page">
				<div class="page_layout clearfix">
					<div class="page_header_left">
						<h1 class="page_title">Agenda <?php if(isset($katg)){echo $katg_row->nama_kategori;} ?></h1>
					</div>
					<div class="page_header_right">
						<ul class="bread_crumb">
							<li>
								<a title="Home" href="<?php echo base_url(); ?>">
									Home
								</a>
							</li>
							<li class="separator icon_small_arrow right_gray">
								&nbsp;
							</li>
							<li class="separator icon_small_arrow right_gray">
								&nbsp;
							</li>
							<li>
								Agenda
							</li>
						</ul>
					</div>
				</div>
				<div class="page_layout clearfix">
					<div class="divider_block clearfix">
						<hr class="divider first">
						<hr class="divider subheader_arrow">
						<hr class="divider last">
					</div>
					<div class="row">
						<div class="column column_2_3">
							<div class="row">
								<ul class="blog big">
									<?php foreach($agenda_pagination->result() as $res){?>
									<li class="post">
										<a href="<?php echo site_url("front/agenda_details/$res->id_agenda"); ?>" title="<?php echo $res->judul_agenda; ?>">
											<img src='<?php echo base_url("upload/agenda/$res->cover_agenda") ?>' alt='img' style="max-width:300px; min-height:200px;">
										</a>
										<div class="post_content">
											<h2 class="with_number">
												<a href="<?php echo site_url("front/agenda_details/$res->id_agenda"); ?>" title="Built on Brotherhood, Club Lives Up to Name"><?php echo $res->judul_agenda; ?></a>
											</h2>
											<ul class="post_details">
												<li class="category"><a href="<?php echo site_url("front/agenda_where_kategori/$res->id_kategori"); ?>" title="<?php echo $res->nama_kategori; ?>"><?php echo $res->nama_kategori; ?></a></li>
												<li class="date">
													<?php echo $res->tanggal_posting; ?>
												</li>
											</ul>
											<p><?php echo substr(strip_tags($res->isi_agenda), 0, 180)."..."; ?></p>
											<a class="read_more" href="<?php echo site_url("front/agenda_details/$res->id_agenda"); ?>" title="Read more"><span class="arrow"></span><span>READ MORE</span></a>
										</div>
									</li>
									<?php } ?>
								</ul>
							</div>
							<ul class="pagination clearfix page_margin_top_section">
								
								<?php echo $pagination; ?>
							</ul>
						</div>
						<div class="column column_1_3 page_margin_top">
							<h4 class="box_header">Latest Posts</h4>
							<div class="vertical_carousel_container clearfix">
								<ul class="blog small vertical_carousel autoplay-1 scroll-1 navigation-1 easing-easeInOutQuint duration-750">
									<?php foreach($agenda->result() as $ber){ ?>
									<li class="post">
										<a href="#" title="Study Linking Illnes and Salt Leaves Researchers Doubtful">
											<span class="icon small gallery"></span>
											<img src='<?php echo base_url("upload/agenda/$ber->cover_agenda") ?>' alt='img' style="max-width:100px; max-height:150px;">
										</a>
										<div class="post_content">
											<h5>
												<a href="<?php echo site_url("front/agenda_details/$ber->id_agenda"); ?>" title="Study Linking Illnes and Salt Leaves Researchers Doubtful"><?php echo substr(strip_tags($ber->judul_agenda), 0, 100).".."; ?></a>
											</h5>
											<ul class="post_details simple">
												<li class="category"><a href="<?php echo site_url("front/agenda_where_kategori/$ber->id_kategori"); ?>" title="<?php echo $ber->nama_kategori; ?>"><?php echo $ber->nama_kategori; ?></a></li>
												<li class="date">
													<?php echo $ber->tanggal_posting; ?>
												</li>
											</ul>
										</div>
									</li>
									<?php } ?>
								</ul>
							</div>
							<h4 class="box_header page_margin_top_section">Kategori Agenda</h4>
							<ul class="taxonomies columns clearfix page_margin_top">
								<?php foreach($katg_agenda->result() as $ktg): ?>
								<li>
									<a href="<?php echo site_url("front/agenda_where_kategori/$ktg->id_kategori"); ?>" title="<?php echo $ktg->nama_kategori ?>"><?php echo $ktg->nama_kategori ?></a>
								</li>
								<?php endforeach; ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			
			<?php include "layout/footer.php"; ?>
		</div>
		<div class="background_overlay"></div>
		<!--js-->
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.ba-bbq.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-ui-1.11.1.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.carouFredSel-6.2.1-packed.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.touchSwipe.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.transit.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.sliderControl.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.timeago.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.hint.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.prettyPhoto.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.qtip.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.blockUI.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/main.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/odometer.min.js"></script>
		
		
		<script src="<?php echo base_url('aset/highcharts') ?>/js/highcharts.js"></script>
		<script src="<?php echo base_url('aset/highcharts') ?>/js/modules/exporting.js"></script>
		
		<script src="<?php echo base_url('aset/DataTables') ?>/media/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url('aset/DataTables') ?>/media/js/dataTables.bootstrap.js"></script>
		
		<script type="text/javascript">
		
		</script>
		
	</body>
</html>