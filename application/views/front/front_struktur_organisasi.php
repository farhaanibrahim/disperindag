<?php 
/*
        // create curl resource 
        $ch = curl_init(); 

        // set url 
        curl_setopt($ch, CURLOPT_URL, "http://ikm.kotabogor.go.id/api/list_t_potensi.php"); 

        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

        // $output contains the output string 
        $potensi = curl_exec($ch); 
		$obj_pot = json_decode($potensi);
        // close curl resource to free up system resources 
        curl_close($ch);  
*/
?>
<?php

$start = date("Y-m-d", mktime(0,0,0,date("m"),date("d")-7,date("Y")) );
$end = date("Y-m-d");
$url = "http://www.kemendag.go.id/addon/api/website_api/harga_bapok";
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL ,$url);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY:9b026676adc83abca0b2628e965f7d137691f34c'));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, "start_date=$start&end_date=$end");

$output = curl_exec($ch);
$output = str_replace('M	', '', $output);

$obj = json_decode($output);
 
curl_close($ch);

?>
<?php

$url = "http://www.kemendag.go.id/addon/api/website_api/daily_currency";
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL ,$url);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY:9b026676adc83abca0b2628e965f7d137691f34c'));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$output2 = curl_exec($ch);
$output2 = str_replace('M	', '', $output2);

$obj2 = json_decode($output2);

curl_close($ch);

?>
<?php foreach($instansi->result() as $ins){} ?>
<!DOCTYPE html>
<html>
	<head>
		<title>DISPERINDAG</title>
		<!--meta-->
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.2" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="Kementrian Perdagangan, Kementrian Perdagangan Kota Bogor" />
		<meta name="description" content="Kementrian Perdagangan Kota Bogor" />
		<!--style-->
		
		<link rel="stylesheet" href="<?php echo base_url('aset/bootstrap/css'); ?>/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/reset.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/superfish.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/prettyPhoto.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/jquery.qtip.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/menu_styles.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/animations.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/responsive.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/odometer-theme-default.css">
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/DataTables') ?>/media/css/jquery.dataTables.min.css">
		
		<!--<link rel="stylesheet" type="text/css" href="style/dark_skin.css">-->
		<!--<link rel="stylesheet" type="text/css" href="style/high_contrast_skin.css">-->
		<link rel="shortcut icon" href="<?php echo base_url('aset/front') ?>/images/favicon.ico">
	</head>
	<body>
		<div class="site_container">
			<?php include_once "layout/menu_nav.php"; ?>
			<!-- slider -->

			<div class="col-md-8">
				<div class="panel panel-default">
							  <div class="panel-heading">
							    <h3 class="panel-title">Struktur Organisasi</h3>
							  </div>
							  <div class="panel-body">
							    <h2>Struktur Organisasi</h2>
								<br><br>
								<table class="table table-bordered" style="color:black;">
											<thead>
												<tr>
													<th>No. </th>
													<th>Foto</th>
													<th>Nama</th>
													<th>Jabatan</th>
												</tr>
											</thead>
											<tbody>
												<?php
												$no = 1;
												foreach($people->result() as $row)
												{
													?>
													<tr>
														<td><?php echo $no; ?></td>
														<td><img class="img img-thumbnail" src="<?php echo base_url('upload'); ?>/<?php echo $row->foto; ?>" style="width:150px;"></td>
														<td><?php echo $row->nama; ?></td>
														<td><?php echo $row->jabatan; ?></td>
													</tr>
													<?php
													$no++;
												}
												?>
											</tbody>
										</table>
							  </div>
							</div>
			</div>
			
			<div class="col-md-4">
				<ul class="blog horizontal_carousel autoplay-1 scroll-1 visible-3 navigation-1 easing-easeInOutQuint duration-750" >
								<li class="post" style="margin:0; padding:0;">
									<a href="post.html" title="New Painkiller Rekindles Addiction Concerns">
										<img src='<?php echo base_url('upload/ikm') ?>/a.png' alt='img' height="200" width="300">
									</a>
									<h5 style="margin:0;"><b><i><a style="color:red;" href="post.html" title="New Painkiller Rekindles Addiction Concerns">www.ikm.kotabogor.go.id</a></i></b></h5>
									
								</li>
								<li class="post" style="margin:0; padding:0;">
									<a href="post.html" title="New Painkiller Rekindles Addiction Concerns">
										<img src='<?php echo base_url('upload/ikm') ?>/b.png' alt='img' height="200" width="300">
									</a>
									<h5 style="margin:0;"><b><i><a style="color:red;" href="post.html" title="New Painkiller Rekindles Addiction Concerns">www.ikm.kotabogor.go.id</a></i></b></h5>
									
								</li>
							</ul>

							<div class="panel panel-default">
							  <div class="panel-heading">
							    <h3 class="panel-title">Pasar</h3>
							  </div>
							  <div class="panel-body">
							    <ul class="accordion medium clearfix">
								<?php foreach($pasar->result() as $ps){ ?>
								<li>
									<div id="accordion-cras-rutrumaprimer<?php echo $ps->id_pasar ?>">
										<h4><?php echo $ps->nama_pasar; ?></h4>
									</div>
									<ul>
										<li class="item_content clearfix">
										
											<p style="margin:0; padding:0;">
												<?php if($ps->foto != ""): ?>
												<img src="<?php echo base_url("upload/pasar/$ps->foto") ?>" width="100%" />
												<?php endif; ?>
											</p>
											<p style="margin:0; padding:0;">
												<?php echo $ps->alamat; ?>
											</p>
										</li>
									</ul>
								</li>
								<?php } ?>											
							</ul>
							  </div>
							</div>
							

							<div class="panel panel-default">
							  <div class="panel-heading">
							    <h3 class="panel-title">Link Web</h3>
							  </div>
							  <div class="panel-body">
							    <ul class="blog small clearfix">
								<li class="post" style="margin:0; padding:0; max-width: 100%;">
									<a href="http://www.kemendag.go.id" title="Kemendag">
										<img src='<?php echo base_url('') ?>/upload/sponsor/kemendag.jpg' alt='img'>
									</a>
									
								</li>
								<li class="post" style="margin:0; padding:0;">
									<a href="http://ews.kemendag.go.id" title="EWS Kemendag">
										<img src='<?php echo base_url('') ?>/upload/sponsor/ews_kemendag.png' alt='img'>
									</a>
									
								</li>
								<li class="post" style="margin:0; padding:0;">
									<a href="http://kemenprin.go.id" title="Kemenprin">
										<img src='<?php echo base_url('') ?>/upload/sponsor/kemenprin.png' alt='img'>
									</a>
									
								</li>
								<li class="post" style="margin:0; padding:0;">
									<a href="http://kotabogor.go.id" title="Kota Bogor">
										<img src='<?php echo base_url('') ?>/upload/sponsor/kotabogorweb.png' alt='img'>
									</a>
									
								</li>
								<li class="post" style="margin:0; padding:0;">
									<a href="http://www.jabarprov.go.id" title="Jawa Barat">
										<img src='<?php echo base_url('') ?>/upload/sponsor/web_jabar.png' alt='img'>
									</a>
									
								</li>
							</ul>
							  </div>
							</div>

							<div class="panel panel-default">
							  <div class="panel-heading">
							    <h3 class="panel-title">Statistik Pengunjung</h3>
							  </div>
							  <div class="panel-body">
							    <table style="color: black;">
									<tr>
										<th>Today</th>
										<th>:</th>
										<th><?php echo $visitor->num_rows(); ?></th>
									</tr>
								</table>
							  </div>
							</div>


			</div>

			<div class="col-md-12">
				<div class="row">
					<?php include "layout/footer.php"; ?>
				</div>
			</div>
			
		<div class="background_overlay"></div>
		<!--js-->
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.ba-bbq.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-ui-1.11.1.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.carouFredSel-6.2.1-packed.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.touchSwipe.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.transit.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.sliderControl.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.timeago.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.hint.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.prettyPhoto.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.qtip.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.blockUI.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/main.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/odometer.min.js"></script>
		
		
		<script src="<?php echo base_url('aset/DataTables') ?>/media/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url('aset/DataTables') ?>/media/js/dataTables.bootstrap.js"></script>
		
		<script src="<?php echo base_url('aset/highcharts') ?>/js/highcharts.js"></script>
		<script src="<?php echo base_url('aset/highcharts') ?>/js/modules/exporting.js"></script>
		
		
		
		
		<script type="text/javascript">
		
		$(document).ready(function () {
			$('#data_tabel').dataTable({
				
				"bLengthChange": false,
				"iDisplayLength": 7
			});
			
			
			$( "#tabss #primer #accordion" ).accordion();
			$( "#sekunder #accordion2" ).accordion();
			$( "#tersier #accordion3" ).accordion();
			$( "#lainnya #accordion4" ).accordion();
		});
		</script>
		
	</body>
</html>