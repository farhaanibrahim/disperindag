<?php foreach($instansi->result() as $ins){} ?>
<!DOCTYPE html>
<html>
	<head>
		<title>Kementrian Perdagangan Kota Bogor - Home</title>
		<!--meta-->
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.2" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="Kementrian Perdagangan, Kementrian Perdagangan Kota Bogor" />
		<meta name="description" content="Kementrian Perdagangan Kota Bogor" />
		<!--style-->
		<link rel="stylesheet" href="<?php echo base_url('aset/bootstrap/css'); ?>/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/DataTables') ?>/media/css/jquery.dataTables.min.css">
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/reset.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/superfish.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/prettyPhoto.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/jquery.qtip.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/menu_styles.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/animations.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/responsive.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/odometer-theme-default.css">

		
		<!--<link rel="stylesheet" type="text/css" href="style/dark_skin.css">-->
		<!--<link rel="stylesheet" type="text/css" href="style/high_contrast_skin.css">-->
		<link rel="shortcut icon" href="<?php echo base_url('aset/front') ?>/images/favicon.ico">
	</head>
	<!--<body class="image_1">
	<body class="image_1 overlay">
	<body class="image_2">
	<body class="image_2 overlay">
	<body class="image_3">
	<body class="image_3 overlay">
	<body class="image_4">
	<body class="image_4 overlay">
	<body class="image_5">
	<body class="image_5 overlay">
	<body class="pattern_1">
	<body class="pattern_2">
	<body class="pattern_3">
	<body class="pattern_4">
	<body class="pattern_5">
	<body class="pattern_6">
	<body class="pattern_7">
	<body class="pattern_8">
	<body class="pattern_9">
	<body class="pattern_10">-->
	<body>
		<div class="site_container">
			<div class="header_top_bar_container clearfix">
				<div class="header_top_bar">
					<form class="search" action="search.html" method="get">
						<input type="text" name="s" placeholder="Search..." value="Search..." class="search_input hint">
						<input type="submit" class="search_submit" value="">
					</form>
					<!--<ul class="social_icons dark clearfix">
					<ul class="social_icons colors clearfix">-->
					<ul class="social_icons clearfix">
						<li>
							<a target="_blank" href="http://facebook.com/QuanticaLabs" class="social_icon facebook" title="facebook">
								&nbsp;
							</a>
						</li>
						<li>
							<a target="_blank" href="https://twitter.com/QuanticaLabs" class="social_icon twitter" title="twitter">
								&nbsp;
							</a>
						</li>
						<li>
							<a href="mailto:contact@Kementrian Perdagangan Kota Bogor.com" class="social_icon mail" title="mail">
								&nbsp;
							</a>
						</li>
						<li>
							<a href="http://themeforest.net/user/QuanticaLabs/portfolio" class="social_icon envato" title="envato">
								&nbsp;
							</a>
						</li>
					</ul>
				</div>
			</div>
			<?php include_once "layout/menu_nav.php"; ?>
			<!-- slider -->
			
			<div class="col-md-12" style="color: black;">
				<div class="container">
					<div class="row">
						<div class="page_layout clearfix">
					<div class="page_header_left">
						<h1 class="page_title">Tanda Daftar Perusahaan</h1>
					</div>
					<div class="page_header_right">
						<ul class="bread_crumb">
							<li>
								<a title="Home" href="home.html">
									Home
								</a>
							</li>
							<li class="separator icon_small_arrow right_gray">
								&nbsp;
							</li>
							<li>
								Perizinan
							</li>
						</ul>
					</div>
				</div>

				<div class="divider_block clearfix">
					<hr class="divider first">
					<hr class="divider subheader_arrow">
					<hr class="divider last">
				</div>
				<br><br>

						<ul class="nav nav-tabs">
							<li class="active"><a href="#siup" data-toggle="tab" aria-expanded="true">SIUP</a></li>
							<li class=""><a href="#tdp" data-toggle="tab" aria-expanded="false">TDP</a></li>
							<li class=""><a href="#iupr" data-toggle="tab" aria-expanded="false">IUPR</a></li>
							<li class=""><a href="#iupp" data-toggle="tab" aria-expanded="false">IUPP</a></li>
							<li class=""><a href="#iuts" data-toggle="tab" aria-expanded="false">IUTS</a></li>
							<li class=""><a href="#stpw" data-toggle="tab" aria-expanded="false">STPW</a></li>
							<li class=""><a href="#tdg" data-toggle="tab" aria-expanded="false">TDG</a></li>
							<li class=""><a href="#siupmb" data-toggle="tab" aria-expanded="false">SIUP-MB</a></li>
							<li class=""><a href="#tdpud" data-toggle="tab" aria-expanded="false">TDPUD</a></li>
							<li class=""><a href="#b3" data-toggle="tab" aria-expanded="false">B3</a></li>
						</ul>
						<div id="myTabContent" class="tab-content">
						  <div class="tab-pane fade active in" id="siup">
						  	<br><br><br>
						    
						  	<table id="table" class="table table-bordered">
		                        <thead>
		                            <tr>
		                                <th>No.</th>
		                                <th>Foto</th>
		                                <th>Nama Perusahaan</th>
		                                <th>Nama Penanggung Jawab</th>
		                                <th>No. Telp & Fax</th>
		                                <th>Modal Usaha</th>
		                                <th>Kelembagaan</th>
		                                <th>Kegiatan Usaha (KBLI)</th>
		                                <th>No. SIUP & Tanggal SIUP</th>
		                                <th>Masa Berlaku</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                        	<?php $no=1; ?>
		                        	<?php foreach ($siup->result() as $siup): ?>
		                        		<tr>
		                        			<td><?php echo $no; ?></td>
		                        			<td><img class="img img-thumbnail" src="<?php echo base_url('upload'); ?>/<?php echo $siup->foto ?>"></td>
		                        			<td><?php echo $siup->nm_perusahaan; ?></td>
		                        			<td><?php echo $siup->nm_penanggungjwb; ?></td>
		                        			<td><?php echo $siup->no_telp." & ".$siup->no_fax;; ?></td>
		                        			<td><?php echo $siup->modal_usaha; ?></td>
		                        			<td><?php echo $siup->kelembagaan; ?></td>
		                        			<td><?php echo $siup->kbli; ?></td>
		                        			<td><?php echo $siup->no_siup." & ".$siup->tgl_siup; ?></td>
		                        			<td><?php echo $siup->masaberlaku; ?></td>
		                        		</tr>
		                        		<?php $no++; ?>
		                        	<?php endforeach ?>
		                        </tbody>
		                    </table>

						  </div>
						  
						  <div class="tab-pane fade" id="tdp">

						  	<br><br>
						  	<table class="table table-bordered" id="table2">
						  		<thead>
						  			<tr>
						  				<th>No.</th>
						  				<th>Foto</th>
						  				<th>Nama Perusahaan</th>
						  				<th>Nama Penanggung Jawab</th>
						  				<th>Alamat Perusahaan</th>
						  				<th>NPWP</th>
						  				<th>No. Telp & Fax</th>
						  				<th>Kegiatan Usaha Pokok</th>
						  				<th>KBLI</th>
						  				<th>Status Kantor</th>
						  				<th>No. TDP & Tanggal TDP</th>
						  				<th>Masa Berlaku</th>
						  			</tr>
						  		</thead>
						  		<tbody>
						  		<?php $no2=1; ?>
						  		<?php foreach ($tdp->result() as $tdp): ?>
						  			<tr>
						  				<td><?php echo $no2; ?></td>
						  				<td><img class="img img-thumbnail" src="<?php echo base_url('upload'); ?>/<?php echo $tdp->foto ?>"></td>
						  				<td><?php echo $tdp->nm_perusahaan; ?></td>
						  				<td><?php echo $tdp->nm_penanggungjwb; ?></td>
						  				<td><?php echo $tdp->almt_perusahaan; ?></td>
						  				<td><?php echo $tdp->npwp; ?></td>
						  				<td><?php echo $tdp->no_telp." & ".$tdp->no_fax; ?></td>
						  				<td><?php echo $tdp->keg_usaha_pokok; ?></td>
						  				<td><?php echo $tdp->kbli; ?></td>
						  				<td><?php echo $tdp->status_kantor; ?></td>
						  				<td><?php echo $tdp->no_tdp." & ".$tdp->tgl_tdp; ?></td>
						  				<td><?php echo $tdp->masaberlaku; ?></td>
						  			</tr>
						  			<?php $no2++; ?>
						  		<?php endforeach ?>
						  		</tbody>
						  	</table>

						  </div>

						  <div class="tab-pane fade" id="iupr">

						  	<br><br>
						  	<table class="table table-bordered" id="table3">
						  		<thead>
						  			<tr>
						  				<th>No.</th>
						  				<th>Foto</th>
						  				<th>Tanggal Pendaftaran</th>
						  				<th>Nama Perusahaan</th>
						  				<th>Nama Penanggung Jawab</th>
						  				<th>Alamat Perusahaan</th>
						  				<th>Jenis Usaha</th>
						  				<th>SIUP</th>
						  				<th>TDP</th>
						  				<th>No. IUPR & Tanggal IUPR</th>
						  			</tr>
						  		</thead>
						  		<tbody>
						  		<?php $no3=1; ?>
						  		<?php foreach ($iupr->result() as $iupr): ?>
						  			<tr>
						  				<td><?php echo $no3; ?></td>
						  				<td><img class="img img-thumbnail" src="<?php echo base_url('upload'); ?>/<?php echo $iupr->foto ?>"></td>
						  				<td><?php echo $iupr->tgl_pendaftaran ?></td>
						  				<td><?php echo $iupr->nm_perusahaan ?></td>
						  				<td><?php echo $iupr->nm_penanggungjwb ?></td>
						  				<td><?php echo $iupr->almt_perusahaan ?></td>
						  				<td><?php echo $iupr->jenis_usaha ?></td>
						  				<td><?php echo $iupr->siup ?></td>
						  				<td><?php echo $iupr->tdp ?></td>
						  				<td><?php echo $iupr->no_iupr." & ".$iupr->tgl_iupr ?></td>
						  			</tr>
						  			<?php $no3++; ?>
						  		<?php endforeach ?>
						  		</tbody>
						  	</table>

						  </div>

						  <div class="tab-pane fade" id="iupp">

						  	<br><br>
						  	<table class="table table-bordered" id="table4">
						  		<thead>
						  			<tr>
						  				<th>No.</th>
						  				<th>Foto</th>
						  				<th>Tanggal Pendaftaran</th>
						  				<th>Nama Perusahaan</th>
						  				<th>Nama Penanggung Jawab</th>
						  				<th>Alamat Perusahaan</th>
						  				<th>Jenis Usaha</th>
						  				<th>SIUP</th>
						  				<th>TDP</th>
						  				<th>No. IUPP & tanggal IUPP</th>
						  			</tr>
						  		</thead>
						  		<tbody>
						  		<?php $no4=1; ?>
						  		<?php foreach ($iupp->result() as $iupp): ?>
						  			<tr>
						  				<td><?php echo $no4; ?></td>
						  				<td><img class="img img-thumbnail" src="<?php echo base_url('upload'); ?>/<?php echo $iupp->foto ?>"></td>
						  				<td><?php echo $iupp->tgl_pendaftaran ?></td>
						  				<td><?php echo $iupp->nm_perusahaan ?></td>
						  				<td><?php echo $iupp->nm_penanggungjwb ?></td>
						  				<td><?php echo $iupp->almt_perusahaan ?></td>
						  				<td><?php echo $iupp->jenis_usaha ?></td>
						  				<td><?php echo $iupp->siup ?></td>
						  				<td><?php echo $iupp->tdp ?></td>
						  				<td><?php echo $iupp->no_iupp." & ".$iupp->tgl_iupp ?></td>
						  			</tr>
						  			<?php $no4++; ?>
						  		<?php endforeach ?>
						  		</tbody>
						  	</table>

						  </div>

						  <div class="tab-pane fade" id="iuts">

						  	<br><br>
						  	<table class="table table-bordered" id="table5">
						  		<thead>
						  			<tr>
						  				<th>No.</th>
						  				<th>Foto</th>
						  				<th>Nama Perusahaan Pemberi</th>
						  				<th>Nama Penanggung Jawab</th>
						  				<th>Alamat Perusahaan</th>
						  				<th>NPWP</th>
						  				<th>No. Telp & Fax</th>
						  				<th>Kegiatan Usaha Pokok</th>
						  				<th>KBLI</th>
						  				<th>Status Kantor</th>
						  				<th>SIUP</th>
						  				<th>TDP</th>
						  				<th>No. IUTS & tanggal IUTS</th>
						  				<th>Masa Berlaku</th>
						  			</tr>
						  		</thead>
						  		<tbody>
						  			<?php $no5 = 1; ?>
						  			<?php foreach ($iuts->result() as $iuts): ?>
						  				<tr>
						  					<td><?php echo $no5; ?></td>
						  					<td><img class="img img-thumbnail" src="<?php echo base_url('upload'); ?>/<?php echo $iuts->foto ?>"></td>
						  					<td><?php echo $iuts->nm_perusahaan ?></td>	
						  					<td><?php echo $iuts->nm_penanggungjwb ?></td>
						  					<td><?php echo $iuts->almt_perusahaan ?></td>
						  					<td><?php echo $iuts->npwp ?></td>
						  					<td><?php echo $iuts->no_telp." & ".$iuts->no_fax ?></td>
						  					<td><?php echo $iuts->keg_usaha_pokok ?></td>
						  					<td><?php echo $iuts->kbli ?></td>
						  					<td><?php echo $iuts->status_kantor ?></td>
						  					<td><?php echo $iuts->siup ?></td>
						  					<td><?php echo $iuts->tdp ?></td>
						  					<td><?php echo $iuts->no_iuts." & ".$iuts->tgl_iuts ?></td>
						  					<td><?php echo $iuts->masaberlaku ?></td>
						  				</tr>
						  				<?php $no5++; ?>
						  			<?php endforeach ?>
						  		</tbody>
						  	</table>

						  </div>

						  <div class="tab-pane fade" id="stpw">

						  	<br><br>
						  	<table class="table table-bordered" id="table6">
						  		<thead>
						  			<tr>
						  				<th>No.</th>
						  				<th>Foto</th>
						  				<th>No. Pendaftaran</th>
						  				<th>Nama Perusahaan</th>
						  				<th>Nama Penanggung Jawab</th>
						  				<th>Alamat Perusahaan</th>
						  				<th>NPWP</th>
						  				<th>IUTS</th>
						  				<th>No. STPW & Tanggal STPW</th>
						  				<th>No. Telp</th>
						  				<th>Masa Berlaku</th>
						  			</tr>
						  		</thead>
						  		<tbody>
						  		<?php $no6=1; ?>
						  		<?php foreach ($stpw->result() as $stpw): ?>
						  			<tr>
						  				<td><?php echo $no6; ?></td>
						  				<td><img class="img img-thumbnail" src="<?php echo base_url('upload'); ?>/<?php echo $stpw->foto ?>"></td>
						  				<td><?php echo $stpw->no_pendaftaran ?></td>
						  				<td><?php echo $stpw->nm_perusahaan ?></td>
						  				<td><?php echo $stpw->nm_penanggungjwb ?></td>
						  				<td><?php echo $stpw->almt_perusahaan ?></td>
						  				<td><?php echo $stpw->npwp ?></td>
						  				<td><?php echo $stpw->iuts ?></td>
						  				<td><?php echo $stpw->no_stpw." & ".$stpw->tgl_stpw ?></td>
						  				<td><?php echo $stpw->no_telp ?></td>
						  				<td><?php echo $stpw->masaberlaku ?></td>
						  			</tr>
						  			<?php $no6++; ?>
						  		<?php endforeach ?>
						  		</tbody>
						  	</table>

						  </div>

						  <div class="tab-pane fade" id="tdg">

						  	<br><br>
						  	<table class="table table-bordered" id="table7">
						  		<thead>
						  			<tr>
						  				<th>No.</th>
						  				<th>Foto</th>
						  				<th>Nama Gudang</th>
						  				<th>Nama Penanggung Jawab</th>
						  				<th>Alamat Penanggung Jawab</th>
						  				<th>No. TDG & Tanggal TDG</th>
						  				<th>Alamat Gudang</th>
						  				<th>No. Telp/Fax/Email</th>
						  				<th>Titik Kordinat</th>
						  				<th>Luas Gudang (m2)</th>
						  				<th>Kapasitas (m3/ton)</th>
						  				<th>Kelengkapan Gudang</th>
						  			</tr>
						  		</thead>
						  		<tbody>
						  		<?php $no7=1; ?>
						  		<?php foreach ($tdg->result() as $tdg): ?>
						  			<tr>
						  				<td><?php echo $no7; ?></td>
						  				<td><img class="img img-thumbnail" src="<?php echo base_url('upload'); ?>/<?php echo $tdg->foto ?>"></td>
						  				<td><?php echo $tdg->nm_perusahaan ?></td>
						  				<td><?php echo $tdg->nm_penanggungjwb ?></td>
						  				<td><?php echo $tdg->almt_penanggungjwb ?></td>
						  				<td><?php echo $tdg->no_tdg."&".$tdg->tgl_tdg ?></td>
						  				<td><?php echo $tdg->almt_gudang ?></td>
						  				<td><?php echo $tdg->no_telp."/".$tdg->no_fax."/".$tdg->email ?></td>
						  				<td></td>
						  				<td><?php echo $tdg->luas_gudang ?></td>
						  				<td><?php echo $tdg->kapasitas_gudang ?></td>
						  				<td><?php echo $tdg->kelengkapan_gudang ?></td>
						  			</tr>
						  			<?php $no7++; ?>
						  		<?php endforeach ?>
						  		</tbody>
						  	</table>

						  </div>

						  <div class="tab-pane fade" id="siupmb">

						  	<br><br>
						  	<table class="table table-bordered" id="table8">
						  		<thead>
						  			<tr>
						  				<th>No.</th>
						  				<th>Foto</th>
						  				<th>Nama Perusahaan</th>
						  				<th>Alamat Perusahaan</th>
						  				<th>SIUP-MB</th>
						  				<th>TDP</th>
						  				<th>Tanggal Keluar</th>
						  				<th>Tanggal Berakhir</th>
						  				<th>Insansi Yang Mengeluarkan</th>
						  				<th>Keterangan</th>
						  			</tr>
						  		</thead>
						  		<tbody>
						  		<?php $no8 = 1; ?>
						  		<?php foreach ($siupmb->result() as $siupmb): ?>
						  			<tr>
						  				<td><?php echo $no8; ?></td>
						  				<td><img class="img img-thumbnail" src="<?php echo base_url('upload'); ?>/<?php echo $siupmb->foto ?>"></td>
						  				<td><?php echo $siupmb->nm_perusahaan ?></td>
						  				<td><?php echo $siupmb->almt_perusahaan ?></td>
						  				<td><?php echo $siupmb->siupmb ?></td>
						  				<td><?php echo $siupmb->tdp ?></td>
						  				<td><?php echo $siupmb->tgl_keluar ?></td>
						  				<td><?php echo $siupmb->tgl_berakhir ?></td>
						  				<td><?php echo $siupmb->ins_yg_mengeluarkan ?></td>
						  				<td><?php echo $siupmb->keterangan ?></td>
						  			</tr>
						  			<?php $no8++; ?>
						  		<?php endforeach ?>
						  		</tbody>
						  	</table>

						  </div>

						  <div class="tab-pane fade" id="tdpud">

						  	<br><br>
						  	<table class="table table-bordered" id="table9">
						  		<thead>
						  			<tr>
						  				<th>No.</th>
						  				<th>Foto</th>
						  				<th>Nama Perusahaan</th>
						  				<th>Nama Penanggung Jawab</th>
						  				<th>Alamat Perusahaan</th>
						  				<th>NPWP</th>
						  				<th>No. Telp & Fax</th>
						  				<th>Kegiatan Usaha Pokok</th>
						  				<th>KBLI</th>
						  				<th>Status Kantor</th>
						  				<th>TDP</th>
						  				<th>No. TDPUD & Tanggal TDPUD</th>
						  				<th>Masa Berlaku</th>
						  			</tr>
						  		</thead>
						  		<tbody>
						  		<?php $no9=1; ?>
						  		<?php foreach ($tdpud->result() as $tdpud): ?>
						  			<tr>
						  				<td><?php echo $no9 ?></td>
						  				<td><img class="img img-thumbnail" src="<?php echo base_url('upload'); ?>/<?php echo $tdpud->foto ?>"></td>
						  				<td><?php echo $tdpud->nm_perusahaan ?></td>
						  				<td><?php echo $tdpud->nm_penanggungjwb ?></td>
						  				<td><?php echo $tdpud->almt_perusahaan ?></td>
						  				<td><?php echo $tdpud->npwp ?></td>
						  				<td><?php echo $tdpud->no_telp." & ".$tdpud->no_fax ?></td>
						  				<td><?php echo $tdpud->keg_usaha_pokok ?></td>
						  				<td><?php echo $tdpud->kbli ?></td>
						  				<td><?php echo $tdpud->status_kantor ?></td>
						  				<td><?php echo $tdpud->tdp ?></td>
						  				<td><?php echo $tdpud->no_tdpud." & ".$tdpud->tgl_tdpud ?></td>
						  				<td><?php echo $tdpud->masaberlaku ?></td>
						  			</tr>
						  			<?php $no9++; ?>
						  		<?php endforeach ?>
						  		</tbody>
						  	</table>

						  </div>

						  <div class="tab-pane fade" id="b3">

						  	<br><br>
						  	<table class="table table-bordered" id="table10">
						  		<thead>
						  			<tr>
						  				<th>NO.</th>
						  				<th>NO. CAS</th>
						  				<th>POS TARIF/HS</th>
						  				<th>URAIAN BARANG</th>
						  				<th>TATA NIAGA IMPOR</th>
						  				<th>KEPERLUAN LAIN TIDAK UNTUK PANGAN</th>
						  				<th>LABORATORIUM / PENELITIAN</th>
						  			</tr>
						  		</thead>
						  		<tbody>
						  			<?php $no10 = 1; ?>
						  			<?php foreach ($b3->result() as $b3): ?>
						  				<tr>
						  					<td><?php echo $no10; ?></td>
						  					<td><?php echo $b3->no_cas; ?></td>
						  					<td><?php echo $b3->pos_trf_hs; ?></td>
						  					<td><?php echo $b3->uraian_brg; ?></td>
						  					<td><?php echo $b3->tata_niaga_impor; ?></td>
						  					<td><?php echo $b3->kep_lain_tdk_utk_pgn." ".$b3->satuan; ?></td>
						  					<td><?php echo $b3->lab." ".$b3->satuan; ?></td>
						  				</tr>
						  			<?php endforeach ?>
						  		</tbody>
						  	</table>

						  </div>


						</div>
						
					</div>
				</div>
			</div>
			<div class="page" style="color: black;">
				<div class="page_layout clearfix">

				</div>
			</div>

			
			
			<?php include "layout/footer.php"; ?>
		</div>
		<div class="background_overlay"></div>
		<!--js-->
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.ba-bbq.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-ui-1.11.1.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.carouFredSel-6.2.1-packed.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.touchSwipe.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.transit.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.sliderControl.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.timeago.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.hint.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.prettyPhoto.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.qtip.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.blockUI.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/main.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/odometer.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/jquery.gmap.js"></script>
		

		
		<script src="<?php echo base_url('aset/highcharts') ?>/js/highcharts.js"></script>
		<script src="<?php echo base_url('aset/highcharts') ?>/js/modules/exporting.js"></script>
		
		<script src="<?php echo base_url('aset/DataTables') ?>/media/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url('aset/DataTables') ?>/media/js/dataTables.bootstrap.js"></script>
		
		<script type="text/javascript">
		
		</script>
		
	</body>
</html>
<script type="text/javascript">
	$('#table').dataTable();
	$('#table2').dataTable();
	$('#table3').dataTable();
	$('#table4').dataTable();
	$('#table5').dataTable();
	$('#table6').dataTable();
	$('#table7').dataTable();
	$('#table8').dataTable();
	$('#table9').dataTable();
	$('#table10').dataTable();
</script>