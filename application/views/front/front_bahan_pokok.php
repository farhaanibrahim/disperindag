<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Bahan Pokok - Disperindag</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
       <link rel="shortcut icon" href="<?php echo base_url('aset/front') ?>/images/favicon.ico">
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('aset/backend') ?>/css/theme-forest.css"/>
        <!-- EOF CSS INCLUDE -->                                      
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container page-navigation-top">            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal">
                    <li class="xn-logo">
                        <a href="<?php echo site_url('front'); ?>">SIGABAKO</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
					
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo site_url('front'); ?>">Home</a></li>
                    <li class="active">Bahan Pokok</li>
                </ul>
                <!-- END BREADCRUMB -->                
                
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-list"></span> Daftar Bahan Pokok  <small><?php echo $b_pokok->num_rows().' dari '.$total_rows->num_rows();?> Bahan Pokok</small></h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    
                    <div class="row">
						<?php /*
						<div class="col-md-12">
									
							<div class="panel panel-default">
								<div class="panel-body">
									<p>Use search to find contacts. You can search by: name, address, phone. Or use the advanced search.</p>
									<form class="form-horizontal">
										<div class="form-group">
											<div class="col-md-12">
												<div class="input-group">
													<div class="input-group-addon">
														<span class="fa fa-search"></span>
													</div>
													<input type="text" class="form-control" placeholder="Who are you looking for?"/>
													<div class="input-group-btn">
														<button class="btn btn-primary">Search</button>
													</div>
												</div>
											</div>
											<div class="col-md-4">
											</div>
										</div>
									</form>                                    
								</div>
							</div>
							
						</div>
						*/ ?>
						<div class="col-md-3">

                            <!-- LINKED LIST GROUP-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Daftar Pasar</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="list-group border-bottom">
										<a href="<?php echo site_url("front/bahan_pokok/"); ?>" class="list-group-item"><i class="fa fa-list"></i>Semua Pasar</a>
										<?php foreach($pasar->result() as $ps){ ?>
                                        <a href="<?php echo site_url("front/bahan_pokok_pasar/$ps->id_pasar"); ?>" class="list-group-item"><i class="fa fa-list"></i><?php echo $ps->nama_pasar; ?></a>
										<?php } ?>
                                    </div>                              
                                </div>
                            </div>
                            <!-- END LINKED LIST GROUP-->                        

                        </div>
						<div class="col-md-9">
							<div class="row">
								<?php 
								if($b_pokok->num_rows() > 0 ){
								foreach($b_pokok->result() as $bp): 
								?>
								<div class="col-md-3">
									<!-- CONTACT ITEM -->
									<div class="panel panel-primary">
										<div class="panel-body profile bg-primary">
											<div class="profile-image">
												<?php if($bp->foto_sub_b_pokok != NULL){ ?>
												<img src="<?php echo base_url("upload/sub_bahan_pokok/$bp->foto_sub_b_pokok") ?>" style="min-height:100px;" alt="<?php echo $bp->nama_sub_bahan_pokok ?>"/>
												<?php }else{ ?>
												<img src="<?php echo base_url("upload/bahan_pokok/no_image.jpg") ?>" style="min-height:100px;" alt="<?php echo $bp->nama_sub_bahan_pokok ?>"/>
												<?php } ?>
											</div>
											<div class="profile-data">
												<div class="profile-data-name"><?php echo $bp->nama_sub_bahan_pokok ?></div>
												<div class="profile-data-title">Asal Pasar : <?php echo $bp->nama_pasar ?></div>
											</div>
										</div>                                
										<div class="panel-body">                                    
											<div class="contact-info">
												<p><small>Harga</small><br/><?php echo " Rp.".number_format( $bp->harga, 0 , ',' , '.' )." /$bp->satuan" ?></p>                                  
												<p><small>Tanggal</small><br/><?php echo $bp->tanggal; ?></p>
												
											</div>
										</div>                                
									</div>
									<!-- END CONTACT ITEM -->
								</div>
								<?php 
								endforeach;
								}else{
								?>
								<div class="error-container">
									<div class="error-code"></div>
									<div class="error-text">Bahan Pokok Belum Tersedia</div>
									<div class="error-subtext">Kami mohon maaf saat ini, informasi bahan pokok yang anda cari belum tersedia, mohon kunjungi kembali beberapa saat lagi.</div>
									<div class="error-actions">                                
										<div class="row">
											<div class="col-md-6">
												<button class="btn btn-info btn-block btn-lg" onClick="document.location.href = '<?php echo site_url('front/bahan_pokok/') ?>';">Kembali</button>
											</div>
											<div class="col-md-6">
												<button class="btn btn-success btn-block btn-lg" onClick="document.location.href = '<?php echo site_url('front/') ?>';">Home</button>
											</div>
										</div>                                
									</div>
									<div class="error-subtext">Or you can use search to find anything you need.</div>
									<div class="row">
										<div class="col-md-12">
											<div class="input-group">
												<input type="text" placeholder="Search..." class="form-control"/>
												<div class="input-group-btn">
													<button class="btn btn-primary"><span class="fa fa-search"></span></button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php
								}
								?>
							</div>
							<div class="row">
								<div class="col-md-12">
									<ul class="pagination pagination-sm pull-right push-down-10 push-up-10">
										<?php echo $pagination ?>
									</ul>                            
								</div>
							</div>

                        </div> 
					</div>
                </div>
                <!-- END PAGE CONTENT WRAPPER --> 

				
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="pages-login.html" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->
		
		 <!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->

        <!-- THIS PAGE PLUGINS -->

        <!-- END PAGE PLUGINS -->         

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/plugins.js"></script>        
        <script type="text/javascript" src="<?php echo base_url('aset/backend') ?>/js/actions.js"></script>        
        <!-- END TEMPLATE -->
		
    <!-- END SCRIPTS -->         
    </body>
</html>






