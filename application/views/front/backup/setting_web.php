<!DOCTYPE HTML>
<?php foreach($instansi->result() as $is_row); ?>
<?php foreach($data_login->result() as $is_row2); ?>
<html>

<head>
    <title><?php echo $title; ?></title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

     <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/mystyles.css">
	<link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend'); ?>/js/DataTables/media/css/jquery.dataTables.min.css">
    <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/modernizr.js"></script>
	<!-- /FAVICON---->
	<link rel="shortcut icon" href="<?php echo base_url("upload/$is_row->logo"); ?>"/>


</head>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        <header id="main-header">
			<?php include_once "layout_front/header.php";  ?>
		</header>
		
		<div class="container">
            <h1 class="page-title"><?php echo $title; ?></h1>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <?php include_once "layout_front/menu_nav.php"; ?>
                </div>
                <div class="col-md-9">
					<ul class="nav nav-tabs" id="myTab">
						<li class="active"><a href="#tab-0" data-toggle="tab"><i class="fa fa-gear"></i> Setting Website</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane fade in active" id="tab-0"><br>
							<div class="row">
								<form action="<?php echo site_url('backend/setting_web_ac'); ?>" method="post" id="form_input">
								<div class="col-md-6">
									<input name="logo_lama" class="form-control" id="logo_lama" placeholder="" type="hidden" value="<?php echo $is_row->logo; ?>" />
									<div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>Judul Website</label>
										<input name="nama" class="form-control" id="nama" placeholder="" type="text" value="<?php echo $is_row->nama; ?>" />
									</div>
									<div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>Kepsek</label>
										<input name="kepsek" class="form-control" id="kepsek" placeholder="" type="text" value="<?php echo $is_row->kepsek; ?>" />
									</div>
									<div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>NIP Kepsek</label>
										<input name="nip_kepsek" class="form-control" id="nip_kepsek" placeholder="" type="text" value="<?php echo $is_row->nip_kepsek; ?>"/>
									</div>
									<div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>No Telp</label>
										<input name="no_telp" class="form-control" id="no_telp" placeholder="" type="text" value="<?php echo $is_row->no_telp; ?>" />
									</div>
									<div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>Email</label>
										<input name="email" class="form-control" id="email" placeholder="" type="email" value="<?php echo $is_row->email; ?>" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-group-icon-left"><i class="fa fa-file input-icon"></i>
										<label>Logo</label>
										<input name="logo" class="form-control" id="logo" placeholder="" type="file" />
									</div>
									<div class="form-group form-group-icon-left"><i class="fa fa-globe input-icon"></i>
										<label>Tentang Singkat</label>
										<textarea name="tentang_singkat" id="tentang_singkat" class="form-control"><?php echo $is_row->tentang_singkat; ?></textarea>
									</div>
									<div class="form-group form-group-icon-left"><i class="fa fa-globe input-icon"></i>
										<label>Alamat</label>
										<textarea name="alamat" id="alamat" class="form-control" ><?php echo $is_row->alamat; ?></textarea>
									</div>
									<div>
										<br>
										<h4 style="font-size:12px;">
										<b>Note</b> : <br>
										Ukuran Gambar Maximum : 656 x 717.<br>
										Format File : jpg,jpeg,png atau gif.<br>
										</h4>
									</div>
									<div class="gap gap-small"></div>
								</div>
								<div class="col-md-12" align="right">
									<input type="submit" class="btn btn-primary" placeholder="Alamat" value="Simpan"><br><br>
									<div class="progress progress-striped active" id="prog_bar" style="display:none;">
										<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
											<span class="info_prog2">Mohon Tunggu.. </span>
										</div>
									</div>
								</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


        <div class="gap"></div>
		<footer id="main-footer">
            <?php include_once "layout_front/footer.php"; ?>
        </footer>
	   
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/jquery.form.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/custom.js"></script>
		
		<script src="<?php echo base_url('aset/asset_frontend'); ?>/js/DataTables/media/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend'); ?>/js/DataTables/media/js/dataTables.bootstrap.js"></script>
		
    </div>
</body>
<script>
$(function () {
	$('#data_tabel').dataTable();
	$('#data_tabel2').dataTable();
	$('#data_tabel3').dataTable();
	if ($('#map-canvas').length) {
		var map,
			service;

		jQuery(function($) {
			$(document).ready(function() {
				var latlng = new google.maps.LatLng(-6.558789, 106.739837);
				var myOptions = {
					zoom: 16,
					center: latlng,
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					scrollwheel: false
				};

				map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);


				var marker = new google.maps.Marker({
					position: latlng,
					map: map
				});
				marker.setMap(map);


				$('a[href="#google-map-tab"]').on('shown.bs.tab', function(e) {
					google.maps.event.trigger(map, 'resize');
					map.setCenter(latlng);
				});
			});
		});
	}
	var options2 = { 
		beforeSend: function() {
			$("#info_sukses").hide();
			$("#info_gagal").hide();	
			$("#info_nomor").hide();	
			$("#prog_bar").show();	
		},
		success: function() {
		},
		complete: function(response) {
			$("#prog_bar").hide();
			if(response.responseText == '1'){
				$("#info_sukses").show();
				window.location="<?php echo site_url('backend/pengaturan') ?>";
			}else{
				$("#info_gagal").show();
			}
		},
		error: function(){
			//$("#form_edit_ads #message").html("<font color='red'> ERROR: unable to upload files</font>");
		}
	}; 
	$("#form_input").ajaxForm(options2);
});
</script>
</html>