<?php foreach($instansi->result() as $is_row); ?>
<?php foreach($data_login->result() as $is_row2); ?>
<?php foreach($data_arsip->result() as $arsip); ?>
<?php foreach($opd->result() as $opd); ?>
<?php foreach($sub_opd->result() as $sub_opd); ?>
<?php foreach($kl->result() as $kl); ?>
<?php foreach($sub_kl->result() as $sub_kl); ?>
<?php foreach($kl_rinci->result() as $kl_rinci); ?>
<?php foreach($kl_arsip->result() as $kl_arsip); ?>
<?php foreach($pengentry->result() as $pengentry); ?>
<!DOCTYPE HTML>
<html>

<head>
    <title><?php echo $title; ?></title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
	<link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/mystyles.css">
    <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/modernizr.js"></script>
	<!-- /FAVICON---->
	<link rel="shortcut icon" href="<?php echo base_url("upload/$is_row->logo"); ?>"/>


</head>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        <header id="main-header">
			<?php include_once "layout_front/header.php";  ?>
		</header>

        <div class="container">
            <ul class="breadcrumb">
                <li><a href="<?php echo site_url() ?>">Home</a>
                </li>
                <li><a href="<?php echo site_url('front/cari_arsip/lama') ?>">List Arsip</a>
                </li>
                <li class="active"><?php echo $arsip->judul_arsip; ?></li>
            </ul>
            <div class="booking-item-details">
                <header class="booking-item-header">
                    <div class="row">
                        <div class="col-md-9">
                            <h2 class="lh1em"><?php echo $arsip->judul_arsip; ?></h2>
                            <p class="lh1em text-small"><i class="fa fa-map-marker"></i> <?php echo $arsip->alamat_arsip; ?></p>
                            <ul class="list list-inline text-small">
                                <li><a href="#"><i class="fa fa-user"></i> <?php echo $opd->nama_opd; ?></a></li>
                                <li><a href="#"><i class="fa fa-user"></i> <?php echo $sub_opd->nm_sub_opd; ?></a></li>
                                </li>
                                <li><i class="fa fa-phone"></i> +1 (572) 426-3323</li>
                            </ul>
                        </div>
                    </div>
                </header>
                <div class="row">
                    <div class="col-md-7">
                        <div class="tabbable booking-details-tabbable">
                            <ul class="nav nav-tabs" id="myTab">
                                <li class="active"><a href="#tab-1" data-toggle="tab"><i class="fa fa-file"></i>File Fisik</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab-1">
                                    <?php if($arsip->jenis_fisik == "video/mp4"){ ?>
									<video width="660" controls>
									  <source src="<?php echo base_url("upload/arsip/$arsip->file_fisik") ?>" type="video/mp4">
									  <source src="movie.ogg" type="video/ogg">
									  Your browser does not support the video tag.
									</video>
									<?php }else if( ($arsip->jenis_fisik == "image/png")or($arsip->jenis_fisik == "image/jpg")or($arsip->jenis_fisik == "image/jpeg")or($arsip->jenis_fisik == "image/gif") ){ ?>
									<div class="fotorama" data-allowfullscreen="true" data-nav="thumbs">
                                        <img width="100%" src="<?php echo base_url("upload/arsip/$arsip->file_fisik") ?>" alt="Image Alternative text" title="<?php echo $arsip->judul_arsip; ?>" />
                                    </div>
									<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="booking-item-meta">
                            <h3 class="lh1em mt40" style="padding-bottom:10px; font-weight:bold; border-bottom:0px solid #666;">Detail Arsip</h3><br>
                            <table width="100%" cellpadding="25" cellspacing="5">
								<tr>
									<th>Atribut</th>
									<th width="10%">  </th>
									<th>Keterangan</th>
								</tr><tr>
									
									<td valign="top">Judul Arsip</td>
									<td valign="top"> : </td>
									<td valign="top"><?php echo $arsip->judul_arsip; ?></td>
								</tr><tr>
									
									<td valign="top">Klasifikasi</td>
									<td valign="top"> : </td>
									<td valign="top"><?php echo $kl->nm_klasifikasi; ?></td>
								</tr><tr>
									
									<td valign="top">Sub Klasifikasi</td>
									<td valign="top"> : </td>
									<td valign="top"><?php echo $sub_kl->nm_sub_klasifikasi; ?></td>
								</tr><tr>
									
									<td valign="top">Klasifikasi Rinci</td>
									<td valign="top"> : </td>
									<td valign="top"><?php echo $kl_rinci->nm_klasifikasi_rinci; ?></td>
								</tr><tr>
									
									<td valign="top">Klasifikasi Arsip</td>
									<td valign="top"> : </td>
									<td valign="top"><?php echo $kl_arsip->nm_klasifikasi_arsip; ?></td>
								</tr><tr>
									<td valign="top">Pencipta</td>
									<td valign="top"> : </td>
									<td valign="top"><?php echo $opd->nama_opd; ?></td>
								</tr><tr>
									<td valign="top">Pengelola</td>
									<td valign="top"> : </td>
									<td valign="top"><?php echo $sub_opd->nm_sub_opd; ?></td>
								</tr><tr>
									<td valign="top">Tanggal Buat</td>
									<td valign="top"> : </td>
									<td valign="top"><?php echo $arsip->tanggal_buat; ?></td>
								</tr><tr>
									<td valign="top">Tanggal Entri</td>
									<td valign="top"> : </td>
									<td valign="top"><?php echo $arsip->tanggal_entri; ?></td>
								</tr><tr>
									<td valign="top">Jumlah Fisik</td>
									<td valign="top"> : </td>
									<td valign="top"><?php echo $arsip->jumlah_fisik; ?></td>
								</tr><tr>
									<td valign="top">Jenis Fisik</td>
									<td valign="top"> : </td>
									<td valign="top"><?php echo $arsip->jenis_fisik; ?></td>
								</tr><tr>
									<td valign="top">Kondisi</td>
									<td valign="top"> : </td>
									<td valign="top"><?php echo $arsip->kondisi; ?></td>
								</tr><tr>
									<td valign="top">Tingkat Perkembangan</td>
									<td valign="top"> : </td>
									<td valign="top"><?php echo $arsip->tingkat_perkembangan; ?></td>
								</tr><tr>
									<td valign="top">Alamat</td>
									<td valign="top"> : </td>
									<td valign="top"><?php echo $arsip->alamat_arsip; ?></td>
								</tr><tr>
									<td valign="top">Sampul</td>
									<td valign="top"> : </td>
									<td valign="top"><?php echo $arsip->sampul; ?></td>
								</tr><tr>
									<td valign="top">Bok</td>
									<td valign="top"> : </td>
									<td valign="top"><?php echo $arsip->boks; ?></td>
								</tr><tr>
									<td valign="top">Rak</td>
									<td valign="top"> : </td>
									<td valign="top"><?php echo $arsip->rak; ?></td>
								</tr><tr>
									<td valign="top">Pengentri</td>
									<td valign="top"> : </td>
									<td valign="top"><?php echo $pengentry->nama; ?></td>
								</tr>
							</table>
                        </div>
                    </div>
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-6">
								  <div class="gap gap-small">
									<h3>Isi Ringkas Arsip</h3>
									<p>
									<?php
									if($arsip->isi_ringkas == ""){
										echo "Isi ringkas belum tersedia";
									}else{
										echo $arsip->isi_ringkas;
									}
									?>
									</p>
								</div>
							</div>
							<div class="col-md-6">
								  <div class="gap gap-small">
									<h3>Keterangan Arsip</h3>
									<p><?php 
									if($arsip->keterangan == ""){
										echo "Keterangan belum tersedia";
									}else{
										echo $arsip->keterangan;
									}
									?></p>
								</div>
							</div>
						
                    </div>
                </div>
            </div>
            <div class="gap"></div>
			</div>
        </div>


		<div class="gap"></div>
		<footer id="main-footer">
            <?php include_once "layout_front/footer.php"; ?>
        </footer>
        
		<script src="<?php echo base_url('aset/asset_frontend');  ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/custom.js"></script>
    </div>
</body>

</html>


