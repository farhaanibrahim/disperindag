<?php foreach($instansi->result() as $is_row){} ?>
<!DOCTYPE HTML>
<html>

<head>
    <title><?php echo $title; ?></title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend') ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend') ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend') ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend') ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend') ?>/css/mystyles.css">
    <script src="<?php echo base_url('aset/asset_frontend') ?>/js/modernizr.js"></script>
	<!-- /FAVICON---->
	<link rel="shortcut icon" href="<?php echo base_url("upload/$is_row->logo"); ?>"/>

</head>

<body>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        
		<header id="main-header">
            <?php include_once "layout/header_front.php";  ?>
        </header>
		
        <!-- TOP AREA -->
        <div class="top-area show-onload" style="margin:0 auto; height:545px;">
            <div class="bg-holder full">
                <div class="bg-mask"></div>
                <div class="bg-parallax" style="background-image:url(<?php echo base_url(); ?>/upload/image_slider/1.jpg);"></div>
                <div class="bg-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="loc-info text-left hidden-xs hidden-sm">
                                    <h3 class="loc-info-title">Selamat Datang di</h3>
                                    <span style="font-size:20px; line-height:normal;"><?php echo $is_row->nama ?></span>
                                    
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="search-tabs search-tabs-bg mt50">
                                    <div class="tabbable">
                                        <ul class="nav nav-tabs" id="myTab">
                                            <li class="active">
												<a href="#tab-1" data-toggle="tab"><i class="fa fa-lock"></i> <span >Selamat Datang di</span></a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane fade in active" id="tab-1">
                                                <div class="alert alert-success" style="display:none;" id="info_sukses">
													<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
													</button>
													<p class="text-small">Login Berhasil.. Mohon Tunggu halaman selanjutnya.. </p>
												</div>
												<div class="alert alert-danger" style="display:none;" id="info_gagal">
													<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
													</button>
													<p class="text-small">Login Gagal!!.. Username & Password tidak cocok.</p>
												</div>
												<h2>Login</h2>
                                                <form method="post" action="<?php echo site_url('backend/login'); ?>" id="form_login">
                                                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-user input-icon"></i>
                                                        <label>Username</label>
                                                        <input class="typeahead form-control" name="username" placeholder="Username" type="text" autofocus/>
                                                    </div>
													<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-lock input-icon"></i>
                                                        <label>Password</label>
                                                        <input class="typeahead form-control" name="password" placeholder="Password" type="password" />
                                                    </div>
													<button class="btn btn-primary btn-lg" type="submit">Login <span class="fa fa-arrow-right"></span></button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END TOP AREA  -->

		<footer id="main-footer">
            <?php include_once "layout/footer.php"; ?>
        </footer>
		
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/jquery.form.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/custom.js"></script>
		<script>
		  $(function () {
				var options2 = { 
					beforeSend: function() {
						$("#info_sukses").hide();
						$("#info_gagal").hide();	
						$("#prog_bar").show();	
					},
					success: function() {
					},
					complete: function(response) {
						$("#prog_bar").hide();
						if(response.responseText == '1'){
							$("#info_sukses").show();
							window.location = "<?php echo site_url('backend'); ?>";
						}else{
							$("#info_gagal").show();
						}
					},
					error: function(){
						//$("#form_edit_ads #message").html("<font color='red'> ERROR: unable to upload files</font>");
					}
				}; 
				$("#form_login").ajaxForm(options2);
		  });
		</script>
    </div>
</body>

</html>


