<!DOCTYPE HTML>
<?php foreach($instansi->result() as $is_row); ?>
<?php foreach($data_login->result() as $is_row2); ?>
<html>

<head>
    <title><?php echo $title; ?></title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

     <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/mystyles.css">
    <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/modernizr.js"></script>
	<!-- /FAVICON---->
	<link rel="shortcut icon" href="<?php echo base_url("upload/$is_row->logo"); ?>"/>


</head>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        <header id="main-header">
			<?php include_once "layout_front/header.php";  ?>
		</header>
		
		<div class="container">
            <h1 class="page-title">Selamat Datang <?php echo $login->nama; ?></h1>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-md-12">
					<div class="row">
						<div class="col-lg-3 col-sm-12 col-xs-4">
							<div class="small-box bg-primary">
								<div class="inner">
								  <h3 class="text-white"><?php echo $entri_arsip_this_month->num_rows(); ?></h3>
								  <p>Jumlah Entri on <?php echo date('M'); ?></p>
								</div>
								<div class="icon"><br>
								  <i class="fa fa-archive"></i>
								</div>
								<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<div class="col-lg-3 col-sm-12 col-xs-4">
							<div class="small-box bg-green">
								<div class="inner">
								  <h3 class="text-white"><?php echo $list_opd->num_rows(); ?></h3>
								  <p>Jumlah Semua OPD</p>
								</div>
								<div class="icon"><br>
								  <i class="fa fa-users"></i>
								</div>
								<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<div class="col-lg-3 col-sm-12 col-xs-4">
							<div class="small-box bg-orange">
								<div class="inner">
								  <h3 class="text-white"><?php echo $list_klasifikasi->num_rows(); ?></h3>
								  <p>Jumlah Semua Klasifikasi</p>
								</div>
								<div class="icon"><br>
								  <i class="fa fa-file"></i>
								</div>
								<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<div class="col-lg-3 col-sm-12 col-xs-4">
							<div class="small-box bg-purple">
								<div class="inner">
								  <h3 class="text-white"><?php echo $list_admin->num_rows(); ?></h3>
								  <p>Jumlah Semua User</p>
								</div>
								<div class="icon"><br>
								  <i class="fa fa-user"></i>
								</div>
								<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
					</div>
					<br>
					<br>
					<div class="row">
						<div class="col-lg-8 col-sm-12 col-xs-4" style="border:0px solid black;">
							<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
						</div>
						<div class="col-lg-4 col-sm-12 col-xs-4" style="border:0px solid black;">
							<div id="container2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
						</div>
					</div>
				</div>
			</div>
		</div>


        <div class="gap"></div>
		<footer id="main-footer">
            <?php include_once "layout_front/footer.php"; ?>
        </footer>
	   
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/custom.js"></script>
		
		<script src="<?php echo base_url('aset/asset_frontend'); ?>/js/highcharts/js/highcharts.js"></script>
		<script src="<?php echo base_url('aset/asset_frontend'); ?>/js/highcharts/js/modules/funnel.js"></script>
		<script src="<?php echo base_url('aset/asset_frontend'); ?>/js/highcharts/js/modules/exporting.js"></script>
    </div>
</body>
<script>
$(function () {
	<?php 
		$bln[1] = "1";
		$bln[2] = "2";
		$bln[3] = "3";
		$bln[4] = "4";
		$bln[5] = "5";
		$bln[6] = "5";
		$bln[7] = "7";
		$bln[8] = "8";
		$bln[9] = "9";
		$bln[10] = "10";
		$bln[11] = "11";
		$bln[12] = "12";
		$hari = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
	?>
	$('#container').highcharts({
        chart: {
			type: 'line'
		},
		title: {
			text: 'Jumlah Entri Data bulan ini (<?php echo date(' F ') ?>)'
		},
		subtitle: {
			text: 'Sumber: <?php echo $is_row->nama; ?>'
		},
		xAxis: {
			categories: [
				<?php 
				for($x=1; $x<=$hari; $x++){
					echo $x.',';
				}
				?>
			],
			crosshair: true
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Jumlah Service'
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>{point.y:.1f} Entri</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
		series: [
		{
			name: 'Tanggal',
			data: [
				<?php
				for($x=1; $x<=$hari; $x++){
					$val = 0;
					foreach($entri_arsip_this_month->result() as $res){
						$tgl = explode('-',$res->tanggal_entri);
						if( ($tgl[1] == date('m')) and ($tgl[2] == $x) ){
							$val++;
						}
					}
					echo $val.',';
				}
				?>
			]
		}, 
		]
    });
	$('#container2').highcharts({
        chart: {
            type: 'pie',
            marginRight: 100
        },
        title: {
                text: 'Jumlah Klasifikasi'
		},
		tooltip: {
			pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: false
				},
				showInLegend: true
			}
		},
        series: [{
            name: 'Jumlah',
            data: [
                ['Klasifikasi',   <?php echo $list_klasifikasi->num_rows(); ?>],
                ['Sub Klasifikasi',       <?php echo $list_sub_klasifikasi->num_rows(); ?>],
                ['Rincian Klasifikasi', <?php echo $list_rinci_klasifikasi->num_rows(); ?>],
                ['Klasifikasi Arsip',    <?php echo $list_arsip_klasifikasi->num_rows(); ?>]
            ]
        }]
    });
if ($('#map-canvas').length) {
    var map,
        service;

    jQuery(function($) {
        $(document).ready(function() {
            var latlng = new google.maps.LatLng(-6.558789, 106.739837);
            var myOptions = {
                zoom: 16,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false
            };

            map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);


            var marker = new google.maps.Marker({
                position: latlng,
                map: map
            });
            marker.setMap(map);


            $('a[href="#google-map-tab"]').on('shown.bs.tab', function(e) {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(latlng);
            });
        });
    });
}
});
</script>
</html>


