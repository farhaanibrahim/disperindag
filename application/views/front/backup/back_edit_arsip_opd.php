<!DOCTYPE HTML>
<?php foreach($instansi->result() as $is_row); ?>
<?php foreach($data_login->result() as $is_row2); ?>
<?php foreach($arsip->result() as $row); ?>
<html>

<head>
    <title><?php echo $title; ?></title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

     <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/mystyles.css">
	<link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend'); ?>/js/DataTables/media/css/jquery.dataTables.min.css">
    <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/modernizr.js"></script>
	<!-- /FAVICON---->
	<link rel="shortcut icon" href="<?php echo base_url("upload/$is_row->logo"); ?>"/>


</head>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        <header id="main-header">
			<?php include_once "layout_front/header.php";  ?>
		</header>
		
		<div class="container">
            <h1 class="page-title"><?php echo $title; ?></h1>
        </div>




        <div class="container">
            <div class="row">
                <?php /*
                <div class="col-md-3">
                    <?php include_once "layout_front/menu_nav.php"; ?>
                </div>
                */ ?>
                <div class="col-md-12">
					<ul class="nav nav-tabs" id="myTab">
						<li class="active"><a href="#tab-0" data-toggle="tab"><i class="fa fa-plus"></i> Entri Data</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane fade in active" id="tab-0"><br>
							<div class="row">
								<form action="<?php echo site_url('backend/edit_arsip_ac/by_opd'); ?>" enctype="multipart/form-data" method="post" id="form_input">
								<input name="id_arsip" class="form-control" id="id_arsip" placeholder="" type="hidden" value="<?php echo $row->id_arsip; ?>"/>
								<input name="jenis_fisik" class="form-control" id="jenis_fisik" placeholder="" type="hidden" value="<?php echo $row->jenis_fisik; ?>"/>
								<input name="file_fisik_lama" class="form-control" id="file_fisik" placeholder="" type="hidden" value="<?php echo $row->file_fisik; ?>"/>
								<div class="col-md-3">
									<div class="form-group form-group-lg form-group form-group-lg form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>Klasifikasi</label>
										<select class="form-control" name="klasifikasi" id="klasifikasi">
											<option value="<?php echo $row->arsip_kd_klasifikasi; ?>">Kode Sebelumya : <?php echo $row->arsip_kd_klasifikasi; ?></option>
											<?php foreach($list_klasifikasi->result() as $klf): ?>
											<option value="<?php echo $klf->kd_klasifikasi; ?>"><?php echo $klf->kd_klasifikasi." - ".$klf->nm_klasifikasi; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>Sub Klasifikasi</label>
										<select class="form-control" readonly name="sub_klasifikasi" id="sub_klasifikasi">
											<option value="<?php echo $row->arsip_sub_klasifikasi; ?>">Kode Sebelumya : <?php echo $row->arsip_sub_klasifikasi; ?></option>
										</select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>Rincian Klasifikasi</label>
										<select class="form-control" readonly name="klasifikasi_rinci" id="klasifikasi_rinci">
											<option value="<?php echo $row->arsip_klasifikasi_rinci; ?>">Kode Sebelumya : <?php echo $row->arsip_klasifikasi_rinci; ?></option>
										</select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>Kode Arsip</label>
										<select class="form-control" readonly name="klasifikasi_arsip" id="klasifikasi_arsip">
											<option value="<?php echo $row->arsip_klasifikasi_arsip; ?>">Kode Sebelumya : <?php echo $row->arsip_klasifikasi_arsip; ?></option>
										</select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>Pencipta Arsip</label>
										<select class="form-control" name="pencipta_arsip" id="pencipta_arsip">
											<option value="<?php echo $row->pencipta_arsip; ?>">Kode Sebelumya : <?php echo $row->pencipta_arsip; ?></option>
											<?php foreach($list_opd->result() as $opd): ?>
											<option value="<?php echo $opd->kd_opd; ?>"><?php echo $opd->nama_opd; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>Pengelola Arsip</label>
										<select class="form-control" readonly name="pengelola_arsip" id="pengelola_arsip">
											<option value="<?php echo $row->pengelola_arsip; ?>">Kode Sebelumya : <?php echo $row->pengelola_arsip; ?></option>
										</select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>Aktif</label>
										<input name="aktif" class="form-control" id="aktif" placeholder="contoh 3 tahun di isi (3)" type="number" value="<?php echo $row->aktif; ?>" />
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>In Aktif</label>
										<input name="inaktif" class="form-control" id="inaktif" placeholder="contoh 4 tahun di isi (4)" type="number" value="<?php echo $row->inaktif; ?>" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>Judul Arsip</label>
										<input name="judul_arsip" class="form-control" id="judul_arsip" placeholder="Judul Arsip" type="text" value="<?php echo $row->judul_arsip; ?>" />
									</div>
									<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>No Arsip</label>
										<input name="no_arsip" class="form-control" id="no_arsip" placeholder="Nomor Arsip" type="text" value="<?php echo $row->no_arsip; ?>" />
									</div>
									<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>Tanggal Buat</label>
										<input name="tanggal_buat" class="form-control" id="tanggal_buat" placeholder="Tanggal Buat Arsip" type="text" value="<?php echo $row->tanggal_buat; ?>" />
									</div>
									<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>Tanggal Serah</label>
										<input name="tanggal_serah" class="form-control" id="tanggal_serah" placeholder="Tanggal Serah Arsip" type="text" value="<?php echo $row->tanggal_serah; ?>" />
									</div>
									<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>Jumlah Fisik</label>
										<input name="jumlah_fisik" class="form-control" id="jumlah_fisik" placeholder="Jumlah Lembar/Halaman" type="number" value="<?php echo $row->jumlah_fisik; ?>" />
									</div>
									<div class="form-group form-group-lg form-group-icon-left">
										<label>Kondisi</label>
										<div class="form-group form-group-lg form-group-select-plus">
											<div class="radio-inline " style="margin-top:10px;">
												<label><input class="i-radio" type="radio" name="kondisi" value="Baik" <?php if($row->kondisi == "Baik"){ echo "checked"; } ?>/>Baik</label>
											</div>
											<div class="radio-inline" style="margin-top:10px;">
												<label><input class="i-radio" type="radio" name="kondisi" value="Rusak" <?php if($row->kondisi == "Rusak"){ echo "checked"; } ?> />Rusak</label>
											</div>
											<div class="radio-inline" style="margin-top:10px;">
												<label><input class="i-radio" type="radio" name="kondisi" value="Cacat" <?php if($row->kondisi == "Cacat"){ echo "checked"; } ?> />Cacat</label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-file input-icon"></i>
										<label>File Arsip/Jenis Fisik</label>
										<input name="file_fisik" class="form-control" id="file_fisik" placeholder="" type="file"/>
									</div>
									<div class="form-group form-group-lg form-group-icon-left">
										<label>Tingkat Perkembangan</label>
										<div class="form-group form-group-lg form-group-select-plus">
											<div class="radio-inline " style="margin-top:10px;">
												<label><input class="i-radio" type="radio" name="tingkat_perkembangan" value="Asli" <?php if($row->tingkat_perkembangan == "Asli"){ echo "checked"; } ?>/>Asli</label>
											</div>
											<div class="radio-inline" style="margin-top:10px;">
												<label><input class="i-radio" type="radio" name="tingkat_perkembangan" value="Salinan" <?php if($row->tingkat_perkembangan == "Salinan"){ echo "checked"; } ?>/>Salinan</label>
											</div>
											<div class="radio-inline" style="margin-top:10px;">
												<label><input class="i-radio" type="radio" name="tingkat_perkembangan" value="Copy" <?php if($row->tingkat_perkembangan == "Copy"){ echo "checked"; } ?> />Copy</label>
											</div>
										</div>
									</div>
									<div class="form-group form-group-lg form-group-icon-left">
										<label>Status</label>
										<select class="form-control" name="status" required>
											<option value="<?php echo $row->status; ?>">Sebelumya : <?php echo $row->status; ?></option>
											<option value="Di nilai Kembali">Di nilai Kembali</option>
											<option value="Musnah">Musnah</option>
											<option value="Permanen">Permanen</option>
										</select>
									</div>
									<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>Alamat Arsip</label>
										<textarea name="alamat" id="alamat" class="form-control" ><?php echo $row->alamat_arsip; ?></textarea>
									</div>
									<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>Isi Ringkas</label>
										<textarea name="isi_ringkas" id="isi_ringkas" class="form-control" ><?php echo $row->isi_ringkas; ?></textarea>
									</div>
									<div class="gap gap-small"></div>
								</div>
								<div class="col-md-12" align="">
									<input type="submit" class="btn btn-primary" value="Simpan"><br><br>
									<div class="progress progress-striped active" id="prog_bar" style="display:none;">
										<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
											<span class="info_prog2">Mohon Tunggu.. </span>
										</div>
									</div>
									<div class="alert alert-success" style="display:none" id="info_sukses">
										<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
										</button>
										<p class="text-small">Berhasil.. Data Arsip sudah diedit.. </p>
									</div>
									<div class="alert alert-danger" style="display:none;" id="info_gagal">
										<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
										</button>
										<p class="text-small">Maaf sementara terjadi Kegagalan Sistem.. Silahkan coba beberapa saat lagi. </p>
									</div>
									<div class="alert alert-danger" style="display:none;" id="info_gagal2">
										<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
										</button>
										<p class="text-small">Input Gagal <label class="pesan_error"></label> </p>
									</div>
								</div>
								</form>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>


        <div class="gap"></div>
		<footer id="main-footer">
            <?php include_once "layout_front/footer.php"; ?>
        </footer>
	   
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/jquery.form.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/custom.js"></script>
		
		<script src="<?php echo base_url('aset/asset_frontend'); ?>/js/DataTables/media/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend'); ?>/js/DataTables/media/js/dataTables.bootstrap.js"></script>
		
    </div>
</body>
<script>
$(function () {
	$('#data_tabel').dataTable();
	$("#tanggal_buat").datepicker({
		format: 'yyyy-mm-dd'
	});
	$("#tanggal_serah").datepicker({
		format: 'yyyy-mm-dd'
	});
	
	//
	$("#klasifikasi").change(function(){
		var klasifikasi = $("#klasifikasi option:selected").val();
		$.ajax({
			url: "<?php echo site_url('api_sercvice/klasifikasi_sub_where_id'); ?>",
			type: "POST",
			data	: "klasifikasi="+klasifikasi,
			success : function (msg) {
				if(msg != 'kosong'){
					document.getElementById("sub_klasifikasi").disabled = false;
					$("#sub_klasifikasi").html(msg);
				}else{
					alert('Maaf Data Sub Klasifikasi dari Klasifikasi ini masih kosong..');
				}
			}
		});
	});
	$("#sub_klasifikasi").change(function(){
		var sub_klasifikasi = $("#sub_klasifikasi option:selected").val();
		$.ajax({
			url: "<?php echo site_url('api_sercvice/klasifikasi_rinci_where_id'); ?>",
			type: "POST",
			data	: "sub_klasifikasi="+sub_klasifikasi,
			success : function (msg) {
				if(msg != 'kosong'){
					document.getElementById("klasifikasi_rinci").disabled = false;
					$("#klasifikasi_rinci").html(msg);
				}else{
					alert('Maaf Data Rincian Klasifikasi dari Sub Klasifikasi ini masih kosong..');
				}
			}
		});
	});
	$("#klasifikasi_rinci").change(function(){
		var klasifikasi_rinci = $("#klasifikasi_rinci option:selected").val();
		$.ajax({
			url: "<?php echo site_url('api_sercvice/klasifikasi_arsip_where_id'); ?>",
			type: "POST",
			data	: "klasifikasi_rinci="+klasifikasi_rinci,
			success : function (msg) {
				if(msg != 'kosong'){
					document.getElementById("klasifikasi_arsip").disabled = false;
					$("#klasifikasi_arsip").html(msg);
				}else{
					alert('Maaf Data Klasifikasi Arsip dari Rincian Klasifikasi ini masih kosong..');
				}
			}
		});
	});
	$("#pencipta_arsip").change(function(){
		var pencipta_arsip = $("#pencipta_arsip option:selected").val();
		$.ajax({
			url: "<?php echo site_url('api_sercvice/opd_sub_where_id'); ?>",
			type: "POST",
			data	: "pencipta_arsip="+pencipta_arsip,
			success : function (msg) {
				if(msg != 'kosong'){
					document.getElementById("pengelola_arsip").disabled = false;
					$("#pengelola_arsip").html(msg);
				}else{
					alert('Maaf Data Klasifikasi Arsip dari Rincian Klasifikasi ini masih kosong..');
				}
			}
		});
	});
	//
	var options2 = { 
		beforeSend: function() {
			$("#info_sukses").hide();
			$("#info_gagal").hide();	
			$("#info_gagal2").hide();	
			$("#info_nomor").hide();	
			$("#prog_bar").show();	
		},
		success: function() {
		},
		complete: function(response) {
			$("#prog_bar").hide();
			if(response.responseText == '1'){
				$("#info_sukses").show();
				window.location="<?php echo site_url('backend/daftar_arsip') ?>";
			}else if(response.responseText == '0'){
				$("#info_gagal").show();
			}else{
				$("#pesan_error").html(response.responseText);
				$("#info_gagal2").show();
			}
		},
		error: function(){
			//$("#form_edit_ads #message").html("<font color='red'> ERROR: unable to upload files</font>");
		}
	}; 
	$("#form_input").ajaxForm(options2);
});
</script>
</html>