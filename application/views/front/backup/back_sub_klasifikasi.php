<!DOCTYPE HTML>
<?php foreach($instansi->result() as $is_row); ?>
<?php foreach($data_login->result() as $is_row2); ?>
<?php foreach($klasifikasi_info->result() as $kl_info); ?>
<html>

<head>
    <title><?php echo $title; ?></title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

     <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/mystyles.css">
	<link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend'); ?>/js/DataTables/media/css/jquery.dataTables.min.css">
    <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/modernizr.js"></script>
	<!-- /FAVICON---->
	<link rel="shortcut icon" href="<?php echo base_url("upload/$is_row->logo"); ?>"/>


</head>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        <header id="main-header">
			<?php include_once "layout_front/header.php";  ?>
		</header>
		
		<div class="container">
            <h1 class="page-title"><?php echo $title.' '.$kl_info->nm_klasifikasi; ?></h1>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <?php include_once "layout_front/menu_nav.php"; ?>
                </div>
                <div class="col-md-9">
				
					<?php if($this->session->flashdata("klasifikasi") == "Sukses") { ?>
					<div class="alert alert-success" style="" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small">Berhasil.. Data Klasifikasi sudah dieksekusi.. </p>
					</div>
					<?php }else if($this->session->flashdata("klasifikasi") == "hapus_sukses"){ ?>
					<div class="alert alert-success" style="" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small">Berhasil.. data telah dihapus.. </p>
					</div>
					<?php }else if($this->session->flashdata("klasifikasi") == "sudah ada"){ ?>
					<div class="alert alert-danger" style="" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small">Gagal.. Maaf Kode ini sudah digunakan oleh data Klasifikasi lainnya, silahkan coba dengan kode lain.. </p>
					</div>
					<?php } else if($this->session->flashdata("klasifikasi") == "Gagal"){ ?>
					<div class="alert alert-danger" style="" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small">Maaf sementara terjadi kegagalan sistem.. Silahkan coba beberapa saat lagi. </p>
					</div>
					<?php } ?>
					
					<ul class="nav nav-tabs" id="myTab">
						<li class="active"><a href="#tab-0" data-toggle="tab"><i class="fa fa-file"></i> Daftar Klasifikasi</a></li>
						<li class=""><a href="#tab-1" data-toggle="tab"><i class="fa fa-plus"></i> Tambah Klasifikasi</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane fade in active" id="tab-0"><br>
							<table id="data_tabel" class="table table-bordered table-striped table-booking-history" style="color:black;">
								<thead>
									<tr>
										<th>No</th>
										<th>Kode Sub</th>
										<th>Nama Sub Klasifikasi</th>
										<th align="center">Rincian Klasifikasi</th>
										<th>Opsi</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$no = 1;
									foreach($list_sub_klasifikasi->result() as $row): ?>
									<tr>
										<td><?php echo $no; ?></td>
										<td><?php echo $row->kd_sub_klasifikasi ?></td>
										<td><?php echo $row->nm_sub_klasifikasi ?></td>
										<td align="center">
											<a href="<?php echo site_url("backend/rinci_klasifikasi/$row->kd_sub_klasifikasi") ?>" class="btn btn-sm btn-success"><span class="fa fa-search"></span></a>
										</td>
										<td>
											<a href="<?php echo site_url("backend/edit_sub_klasifikasi/$row->id_sub_klasifikasi") ?>" class="btn btn-sm btn-warning"><span class="fa fa-pencil"></span></a>
											<a href="<?php echo site_url("backend/hapus_sub_klasifikasi/$row->id_sub_klasifikasi") ?>" class="btn btn-sm btn-danger"><span class="fa fa-trash-o"></span></a>
										</td>
									</tr>
									<?php 
									$no++;
									endforeach; ?>
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="tab-1"><br>
							<div class="row">
								<form action="<?php echo site_url('backend/tambah_klasifikasi_ac'); ?>" method="post" id="form_input">
								<div class="col-md-4">
									<div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>Kode Klasifikasi</label>
										<input name="kd_klasifikasi" class="form-control" id="kd_klasifikasi" placeholder="Ketik Kode Klasifikasi disini" type="text" value="" />
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
										<label>Nama Klasifikasi</label>
										<input name="nm_klasifikasi" class="form-control" id="nm_klasifikasi" placeholder="Ketik Nama Klasifikasi disini" type="text" value="" />
									</div>
								</div>
								<div class="col-md-12" align="left">
									<input type="submit" class="btn btn-primary" placeholder="Alamat" value="Simpan"><br><br>
									<div class="progress progress-striped active" id="prog_bar" style="display:none;">
										<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
											<span class="info_prog2">Mohon Tunggu.. </span>
										</div>
									</div>
								</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


        <div class="gap"></div>
		<footer id="main-footer">
            <?php include_once "layout_front/footer.php"; ?>
        </footer>
	   
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/jquery.form.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/custom.js"></script>
		
		<script src="<?php echo base_url('aset/asset_frontend'); ?>/js/DataTables/media/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend'); ?>/js/DataTables/media/js/dataTables.bootstrap.js"></script>
		
    </div>
</body>
<script>
$(function () {
	$('#data_tabel').dataTable();
});
</script>
</html>