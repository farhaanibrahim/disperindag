<!DOCTYPE HTML>
<?php foreach($instansi->result() as $is_row); ?>
<?php foreach($data_login->result() as $is_row2); ?>
<?php foreach($arsip->result() as $row); ?>
<html>

<head>
    <title><?php echo $title; ?></title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

     <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/mystyles.css">
	<link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend'); ?>/js/DataTables/media/css/jquery.dataTables.min.css">
    <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/modernizr.js"></script>
	<!-- /FAVICON---->
	<link rel="shortcut icon" href="<?php echo base_url("upload/$is_row->logo"); ?>"/>


</head>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        <header id="main-header">
			<?php include_once "layout_front/header.php";  ?>
		</header>
		
		<div class="container">
            <h1 class="page-title"><?php echo $title; ?></h1>
        </div>




        <div class="container">
            <div class="row">
				<?php /*
                <div class="col-md-3">
                    <?php include_once "layout_front/menu_nav.php"; ?>
                </div>
                */ ?>
				<div class="col-md-12">					
					<ul class="nav nav-tabs" id="myTab">
						<li class="active"><a href="#tab-0" data-toggle="tab"><i class="fa fa-archive"></i> Konfirmasi Arsip</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane fade in active" id="tab-0" style="margin: 0 10px;"><br>
							<h4>Apakah anda akan mengubah Status Arsip dibawah ini, Jika iya bagaimana status arsip ini?<h4><br>
							<form action="<?php echo site_url('backend/konfirmasi_arsip_ac') ?>" method="post" role="form">
							<input type="hidden" value="<?php echo $row->id_arsip; ?>" name="id_arsip">
							<div class="row">
								<div class="col-md-12">
									<div class="radio radio-lg">
										<label><input class="i-radio" type="radio" name="status_approval" value="Menunggu Konfirmasi" <?php if($row->approve_status == "Menunggu Konfirmasi"){ echo "checked"; } ?> />Menunggu Konfirmasi</label>
									</div>
									<div class="radio radio-lg">
										<label><input class="i-radio" type="radio" name="status_approval" value="Tidak Valid" <?php if($row->approve_status == "Tidak Valid"){ echo "checked"; } ?>/>Tidak Valid</label>
									</div>
									<div class="radio radio-lg">
										<label><input class="i-radio" type="radio" name="status_approval" value="Valid"<?php if($row->approve_status == "Valid"){ echo "checked"; } ?>/>Valid</label>
									</div>
									<div class="radio radio-lg">
										<label><input class="i-radio" type="radio" name="status_approval" value="Valid"<?php if($row->approve_status == "Valid Pimpinan"){ echo "checked"; } ?>/>Valid Pimpinan</label>
									</div>
								</div>
								<div class="col-md-12">
									<button type="submit" class="btn btn-lg btn-primary">Simpan <span class="fa fa-arrow-right"></span></button>
								</div>
							</div>
							</form>
						</div>
						
					</div>
				</div>
			</div>
		</div>


        <div class="gap"></div>
		<footer id="main-footer">
            <?php include_once "layout_front/footer.php"; ?>
        </footer>
	   
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/jquery.form.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/custom.js"></script>
		
		<script src="<?php echo base_url('aset/asset_frontend'); ?>/js/DataTables/media/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend'); ?>/js/DataTables/media/js/dataTables.bootstrap.js"></script>
		
    </div>
</body>
<script>
$(function () {
	$('#data_tabel').dataTable();
});
</script>
</html>