<!DOCTYPE HTML>
<?php foreach($instansi->result() as $is_row); ?>
<?php foreach($data_login->result() as $is_row2); ?>
<html>

<head>
    <title><?php echo $title; ?></title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

     <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend');  ?>/css/mystyles.css">
	<link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend'); ?>/js/DataTables/media/css/jquery.dataTables.min.css">
    <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/modernizr.js"></script>
	<!-- /FAVICON---->
	<link rel="shortcut icon" href="<?php echo base_url("upload/$is_row->logo"); ?>"/>


</head>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        <header id="main-header">
			<?php include_once "layout_front/header.php";  ?>
		</header>
		
		<div class="container">
            <h1 class="page-title"><?php echo $title; ?></h1>
        </div>




        <div class="container">
            <div class="row">
				<?php /*
                <div class="col-md-3">
                    <?php include_once "layout_front/menu_nav.php"; ?>
                </div>
                */ ?>
				<div class="col-md-12">					
					<ul class="nav nav-tabs" id="myTab">
						<li class=""><a href="#tab-0" data-toggle="tab"><i class="fa fa-archive"></i> Daftar Arsip</a></li>
						<li class="active"><a href="#tab-1" data-toggle="tab"><i class="fa fa-refresh"></i> Update Arsip</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane fade in" id="tab-0"><br>
							<?php if($this->session->flashdata('arsip') == 'edit_sukses'){ ?>
							<div class="alert alert-success" style="" id="info_sukses">
								<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
								</button>
								<p class="text-small">Berhasil.. Data Arsip telah selesai diedit.. </p>
							</div>
							<?php }else if($this->session->flashdata('arsip') == 'approve_sukses'){ ?>
							<div class="alert alert-success" style="" id="info_sukses">
								<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
								</button>
								<p class="text-small">Berhasil.. Data Arsip telah selesai dikonfirmasi.. </p>
							</div>
							<?php }else if($this->session->flashdata('arsip') == 'approve_gagal'){ ?>
							<div class="alert alert-danger" style="" id="info_sukses">
								<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
								</button>
								<p class="text-small">Maaf.. Anda tidak berhak merubah status persetujuan konfirmasi arsip.. </p>
							</div>
							<?php }else if($this->session->flashdata('arsip') == 'input_sukses'){ ?>
							<div class="alert alert-success" style="" id="info_sukses">
								<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
								</button>
								<p class="text-small">Berhasil.. Data Arsip telah ditambahkan.. </p>
							</div>
							<?php } ?>
							<table id="data_tabel" class="table table-bordered table-striped table-booking-history" style="color:black;">
								<thead>
									<tr>
										<th>No</th>
										<th>No Arsip</th>
										<th>Klasifikasi</th>
										<th>Judul Arsip</th>
										<th>Tanggal Buat</th>
										<th>Tanggal Entri</th>
										<th>Tanggal Serah</th>
										<th>Status Approval</th>
										<th>Detail</th>
										<th>Opsi</th>
										<th>Opsi Approval</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$no = 1;
									foreach($list_arsip->result() as $row): 
									?>									
									<tr>
										<td><?php echo $no; ?></td>
										<td width="10%"><?php echo $row->no_arsip ?></td>
										<td><?php echo "$row->arsip_kd_klasifikasi/$row->arsip_sub_klasifikasi/$row->arsip_klasifikasi_rinci/$row->arsip_klasifikasi_arsip" ?></td>
										<td><?php echo $row->judul_arsip ?></td>
										<td><?php echo $row->tanggal_buat ?></td>
										<td><?php echo $row->tanggal_entri ?></td>
										<td><?php echo $row->tanggal_serah ?></td>
										<?php
										if($row->approve_status == "Valid"){
											$bg = "#dff0d8";
										}else if($row->approve_status == "Tidak Valid"){
											$bg = "#f2dede";
										}else if($row->approve_status == "Menunggu Konfirmasi"){
											$bg = "#f8e5be";
										}
										?>
										<td style="background-color:<?php echo $bg; ?>"><?php echo $row->approve_status ?></td>
										<td align="center">
											<a href="<?php echo site_url("front/detail_arsip/$row->id_arsip") ?>" class="btn btn-sm btn-info"><span class="fa fa-list"></span></a>
										</td>
										<td align="center">
											<?php
											if($row->approve_status == "Valid"){
												if( $is_row2->level != "OPD" ){ ?>
													<a href="<?php echo site_url("backend/edit_arsip/by_other/$row->id_arsip") ?>" class="btn btn-sm btn-warning" title="Edit"><span class="fa fa-pencil"></span></a>
												<?php }else{
													echo "Belum Tersedia";
												}
											}else{
												if( $is_row2->level == "OPD" ){ ?>
												<a href="<?php echo site_url("backend/edit_arsip/by_opd/$row->id_arsip") ?>" class="btn btn-sm btn-warning" title="Edit"><span class="fa fa-pencil"></span></a>
												<?php
												}else{
													echo "Belum Tersedia";
												}
											} 
											?>
											</td>
										<td align="center">
											<?php if( ($is_row2->level == "Admin") or ($is_row2->level == "Petugas") or ($is_row2->level == "Super Admin")){ ?>
											<a href="<?php echo site_url("backend/konfirmasi_arsip/$row->id_arsip") ?>" class="btn btn-sm btn-primary" title="Konfirmasi"><span class="fa fa-pencil"></span></a>
											<?php }else{
												echo "Tidak Tersedia";
											} ?>
										</td>
									</tr>
									<?php 
									$no++;
									endforeach; ?>
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade in active" id="tab-1"><br>
							 <div class="row">
                                <div class="col-md-12 text-center">
                                    <p style="font-size:148px; color:#5776B7;"><span class="fa fa-refresh"></span></p>
                                    <h1>Update Arsip </h1>
                                    <p>Silahkan klik tombol dibawah ini untuk update data-data arsip.</p>
									<p id="msg_arsip" style="display:none">
										Penambahan Arsip History :  <b><span id="update_arsip_msg1"></b></span> &nbsp
										Penambahan Tabel Aktif :  <b><span id="update_arsip_msg2"></b></span> &nbsp
										Penambahan Tabel InAktif :  <b><span id="update_arsip_msg3"></b></span> &nbsp
									</p>
									<button class="btn btn-primary btn-lg mt5" id="btn_update_arsip">Update Arsip <i class="fa fa-refresh"></i></button>
                                </div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>


        <div class="gap"></div>
		<footer id="main-footer">
            <?php include_once "layout_front/footer.php"; ?>
        </footer>
	   
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/jquery.form.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend');  ?>/js/custom.js"></script>
		
		<script src="<?php echo base_url('aset/asset_frontend'); ?>/js/DataTables/media/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend'); ?>/js/DataTables/media/js/dataTables.bootstrap.js"></script>
		
    </div>
</body>
<script>
$(function () {
	$('#data_tabel').dataTable();
	$("#btn_update_arsip").click(function(){
		$.ajax({
			url: "<?php echo site_url('backend/update_status_arsip'); ?>",
			success : function (msg) {
				var res=JSON.parse(msg);
				$("#msg_arsip").fadeOut();
				$("#msg_arsip").fadeIn();
				$("#update_arsip_msg1").html(res.arsip_history);
				$("#update_arsip_msg3").html(res.t_inaktif);
				$("#update_arsip_msg2").html(res.t_aktif);
			}
		});
	});
});
</script>
</html>