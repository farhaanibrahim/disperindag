<?php

$start = date("Y-m-d", mktime(0,0,0,date("m"),date("d")-7,date("Y")) );
$end = date("Y-m-d");
$url = "http://www.kemendag.go.id/addon/api/website_api/harga_bapok";
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL ,$url);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY:9b026676adc83abca0b2628e965f7d137691f34c'));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, "start_date=$start&end_date=$end");

$output = curl_exec($ch);
$output = str_replace('M	', '', $output);

$obj = json_decode($output);
 
curl_close($ch);
?>
<?php

$url = "http://www.kemendag.go.id/addon/api/website_api/daily_currency";
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL ,$url);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY:9b026676adc83abca0b2628e965f7d137691f34c'));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$output2 = curl_exec($ch);
$output2 = str_replace('M	', '', $output2);

$obj2 = json_decode($output2);

curl_close($ch);


?>
<?php 
        // create curl resource 
        $ch = curl_init(); 

        // set url 
        curl_setopt($ch, CURLOPT_URL, "http://ikm.kotabogor.go.id/api/list_t_potensi.php"); 

        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

        // $output contains the output string 
        $potensi = curl_exec($ch); 
		$obj_pot = json_decode($potensi);
        // close curl resource to free up system resources 
        curl_close($ch);  
		
?>
<?php foreach($instansi->result() as $ins){} ?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="<?php echo base_url('aset/bootstrap/css'); ?>/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/reset2.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/style2.css">


	<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/DataTables') ?>/media/css/jquery.dataTables.min.css">
	<title>DISPERINDAG</title>
</head>
<body>
	<div class="col-md-12">
		<div class="row">
			<img src="<?php echo base_url('upload/banner3.jpg') ?>" style="width:100%; height:160px;" />
		</div>
	</div>
	<div class="col-md-12">
		<div class="row">
			<nav class="navbar navbar-default">
			  <div class="container-fluid">

			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      <ul class="nav navbar-nav">
			        <li class="active"><a href="#">Beranda <span class="sr-only">(current)</span></a></li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Tentang Kami <span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <li><a href="#">Perdagangan</a></li>
			            <li><a href="#">Perindustrian</a></li>
			            <li><a href="#">Metrologi</a></li>
			          </ul>
			        </li>
			        <li><a href="#">Bahan Pokok</a></li>
			        <li><a href="#">Kalender Event</a></li>
			        <li><a href="#">Berita</a></li>
			        <li><a href="#">Agenda</a></li>
			        <li><a href="#">Regulasi</a></li>
			        <li><a href="#">Kontak</a></li>
			      </ul>
			      
			      <ul class="nav navbar-nav navbar-right">
			        <li><a href="#" class="btn btn-primary">Login</a></li>
			      </ul>
			    </div>
			  </div>
			</nav>
		</div>
	</div>

	<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading">
		    	<h3 class="panel-title">Potensi IKM Kota Bogor</h3>
		  	</div>
		  	<div class="panel-body">
		    	<table id="data_tabel" class="table">
								<thead>
									<tr>
										<th>Perusahaan</th>
										<th>Sektor Ekonomi</th>
										<th>Produk Utama</th>
										<th>Kelurahan</th>
										<th>Kecamatan</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($obj_pot->{'records'} as $pot){ ?>
									<tr>
										<td><?php echo $pot->{'ikm_namaperusahaan'} ?></td>
										<td><?php echo $pot->{'sek_nama'} ?></td>
										<td><?php echo $pot->{'ikm_produkutama'} ?></td>
										<td><?php echo $pot->{'kel_nama'} ?></td>
										<td><?php echo $pot->{'kec_nama'} ?></td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
		  	</div>
	</div>

	<div class="panel panel-default">
	  <div class="panel-heading">Informasi</div>
	  <div class="panel-body">
	    <div class="column column_1_1" style="margin:0; padding:0;">
								<div class="header_top_bar_container style_2 border clearfix" style="margin:0; padding:0;">
									<div class="header_top_bar">
										
										<!--<ul class="social_icons dark clearfix"><-->
										<ul class="social_icons colors clearfix">
											
											
										</ul>
										<div class="latest_news_scrolling_list_container">
											<ul>
												<li class="category">INFO HARGA BOGOR</li>
												<li class="left"><a href="#"></a></li>
												<li class="right"><a href="#"></a></li>
												<li class="posts">
													<ul class="latest_news_scrolling_list">
														<?php if($tr_b_pokok->num_rows() > 0){ ?>
															<?php foreach($tr_b_pokok->result() as $up){ ?>
															<li><a href="<?php echo site_url("front/bahan_pokok_sub/$up->id_bahan_pok") ?>" title=""><?php echo $up->nama_sub_bahan_pokok ?><?php echo ' Rp.'.number_format( $up->harga, 0 , ',' , '.' ).'/'.$up->satuan; ?></a></li>
															<?php } ?>
														<?php }else{ ?>
															<li><a>Info Bahan Pokok Belum Tersedia</a></li>
														<?php } ?>
													</ul>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						<?php if($obj->{'status'} == 'success'){ ?>
						<div class="column column_1_1" style="margin:0; padding:0;">
							<div class="header_top_bar_container style_2 border clearfix" style="margin:0; padding:0;">
								<div class="header_top_bar">
									
									<div class="latest_news_scrolling_list_container">
										<ul>
											<li class="category">INFO HARGA NASIONAL</li>
											<li class="left"><a href="#"></a></li>
											<li class="right"><a href="#"></a></li>
											<li class="posts">
												<ul class="latest_news_scrolling_list">
													<?php foreach($obj->{'message'} as $m){ ?>
														<li><a><?php echo $m->nama_komoditi.' '.$m->ukuran.' '.$m->harga ?></a></li>
													<?php } ?>
												</ul>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<?php } ?>
						<?php if($obj2->{'status'} == 'success'){ ?>
						<div class="column column_1_1" style="margin:0; padding:0;">
							<div class="header_top_bar_container style_2 border clearfix" style="margin:0; padding:0;">
								<div class="header_top_bar">
									
									<!--<ul class="social_icons dark clearfix">
									<ul class="social_icons colors clearfix">-->
									
									<div class="latest_news_scrolling_list_container">
										<ul>
											<li class="category">NILAI TUKAR RUPIAH</li>
											<li class="left"><a href="#"></a></li>
											<li class="right"><a href="#"></a></li>
											<li class="posts">
												<ul class="latest_news_scrolling_list">
													<?php foreach($obj2->{'message'} as $m2){ ?>
														<li><a><?php echo $m2->kurs.', Jual : '.$m2->nilai_jual.', Beli : '.$m2->nilai_beli;  ?></a></li>
													<?php } ?>
												</ul>
											</li>
										</ul>
									</div>
									
								</div>
							</div>
						</div>
						<?php } ?>

		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">Berita</div>
		<div class="panel-body">
		    <div class="horizontal_carousel_container page_margin_top">
								<ul class="blog horizontal_carousel autoplay-1 visible-4 scroll-1 navigation-1 easing-easeInOutQuint duration-750">
									<?php foreach($berita->result() as $berita_row){ ?>
									<li class="post">
										<a href="<?php echo site_url("front/berita_details/$berita_row->id_berita"); ?>">
											<img src='<?php echo base_url("upload/berita/$berita_row->cover_berita") ?>' alt='img' style="height:150px;">
										</a>
										<h5><a href="<?php echo site_url("front/berita_details/$berita_row->id_berita"); ?>" title="High Altitudes May Aid Weight Control"><?php echo $berita_row->judul; ?></a></h5>
										<ul class="post_details simple">
											<li class="category"><a href="<?php echo site_url("front/berita_where_kategori/$berita_row->id_kategori"); ?>" title="<?php echo $berita_row->nama_kategori; ?>"><?php echo $berita_row->nama_kategori; ?></a></li>
											<li class="date">
												<?php echo $berita_row->tanggal_posting; ?>
											</li>
										</ul>
									</li>
									<?php } ?>
								</ul>
							</div>
		</div>
	</div>

	</div>	
	<div class="col-md-4">
		
	</div>
	
	<script src="<?php echo base_url('aset/DataTables') ?>/media/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url('aset/DataTables') ?>/media/js/dataTables.bootstrap.js"></script>

		
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.carouFredSel-6.2.1-packed.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.sliderControl.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/main.js"></script>

</body>
</html>
<script type="text/javascript">
		
		$(document).ready(function () {
			$('#data_tabel').dataTable({});
		});
</script>