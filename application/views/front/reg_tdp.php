<?php foreach($instansi->result() as $ins){} ?>
<!DOCTYPE html>
<html>
	<head>
		<title>Kementrian Perdagangan Kota Bogor - Home</title>
		<!--meta-->
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.2" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="Kementrian Perdagangan, Kementrian Perdagangan Kota Bogor" />
		<meta name="description" content="Kementrian Perdagangan Kota Bogor" />
		<!--style-->
		<link rel="stylesheet" href="<?php echo base_url('aset/bootstrap/css'); ?>/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/DataTables') ?>/media/css/jquery.dataTables.min.css">
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/reset.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/superfish.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/prettyPhoto.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/jquery.qtip.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/menu_styles.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/animations.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/responsive.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('aset/front') ?>/style/odometer-theme-default.css">

		
		<!--<link rel="stylesheet" type="text/css" href="style/dark_skin.css">-->
		<!--<link rel="stylesheet" type="text/css" href="style/high_contrast_skin.css">-->
		<link rel="shortcut icon" href="<?php echo base_url('aset/front') ?>/images/favicon.ico">
	</head>
	<!--<body class="image_1">
	<body class="image_1 overlay">
	<body class="image_2">
	<body class="image_2 overlay">
	<body class="image_3">
	<body class="image_3 overlay">
	<body class="image_4">
	<body class="image_4 overlay">
	<body class="image_5">
	<body class="image_5 overlay">
	<body class="pattern_1">
	<body class="pattern_2">
	<body class="pattern_3">
	<body class="pattern_4">
	<body class="pattern_5">
	<body class="pattern_6">
	<body class="pattern_7">
	<body class="pattern_8">
	<body class="pattern_9">
	<body class="pattern_10">-->
	<body>
		<div class="site_container">
			<div class="header_top_bar_container clearfix">
				<div class="header_top_bar">
					<form class="search" action="search.html" method="get">
						<input type="text" name="s" placeholder="Search..." value="Search..." class="search_input hint">
						<input type="submit" class="search_submit" value="">
					</form>
					<!--<ul class="social_icons dark clearfix">
					<ul class="social_icons colors clearfix">-->
					<ul class="social_icons clearfix">
						<li>
							<a target="_blank" href="http://facebook.com/QuanticaLabs" class="social_icon facebook" title="facebook">
								&nbsp;
							</a>
						</li>
						<li>
							<a target="_blank" href="https://twitter.com/QuanticaLabs" class="social_icon twitter" title="twitter">
								&nbsp;
							</a>
						</li>
						<li>
							<a href="mailto:contact@Kementrian Perdagangan Kota Bogor.com" class="social_icon mail" title="mail">
								&nbsp;
							</a>
						</li>
						<li>
							<a href="http://themeforest.net/user/QuanticaLabs/portfolio" class="social_icon envato" title="envato">
								&nbsp;
							</a>
						</li>
					</ul>
				</div>
			</div>
			<?php include_once "layout/menu_nav.php"; ?>
			<!-- slider -->
			
			<div class="col-md-12" style="color: black;">
				<div class="container">
					<div class="row">
						<div class="page_layout clearfix">
							<div class="page_header_left">
								<h1 class="page_title">Registrasi TDP</h1>
							</div>
							<div class="page_header_right">
								<ul class="bread_crumb">
									<li>
										<a title="Home" href="home.html">
											Home
										</a>
									</li>
									<li class="separator icon_small_arrow right_gray">
										&nbsp;
									</li>
									<li>
										Perizinan
									</li>
								</ul>
							</div>
						</div>

						<div class="divider_block clearfix">
							<hr class="divider first">
							<hr class="divider subheader_arrow">
							<hr class="divider last">
						</div>
						<br><br>
						
						<?php if($this->session->flashdata('tdp') == 'tambah_sukses'){ ?>
					<div class="alert alert-success" style="" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small"><span class="fa fa-check"></span> Berhasil.. Data Perusahaan telah selesai ditambah.. </p>
					</div>
					<?php }else if($this->session->flashdata('tdp') == 'edit_sukses'){ ?>
					<div class="alert alert-success" style="" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small"><span class="fa fa-check"></span> Berhasil.. Data Perusahaan telah selesai diedit.. </p>
					</div>
					<?php }else if($this->session->flashdata('tdp') == 'hapus_sukses'){ ?>
					<div class="alert alert-success" style="" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small"><span class="fa fa-check"></span> Berhasil.. Data Perusahaan telah selesai dihapus.. </p>
					</div>
					<?php }else if($this->session->flashdata('tdp') == 'error_upload'){ ?>
					<div class="alert alert-warning" style="" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small"><span class="fa fa-danger"></span> Maaf.. Input dibatalkan, <?php echo $this->session->flashdata('regulasi_error'); ?> </p>
					</div>
					<?php }else if($this->session->flashdata('tdp') == 'input_gagal'){ ?>
					<div class="alert alert-danger" style="" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small"> Maaf.. saat ini sedang mengalami gangguang, silahkan coba beberapa saat lagi.. </p>
					</div>
					<?php } ?>

						<ul class="nav nav-tabs">
                             <li class="active"><a href="#siup" data-toggle="tab" aria-expanded="true">SIUP</a></li>
                             <li class=""><a href="#tdp" data-toggle="tab" aria-expanded="false">TDP</a></li>
                             <li class=""><a href="#iupr" data-toggle="tab" aria-expanded="false">IUPR</a></li>
                             <li class=""><a href="#iupp" data-toggle="tab" aria-expanded="false">IUPP</a></li>
                             <li class=""><a href="#iuts" data-toggle="tab" aria-expanded="false">IUTS</a></li>
                             <li class=""><a href="#stpw" data-toggle="tab" aria-expanded="false">STPW</a></li>
                             <li class=""><a href="#tdg" data-toggle="tab" aria-expanded="false">TDG</a></li>
                             <li class=""><a href="#siupmb" data-toggle="tab" aria-expanded="false">SIUP-MB</a></li>
                             <li class=""><a href="#tdpud" data-toggle="tab" aria-expanded="false">TDPUD</a></li>
                             <li class=""><a href="#b3" data-toggle="tab" aria-expanded="false">B3</a></li>
                         </ul>
                         <div id="myTabContent" class="tab-content">
                         	<br><br>
                         	<div class="tab-pane fade active in" id="siup">
                         		<form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('front/reg_perizinan'); ?>">
                         			<input type="hidden" name="table" value="t_siup">
                              <div class="col-md-6">

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Nama Perusahaan</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" name="nm_perusahaan" placeholder="Nama Perusahaan" class="form-control" required/>
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Nama Penanggung jawab</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" name="nm_penanggungjwb" placeholder="Penanggung jawab" class="form-control" required/>
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Alamat Perusahaan</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <textarea class="form-control" name="almt_perusahaan"></textarea>
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Kecamatan</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <select class="form-control" name="kecamatan" id="kecamatan" required>
                                                <option>-- Kecamatan --</option>
                                                <?php foreach ($kecamatan->result() as $row): ?>
                                                    <option value="<?php echo $row->id_kecamatan; ?>"><?php echo $row->nm_kecamatan; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Kelurahan</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <select class="form-control" name="kelurahan" id="kelurahan" required disabled>

                                            </select>
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">No. Telp</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="phone" name="no_telp" id="no_telp" class="form-control" placeholder="No. Telp">
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">No. Fax</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" name="no_fax" id="no_fax" class="form-control" placeholder="No. Fax">
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Email</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                                        </div>                                            
                                    </div>
                                </div>
                                
                            </div>

                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Modal Usaha</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" name="modal_usaha" id="modal_usaha" class="form-control" placeholder="Modal Usaha">
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Kelembagaan</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <select class="form-control" name="kelembagaan">
                                                <option>--  Kelembagaan --</option>
                                                <option value="Distributor">Distributor</option>
                                                <option value="Sub Distributor">Sub Distributor</option>
                                                <option value="Agen">Agen</option>
                                                <option value="Sub Agen">Sub Agen</option>
                                            </select>
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">KBLI</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" name="kbli" placeholder="KBLI" class="form-control" required/>
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Barang Dagangan</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <textarea class="form-control" name="barang_dagangan" required></textarea>
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">No. SIUP </label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" name="no_sk" placeholder="No. SK" class="form-control" required/>
                                        </div>                                            
                                    </div>
                                </div> 

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tanggal SIUP </label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="date" name="tgl_sk" placeholder="Tanggal SK" class="form-control" required/>
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Masaberlaku</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input type="date" name="masaberlaku" placeholder="Masa Berlaku" class="form-control" required/>
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Foto</label>
                                    <div class="col-md-9">                                            
                                        <input type="file" name="userfile" />          
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3 control-label"></div>
                                    <div class="col-md-9">
                                       <button class="btn btn-primary" name="btnSiup">Submit</button>
                                       <button type="reset" class="btn btn-default">Clear Form</button>  
                                   </div>
                               </div>

                           </div>
                         		</form>
                         	</div>

                         	<div class="tab-pane fade" id="tdp">
                    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('front/reg_perizinan'); ?>">
                      <input type="hidden" name="table" value="t_tdp">
                      <div class="col-md-6">

                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama Perusahaan</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" name="nm_perusahaan" placeholder="Nama Perusahaan" class="form-control" required/>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama Penanggung jawab</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" name="nm_penanggungjwb" placeholder="Penanggung jawab" class="form-control" required/>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Alamat Perusahaan</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <textarea class="form-control" name="almt_perusahaan"></textarea>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Kecamatan</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <select class="form-control" name="kecamatan" id="kecamatan2" required>
                                        <option>-- Kecamatan --</option>
                                        <?php foreach ($kecamatan->result() as $row2): ?>
                                            <option value="<?php echo $row2->id_kecamatan; ?>"><?php echo $row2->nm_kecamatan; ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Kelurahan</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <select class="form-control" name="kelurahan" id="kelurahan2" required disabled>

                                    </select>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">NPWP</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" name="npwp" placeholder="NPWP" class="form-control" required/>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">No. Telp</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="phone" name="no_telp" id="no_telp" class="form-control" placeholder="No. Telp">
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">No. Fax</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" name="no_fax" id="no_fax" class="form-control" placeholder="No. Fax">
                                </div>                                            
                            </div>
                        </div>


                    </div>


                    <div class="col-md-6">

                        <div class="form-group">
                            <label class="col-md-3 control-label">Email</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                                </div>                                            
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 control-label">Kegiatan Usaha Pokok</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <textarea class="form-control" name="keg_usaha_pokok" required></textarea>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">KBLI</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" name="kbli" placeholder="KBLI" class="form-control" required/>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Status Kantor</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <select class="form-control" name="status_kantor">
                                        <option>--  Status Kantor --</option>
                                        <option value="Kantor Tunggal">Kantor Tunggal</option>
                                        <option value="Kantor Pusat">Kantor Pusat</option>
                                    </select>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">No. TDP </label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" name="no_sk" placeholder="No. SK" class="form-control" required/>
                                </div>                                            
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="col-md-3 control-label">Tanggal TDP </label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="date" name="tgl_sk" placeholder="Tanggal SK" class="form-control" required/>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Masaberlaku</label>
                            <div class="col-md-9">                                            
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                    <input type="date" name="masaberlaku" placeholder="Masa Berlaku" class="form-control" required/>
                                </div>                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Foto</label>
                            <div class="col-md-9">                                            
                                <input type="file" name="userfile" />          
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3 control-label"></div>
                            <div class="col-md-9">
                               <button class="btn btn-primary" name="btnTdp">Submit</button>
                               <button type="reset" class="btn btn-default">Clear Form</button>  
                           </div>
                       </div>
                   </div>
               </form>
           </div>

           <div class="tab-pane fade" id="iupr">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('front/reg_perizinan'); ?>">
              <input type="hidden" name="table" value="t_iupr">
              <div class="col-md-6">

                <div class="form-group">
                 <label class="col-md-3 control-label">Tanggal Pendaftaran</label>
                 <div class="col-md-9">                                            
                  <div class="input-group">
                   <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                   <input type="date" name="tgl_pendaftaran" placeholder="Tanggal Pendaftaran" class="form-control" required/>
               </div>                                            
           </div>
       </div>

       <div class="form-group">
        <label class="col-md-3 control-label">Nama Perusahaan</label>
        <div class="col-md-9">                                            
            <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                <input type="text" name="nm_perusahaan" placeholder="Nama Perusahaan" class="form-control" required/>
            </div>                                            
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Nama Penanggung jawab</label>
        <div class="col-md-9">                                            
            <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                <input type="text" name="nm_penanggungjwb" placeholder="Penanggung jawab" class="form-control" required/>
            </div>                                            
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Alamat Perusahaan</label>
        <div class="col-md-9">                                            
            <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                <textarea class="form-control" name="almt_perusahaan"></textarea>
            </div>                                            
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Kecamatan</label>
        <div class="col-md-9">                                            
            <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                <select class="form-control" name="kecamatan" id="kecamatan3" required>
                    <option>-- Kecamatan --</option>
                    <?php foreach ($kecamatan->result() as $row3): ?>
                        <option value="<?php echo $row3->id_kecamatan; ?>"><?php echo $row3->nm_kecamatan; ?></option>
                    <?php endforeach ?>
                </select>
            </div>                                            
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Kelurahan</label>
        <div class="col-md-9">                                            
            <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                <select class="form-control" name="kelurahan" id="kelurahan3" required disabled>

                </select>
            </div>                                            
        </div>
    </div>

</div>
<div class="col-md-6">

    <div class="form-group">
     <label class="col-md-3 control-label">Jenis Usaha</label>
     <div class="col-md-9">                                            
      <div class="input-group">
       <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
       <input type="text" name="jenis_usaha" placeholder="KBLI" class="form-control" required/>
   </div>                                            
</div>
</div>

<div class="form-group">
   <label class="col-md-3 control-label">SIUP</label>
   <div class="col-md-9">                                            
      <div class="input-group">
         <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
         <input type="text" name="siup" placeholder="SIUP" class="form-control" required/>
     </div>                                            
 </div>
</div>

<div class="form-group">
   <label class="col-md-3 control-label">TDP</label>
   <div class="col-md-9">                                            
      <div class="input-group">
         <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
         <input type="text" name="tdp" placeholder="TDP" class="form-control" required/>
     </div>                                            
 </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">No. IUPR </label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="text" name="no_sk" placeholder="No. SK" class="form-control" required/>
        </div>                                            
    </div>
</div> 

<div class="form-group">
    <label class="col-md-3 control-label">Tanggal IUPR </label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="date" name="tgl_sk" placeholder="Tanggal SK" class="form-control" required/>
        </div>                                            
    </div>
</div>

<div class="form-group">
 <label class="col-md-3 control-label">Foto</label>
 <div class="col-md-9">                                            
  <input type="file" name="userfile" />          
</div>
</div>

<div class="form-group">
 <div class="col-md-3 control-label"></div>
 <div class="col-md-9">
  <button class="btn btn-primary" name="btnIupr">Submit</button>
  <button type="reset" class="btn btn-default">Clear Form</button>  
</div>
</div>

</div>
</form>
</div>

<div class="tab-pane fade" id="iupp">
    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('front/reg_perizinan'); ?>">
      <input type="hidden" name="table" value="t_iupp">
      <div class="col-md-6">

        <div class="form-group">
         <label class="col-md-3 control-label">Tanggal Pendaftaran</label>
         <div class="col-md-9">                                            
          <div class="input-group">
           <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
           <input type="date" name="tgl_pendaftaran" placeholder="Tanggal Pendaftaran" class="form-control" required/>
       </div>                                            
   </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">Nama Perusahaan</label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="text" name="nm_perusahaan" placeholder="Nama Perusahaan" class="form-control" required/>
        </div>                                            
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">Nama Penanggung jawab</label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="text" name="nm_penanggungjwb" placeholder="Penanggung jawab" class="form-control" required/>
        </div>                                            
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">Alamat Perusahaan</label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <textarea class="form-control" name="almt_perusahaan"></textarea>
        </div>                                            
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">Kecamatan</label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <select class="form-control" name="kecamatan" id="kecamatan4" required>
                <option>-- Kecamatan --</option>
                <?php foreach ($kecamatan->result() as $row4): ?>
                    <option value="<?php echo $row4->id_kecamatan; ?>"><?php echo $row4->nm_kecamatan; ?></option>
                <?php endforeach ?>
            </select>
        </div>                                            
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">Kelurahan</label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <select class="form-control" name="kelurahan" id="kelurahan4" required disabled>

            </select>
        </div>                                            
    </div>
</div>

<div class="form-group">
       <label class="col-md-3 control-label">Jenis Usaha</label>
       <div class="col-md-9">                                            
          <div class="input-group">
             <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
             <input type="text" name="jenis_usaha" placeholder="KBLI" class="form-control" required/>
         </div>                                            
     </div>
 </div>

</div>
<div class="col-md-6">

    <div class="form-group">
        <label class="col-md-3 control-label">SIUP</label>
        <div class="col-md-9">                                            
          <div class="input-group">
             <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
             <input type="text" name="siup" placeholder="SIUP" class="form-control" required/>
         </div>                                            
     </div>
 </div>

<div class="form-group">
   <label class="col-md-3 control-label">TDP</label>
   <div class="col-md-9">                                            
      <div class="input-group">
         <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
         <input type="text" name="tdp" placeholder="TDP" class="form-control" required/>
     </div>                                            
 </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">No. IUPP </label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="text" name="no_sk" placeholder="No. SK" class="form-control" required/>
        </div>                                            
    </div>
</div> 

<div class="form-group">
    <label class="col-md-3 control-label">Tanggal IUPP </label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="date" name="tgl_sk" placeholder="Tanggal SK" class="form-control" required/>
        </div>                                            
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">No. Rekomendasi </label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="text" name="no_rekomendasi" placeholder="No. Rekomandasi" class="form-control" required/>
        </div>                                            
    </div>
</div> 

<div class="form-group">
    <label class="col-md-3 control-label">No. yang direncanakan </label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="text" name="no_rencana" placeholder="Nomor yang direncanakan" class="form-control" required/>
        </div>                                            
    </div>
</div>

<div class="form-group">
 <label class="col-md-3 control-label">Foto</label>
 <div class="col-md-9">                                            
  <input type="file" name="userfile" />          
</div>
</div>

<div class="form-group">
 <div class="col-md-3 control-label"></div>
 <div class="col-md-9">
  <button class="btn btn-primary" name="btnIupp">Submit</button>
  <button type="reset" class="btn btn-default">Clear Form</button>  
</div>
</div>

</div>
</form>
</div>

<div class="tab-pane fade" id="iuts">
    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('front/reg_perizinan'); ?>">
      <input type="hidden" name="table" value="t_iuts">
      <div class="col-md-6">

        <div class="form-group">
            <label class="col-md-3 control-label">Nama Perusahaan Pemberi</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="nm_perusahaan" placeholder="Nama Perusahaan" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Nama Penanggung jawab</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="nm_penanggungjwb" placeholder="Penanggung jawab" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Alamat Perusahaan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="almt_perusahaan"></textarea>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kecamatan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kecamatan" id="kecamatan5" required>
                        <option>-- Kecamatan --</option>
                        <?php foreach ($kecamatan->result() as $row5): ?>
                            <option value="<?php echo $row5->id_kecamatan; ?>"><?php echo $row5->nm_kecamatan; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kelurahan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kelurahan" id="kelurahan5" required disabled>

                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">NPWP</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="npwp" placeholder="NPWP" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">No. Telp</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="phone" name="no_telp" id="no_telp" class="form-control" placeholder="No. Telp">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">No. Fax</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="no_fax" id="no_fax" class="form-control" placeholder="No. Fax">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Email</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                </div>                                            
            </div>
        </div>


        <div class="form-group">
            <label class="col-md-3 control-label">Kegiatan Usaha Pokok</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="keg_usaha_pokok" required></textarea>
                </div>                                            
            </div>
        </div>

    </div>
    <div class="col-md-6">

        <div class="form-group">
            <label class="col-md-3 control-label">KBLI</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="kbli" placeholder="KBLI" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Status Kantor</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="status_kantor">
                        <option>--  Status Kantor --</option>
                        <option value="Kantor Tunggal">Kantor Tunggal</option>
                        <option value="Kantor Pusat">Kantor Pusat</option>
                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">SIUP</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="siup" id="siup" class="form-control" placeholder="SIUP">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
         <label class="col-md-3 control-label">TDP</label>
         <div class="col-md-9">                                            
          <div class="input-group">
           <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
           <input type="text" name="tdp" placeholder="TDP" class="form-control" required/>
       </div>                                            
   </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">No. IUTS </label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="text" name="no_sk" placeholder="No. SK" class="form-control" required/>
        </div>                                            
    </div>
</div> 

<div class="form-group">
    <label class="col-md-3 control-label">Tanggal IUTS </label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="date" name="tgl_sk" placeholder="Tanggal SK" class="form-control" required/>
        </div>                                            
    </div>
</div>


<div class="form-group">
    <label class="col-md-3 control-label">No. Rekomendasi </label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="text" name="no_rekomendasi" placeholder="No. Rekomandasi" class="form-control" required/>
        </div>                                            
    </div>
</div> 

<div class="form-group">
    <label class="col-md-3 control-label">No. yang direncanakan </label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="text" name="no_rencana" placeholder="Nomor yang direncanakan" class="form-control" required/>
        </div>                                            
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">Masaberlaku</label>
    <div class="col-md-9">                                            
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
            <input type="date" name="masaberlaku" placeholder="Masa Berlaku" class="form-control" required/>
        </div>                                            
    </div>
</div>


<div class="form-group">
    <label class="col-md-3 control-label">Foto</label>
    <div class="col-md-9">                                            
        <input type="file" name="userfile" />          
    </div>
</div>

<div class="form-group">
    <div class="col-md-3 control-label"></div>
    <div class="col-md-9">
       <button class="btn btn-primary" name="btnIuts">Submit</button>
       <button type="reset" class="btn btn-default">Clear Form</button>  
   </div>
</div>

</div>
</form>
</div>

<div class="tab-pane fade" id="stpw">
    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('front/reg_perizinan'); ?>">
      <input type="hidden" name="table" value="t_stpw">
      <div class="col-md-6">
        <div class="form-group">
            <label class="col-md-3 control-label">No. Pendaftaran</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="no_pendaftaran" placeholder="No. Pendaftaran" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Nama Perusahaan Penerima</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="nm_perusahaan" placeholder="Nama Perusahaan" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Nama Penanggung jawab</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="nm_penanggungjwb" placeholder="Penanggung jawab" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Alamat Perusahaan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="almt_perusahaan"></textarea>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kecamatan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kecamatan" id="kecamatan6" required>
                        <option>-- Kecamatan --</option>
                        <?php foreach ($kecamatan->result() as $row6): ?>
                            <option value="<?php echo $row6->id_kecamatan; ?>"><?php echo $row6->nm_kecamatan; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kelurahan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kelurahan" id="kelurahan6" required disabled>

                    </select>
                </div>                                            
            </div>
        </div>

    </div>
    <div class="col-md-6">

        <div class="form-group">
            <label class="col-md-3 control-label">NPWP</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="npwp" placeholder="NPWP" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">IUTS</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="iuts" id="iuts" class="form-control" placeholder="IUTS">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">No. STPW </label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="no_sk" placeholder="No. SK" class="form-control" required/>
                </div>                                            
            </div>
        </div> 

        <div class="form-group">
            <label class="col-md-3 control-label">Tanggal STPW </label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="date" name="tgl_sk" placeholder="Tanggal SK" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">No. Telp</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="phone" name="no_telp" id="no_telp" class="form-control" placeholder="No. Telp">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Masaberlaku</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                    <input type="date" name="masaberlaku" placeholder="Masa Berlaku" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Foto</label>
            <div class="col-md-9">                                            
                <input type="file" name="userfile" />          
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-3 control-label"></div>
            <div class="col-md-9">
               <button class="btn btn-primary" name="btnStpw">Submit</button>
               <button type="reset" class="btn btn-default">Clear Form</button>  
           </div>
       </div>

   </div>
</form>
</div>

<div class="tab-pane fade" id="tdg">
    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('front/reg_perizinan'); ?>">
      <input type="hidden" name="table" value="t_tdg">
      <div class="col-md-6">

        <div class="form-group">
            <label class="col-md-3 control-label">Nama Perusahaan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="nm_perusahaan" placeholder="Nama Perusahaan" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Nama Penanggung jawab</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="nm_penanggungjwb" placeholder="Penanggung jawab" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kecamatan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kecamatan" id="kecamatan7" required>
                        <option>-- Kecamatan --</option>
                        <?php foreach ($kecamatan->result() as $row7): ?>
                            <option value="<?php echo $row7->id_kecamatan; ?>"><?php echo $row7->nm_kecamatan; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kelurahan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kelurahan" id="kelurahan7" required disabled>

                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Alamat Penanggung Jawab</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="almt_penanggungjwb"></textarea>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Alamat Gudang</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="almt_gudang"></textarea>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">No. Telp</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="no_telp" class="form-control" placeholder="No. Telp">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">No. Fax</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="no_fax" class="form-control" placeholder="No. Fax">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Email</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="email" class="form-control" placeholder="Email">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Jenis Isi Gudang</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="jenis_isi_gudang" id="jenis_isi_gudang" class="form-control" placeholder="Jenis Isi Gudang">
                </div>                                            
            </div>
        </div>

    </div>
    <div class="col-md-6">

        <div class="form-group">
            <label class="col-md-3 control-label">Klasifikasi Gudang</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="klasifikasi_gudang" id="klasifikasi_gudang" class="form-control" placeholder="Klasifikasi Gudang">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Luas Gudang</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="luas_gudang" id="ls_gudang" class="form-control" placeholder="Jenis Isi Gudang">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kapasitas Gudang</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="kapasitas_gudang" id="jenis_isi_gudang" class="form-control" placeholder="Kapasitas Gudang">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kelengkapan Gudang</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <input type="radio" name="kelengkapan_gudang" value="Berpendingin">Berpendingin <br>
                    <input type="radio" name="kelengkapan_gudang" value="Tidak Berpendingin">Tidak Berpendingin
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">No. TDG </label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="no_tdg" placeholder="No. TDG" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Tanggal TDG </label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="date" name="tgl_tdg" placeholder="No. TDG" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">TDP </label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="tdp" placeholder="TDP" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Masaberlaku</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                    <input type="date" name="masaberlaku" placeholder="Masa Berlaku" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Titik Kordinat</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="longitude" id="klasifikasi_gudang" class="form-control" placeholder="Longitude">
                    <input type="text" name="latitude" id="klasifikasi_gudang" class="form-control" placeholder="Latitude">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Foto</label>
            <div class="col-md-9">                                            
                <input type="file" name="userfile" />          
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-3 control-label"></div>
            <div class="col-md-9">
             <button class="btn btn-primary" name="btnTdg">Submit</button>
             <button type="reset" class="btn btn-default">Clear Form</button>  
         </div>
     </div>

 </div>
</form>
</div>

<div class="tab-pane fade" id="siupmb">
    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('front/reg_perizinan'); ?>">
       <input type="hidden" name="table" value="t_siupmb">
       <div class="col-md-6">

        <div class="form-group">
            <label class="col-md-3 control-label">Nama Perusahaan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="nm_perusahaan" placeholder="Nama Perusahaan" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Alamat Perusahaan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="almt_perusahaan"></textarea>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kecamatan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kecamatan" id="kecamatan8" required>
                        <option>-- Kecamatan --</option>
                        <?php foreach ($kecamatan->result() as $row8): ?>
                            <option value="<?php echo $row8->id_kecamatan; ?>"><?php echo $row8->nm_kecamatan; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kelurahan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kelurahan" id="kelurahan8" required disabled>

                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">SIUP-MB</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="siupmb" id="siupmb" class="form-control" placeholder="SIUP-MB">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">TDP</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="tdp" id="tdp" class="form-control" placeholder="SIUP-MB">
                </div>                                            
            </div>
        </div>

    </div>
    <div class="col-md-6">

        <div class="form-group">
            <label class="col-md-3 control-label">Tanggal Keluar </label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="date" name="tgl_keluar" placeholder="Tanggal Keluar" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Tanggal Berakhir </label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="date" name="tgl_berakhir" placeholder="Tanggal Berakhir" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Instansi yang Mengeluarkan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="ins_yg_mengeluarkan" placeholder="" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        

        <div class="form-group">
            <label class="col-md-3 control-label">Keterangan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="keterangan" required></textarea>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Foto</label>
            <div class="col-md-9">                                            
                <input type="file" name="userfile" />          
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-3 control-label"></div>
            <div class="col-md-9">
               <button class="btn btn-primary" name="btnSiupmb">Submit</button>
               <button type="reset" class="btn btn-default">Clear Form</button>  
           </div>
       </div>

   </div>
</form>
</div>

<div class="tab-pane fade" id="tdpud">
    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('front/reg_perizinan'); ?>">
      <input type="hidden" name="table" value="t_tdpud">
      <div class="col-md-6">

        <div class="form-group">
            <label class="col-md-3 control-label">Nama Perusahaan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="nm_perusahaan" placeholder="Nama Perusahaan" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Nama Penanggung jawab</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="nm_penanggungjwb" placeholder="Penanggung jawab" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Alamat Perusahaan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="almt_perusahaan"></textarea>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kecamatan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kecamatan" id="kecamatan9" required>
                        <option>-- Kecamatan --</option>
                        <?php foreach ($kecamatan->result() as $row9): ?>
                            <option value="<?php echo $row9->id_kecamatan; ?>"><?php echo $row9->nm_kecamatan; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kelurahan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kelurahan" id="kelurahan9" required disabled>

                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">NPWP</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="npwp" placeholder="NPWP" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">No. Telp</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="phone" name="no_telp" id="no_telp" class="form-control" placeholder="No. Telp">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">No. Fax</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="no_fax" id="no_fax" class="form-control" placeholder="No. Fax">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Email</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                </div>                                            
            </div>
        </div>

    </div>

    <div class="col-md-6">

        <div class="form-group">
            <label class="col-md-3 control-label">Kegiatan Usaha Pokok</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="keg_usaha_pokok" required></textarea>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">KBLI</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="kbli" placeholder="KBLI" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Status Kantor</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="status_kantor">
                        <option>--  Status Kantor --</option>
                        <option value="Kantor Tunggal">Kantor Tunggal</option>
                        <option value="Kantor Pusat">Kantor Pusat</option>
                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">TDP</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="tdp" placeholder="TDP" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">No. TDPUD </label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="no_sk" placeholder="No. SK" class="form-control" required/>
                </div>                                            
            </div>
        </div> 

        <div class="form-group">
            <label class="col-md-3 control-label">Tanggal TDPUD </label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="date" name="tgl_sk" placeholder="Tanggal SK" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Jenis TDPUD</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="jenis_tdpud">
                        <option>--  Jenis --</option>
                        <option value="Distributor">Distributor</option>
                        <option value="Agen">Agen</option>
                        <option value="Sub Agen">Sub Agen</option>
                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Masaberlaku</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                    <input type="date" name="masaberlaku" placeholder="Masa Berlaku" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Foto</label>
            <div class="col-md-9">                                            
                <input type="file" name="userfile" />          
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-3 control-label"></div>
            <div class="col-md-9">
             <button class="btn btn-primary" name="btnTdpud">Submit</button>
             <button type="reset" class="btn btn-default">Clear Form</button>  
         </div>
     </div>
 </div>
</form>
</div>

<div class="tab-pane fade" id="b3">
    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('front/reg_perizinan'); ?>">
      <input type="hidden" name="table" value="t_b3">
      <div class="col-md-6">

        <div class="form-group">
            <label class="col-md-3 control-label">No. CAS</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="no_cas" placeholder="No. Cas" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">POS TARIF/HS</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="pos_trf_hs" class="form-control" placeholder="Izin Penyimpanan B3">
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kecamatan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kecamatan" id="kecamatan10" required>
                        <option>-- Kecamatan --</option>
                        <?php foreach ($kecamatan->result() as $row10): ?>
                            <option value="<?php echo $row10->id_kecamatan; ?>"><?php echo $row10->nm_kecamatan; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Kelurahan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <select class="form-control" name="kelurahan" id="kelurahan10" required disabled>

                    </select>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Alamat Penyimpanan</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="almt_penyimpanan"></textarea>
                </div>                                            
            </div>
        </div>

        


    </div>
    <div class="col-md-6">

    <div class="form-group">
            <label class="col-md-3 control-label">Nama Penanggung jawab</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="nm_penanggungjwb" placeholder="Penanggung jawab" class="form-control" required/>
                </div>                                            
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Uraian Barang</label>
            <div class="col-md-9">                                            
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <textarea class="form-control" name="uraian_brg"></textarea>
                </div>                                            
            </div>
        </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Tata Niaga Impor</label>
        <div class="col-md-9">                                            
            <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                <input type="text" name="tata_niaga_impor" placeholder="Tata Niaga Impor" class="form-control" required/>
            </div>                                            
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Keperluan diluar pangan</label>
        <div class="col-md-9">                                            
            <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                <input type="text" name="kep_lain_tdk_utk_pgn" placeholder="Keperluan diluar pangan" class="form-control" required/>
            </div>                                            
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Laboratorium / Penelitian</label>
        <div class="col-md-9">                                            
            <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                <input type="text" name="lab" placeholder="Laboratorium / Penelitian" class="form-control" required/>
            </div>                                            
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-3 control-label"></div>
        <div class="col-md-9">
           <button class="btn btn-primary" name="btnB3">Submit</button>
           <button type="reset" class="btn btn-default">Clear Form</button>  
       </div>
   </div>

</div>
</form>

</div>
                         </div>

						
					</div>
				</div>
			</div>
			<div class="page" style="color: black;">
				<div class="page_layout clearfix">

				</div>
			</div>

			
			
			<?php include "layout/footer.php"; ?>
		</div>
		<div class="background_overlay"></div>
		<!--js-->
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.ba-bbq.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery-ui-1.11.1.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.carouFredSel-6.2.1-packed.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.touchSwipe.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.transit.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.sliderControl.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.timeago.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.hint.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.prettyPhoto.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.qtip.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/jquery.blockUI.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/main.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/js/odometer.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('aset/front') ?>/jquery.gmap.js"></script>
		

		
		<script src="<?php echo base_url('aset/highcharts') ?>/js/highcharts.js"></script>
		<script src="<?php echo base_url('aset/highcharts') ?>/js/modules/exporting.js"></script>
		
		<script src="<?php echo base_url('aset/DataTables') ?>/media/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url('aset/DataTables') ?>/media/js/dataTables.bootstrap.js"></script>
		
		<script type="text/javascript">
		
		</script>
		
	</body>
</html>
<script type="text/javascript">
	$('#table').dataTable();

	$("#kecamatan").change(function(){
        var id_kecamatan = $("#kecamatan option:selected").val();
        $.ajax({
            url: "<?php echo site_url('backend/kelurahan')?>",
            type: "POST",
            data    : "id_kecamatan="+id_kecamatan,
            success : function (msg) {
                document.getElementById("kelurahan").disabled = false;
                $("#kelurahan").html(msg);
                //$("#kelurahan").css("color","black");
            }
        });
    });

    $("#izin_usaha").change(function(){
        var izin_usaha = $("#izin_usaha option:selected").val();
        $.ajax({
            url: "<?php echo site_url('backend/siup')?>",
            type: "POST",
            data    : "iu="+izin_usaha,
            success : function (msg) {
                document.getElementById("siup").disabled = false;
                $("#siup").val(msg);
                //$("#kelurahan").css("color","black");
            }
        });
        if (izin_usaha == 'TDG') {
            document.getElementById("stok").disabled = false;
        } else {
            document.getElementById("stok").disabled = true;
        }
    });
</script>