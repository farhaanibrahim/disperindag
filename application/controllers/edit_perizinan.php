<?php 
/**
* 
*/
class Edit_perizinan extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('back_model');
		$this->load->model('front_model');
	}

	public function edit_siup($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['kecamatan'] = $this->back_model->mst_kecamatan();
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'EditSIUP | Disperindag';

			$data['siup'] = $this->back_model->data_siup_byid($id);

			$this->load->view('backend/edit_data_siup',$data);
		} else {
			redirect(base_url('backend'));
		}
		
	}

	public function edit_tdp($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['kecamatan'] = $this->back_model->mst_kecamatan();
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Edit TDP | Disperindag';

			$data['tdp'] = $this->back_model->data_tdp_byid($id);

			$this->load->view('backend/edit_data_tdp',$data);
		} else {
			redirect(base_url('backend'));
		}
		
	}

	public function edit_iupr($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['kecamatan'] = $this->back_model->mst_kecamatan();
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Edit IUPR | Disperindag';

			$data['iupr'] = $this->back_model->data_iupr_byid($id);

			$this->load->view('backend/edit_data_iupr',$data);
		} else {
			redirect(base_url('backend'));
		}
		
	}

	public function edit_iupp($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['kecamatan'] = $this->back_model->mst_kecamatan();
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Edit IUPP | Disperindag';

			$data['iupp'] = $this->back_model->data_iupp_byid($id);

			$this->load->view('backend/edit_data_iupp',$data);
		} else {
			redirect(base_url('backend'));
		}
		
	}

	public function edit_iuts($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['kecamatan'] = $this->back_model->mst_kecamatan();
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Edit IUTS | Disperindag';

			$data['iuts'] = $this->back_model->data_iuts_byid($id);

			$this->load->view('backend/edit_data_iuts',$data);
		} else {
			redirect(base_url('backend'));
		}
		
	}
	public function edit_stpw($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['kecamatan'] = $this->back_model->mst_kecamatan();
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Edit STPW | Disperindag';

			$data['stpw'] = $this->back_model->data_stpw_byid($id);

			$this->load->view('backend/edit_data_stpw',$data);
		} else {
			redirect(base_url('backend'));
		}
		
	}

	public function edit_tdg($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['kecamatan'] = $this->back_model->mst_kecamatan();
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Edit TDG | Disperindag';

			$data['tdg'] = $this->back_model->data_tdg_byid($id);

			$this->load->view('backend/edit_data_tdg',$data);
		} else {
			redirect(base_url('backend'));
		}
		
	}

	public function edit_siupmb($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['kecamatan'] = $this->back_model->mst_kecamatan();
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Edit SIUP-MB | Disperindag';

			$data['siupmb'] = $this->back_model->data_siupmb_byid($id);

			$this->load->view('backend/edit_data_siupmb',$data);
		} else {
			redirect(base_url('backend'));
		}
		
	}

	public function edit_tdpud($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['kecamatan'] = $this->back_model->mst_kecamatan();
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Edit TDPUD | Disperindag';

			$data['tdpud'] = $this->back_model->data_tdpud_byid($id);

			$this->load->view('backend/edit_data_tdpud',$data);
		} else {
			redirect(base_url('backend'));
		}
		
	}

	public function edit_b3($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['kecamatan'] = $this->back_model->mst_kecamatan();
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Edit B3 | Disperindag';

			$data['b3'] = $this->back_model->data_b3_byid($id);

			$this->load->view('backend/edit_data_b3',$data);
		} else {
			redirect(base_url('backend'));
		}
		
	}

	public function edit_perizinan_ac()
	{
		if ($this->session->userdata('admin_id')) {
			$config['upload_path'] = './upload';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['max_size']	= '6012312481248';
			$config['max_width']  = '211241242024';
			$config['max_height']  = '171212412468';

			$this->load->library('upload', $config);

			if (isset($_POST['btnSiup'])) {
				$table = $this->input->post('table');
				$id = $this->input->post('id');
				if ($this->upload->do_upload('userfile')) {
					$data = array('upload_data' => $this->upload->data());

					$file = array('upload_sukses' => $this->upload->data());
					$nama_file = $file['upload_sukses']['file_name'];
					$data = array(
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'no_telp'=>$this->input->post('no_telp'),
						'no_fax'=>$this->input->post('no_fax'),
						'email'=>$this->input->post('email'),
						'modal_usaha'=>$this->input->post('modal_usaha'),
						'kelembagaan'=>$this->input->post('kelembagaan'),
						'kbli'=>$this->input->post('kbli'),
						'barang_dagangan'=>$this->input->post('barang_dagangan'),
						'no_siup'=>$this->input->post('no_sk'),
						'tgl_siup'=>$this->input->post('tgl_sk'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'foto'=>$nama_file
						);
					$this->back_model->update_perizinan($data,$table,$id);
					$this->session->set_flashdata('update','Data SIUP telah diupdate');
					redirect(base_url('backend/tdp'));
				} else {
					$data = array(
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'no_telp'=>$this->input->post('no_telp'),
						'no_fax'=>$this->input->post('no_fax'),
						'email'=>$this->input->post('email'),
						'modal_usaha'=>$this->input->post('modal_usaha'),
						'kelembagaan'=>$this->input->post('kelembagaan'),
						'kbli'=>$this->input->post('kbli'),
						'barang_dagangan'=>$this->input->post('barang_dagangan'),
						'no_siup'=>$this->input->post('no_sk'),
						'tgl_siup'=>$this->input->post('tgl_sk'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'foto'=>$this->input->post('foto_lama')
						);
					$this->back_model->update_perizinan($data,$table,$id);
					$this->session->set_flashdata('update','Data SIUP telah diupdate');
					redirect(base_url('backend/tdp'));
				}
				
			} elseif (isset($_POST['btnTdp'])) {
				$table = $this->input->post('table');
				$id = $this->input->post('id');
				if ($this->upload->do_upload('userfile')) {
					$data = array('upload_data' => $this->upload->data());

					$file = array('upload_sukses' => $this->upload->data());
					$nama_file = $file['upload_sukses']['file_name'];
					$data = array(
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'npwp'=>$this->input->post('npwp'),
						'no_telp'=>$this->input->post('no_telp'),
						'no_fax'=>$this->input->post('no_fax'),
						'email'=>$this->input->post('email'),
						'keg_usaha_pokok'=>$this->input->post('keg_usaha_pokok'),
						'kbli'=>$this->input->post('kbli'),
						'status_kantor'=>$this->input->post('status_kantor'),
						'no_tdp'=>$this->input->post('no_sk'),
						'tgl_tdp'=>$this->input->post('tgl_sk'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'foto'=>$nama_file
						);
					$this->back_model->update_perizinan($data,$table,$id);
					$this->session->set_flashdata('update','Data SIUP telah diupdate');
					redirect(base_url('backend/tdp'));
				} else {
					$data = array(
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'npwp'=>$this->input->post('npwp'),
						'no_telp'=>$this->input->post('no_telp'),
						'no_fax'=>$this->input->post('no_fax'),
						'email'=>$this->input->post('email'),
						'keg_usaha_pokok'=>$this->input->post('keg_usaha_pokok'),
						'kbli'=>$this->input->post('kbli'),
						'status_kantor'=>$this->input->post('status_kantor'),
						'no_tdp'=>$this->input->post('no_sk'),
						'tgl_tdp'=>$this->input->post('tgl_sk'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'foto'=>$this->input->post('foto_lama')
						);
					$this->back_model->update_perizinan($data,$table,$id);
					$this->session->set_flashdata('update','Data SIUP telah diupdate');
					redirect(base_url('backend/tdp'));
				}
			} elseif (isset($_POST['btnIupr'])) {
				$table = $this->input->post('table');
				$id = $this->input->post('id');
				if ($this->upload->do_upload('userfile')) {
					$data = array('upload_data' => $this->upload->data());

					$file = array('upload_sukses' => $this->upload->data());
					$nama_file = $file['upload_sukses']['file_name'];
					$data = array(
						'tgl_pendaftaran'=>$this->input->post('tgl_pendaftaran'),
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'jenis_usaha'=>$this->input->post('jenis_usaha'),
						'siup'=>$this->input->post('siup'),
						'tdp'=>$this->input->post('tdp'),
						'no_iupr'=>$this->input->post('no_sk'),
						'tgl_iupr'=>$this->input->post('tgl_sk'),
						'no_rekomendasi'=>$this->input->post('no_rekomendasi'),
						'foto'=>$nama_file
						);
					$this->back_model->update_perizinan($data,$table,$id);
					$this->session->set_flashdata('update','Data SIUP telah diupdate');
					redirect(base_url('backend/tdp'));
				} else {
					$data = array(
						'tgl_pendaftaran'=>$this->input->post('tgl_pendaftaran'),
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'jenis_usaha'=>$this->input->post('jenis_usaha'),
						'siup'=>$this->input->post('siup'),
						'tdp'=>$this->input->post('tdp'),
						'no_iupr'=>$this->input->post('no_sk'),
						'tgl_iupr'=>$this->input->post('tgl_sk'),
						'no_rekomendasi'=>$this->input->post('no_rekomendasi'),
						'foto'=>$this->input->post('foto_lama')
						);
					$this->back_model->update_perizinan($data,$table,$id);
					$this->session->set_flashdata('update','Data SIUP telah diupdate');
					redirect(base_url('backend/tdp'));
				}
			} elseif (isset($_POST['btnIupp'])) {
				$table = $this->input->post('table');
				$id = $this->input->post('id');
				if ($this->upload->do_upload('userfile')) {
					$data = array('upload_data' => $this->upload->data());

					$file = array('upload_sukses' => $this->upload->data());
					$nama_file = $file['upload_sukses']['file_name'];
					$data = array(
						'tgl_pendaftaran'=>$this->input->post('tgl_pendaftaran'),
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'jenis_usaha'=>$this->input->post('jenis_usaha'),
						'siup'=>$this->input->post('siup'),
						'tdp'=>$this->input->post('tdp'),
						'no_iupp'=>$this->input->post('no_sk'),
						'tgl_iupp'=>$this->input->post('tgl_sk'),
						'no_rekomendasi'=>$this->input->post('no_rekomendasi'),
						'no_rencana'=>$this->input->post('no_rencana'),
						'foto'=>$nama_file
						);
					$this->back_model->update_perizinan($data,$table,$id);
					$this->session->set_flashdata('update','Data SIUP telah diupdate');
					redirect(base_url('backend/tdp'));
				} else {
					$data = array(
						'tgl_pendaftaran'=>$this->input->post('tgl_pendaftaran'),
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'jenis_usaha'=>$this->input->post('jenis_usaha'),
						'siup'=>$this->input->post('siup'),
						'tdp'=>$this->input->post('tdp'),
						'no_iupp'=>$this->input->post('no_sk'),
						'tgl_iupp'=>$this->input->post('tgl_sk'),
						'no_rekomendasi'=>$this->input->post('no_rekomendasi'),
						'no_rencana'=>$this->input->post('no_rencana'),
						'foto'=>$this->input->post('foto_lama')
						);
					$this->back_model->update_perizinan($data,$table,$id);
					$this->session->set_flashdata('update','Data SIUP telah diupdate');
					redirect(base_url('backend/tdp'));
				}
			} elseif (isset($_POST['btnIuts'])) {
				$table = $this->input->post('table');
				$id = $this->input->post('id');
				if ($this->upload->do_upload('userfile')) {
					$data = array('upload_data' => $this->upload->data());

					$file = array('upload_sukses' => $this->upload->data());
					$nama_file = $file['upload_sukses']['file_name'];
					$data = array(
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'npwp'=>$this->input->post('npwp'),
						'no_telp'=>$this->input->post('no_telp'),
						'no_fax'=>$this->input->post('no_fax'),
						'email'=>$this->input->post('email'),
						'keg_usaha_pokok'=>$this->input->post('keg_usaha_pokok'),
						'kbli'=>$this->input->post('kbli'),
						'status_kantor'=>$this->input->post('status_kantor'),
						'siup'=>$this->input->post('siup'),
						'tdp'=>$this->input->post('tdp'),
						'no_iuts'=>$this->input->post('no_sk'),
						'tgl_iuts'=>$this->input->post('tgl_sk'),
						'no_rekomendasi'=>$this->input->post('no_rekomendasi'),
						'no_rencana'=>$this->input->post('no_rencana'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'foto'=>$nama_file
						);
					$this->back_model->update_perizinan($data,$table,$id);
					$this->session->set_flashdata('update','Data SIUP telah diupdate');
					redirect(base_url('backend/tdp'));
				} else {
					$data = array(
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'npwp'=>$this->input->post('npwp'),
						'no_telp'=>$this->input->post('no_telp'),
						'no_fax'=>$this->input->post('no_fax'),
						'email'=>$this->input->post('email'),
						'keg_usaha_pokok'=>$this->input->post('keg_usaha_pokok'),
						'kbli'=>$this->input->post('kbli'),
						'status_kantor'=>$this->input->post('status_kantor'),
						'siup'=>$this->input->post('siup'),
						'tdp'=>$this->input->post('tdp'),
						'no_iuts'=>$this->input->post('no_sk'),
						'tgl_iuts'=>$this->input->post('tgl_sk'),
						'no_rekomendasi'=>$this->input->post('no_rekomendasi'),
						'no_rencana'=>$this->input->post('no_rencana'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'foto'=>$this->input->post('foto_lama')
						);
					$this->back_model->update_perizinan($data,$table,$id);
					$this->session->set_flashdata('update','Data SIUP telah diupdate');
					redirect(base_url('backend/tdp'));
				}
			} elseif (isset($_POST['btnStpw'])) {
				$table = $this->input->post('table');
				$id = $this->input->post('id');
				if ($this->upload->do_upload('userfile')) {
					$data = array('upload_data' => $this->upload->data());

					$file = array('upload_sukses' => $this->upload->data());
					$nama_file = $file['upload_sukses']['file_name'];
					$data = array(
						'no_pendaftaran'=>$this->input->post('no_pendaftaran'),
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'npwp'=>$this->input->post('npwp'),
						'iuts'=>$this->input->post('iuts'),
						'no_stpw'=>$this->input->post('no_sk'),
						'tgl_stpw'=>$this->input->post('tgl_sk'),
						'no_rekomendasi'=>$this->input->post('no_rekomendasi'),
						'no_telp'=>$this->input->post('no_telp'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'foto'=>$nama_file
						);
					$this->back_model->update_perizinan($data,$table,$id);
					$this->session->set_flashdata('update','Data SIUP telah diupdate');
					redirect(base_url('backend/tdp'));
				} else {
					$data = array(
						'no_pendaftaran'=>$this->input->post('no_pendaftaran'),
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'npwp'=>$this->input->post('npwp'),
						'iuts'=>$this->input->post('iuts'),
						'no_stpw'=>$this->input->post('no_sk'),
						'tgl_stpw'=>$this->input->post('tgl_sk'),
						'no_rekomendasi'=>$this->input->post('no_rekomendasi'),
						'no_telp'=>$this->input->post('no_telp'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'foto'=>$this->input->post('foto_lama')
						);
					$this->back_model->update_perizinan($data,$table,$id);
					$this->session->set_flashdata('update','Data SIUP telah diupdate');
					redirect(base_url('backend/tdp'));
				}
			} elseif (isset($_POST['btnTdg'])) {
				$table = $this->input->post('table');
				$id = $this->input->post('id');
				if ($this->upload->do_upload('userfile')) {
					$data = array('upload_data' => $this->upload->data());

					$file = array('upload_sukses' => $this->upload->data());
					$nama_file = $file['upload_sukses']['file_name'];
					$data = array(
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'almt_penanggungjwb'=>$this->input->post('almt_penanggungjwb'),
						'almt_gudang'=>$this->input->post('almt_gudang'),
						'no_telp'=>$this->input->post('no_telp'),
						'no_fax'=>$this->input->post('no_fax'),
						'email'=>$this->input->post('email'),
						'jenis_isi_gudang'=>$this->input->post('jenis_isi_gudang'),
						'klasifikasi_gudang'=>$this->input->post('klasifikasi_gudang'),
						'luas_gudang'=>$this->input->post('luas_gudang'),
						'kapasitas_gudang'=>$this->input->post('kapasitas_gudang'),
						'kelengkapan_gudang'=>$this->input->post('kelengkapan_gudang'),
						'no_tdg'=>$this->input->post('no_tdg'),
						'tgl_tdg'=>$this->input->post('tgl_tdg'),
						'tdp'=>$this->input->post('tdp'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'longitude'=>$this->input->post('longitude'),
						'latitude'=>$this->input->post('latitude'),
						'foto'=>$nama_file
						);
					$this->back_model->update_perizinan($data,$table,$id);
					$this->session->set_flashdata('update','Data SIUP telah diupdate');
					redirect(base_url('backend/tdp'));
				} else {
					$data = array(
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'almt_penanggungjwb'=>$this->input->post('almt_penanggungjwb'),
						'almt_gudang'=>$this->input->post('almt_gudang'),
						'no_telp'=>$this->input->post('no_telp'),
						'no_fax'=>$this->input->post('no_fax'),
						'email'=>$this->input->post('email'),
						'jenis_isi_gudang'=>$this->input->post('jenis_isi_gudang'),
						'klasifikasi_gudang'=>$this->input->post('klasifikasi_gudang'),
						'luas_gudang'=>$this->input->post('luas_gudang'),
						'kapasitas_gudang'=>$this->input->post('kapasitas_gudang'),
						'kelengkapan_gudang'=>$this->input->post('kelengkapan_gudang'),
						'no_tdg'=>$this->input->post('no_tdg'),
						'tgl_tdg'=>$this->input->post('tgl_tdg'),
						'tdp'=>$this->input->post('tdp'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'longitude'=>$this->input->post('longitude'),
						'latitude'=>$this->input->post('latitude'),
						'foto'=>$this->input->post('foto_lama')
						);
					$this->back_model->update_perizinan($data,$table,$id);
					$this->session->set_flashdata('update','Data SIUP telah diupdate');
					redirect(base_url('backend/tdp'));
				}
			} elseif (isset($_POST['btnSiupmb'])) {
				$table = $this->input->post('table');
				$id = $this->input->post('id');
				if ($this->upload->do_upload('userfile')) {
					$data = array('upload_data' => $this->upload->data());

					$file = array('upload_sukses' => $this->upload->data());
					$nama_file = $file['upload_sukses']['file_name'];
					$data = array(
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'siupmb'=>$this->input->post('siupmb'),
						'tdp'=>$this->input->post('tdp'),
						'tgl_keluar'=>$this->input->post('tgl_keluar'),
						'tgl_berakhir'=>$this->input->post('tgl_berakhir'),
						'ins_yg_mengeluarkan'=>$this->input->post('ins_yg_mengeluarkan'),
						'keterangan'=>$this->input->post('keterangan'),
						'foto'=>$nama_file
						);
					$this->back_model->update_perizinan($data,$table,$id);
					$this->session->set_flashdata('update','Data SIUP telah diupdate');
					redirect(base_url('backend/tdp'));
				} else {
					$data = array(
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'siupmb'=>$this->input->post('siupmb'),
						'tdp'=>$this->input->post('tdp'),
						'tgl_keluar'=>$this->input->post('tgl_keluar'),
						'tgl_berakhir'=>$this->input->post('tgl_berakhir'),
						'ins_yg_mengeluarkan'=>$this->input->post('ins_yg_mengeluarkan'),
						'keterangan'=>$this->input->post('keterangan'),
						'foto'=>$this->input->post('foto_lama')
						);
					$this->back_model->update_perizinan($data,$table,$id);
					$this->session->set_flashdata('update','Data SIUP telah diupdate');
					redirect(base_url('backend/tdp'));
				}
			} elseif (isset($_POST['btnTdpud'])) {
				$table = $this->input->post('table');
				$id = $this->input->post('id');
				if ($this->upload->do_upload('userfile')) {
					$data = array('upload_data' => $this->upload->data());

					$file = array('upload_sukses' => $this->upload->data());
					$nama_file = $file['upload_sukses']['file_name'];
					$data = array(
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'npwp'=>$this->input->post('npwp'),
						'no_telp'=>$this->input->post('no_telp'),
						'no_fax'=>$this->input->post('no_fax'),
						'email'=>$this->input->post('email'),
						'keg_usaha_pokok'=>$this->input->post('keg_usaha_pokok'),
						'kbli'=>$this->input->post('kbli'),
						'status_kantor'=>$this->input->post('status_kantor'),
						'tdp'=>$this->input->post('tdp'),
						'no_tdpud'=>$this->input->post('no_sk'),
						'tgl_tdpud'=>$this->input->post('tgl_sk'),
						'jenis_tdpud'=>$this->input->post('jenis_tdpud'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'foto'=>$nama_file
						);
					$this->back_model->update_perizinan($data,$table,$id);
					$this->session->set_flashdata('update','Data SIUP telah diupdate');
					redirect(base_url('backend/tdp'));
				} else {
					$data = array(
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'npwp'=>$this->input->post('npwp'),
						'no_telp'=>$this->input->post('no_telp'),
						'no_fax'=>$this->input->post('no_fax'),
						'email'=>$this->input->post('email'),
						'keg_usaha_pokok'=>$this->input->post('keg_usaha_pokok'),
						'kbli'=>$this->input->post('kbli'),
						'status_kantor'=>$this->input->post('status_kantor'),
						'tdp'=>$this->input->post('tdp'),
						'no_tdpud'=>$this->input->post('no_sk'),
						'tgl_tdpud'=>$this->input->post('tgl_sk'),
						'jenis_tdpud'=>$this->input->post('jenis_tdpud'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'foto'=>$this->input->post('foto_lama')
						);
					$this->back_model->update_perizinan($data,$table,$id);
					$this->session->set_flashdata('update','Data SIUP telah diupdate');
					redirect(base_url('backend/tdp'));
				}
			} else {
				$table = $this->input->post('table');
				$id = $this->input->post('id');
				$data = array(
					'id'=>'',
					'no_cas'=>$this->input->post('no_cas'),
					'pos_trf_hs'=>$this->input->post('pos_trf_hs'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'almt_penyimpanan'=>$this->input->post('almt_penyimpanan'),
					'uraian_brg'=>$this->input->post('uraian_brg'),
					'tata_niaga_impor'=>$this->input->post('tata_niaga_impor'),
					'kep_lain_tdk_utk_pgn'=>$this->input->post('kep_lain_tdk_utk_pgn'),
					'lab'=>$this->input->post('lab'),
					'satuan'=>$this->input->post('satuan')
					);
				$this->back_model->update_perizinan($data,$table,$id);
				$this->session->set_flashdata('update','Data B3 telah diupdate');
				redirect(base_url('backend/tdp'));
			}

		} else {
			redirect(base_url('backend'));
		}
		
	}
}