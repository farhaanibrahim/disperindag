<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('front_model');
		$this->load->model('back_model');

		$ip = $_SERVER['REMOTE_ADDR'];
		$tanggal = date('Ymd');
		$waktu = time();

		$s = $this->back_model->cek_visitor($ip,$tanggal);

		if ($s->num_rows() == 0) {
			$data = array(
					'ip'=>$ip,
					'tgl'=>$tanggal,
					'hits'=>1
				);
			$this->back_model->insert_pegunjung($data);
		} else {
			foreach ($s->result() as $row) {}
			$hits = $row->hits;
			$set = array(
					'hits'=>$hits+1
				);
			$this->back_model->update_visitor($ip,$tanggal,$set);
		}
		/*
		if ($this->session->userdata('visitor')===false) {
			$this->session->set_userdata(array(
					'visitor'=>$_SERVER['PHP_SELF'],
					'ip'=>$_SERVER['REMOTE_ADDR']
				));
			$data = array(
					'id'=>'',
					'location'=>$this->session->userdata('visitor'),
					'ip'=>$this->session->userdata('ip'),
					'tgl'=>date('Ymd')
				);
			$this->back_model->insert_pegunjung($data);
		}
		$this->back_model->clear_visitor();
		*/
	}

	function index(){
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$data['gallery'] = $this->front_model->daftar_gallery();
		$data['berita'] = $this->front_model->daftar_berita();
		$data['agenda'] = $this->front_model->daftar_agenda();
		
		$data['pasar'] = $this->back_model->daftar_pasar();
		$data['b_pokok'] = $this->back_model->daftar_b_pokok();
		$data['b_pokok_sub'] = $this->back_model->daftar_sub_b_pokok_all();
		$data['tr_b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_min7days();

		$data['visitor'] = $this->back_model->get_visitor();
		$data['total_visitor'] = $this->back_model->total_visitor();
		$data['visitor_online'] = $this->back_model->visitor_online();
		$this->load->view('front/front_index',$data);
	}
	function event_kalender(){
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$data['gallery'] = $this->front_model->daftar_gallery();
		$data['berita'] = $this->front_model->daftar_berita();
		$data['agenda'] = $this->front_model->daftar_agenda();
		
		$data['pasar'] = $this->back_model->daftar_pasar();
		$data['b_pokok'] = $this->back_model->daftar_b_pokok();
		$data['b_pokok_sub'] = $this->back_model->daftar_sub_b_pokok_all();
		$data['tr_b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_min7days();
		$this->load->view('front/front_calendar_event',$data);
	}
	function bahan_pokok(){
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$data['gallery'] = $this->front_model->daftar_gallery();
		$data['berita'] = $this->front_model->daftar_berita();
		$data['agenda'] = $this->front_model->daftar_agenda();
		
		
		$data['pasar'] = $this->back_model->daftar_pasar();
		//$data['b_pokok'] = $this->back_model->daftar_b_pokok();
		
		if(!isset($_GET['per_page'])){
			$data['b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_min7days_batas('0');
		}else{
			$data['b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_min7days_batas($_GET['per_page']);
		}
		
		$this->load->library('pagination');
		$config['page_query_string'] = TRUE;
		$config['base_url'] = site_url('front/bahan_pokok?');
		$config['total_rows'] = $this->back_model->daftar_tr_b_pokok_where_min7days()->num_rows();
		$config['per_page'] = 8	;
		$config['first_link'] = 'First';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		$config['cur_tag_open'] = "<li class='selected'><a>";
		$config['cur_tag_close'] = '</a></li>';
		$config['last_tag_open'] = "<li>";
		$config['last_tag_close'] = '</li>';
		$config['first_tag_open'] = "<li>";
		$config['first_tag_close'] = '</li>';
		$config['prev_tag_open'] = "<li>";
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = "<li>";
		$config['next_tag_close'] = '</li>';
		
		$config['num_tag_open'] = "<li>";
		$config['num_tag_close'] = '</li>';
		
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['total_rows'] = $this->back_model->daftar_b_pokok();
		$this->load->view('front/front_bahan_pokok',$data);
	}
	function bahan_pokok_pasar($id_pasar){
		if(!isset($id_pasar)){
			redirect("front/bahan_pokok");
		}
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$data['gallery'] = $this->front_model->daftar_gallery();
		$data['berita'] = $this->front_model->daftar_berita();
		$data['agenda'] = $this->front_model->daftar_agenda();
		
		$data['pasar'] = $this->back_model->daftar_pasar();
		//$data['b_pokok'] = $this->back_model->daftar_b_pokok();
		
		if(!isset($_GET['per_page'])){
			$data['b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_min7days_batas_pasar($id_pasar,'0');
		}else{
			$data['b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_min7days_batas_pasar($id_pasar,$_GET['per_page']);
		}
		
		$this->load->library('pagination');
		$config['page_query_string'] = TRUE;
		$config['base_url'] = site_url("front/bahan_pokok_pasar/$id_pasar?");
		$config['total_rows'] = $this->back_model->daftar_tr_b_pokok_where_min7days_pasar($id_pasar)->num_rows();
		$config['per_page'] = 8	;
		$config['first_link'] = 'First';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		$config['cur_tag_open'] = "<li class='selected'><a>";
		$config['cur_tag_close'] = '</a></li>';
		$config['last_tag_open'] = "<li>";
		$config['last_tag_close'] = '</li>';
		$config['first_tag_open'] = "<li>";
		$config['first_tag_close'] = '</li>';
		$config['prev_tag_open'] = "<li>";
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = "<li>";
		$config['next_tag_close'] = '</li>';
		
		$config['num_tag_open'] = "<li>";
		$config['num_tag_close'] = '</li>';
		
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['total_rows'] = $this->back_model->daftar_tr_b_pokok_where_id_ps($id_pasar);
		$this->load->view('front/front_bahan_pokok',$data);
	}
	/*
	function bahan_pokok_pasar($id_pasar){
		if(!isset($id_pasar)){
			redirect("front/bahan_pokok");
		}
		$data['instansi'] = $this->front_model->data_tr_instansi();
		
		$data['pasar_id'] = $this->back_model->daftar_pasar_where_id_pasar($id_pasar);
		if($data['pasar_id']->num_rows() > 0){
			$data['pasar'] = $this->back_model->daftar_pasar();
			
			$data['sub_b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_id_ps($id_pasar);
			$this->load->view('front/front_bahan_pokok_sub2',$data);
		}else{
			redirect("front/bahan_pokok");
		}
	}*/
	function bahan_pokok_sub($id_bahan_pokok){
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$data['agenda'] = $this->front_model->daftar_agenda();
		
		$data['pasar'] = $this->back_model->daftar_pasar();
		
		$data['b_pokok'] = $this->back_model->daftar_b_pokok_id_bahan_pokok($id_bahan_pokok);
		if($data['b_pokok']->num_rows() > 0){
			$data['sub_b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_id_b_pokok($id_bahan_pokok);
			$this->load->view('front/front_bahan_pokok_sub',$data);
		}else{
			redirect("front/bahan_pokok");
		}
	}
	function about(){
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$data['berita'] = $this->front_model->daftar_berita();
		$data['agenda'] = $this->front_model->daftar_agenda();
		$data['b_pokok_sub'] = $this->back_model->daftar_sub_b_pokok_upto_date();
		$data['tr_b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_min7days();
		$this->load->view('front/front_about',$data);
	}
	function regulasi(){
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$data['berita'] = $this->front_model->daftar_berita();
		$data['agenda'] = $this->front_model->daftar_agenda();
		$data['regulasi'] = $this->front_model->daftar_regulasi_all();
		$data['b_pokok_sub'] = $this->back_model->daftar_sub_b_pokok_upto_date();
		$data['tr_b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_min7days();
		$this->load->view('front/front_regulasi',$data);
	}
	function agenda(){
		$data['berita'] = $this->front_model->daftar_berita();
		$data['tr_b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_min7days();
		if(!isset($_GET['per_page'])){
			$data['agenda_pagination'] = $this->front_model->daftar_agenda_limit_pagination('0');
		}else{
			$data['agenda_pagination'] = $this->front_model->daftar_agenda_limit_pagination($_GET['per_page']);
		}
		
		$this->load->library('pagination');
		$config['page_query_string'] = TRUE;
		$config['base_url'] = site_url('front/agenda?');
		$config['total_rows'] = $this->front_model->daftar_agenda_all()->num_rows();
		$config['per_page'] = 4	;
		$config['first_link'] = 'First';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		$config['cur_tag_open'] = "<li class='selected'><a>";
		$config['cur_tag_close'] = '</a></li>';
		$config['last_tag_open'] = "<li>";
		$config['last_tag_close'] = '</li>';
		$config['first_tag_open'] = "<li>";
		$config['first_tag_close'] = '</li>';
		$config['prev_tag_open'] = "<li>";
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = "<li>";
		$config['next_tag_close'] = '</li>';
		
		$config['num_tag_open'] = "<li>";
		$config['num_tag_close'] = '</li>';
		
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$data['katg_agenda'] = $this->back_model->daftar_kategori_agenda();
		$data['agenda'] = $this->front_model->daftar_agenda();
		$data['b_pokok_sub'] = $this->back_model->daftar_sub_b_pokok_upto_date();
		$this->load->view('front/front_agenda',$data);
	}
	function agenda_where_kategori($id_katg){
		$data['berita'] = $this->front_model->daftar_berita();
		$data['tr_b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_min7days();
		if(!isset($_GET['per_page'])){
			$data['agenda_pagination'] = $this->front_model->daftar_agenda_limit_pagination_where_ktg('0',$id_katg);
		}else{
			$data['agenda_pagination'] = $this->front_model->daftar_agenda_limit_pagination_where_ktg($_GET['per_page'],$id_katg);
		}
		
		$this->load->library('pagination');
		$config['page_query_string'] = TRUE;
		$config['base_url'] = site_url('front/agenda?');
		$config['total_rows'] = $this->front_model->daftar_agenda_where_katg($id_katg)->num_rows();
		$config['per_page'] = 4	;
		$config['first_link'] = 'First';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		$config['cur_tag_open'] = "<li class='selected'><a>";
		$config['cur_tag_close'] = '</a></li>';
		$config['last_tag_open'] = "<li>";
		$config['last_tag_close'] = '</li>';
		$config['first_tag_open'] = "<li>";
		$config['first_tag_close'] = '</li>';
		$config['prev_tag_open'] = "<li>";
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = "<li>";
		$config['next_tag_close'] = '</li>';
		
		$config['num_tag_open'] = "<li>";
		$config['num_tag_close'] = '</li>';
		
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$data['katg_agenda'] = $this->back_model->daftar_kategori_agenda();
		$data['agenda'] = $this->front_model->daftar_agenda();
		$data['b_pokok_sub'] = $this->back_model->daftar_sub_b_pokok_upto_date();
		$this->load->view('front/front_agenda',$data);
	}
	function agenda_details($id_agenda){
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$data['katg_agenda'] = $this->front_model->daftar_kategori_agenda();
		$data['berita'] = $this->front_model->daftar_berita();
		$data['agenda'] = $this->front_model->daftar_agenda_limit_pagination(0);
		$data['agenda_details'] = $this->front_model->daftar_agenda_where_primary($id_agenda);
		$data['b_pokok_sub'] = $this->back_model->daftar_sub_b_pokok_upto_date();
		$data['tr_b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_min7days();
		if($data['agenda_details']->num_rows() > 0){
			$this->load->view('front/front_agenda_details',$data);
		}else{
			redirect('front');
		}
	}
	function berita(){
		if(!isset($_GET['per_page'])){
			$data['berita_pagination'] = $this->front_model->daftar_berita_limit_pagination('0');
		}else{
			$data['berita_pagination'] = $this->front_model->daftar_berita_limit_pagination($_GET['per_page']);
		}
		
		$this->load->library('pagination');
		$config['page_query_string'] = TRUE;
		$config['base_url'] = site_url('front/berita?');
		$config['total_rows'] = $this->front_model->daftar_berita_all()->num_rows();
		$config['per_page'] = 4	;
		$config['first_link'] = 'First';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		$config['cur_tag_open'] = "<li class='selected'><a>";
		$config['cur_tag_close'] = '</a></li>';
		$config['last_tag_open'] = "<li>";
		$config['last_tag_close'] = '</li>';
		$config['first_tag_open'] = "<li>";
		$config['first_tag_close'] = '</li>';
		$config['prev_tag_open'] = "<li>";
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = "<li>";
		$config['next_tag_close'] = '</li>';
		
		$config['num_tag_open'] = "<li>";
		$config['num_tag_close'] = '</li>';
		
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$data['katg_berita'] = $this->back_model->daftar_kategori_berita();
		$data['berita'] = $this->front_model->daftar_berita();
		$data['b_pokok_sub'] = $this->back_model->daftar_sub_b_pokok_upto_date();
		$data['tr_b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_min7days();
		$this->load->view('front/front_berita',$data);
	}
	function berita_where_kategori($katg){
		if(isset($katg)){
			$data['katg'] = $this->front_model->daftar_kategori_berita_where_id($katg);
			if(!isset($_GET['per_page'])){
				$data['berita_pagination'] = $this->front_model->daftar_berita_where_katg_limit_pagination($katg,'0');
			}else{
				$data['berita_pagination'] = $this->front_model->daftar_berita_where_katg_limit_pagination($katg,$_GET['per_page']);
			}
			
			$this->load->library('pagination');
			$config['page_query_string'] = TRUE;
			$config['base_url'] = site_url("front/berita/$katg?");
			$config['total_rows'] = $this->front_model->daftar_berita_where_katg_all($katg)->num_rows();
			$config['per_page'] = 4	;
			$config['first_link'] = 'First';
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Previous';
			$config['cur_tag_open'] = "<li class='selected'><a>";
			$config['cur_tag_close'] = '</a></li>';
			$config['last_tag_open'] = "<li>";
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = "<li>";
			$config['first_tag_close'] = '</li>';
			$config['prev_tag_open'] = "<li>";
			$config['prev_tag_close'] = '</li>';
			$config['next_tag_open'] = "<li>";
			$config['next_tag_close'] = '</li>';
			
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['katg_berita'] = $this->back_model->daftar_kategori_berita();
			$data['berita'] = $this->front_model->daftar_berita();
			$data['b_pokok_sub'] = $this->back_model->daftar_sub_b_pokok_upto_date();
			$data['tr_b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_min7days();
			$this->load->view('front/front_berita',$data);
		}else{
			redirect('front/berita');
		}
	}
	function berita_details($id_berita){
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$data['katg_berita'] = $this->front_model->daftar_kategori_berita();
		$data['berita2'] = $this->front_model->daftar_berita2();
		$data['berita'] = $this->front_model->daftar_berita();
		$data['berita_details'] = $this->front_model->daftar_berita_where_id($id_berita);
		$data['b_pokok_sub'] = $this->back_model->daftar_sub_b_pokok_upto_date();
		$data['tr_b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_min7days();
		if($data['berita_details']->num_rows() > 0){
			$this->load->view('front/front_berita_details',$data);
		}else{
			redirect('front');
		}
	}
	function contact(){
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$data['berita'] = $this->front_model->daftar_berita();
		$data['agenda'] = $this->front_model->daftar_agenda();
		$data['b_pokok_sub'] = $this->back_model->daftar_sub_b_pokok_upto_date();
		$data['tr_b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_min7days();
		$this->load->view('front/front_contact',$data);
	}
	function login(){
		if($this->session->userdata('admin_id')){
			redirect("front");
		}else{
			$data['title'] = '';
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('front/login',$data);
		}
	}
	function logout(){
		$this->session->unset_userdata('config_search');
	}
	function harga_bapok_api(){
		$url = "http://www.kemendag.go.id/addon/api/website_api/harga_bapok";
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL ,$url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY:9b026676adc83abca0b2628e965f7d137691f34c'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "start_date=2016-10-17&end_date=2016-10-24");

		$output = curl_exec($ch);

		$obj = json_decode($output) ;
		$msg = $obj->{'message'}; 
		foreach($msg as $m){
			echo $m->nama_komoditi."<br>";
			echo $m->ukuran." ";
			echo $m->harga."<br>";
			echo "<br>";
		}
		curl_close($ch);
	}

	public function tdp()
	{
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$data['berita'] = $this->front_model->daftar_berita();
		$data['agenda'] = $this->front_model->daftar_agenda();

		$data['siup'] = $this->back_model->data_siup();
		$data['tdp'] = $this->back_model->data_tdp();
		$data['iupr'] = $this->back_model->data_iupr();
		$data['iupp'] = $this->back_model->data_iupp();
		$data['iuts'] = $this->back_model->data_iuts();
		$data['stpw'] = $this->back_model->data_stpw();
		$data['tdg'] = $this->back_model->data_tdg();
		$data['siupmb'] = $this->back_model->data_siupmb();
		$data['tdpud'] = $this->back_model->data_tdpud();
		$data['b3'] = $this->back_model->data_b3();

		$this->load->view('front/front_tdp',$data);
	}

	public function reg_tdp()
	{
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$data['berita'] = $this->front_model->daftar_berita();
		$data['agenda'] = $this->front_model->daftar_agenda();

		$data['kecamatan'] = $this->back_model->mst_kecamatan();
		$data['izin_usaha'] = $this->back_model->klasifikasi_iu();

		$this->load->view('front/reg_tdp',$data);
	}

	public function reg_perizinan()
	{
		$config['upload_path'] = './upload';
		$config['allowed_types'] = 'jpg|jpeg|png|gif';
		$config['max_size']	= '6012312481248';
		$config['max_width']  = '211241242024';
		$config['max_height']  = '171212412468';

		$this->load->library('upload', $config);

		if (isset($_POST['btnSiup'])) {
			$table = $this->input->post('table');

			if ( ! $this->upload->do_upload('userfile'))
			{
				$data_form = array(
					'id'=>'',
					'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
					'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
					'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'no_telp'=>$this->input->post('no_telp'),
					'no_fax'=>$this->input->post('no_fax'),
					'email'=>$this->input->post('email'),
					'modal_usaha'=>$this->input->post('modal_usaha'),
					'kelembagaan'=>$this->input->post('kelembagaan'),
					'kbli'=>$this->input->post('kbli'),
					'barang_dagangan'=>$this->input->post('barang_dagangan'),
					'no_siup'=>$this->input->post('no_sk'),
					'tgl_siup'=>$this->input->post('tgl_sk'),
					'masaberlaku'=>$this->input->post('masaberlaku'),
					'foto'=>''
					);
				$this->session->set_flashdata('tdp','tambah_sukses');
				$this->back_model->tambah_perizinan($data_form,$table);

				redirect('front/reg_tdp');
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());

				$file = array('upload_sukses' => $this->upload->data());
				$nama_file = $file['upload_sukses']['file_name'];

				$data_form = array(
					'id'=>'',
					'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
					'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
					'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'no_telp'=>$this->input->post('no_telp'),
					'no_fax'=>$this->input->post('no_fax'),
					'email'=>$this->input->post('email'),
					'modal_usaha'=>$this->input->post('modal_usaha'),
					'kelembagaan'=>$this->input->post('kelembagaan'),
					'kbli'=>$this->input->post('kbli'),
					'barang_dagangan'=>$this->input->post('barang_dagangan'),
					'no_siup'=>$this->input->post('no_sk'),
					'tgl_siup'=>$this->input->post('tgl_sk'),
					'masaberlaku'=>$this->input->post('masaberlaku'),
					'foto'=>$nama_file
					);
				$this->session->set_flashdata('tdp','tambah_sukses');
				$this->back_model->tambah_perizinan($data_form,$table);

				redirect('front/reg_tdp');
			}

		} else if (isset($_POST['btnTdp'])) {
			$table = $this->input->post('table');

			if ( ! $this->upload->do_upload('userfile'))
			{
				$data_form = array(
					'id'=>'',
					'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
					'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
					'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'npwp'=>$this->input->post('npwp'),
					'no_telp'=>$this->input->post('no_telp'),
					'no_fax'=>$this->input->post('no_fax'),
					'email'=>$this->input->post('email'),
					'keg_usaha_pokok'=>$this->input->post('keg_usaha_pokok'),
					'kbli'=>$this->input->post('kbli'),
					'status_kantor'=>$this->input->post('status_kantor'),
					'no_tdp'=>$this->input->post('no_sk'),
					'tgl_tdp'=>$this->input->post('tgl_sk'),
					'masaberlaku'=>$this->input->post('masaberlaku'),
					'foto'=>''
					);
				$this->session->set_flashdata('tdp','tambah_sukses');
				$this->back_model->tambah_perizinan($data_form,$table);

				redirect('front/reg_tdp');
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());

				$file = array('upload_sukses' => $this->upload->data());
				$nama_file = $file['upload_sukses']['file_name'];

				$data_form = array(
					'id'=>'',
					'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
					'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
					'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'npwp'=>$this->input->post('npwp'),
					'no_telp'=>$this->input->post('no_telp'),
					'no_fax'=>$this->input->post('no_fax'),
					'email'=>$this->input->post('email'),
					'keg_usaha_pokok'=>$this->input->post('keg_usaha_pokok'),
					'kbli'=>$this->input->post('kbli'),
					'status_kantor'=>$this->input->post('status_kantor'),
					'no_tdp'=>$this->input->post('no_sk'),
					'tgl_tdp'=>$this->input->post('tgl_sk'),
					'masaberlaku'=>$this->input->post('masaberlaku'),
					'foto'=>$nama_file
					);
				$this->session->set_flashdata('tdp','tambah_sukses');
				$this->back_model->tambah_perizinan($data_form,$table);

				redirect('front/reg_tdp');
			}
		} else if (isset($_POST['btnIupr'])) {

			$table = $this->input->post('table');

			if ( ! $this->upload->do_upload('userfile'))
			{
				$data_form = array(
					'id'=>'',
					'tgl_pendaftaran'=>$this->input->post('tgl_pendaftaran'),
					'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
					'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
					'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'jenis_usaha'=>$this->input->post('jenis_usaha'),
					'siup'=>$this->input->post('siup'),
					'tdp'=>$this->input->post('tdp'),
					'no_iupr'=>$this->input->post('no_sk'),
					'tgl_iupr'=>$this->input->post('tgl_sk'),
					'foto'=>''
					);
				$this->session->set_flashdata('tdp','tambah_sukses');
				$this->back_model->tambah_perizinan($data_form,$table);

				redirect('front/reg_tdp');
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());

				$file = array('upload_sukses' => $this->upload->data());
				$nama_file = $file['upload_sukses']['file_name'];

				$data_form = array(
					'id'=>'',
					'tgl_pendaftaran'=>$this->input->post('tgl_pendaftaran'),
					'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
					'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
					'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'jenis_usaha'=>$this->input->post('jenis_usaha'),
					'siup'=>$this->input->post('siup'),
					'no_iupr'=>$this->input->post('no_sk'),
					'tgl_iupr'=>$this->input->post('tgl_sk'),
					'foto'=>$nama_file
					);
				$this->session->set_flashdata('tdp','tambah_sukses');
				$this->back_model->tambah_perizinan($data_form,$table);

				redirect('front/reg_tdp');
			}

		} else if (isset($_POST['btnIupp'])) {

			$table = $this->input->post('table');

			if ( ! $this->upload->do_upload('userfile'))
			{
				$data_form = array(
					'id'=>'',
					'tgl_pendaftaran'=>$this->input->post('tgl_pendaftaran'),
					'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
					'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
					'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'jenis_usaha'=>$this->input->post('jenis_usaha'),
					'siup'=>$this->input->post('siup'),
					'tdp'=>$this->input->post('tdp'),
					'no_iupp'=>$this->input->post('no_sk'),
					'tgl_iupp'=>$this->input->post('tgl_sk'),
					'no_rekomendasi'=>$this->input->post('no_rekomendasi'),
					'no_rencana'=>$this->input->post('no_rencana'),
					'foto'=>''
					);
				$this->session->set_flashdata('tdp','tambah_sukses');
				$this->back_model->tambah_perizinan($data_form,$table);

				redirect('front/reg_tdp');
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());

				$file = array('upload_sukses' => $this->upload->data());
				$nama_file = $file['upload_sukses']['file_name'];

				$data_form = array(
					'id'=>'',
					'tgl_pendaftaran'=>$this->input->post('tgl_pendaftaran'),
					'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
					'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
					'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'jenis_usaha'=>$this->input->post('jenis_usaha'),
					'siup'=>$this->input->post('siup'),
					'no_iupp'=>$this->input->post('no_sk'),
					'tgl_iupp'=>$this->input->post('tgl_sk'),
					'no_rekomendasi'=>$this->input->post('no_rekomendasi'),
					'no_rencana'=>$this->input->post('no_rencana'),
					'foto'=>$nama_file
					);
				$this->session->set_flashdata('tdp','tambah_sukses');
				$this->back_model->tambah_perizinan($data_form,$table);

				redirect('front/reg_tdp');
			}

		} else if (isset($_POST['btnIuts'])) {

			$table = $this->input->post('table');

			if ( ! $this->upload->do_upload('userfile'))
			{
				$data_form = array(
					'id'=>'',
					'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
					'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
					'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'npwp'=>$this->input->post('npwp'),
					'no_telp'=>$this->input->post('no_telp'),
					'no_fax'=>$this->input->post('no_fax'),
					'email'=>$this->input->post('email'),
					'keg_usaha_pokok'=>$this->input->post('keg_usaha_pokok'),
					'kbli'=>$this->input->post('kbli'),
					'status_kantor'=>$this->input->post('status_kantor'),
					'siup'=>$this->input->post('siup'),
					'tdp'=>$this->input->post('tdp'),
					'no_iuts'=>$this->input->post('no_sk'),
					'tgl_iuts'=>$this->input->post('tgl_sk'),
					'no_rekomendasi'=>$this->input->post('no_rekomendasi'),
					'no_rencana'=>$this->input->post('no_rencana'),
					'masaberlaku'=>$this->input->post('masaberlaku'),
					'foto'=>''
					);
				$this->session->set_flashdata('tdp','tambah_sukses');
				$this->back_model->tambah_perizinan($data_form,$table);

				redirect('front/reg_tdp');
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());

				$file = array('upload_sukses' => $this->upload->data());
				$nama_file = $file['upload_sukses']['file_name'];

				$data_form = array(
					'id'=>'',
					'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
					'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
					'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'npwp'=>$this->input->post('npwp'),
					'no_telp'=>$this->input->post('no_telp'),
					'no_fax'=>$this->input->post('no_fax'),
					'email'=>$this->input->post('email'),
					'keg_usaha_pokok'=>$this->input->post('keg_usaha_pokok'),
					'kbli'=>$this->input->post('kbli'),
					'status_kantor'=>$this->input->post('status_kantor'),
					'siup'=>$this->input->post('siup'),
					'no_iuts'=>$this->input->post('no_sk'),
					'tgl_iuts'=>$this->input->post('tgl_sk'),
					'no_rekomendasi'=>$this->input->post('no_rekomendasi'),
					'no_rencana'=>$this->input->post('no_rencana'),
					'masaberlaku'=>$this->input->post('masaberlaku'),
					'foto'=>$nama_file
					);
				$this->session->set_flashdata('tdp','tambah_sukses');
				$this->back_model->tambah_perizinan($data_form,$table);

				redirect('front/reg_tdp');
			}

		} else if (isset($_POST['btnStpw'])) {

			$table = $this->input->post('table');

			if ( ! $this->upload->do_upload('userfile'))
			{
				$data_form = array(
					'id'=>'',
					'no_pendaftaran'=>$this->input->post('no_pendaftaran'),
					'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
					'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
					'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'npwp'=>$this->input->post('npwp'),
					'iuts'=>$this->input->post('iuts'),
					'no_stpw'=>$this->input->post('no_sk'),
					'tgl_stpw'=>$this->input->post('tgl_sk'),
					'no_telp'=>$this->input->post('no_telp'),
					'masaberlaku'=>$this->input->post('masaberlaku'),
					'foto'=>''
					);
				$this->session->set_flashdata('tdp','tambah_sukses');
				$this->back_model->tambah_perizinan($data_form,$table);

				redirect('front/reg_tdp');
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());

				$file = array('upload_sukses' => $this->upload->data());
				$nama_file = $file['upload_sukses']['file_name'];

				$data_form = array(
					'id'=>'',
					'no_pendaftaran'=>$this->input->post('no_pendaftaran'),
					'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
					'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
					'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'npwp'=>$this->input->post('npwp'),
					'iuts'=>$this->input->post('iuts'),
					'no_stpw'=>$this->input->post('no_sk'),
					'tgl_stpw'=>$this->input->post('tgl_sk'),
					'no_telp'=>$this->input->post('no_telp'),
					'masaberlaku'=>$this->input->post('masaberlaku'),
					'foto'=>$nama_file
					);
				$this->session->set_flashdata('tdp','tambah_sukses');
				$this->back_model->tambah_perizinan($data_form,$table);

				redirect('front/reg_tdp');
			}

		} else if (isset($_POST['btnTdg'])) {

			$table = $this->input->post('table');

			if ( ! $this->upload->do_upload('userfile'))
			{
				$data_form = array(
					'id'=>'',
					'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
					'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'almt_penanggungjwb'=>$this->input->post('almt_penanggungjwb'),
					'almt_gudang'=>$this->input->post('almt_gudang'),
					'no_telp'=>$this->input->post('no_telp'),
					'no_fax'=>$this->input->post('no_fax'),
					'email'=>$this->input->post('email'),
					'jenis_isi_gudang'=>$this->input->post('jenis_isi_gudang'),
					'klasifikasi_gudang'=>$this->input->post('klasifikasi_gudang'),
					'luas_gudang'=>$this->input->post('luas_gudang'),
					'kapasitas_gudang'=>$this->input->post('kapasitas_gudang'),
					'kelengkapan_gudang'=>$this->input->post('kelengkapan_gudang'),
					'no_tdg'=>$this->input->post('no_tdg'),
					'tgl_tdg'=>$this->input->post('tgl_tdg'),
					'tdp'=>$this->input->post('tdp'),
					'masaberlaku'=>$this->input->post('masaberlaku'),
					'longitude'=>$this->input->post('longitude'),
					'latitude'=>$this->input->post('latitude'),
					'foto'=>''
					);
				$this->session->set_flashdata('tdp','tambah_sukses');
				$this->back_model->tambah_perizinan($data_form,$table);

				redirect('front/reg_tdp');
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());

				$file = array('upload_sukses' => $this->upload->data());
				$nama_file = $file['upload_sukses']['file_name'];

				$data_form = array(
					'id'=>'',
					'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
					'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'almt_penanggungjwb'=>$this->input->post('almt_penanggungjwb'),
					'almt_gudang'=>$this->input->post('almt_gudang'),
					'no_telp'=>$this->input->post('no_telp'),
					'no_fax'=>$this->input->post('no_fax'),
					'email'=>$this->input->post('email'),
					'jenis_isi_gudang'=>$this->input->post('jenis_isi_gudang'),
					'klasifikasi_gudang'=>$this->input->post('klasifikasi_gudang'),
					'luas_gudang'=>$this->input->post('luas_gudang'),
					'kapasitas_gudang'=>$this->input->post('kapasitas_gudang'),
					'kelengkapan_gudang'=>$this->input->post('kelengkapan_gudang'),
					'no_tdg'=>$this->input->post('no_tdg'),
					'tgl_tdg'=>$this->input->post('tgl_tdg'),
					'tdp'=>$this->input->post('tdp'),
					'masaberlaku'=>$this->input->post('masaberlaku'),
					'longitude'=>$this->input->post('longitude'),
					'latitude'=>$this->input->post('latitude'),
					'foto'=>$nama_file
					);
				$this->session->set_flashdata('tdp','tambah_sukses');
				$this->back_model->tambah_perizinan($data_form,$table);

				redirect('front/reg_tdp');
			}

		} else if (isset($_POST['btnSiupmb'])) {
			$table = $this->input->post('table');

			if ( ! $this->upload->do_upload('userfile'))
			{
				$data_form = array(
					'id'=>'',
					'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
					'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'siupmb'=>$this->input->post('siupmb'),
					'tdp'=>$this->input->post('tdp'),
					'tgl_keluar'=>$this->input->post('tgl_keluar'),
					'tgl_berakhir'=>$this->input->post('tgl_berakhir'),
					'ins_yg_mengeluarkan'=>$this->input->post('ins_yg_mengeluarkan'),
					'keterangan'=>$this->input->post('keterangan'),
					'foto'=>''
					);
				$this->session->set_flashdata('tdp','tambah_sukses');
				$this->back_model->tambah_perizinan($data_form,$table);

				redirect('front/reg_tdp');
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());

				$file = array('upload_sukses' => $this->upload->data());
				$nama_file = $file['upload_sukses']['file_name'];

				$data_form = array(
					'id'=>'',
					'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
					'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'siupmb'=>$this->input->post('siupmb'),
					'tdp'=>$this->input->post('tdp'),
					'tgl_keluar'=>$this->input->post('tgl_keluar'),
					'tgl_berakhir'=>$this->input->post('tgl_berakhir'),
					'ins_yg_mengeluarkan'=>$this->input->post('ins_yg_mengeluarkan'),
					'keterangan'=>$this->input->post('keterangan'),
					'foto'=>$nama_file
					);
				$this->session->set_flashdata('tdp','tambah_sukses');
				$this->back_model->tambah_perizinan($data_form,$table);

				redirect('front/reg_tdp');
			}
		} else if (isset($_POST['btnTdpud'])) {

			$table = $this->input->post('table');

			if ( ! $this->upload->do_upload('userfile'))
			{
				$data_form = array(
					'id'=>'',
					'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
					'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
					'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'npwp'=>$this->input->post('npwp'),
					'no_telp'=>$this->input->post('no_telp'),
					'no_fax'=>$this->input->post('no_fax'),
					'email'=>$this->input->post('email'),
					'keg_usaha_pokok'=>$this->input->post('keg_usaha_pokok'),
					'kbli'=>$this->input->post('kbli'),
					'status_kantor'=>$this->input->post('status_kantor'),
					'tdp'=>$this->input->post('tdp'),
					'no_tdpud'=>$this->input->post('no_sk'),
					'tgl_tdpud'=>$this->input->post('tgl_sk'),
					'jenis_tdpud'=>$this->input->post('jenis_tdpud'),
					'masaberlaku'=>$this->input->post('masaberlaku'),
					'foto'=>''
					);
				$this->session->set_flashdata('tdp','tambah_sukses');
				$this->back_model->tambah_perizinan($data_form,$table);

				redirect('front/reg_tdp');
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());

				$file = array('upload_sukses' => $this->upload->data());
				$nama_file = $file['upload_sukses']['file_name'];

				$data_form = array(
					'id'=>'',
					'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
					'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
					'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'npwp'=>$this->input->post('npwp'),
					'no_telp'=>$this->input->post('no_telp'),
					'no_fax'=>$this->input->post('no_fax'),
					'email'=>$this->input->post('email'),
					'keg_usaha_pokok'=>$this->input->post('keg_usaha_pokok'),
					'kbli'=>$this->input->post('kbli'),
					'status_kantor'=>$this->input->post('status_kantor'),
					'tdp'=>$this->input->post('tdp'),
					'no_tdpud'=>$this->input->post('no_sk'),
					'tgl_tdpud'=>$this->input->post('tgl_sk'),
					'jenis_tdpud'=>$this->input->post('jenis_tdpud'),
					'masaberlaku'=>$this->input->post('masaberlaku'),
					'foto'=>$nama_file
					);
				$this->session->set_flashdata('tdp','tambah_sukses');
				$this->back_model->tambah_perizinan($data_form,$table);

				redirect('front/reg_tdp');
			}

		} else {

			$table = $this->input->post('table');

			$data_form = array(
				'id'=>'',
				'no_cas'=>$this->input->post('no_cas'),
				'pos_trf_hs'=>$this->input->post('pos_trf_hs'),
				'kecamatan'=>$this->input->post('kecamatan'),
				'kelurahan'=>$this->input->post('kelurahan'),
				'almt_penyimpanan'=>$this->input->post('almt_penyimpanan'),
				'uraian_brg'=>$this->input->post('uraian_brg'),
				'tata_niaga_impor'=>$this->input->post('tata_niaga_impor'),
				'kep_lain_tdk_utk_pgn'=>$this->input->post('kep_lain_tdk_utk_pgn'),
				'lab'=>$this->input->post('lab')
				);
			$this->session->set_flashdata('tdp','tambah_sukses');
			$this->back_model->tambah_perizinan($data_form,$table);

			redirect('front/reg_tdp');

		}
	}

	public function edit_tdp()
	{
		if (isset($_POST['btnSubmit'])) {
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['berita'] = $this->front_model->daftar_berita();
			$data['agenda'] = $this->front_model->daftar_agenda();

			$data['tdp'] = $this->back_model->get_tdp_by_npwp($this->input->post('npwp'));
			$this->load->view('front/edit_tdp2',$data);
		} else {
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['berita'] = $this->front_model->daftar_berita();
			$data['agenda'] = $this->front_model->daftar_agenda();
			$this->load->view('front/edit_tdp',$data);
		}
		
	}
	
	function sambutan()
	{
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$data['berita'] = $this->front_model->daftar_berita();
		$data['agenda'] = $this->front_model->daftar_agenda();
		
		$data['pasar'] = $this->back_model->daftar_pasar();
		$data['b_pokok'] = $this->back_model->daftar_b_pokok();
		$data['b_pokok_sub'] = $this->back_model->daftar_sub_b_pokok_all();
		$data['tr_b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_min7days();

		$data['visitor'] = $this->back_model->get_visitor();
		$data['sambutan'] = $this->back_model->sambutan();
		
		$this->load->view('front/front_sambutan',$data);
	}
	
	function struktur_organisasi()
	{
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$data['berita'] = $this->front_model->daftar_berita();
		$data['agenda'] = $this->front_model->daftar_agenda();
		
		$data['pasar'] = $this->back_model->daftar_pasar();
		$data['b_pokok'] = $this->back_model->daftar_b_pokok();
		$data['b_pokok_sub'] = $this->back_model->daftar_sub_b_pokok_all();
		$data['tr_b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_min7days();

		$data['visitor'] = $this->back_model->get_visitor();
		
		$data['people'] = $this->back_model->get_struk_org();
		$this->load->view('front/front_struktur_organisasi',$data);
	}
}

?>