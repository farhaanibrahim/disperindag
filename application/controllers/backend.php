<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backend extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('front_model');
		$this->load->model('back_model');
		$this->load->library('excel');

	}
	function index(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Dashborad | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;
			$this->load->view('backend/back_dashboard',$data);
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function regulasi(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Regulasi | Disperindag";
			$data['title2'] = "Regulasi ";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['regulasi'] = $this->back_model->daftar_regulasi();
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;
			$this->load->view('backend/back_regulasi',$data);
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function tambah_regulasi(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Tambah Regulasi | Disperindag";
			$data['title2'] = "Tambah Regulasi";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;
			$data['pasar'] = $this->back_model->daftar_pasar();
			$this->load->view('backend/back_tambah_regulasi',$data);
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function tambah_regulasi_ac(){
		if($this->session->userdata('admin_id')){
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			if(isset($_FILES['file_regulasi'])){
				$config['upload_path'] = './upload/regulasi/';
				$config['allowed_types'] = 'jpg|jpeg|png|doc|docx|pdf';
				$config['max_size']	= '8048';
				$config['max_width']  = '22024';
				$config['max_height']  = '22068';

				$this->load->library('upload', $config);

				if ( $this->upload->do_upload('file_regulasi')) {
					$data = array('upload_sukses' => $this->upload->data());

					$params['text'] = $data['upload_sukses']['file_name'];
					if($this->back_model->tambah_regulasi($params)){
						$this->session->set_flashdata('regulasi','tambah_sukses');
						//echo "1";
					}else{
						$this->session->set_flashdata('regulasi','gagal');
						//echo "0";
					}
				}else{
					$error = array('error' => $this->upload->display_errors());
					foreach($error as $a){}
					if($a == '<p>You did not select a file to upload.</p>'){
						$params['text'] = '';
						if($this->back_model->tambah_regulasi($params)){
							$this->session->set_flashdata('regulasi','tambah_sukses');
						}else{
							$this->session->set_flashdata('regulasi','gagal');
						}
					}else{
						$this->session->set_flashdata('regulasi', 'error_upload');
						$this->session->set_flashdata('regulasi_error', $error['error']);
						//echo $error['error'];
					}
				}
				redirect('backend/regulasi');
			}else{
				$params['text'] = '';
				if($this->back_model->tambah_pasar($params)){
					$this->session->set_flashdata('regulasi','tambah_sukses');
				}else{
					$this->session->set_flashdata('regulasi','gagal');
				}
			}
			redirect('backend/regulasi');
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function hapus_regulasi($id_regulasi){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Hapus Regulasi | Disperindag";
			$data['title2'] = "Hapus Regulasi";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;
			$data['regulasi'] = $this->back_model->daftar_regulasi_where_id_regulasi($id_regulasi);
			if($data['regulasi']->num_rows() > 0){
				$this->load->view('backend/back_hapus_regulasi',$data);
			}else{
				redirect('backend');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function hapus_regulasi_ac(){
		if($this->session->userdata('admin_id')){
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['regulasi'] = $this->back_model->daftar_regulasi_where_id_regulasi($this->input->post('id_regulasi'));
			if($data['regulasi']->num_rows() > 0){
				foreach($data['regulasi']->result() as $res){}
				if($this->back_model->hapus_regulasi($params)){
					if($res->file != ''){
						unlink("upload/regulasi/".$res->file);
					}
					$this->session->set_flashdata('regulasi','hapus_sukses');
					//echo "1";
				}else{
					$this->session->set_flashdata('regulasi','gagal');
					//echo "0";
				}
			}else{
				redirect('backend/regulasi');
			}
			redirect('backend/regulasi');
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function edit_regulasi($id_regulasi){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Edit Regulasi | Disperindag";
			$data['title2'] = "Edit Regulasi";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;
			$data['regulasi'] = $this->back_model->daftar_regulasi_where_id_regulasi($id_regulasi);
			if($data['regulasi']->num_rows() > 0){
				$this->load->view('backend/back_edit_regulasi',$data);
			}else{
				redirect('backend');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function edit_regulasi_ac(){
		if($this->session->userdata('admin_id')){
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['regulasi'] = $this->back_model->daftar_regulasi_where_id_regulasi($this->input->post('id_regulasi'));
			if($data['regulasi']->num_rows() > 0){
				foreach($data['regulasi']->result() as $res){}
				if(isset($_FILES['file_regulasi'])){
					$config['upload_path'] = './upload/regulasi/';
					$config['allowed_types'] = 'jpg|jpeg|png|doc|docx|pdf';
					$config['max_size']	= '4048';
					$config['max_width']  = '22024';
					$config['max_height']  = '22068';

					$this->load->library('upload', $config);

					if ( $this->upload->do_upload('file_regulasi')) {
						$data = array('upload_sukses' => $this->upload->data());

						$params['text'] = $data['upload_sukses']['file_name'];
						if($this->back_model->edit_regulasi($params)){
							if($res->file != ''){
								unlink("upload/regulasi/".$res->file);
							}
							$this->session->set_flashdata('regulasi','edit_sukses');
							//echo "1";
						}else{
							$this->session->set_flashdata('regulasi','gagal');
							//echo "0";
						}
					}else{
						$error = array('error' => $this->upload->display_errors());
						foreach($error as $a){}
						if($a == '<p>You did not select a file to upload.</p>'){
							$params['text'] = $res->file;
							if($this->back_model->edit_regulasi($params)){
								$this->session->set_flashdata('regulasi','edit_sukses');
							}else{
								$this->session->set_flashdata('regulasi','gagal');
							}
						}else{
							$this->session->set_flashdata('regulasi', 'error_upload');
							$this->session->set_flashdata('regulasi_error', $error['error']);
							echo $error['error'];
						}
					}
					redirect('backend/regulasi');
				}else{
					$params['text'] = $res->file;
					if($this->back_model->edit_regulasi($params)){
						$this->session->set_flashdata('regulasi','edit_sukses');
					}else{
						$this->session->set_flashdata('regulasi','gagal');
					}
				}
			}else{
				redirect('backend/regulasi');
			}
			redirect('backend/regulasi');
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function pasar(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Daftar Pasar | Disperindag";
			$data['title2'] = "Daftar Pasar";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['pasar'] = $this->back_model->daftar_pasar();
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;
			$this->load->view('backend/back_pasar',$data);
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function tambah_pasar(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Tambah Pasar | Disperindag";
			$data['title2'] = "Tambah Pasar";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;
			$data['pasar'] = $this->back_model->daftar_pasar();
			$this->load->view('backend/back_tambah_pasar',$data);
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function tambah_pasar_ac(){
		if($this->session->userdata('admin_id')){
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			if(isset($_FILES['foto'])){
				$config['upload_path'] = './upload/pasar/';
				$config['allowed_types'] = 'jpg|jpeg|png';
				$config['max_size']	= '4048';
				$config['max_width']  = '22024';
				$config['max_height']  = '22068';

				$this->load->library('upload', $config);

				if ( $this->upload->do_upload('foto')) {
					$data = array('upload_sukses' => $this->upload->data());

					$params['text'] = $data['upload_sukses']['file_name'];
					if($this->back_model->tambah_pasar($params)){
						$this->session->set_flashdata('pasar','tambah_sukses');
						//echo "1";
					}else{
						$this->session->set_flashdata('pasar','gagal');
						//echo "0";
					}
				}else{
					$error = array('error' => $this->upload->display_errors());
					foreach($error as $a){}
					if($a == '<p>You did not select a file to upload.</p>'){
						$params['text'] = '';
						if($this->back_model->tambah_pasar($params)){
							$this->session->set_flashdata('pasar','tambah_sukses');
						}else{
							$this->session->set_flashdata('pasar','gagal');
						}
					}else{
						$this->session->set_flashdata('pasar', 'error_upload');
						$this->session->set_flashdata('pasar_error', $error['error']);
						echo $error['error'];
					}
				}
				redirect('backend/pasar');
			}else{
				$params['text'] = '';
				if($this->back_model->tambah_pasar($params)){
					$this->session->set_flashdata('pasar','tambah_sukses');
				}else{
					$this->session->set_flashdata('pasar','gagal');
				}
			}
			redirect('backend/pasar');
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function edit_pasar($id_pasar){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Edit Pasar | Disperindag";
			$data['title2'] = "Edit Pasar";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;
			$data['pasar'] = $this->back_model->daftar_pasar_where_id_pasar($id_pasar);
			if($data['pasar']->num_rows() > 0){
				$this->load->view('backend/back_edit_pasar',$data);
			}else{
				redirect('backend');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function edit_pasar_ac(){
		if($this->session->userdata('admin_id')){
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['pasar'] = $this->back_model->daftar_pasar_where_id_pasar($this->input->post('id_pasar'));
			if($data['pasar']->num_rows() > 0){
				foreach($data['pasar']->result() as $res){}
				if(isset($_FILES['foto'])){
					$config['upload_path'] = './upload/pasar/';
					$config['allowed_types'] = 'jpg|jpeg|png';
					$config['max_size']	= '4048';
					$config['max_width']  = '22024';
					$config['max_height']  = '22068';

					$this->load->library('upload', $config);

					if ( $this->upload->do_upload('foto')) {
						$data = array('upload_sukses' => $this->upload->data());

						$params['text'] = $data['upload_sukses']['file_name'];
						if($this->back_model->edit_pasar($params)){
							if($res->foto != ''){
								unlink("upload/pasar/".$res->foto);
							}
							$this->session->set_flashdata('pasar','edit_sukses');
							//echo "1";
						}else{
							$this->session->set_flashdata('pasar','gagal');
							//echo "0";
						}
					}else{
						$error = array('error' => $this->upload->display_errors());
						foreach($error as $a){}
						if($a == '<p>You did not select a file to upload.</p>'){
							$params['text'] = '';
							if($this->back_model->edit_pasar($params)){
								$this->session->set_flashdata('pasar','edit_sukses');
							}else{
								$this->session->set_flashdata('pasar','gagal');
							}
						}else{
							$this->session->set_flashdata('pasar', 'error_upload');
							$this->session->set_flashdata('pasar_error', $error['error']);
							echo $error['error'];
						}
					}
					redirect('backend/pasar');
				}else{
					$params['text'] = '';
					if($this->back_model->tambah_pasar($params)){
						$this->session->set_flashdata('pasar','edit_sukses');
					}else{
						$this->session->set_flashdata('pasar','gagal');
					}
				}
			}else{
				redirect('backend/pasar');
			}
			redirect('backend/pasar');
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function hapus_pasar($id_pasar){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Hapus Pasar | Disperindag";
			$data['title2'] = "Hapus Pasar";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;
			$data['pasar'] = $this->back_model->daftar_pasar_where_id_pasar($id_pasar);
			if($data['pasar']->num_rows() > 0){
				$this->load->view('backend/back_hapus_pasar',$data);
			}else{
				redirect('backend');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function hapus_pasar_ac(){
		if($this->session->userdata('admin_id')){
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['pasar'] = $this->back_model->daftar_pasar_where_id_pasar($this->input->post('id_pasar'));
			if($data['pasar']->num_rows() > 0){
				foreach($data['pasar']->result() as $res){}
				if($this->back_model->hapus_pasar()){
					if($res->foto != ''){
						unlink("upload/pasar/".$res->foto);
					}
					$this->session->set_flashdata('pasar','hapus_sukses');
					//echo "1";
				}else{
					$this->session->set_flashdata('pasar','gagal');
					//echo "0";
				}
			}else{
				redirect('backend/pasar');
			}
			redirect('backend/pasar');
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function transaksi_b_pokok(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Transaksi Bahan Pokok | Disperindag";
			$data['title2'] = "Transaksi Bahan Pokok | Pokok";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['tr_b_pokok'] = $this->back_model->daftar_tr_b_pokok();
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;
			$this->load->view('backend/back_tr_bahan_pokok',$data);
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function hapus_transaksi_b_pokok($id_tr){
		if($this->session->userdata('admin_id')){

			if(!isset($id_tr)){
				redirect('backend/transaksi_b_pokok');
			}

			$data['title'] = "Hapus Transaksi Bahan Pokok | Disperindag";
			$data['title2'] = "Hapus Transaksi Bahan Pokok | Pokok";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;

			$data['tr_b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_id_tr($id_tr);
			if($data['tr_b_pokok']->num_rows() > 0){
				$data['pasar'] = $this->back_model->daftar_pasar();
				$this->load->view('backend/back_hapus_tr_b_pokok',$data);
			}else{
				redirect('backend/transaksi_b_pokok');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function hapus_transaksi_b_pokok_ac(){
		if($this->session->userdata('admin_id')){

			if(isset($_POST['id_tr_pokok'])){
				$id_tr = $_POST['id_tr_pokok'];
			}else{
				redirect('backend/transaksi_b_pokok');
			}

			$data['tr_b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_id_tr($id_tr);
			if($data['tr_b_pokok']->num_rows() > 0){
				if($this->back_model->hapus_tr_b_pokok()){
					//echo 1;
					$this->session->set_flashdata('tr_b_pokok','hapus_sukses');
				}else{
					//echo 0;
					$this->session->set_flashdata('tr_b_pokok','gagal');
				}
				redirect('backend/transaksi_b_pokok');
			}else{
				redirect('backend/transaksi_b_pokok');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function edit_transaksi_b_pokok($id_tr){
		if($this->session->userdata('admin_id')){

			if(!isset($id_tr)){
				redirect('backend/transaksi_b_pokok');
			}

			$data['title'] = "Edit Transaksi Bahan Pokok | Disperindag";
			$data['title2'] = "Edit Transaksi Bahan Pokok | Pokok";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;

			$data['tr_b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_id_tr($id_tr);
			if($data['tr_b_pokok']->num_rows() > 0){
				$data['pasar'] = $this->back_model->daftar_pasar();
				$data['b_pokok'] = $this->back_model->daftar_b_pokok();
				$data['b_pokok_sub'] = $this->back_model->daftar_sub_b_pokok_all();
				$this->load->view('backend/back_edit_tr_b_pokok',$data);
			}else{
				redirect('backend/transaksi_b_pokok');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function edit_transaksi_b_pokok_ac(){
		if($this->session->userdata('admin_id')){

			if(isset($_POST['id_tr_pokok'])){
				$id_tr = $_POST['id_tr_pokok'];
			}else{
				redirect('backend/transaksi_b_pokok');
			}

			$data['tr_b_pokok'] = $this->back_model->daftar_tr_b_pokok_where_id_tr($id_tr);
			if($data['tr_b_pokok']->num_rows() > 0){
				if($this->back_model->edit_tr_b_pokok()){
					//echo 1;
					$this->session->set_flashdata('tr_b_pokok','edit_sukses');
				}else{
					//echo 0;
					$this->session->set_flashdata('tr_b_pokok','gagal');
				}
				redirect('backend/transaksi_b_pokok');
			}else{
				redirect('backend/transaksi_b_pokok');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function tambah_transaksi_b_pokok(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Tambah Transaksi Bahan Pokok | Disperindag";
			$data['title2'] = "Tambah Transaksi Bahan Pokok | Pokok";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;
			$data['pasar'] = $this->back_model->daftar_pasar();
			$data['b_pokok'] = $this->back_model->daftar_b_pokok();
			$data['b_pokok_sub'] = $this->back_model->daftar_sub_b_pokok_all();
			$this->load->view('backend/back_tambah_tr_b_pokok',$data);
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function tambah_transaksi_b_pokok_ac(){
		if($this->session->userdata('admin_id')){
			if($this->back_model->tambah_tr_b_pokok()){
				//echo 1;
				$this->session->set_flashdata('tr_b_pokok','tambah_sukses');
			}else{
				//echo 0;
				$this->session->set_flashdata('tr_b_pokok','gagal');
			}
			redirect('backend/transaksi_b_pokok');
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function bahan_pokok(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Bahan Pokok | Disperindag";
			$data['title2'] = "Bahan Pokok";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['b_pokok'] = $this->back_model->daftar_b_pokok();
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;
			$this->load->view('backend/back_bahan_pokok',$data);
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}

	function bahan_pokok_berbahaya(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Bahan Pokok Berbahaya | Disperindag";
			$data['title2'] = "Bahan Pokok Berbahaya";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['b_pokok_berbahaya'] = $this->back_model->daftar_b_pokok_berbahaya();
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;
			$this->load->view('backend/back_bahan_pokok_berbahaya',$data);
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}

	function hapus_bahan_pokok($id_bahan_pokok){
		if($this->session->userdata('admin_id')){
			if(isset($id_bahan_pokok)){
				$data['title'] = "Hapus Bahan Pokok | Disperindag";
				$data['title2'] = "Hapus Bahan Pokok";
				$data['instansi'] = $this->front_model->data_tr_instansi();
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				foreach($data['data_login']->result() as $row){}
				$data['b_pokok'] = $this->back_model->daftar_b_pokok_id_bahan_pokok($id_bahan_pokok);
				if($data['b_pokok']->num_rows() > 0){
					$data['level'] = $row->level;
					$data['pasar'] = $this->back_model->daftar_pasar();
					$this->load->view('backend/back_hapus_b_pokok',$data);
				}else{
					redirect('backend/bahan_pokok');
				}
			}else{
				redirect('backend/bahan_pokok');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function hapus_b_pokok_ac(){
		if($this->session->userdata('admin_id')){
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['b_pokok'] = $this->back_model->daftar_b_pokok_id_bahan_pokok($this->input->post('id_bahan_pok'));
			if($data['b_pokok']->num_rows() > 0){
				foreach($data['b_pokok']->result() as $res){}
				if($this->back_model->hapus_b_pokok()){
					if($res->foto_b_pokok != NULL){
						unlink("upload/bahan_pokok/".$res->foto_b_pokok);
					}
					$this->session->set_flashdata('b_pokok','hapus_sukses');
				}else{
					$this->session->set_flashdata('b_pokok','gagal');
				}
			}
			redirect('backend/bahan_pokok');
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function edit_bahan_pokok($id_bahan_pokok){
		if($this->session->userdata('admin_id')){
			if(isset($id_bahan_pokok)){
				$data['title'] = "Edit Bahan Pokok | Disperindag";
				$data['title2'] = "Edit Bahan Pokok";
				$data['instansi'] = $this->front_model->data_tr_instansi();
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				foreach($data['data_login']->result() as $row){}
				$data['b_pokok'] = $this->back_model->daftar_b_pokok_id_bahan_pokok($id_bahan_pokok);
				if($data['b_pokok']->num_rows() > 0){
					$data['level'] = $row->level;
					$data['pasar'] = $this->back_model->daftar_pasar();
					$this->load->view('backend/back_edit_b_pokok',$data);
				}else{
					redirect('backend/bahan_pokok');
				}
			}else{
				redirect('backend/bahan_pokok');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function edit_b_pokok_ac(){
		if($this->session->userdata('admin_id')){
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}

			$config['upload_path'] = './upload/bahan_pokok/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			$config['max_size']	= '5048';
			$config['max_width']  = '22024';
			$config['max_height']  = '22068';

			$this->load->library('upload', $config);

			if ( $this->upload->do_upload('foto_b_pokok')) {
				$data = array('upload_sukses' => $this->upload->data());
				$data['b_pokok'] = $this->back_model->daftar_b_pokok_id_bahan_pokok($this->input->post('id_bahan_pok'));
				foreach($data['b_pokok']->result() as $res){}
				$params['text'] = $data['upload_sukses']['file_name'];
				if($this->back_model->edit_b_pokok($params)){
					unlink("upload/bahan_pokok/".$res->foto_b_pokok);
					$this->session->set_flashdata('b_pokok','edit_sukses');
				}else{
					$this->session->set_flashdata('b_pokok','gagal');
				}
			}else{
				$error = array('error' => $this->upload->display_errors());
				foreach($error as $a){}
				if($a == '<p>You did not select a file to upload.</p>'){
					$params['text'] = "";
					if($this->back_model->edit_b_pokok($params)){
						$this->session->set_flashdata('b_pokok','edit_sukses');
					}else{
						$this->session->set_flashdata('b_pokok','gagal');
					}
				}else{
					$this->session->set_flashdata('b_pokok', 'error_upload');
					$this->session->set_flashdata('b_pokok_error', $error['error']);
					echo $error['error'];
				}
			}

			redirect('backend/bahan_pokok');
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function tambah_b_pokok(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Tambah Bahan Pokok | Disperindag";
			$data['title2'] = "Tambah Pokok";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;
			$data['pasar'] = $this->back_model->daftar_pasar();
			$data['katg_berita'] = $this->back_model->daftar_kategori_berita();
			$this->load->view('backend/back_tambah_b_pokok',$data);
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}

	function tambah_b_pokok_berbahaya(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Tambah Bahan Pokok Berbahaya | Disperindag";
			$data['title2'] = "Tambah Pokok Berbahaya";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;
			$data['pasar'] = $this->back_model->daftar_pasar();
			$data['katg_berita'] = $this->back_model->daftar_kategori_berita();
			$this->load->view('backend/back_tambah_b_pokok_berbahaya',$data);
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}

	function tambah_b_pokok_ac(){
		if($this->session->userdata('admin_id')){
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}

			$config['upload_path'] = './upload/bahan_pokok/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			$config['max_size']	= '5048';
			$config['max_width']  = '22024';
			$config['max_height']  = '22068';

			$this->load->library('upload', $config);

			if ( $this->upload->do_upload('foto_b_pokok')) {
				$data = array('upload_sukses' => $this->upload->data());

				$params['text'] = $data['upload_sukses']['file_name'];
				if($this->back_model->tambah_b_pokok($params)){
					$this->session->set_flashdata('b_pokok','tambah_sukses');
				}else{
					$this->session->set_flashdata('b_pokok','gagal');
				}
			}else{
				$error = array('error' => $this->upload->display_errors());
				foreach($error as $a){}
				if($a == '<p>You did not select a file to upload.</p>'){
					$params['text'] = "";
					if($this->back_model->tambah_b_pokok($params)){
						$this->session->set_flashdata('b_pokok','tambah_sukses');
					}else{
						$this->session->set_flashdata('b_pokok','gagal');
					}
				}else{
					$this->session->set_flashdata('b_pokok', 'error_upload');
					$this->session->set_flashdata('b_pokok_error', $error['error']);
					echo $error['error'];
				}
			}

			redirect('backend/bahan_pokok');
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function tambah_b_pokok_berbahaya_ac(){
		if($this->session->userdata('admin_id')){
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}

			$config['upload_path'] = './upload/bahan_pokok_berbahaya/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			$config['max_size']	= '5048';
			$config['max_width']  = '22024';
			$config['max_height']  = '22068';

			$this->load->library('upload', $config);

			if ( $this->upload->do_upload('foto_b_pokok')) {
				$data = array('upload_sukses' => $this->upload->data());

				$params['text'] = $data['upload_sukses']['file_name'];
				if($this->back_model->tambah_b_pokok_berbahaya($params)){
					$this->session->set_flashdata('b_pokok','tambah_sukses');
				}else{
					$this->session->set_flashdata('b_pokok','gagal');
				}
			}else{
				$error = array('error' => $this->upload->display_errors());
				foreach($error as $a){}
				if($a == '<p>You did not select a file to upload.</p>'){
					$params['text'] = "";
					if($this->back_model->tambah_b_pokok_berbahaya($params)){
						$this->session->set_flashdata('b_pokok','tambah_sukses');
					}else{
						$this->session->set_flashdata('b_pokok','gagal');
					}
				}else{
					$this->session->set_flashdata('b_pokok', 'error_upload');
					$this->session->set_flashdata('b_pokok_error', $error['error']);
					echo $error['error'];
				}
			}

			redirect('backend/bahan_pokok_berbahaya');
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function sub_bahan_pokok(){
		if($this->session->userdata('admin_id')){

			$data['title'] = "Sub Bahan Pokok | Disperindag";
			$data['title2'] = "Sub Bahan Pokok";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));

			$data['sub_b_pokok'] = $this->back_model->daftar_sub_b_pokok_all();
			$this->load->view('backend/back_sub_bahan_pokok',$data);
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function tambah_sub_bahan_pokok(){
		if($this->session->userdata('admin_id')){

			$data['title'] = "Tambah Sub Bahan Pokok | Disperindag";
			$data['title2'] = "Tambah Sub Bahan Pokok";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));

			$this->load->view('backend/back_tambah_sub_b_pokok',$data);
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function tambah_sub_bahan_pokok_ac(){
		if($this->session->userdata('admin_id')){

			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));

			$config['upload_path'] = './upload/sub_bahan_pokok/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			$config['max_size']	= '5048';
			$config['max_width']  = '22024';
			$config['max_height']  = '22068';

			$this->load->library('upload', $config);

			if ( $this->upload->do_upload('foto_sub_b_pokok')) {
				$data = array('upload_sukses' => $this->upload->data());

				$params['text'] = $data['upload_sukses']['file_name'];
				if($this->back_model->tambah_sub_b_pokok($params)){
					$this->session->set_flashdata('b_pokok_sub','tambah_sukses');
				}else{
					$this->session->set_flashdata('b_pokok_sub','gagal');
				}
			}else{
				$error = array('error' => $this->upload->display_errors());
				foreach($error as $a){}
				if($a == '<p>You did not select a file to upload.</p>'){
					$params['text'] = "";
					if($this->back_model->tambah_sub_b_pokok($params)){
						$this->session->set_flashdata('b_pokok_sub','tambah_sukses');
					}else{
						$this->session->set_flashdata('b_pokok_sub','gagal');
					}
				}else{
					$this->session->set_flashdata('b_pokok_sub', 'error_upload');
					$this->session->set_flashdata('b_pokok_sub_error', $error['error']);
					echo $error['error'];
				}
			}

			redirect("backend/sub_bahan_pokok/");
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function edit_sub_bahan_pokok($id_bahan_pokok_sub){
		if($this->session->userdata('admin_id')){

			$data['title'] = "Edit Sub Bahan Pokok | Disperindag";
			$data['title2'] = "Edit Sub Bahan Pokok";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));

			$data['sub_b_pokok'] = $this->back_model->daftar_sub_b_pokok_where_id($id_bahan_pokok_sub);
			if($data['sub_b_pokok']->num_rows() > 0){
				$this->load->view('backend/back_edit_sub_b_pokok',$data);
			}else{
				redirect('backend/bahan_pokok');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function edit_sub_bahan_pokok_ac(){
		if($this->session->userdata('admin_id')){
			if(!isset($_POST['id_bahan_pok_sub'])){
				redirect('backend/bahan_pokok');
			}else{
				$id_bahan_pok_sub = $_POST['id_bahan_pok_sub'];
				$params['id_bahan_pok_sub'] = $_POST['id_bahan_pok_sub'];
			}

			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));

			$data['sub_b_pokok'] = $this->back_model->daftar_sub_b_pokok_where_id($id_bahan_pok_sub);
			if($data['sub_b_pokok']->num_rows() > 0){

				$data['sub_b_pokok'] = $this->back_model->daftar_sub_b_pokok_where_id($id_bahan_pok_sub);
				foreach($data['sub_b_pokok']->result() as $res){}

				$config['upload_path'] = './upload/sub_bahan_pokok/';
				$config['allowed_types'] = 'jpg|jpeg|png';
				$config['max_size']	= '5048';
				$config['max_width']  = '22024';
				$config['max_height']  = '22068';

				$this->load->library('upload', $config);

				if ( $this->upload->do_upload('foto_sub_b_pokok')) {
					$data = array('upload_sukses' => $this->upload->data());

					$params['text'] = $data['upload_sukses']['file_name'];
					if($this->back_model->edit_sub_b_pokok($params)){

						unlink("upload/sub_bahan_pokok/".$res->foto_sub_b_pokok);
						$this->session->set_flashdata('b_pokok_sub','edit_sukses');
					}else{
						$this->session->set_flashdata('b_pokok_sub','gagal');
					}
				}else{
					$error = array('error' => $this->upload->display_errors());
					foreach($error as $a){}
					if($a == '<p>You did not select a file to upload.</p>'){
						$params['text'] = "";
						if($this->back_model->edit_sub_b_pokok($params)){
							$this->session->set_flashdata('b_pokok_sub','edit_sukses');
						}else{
							$this->session->set_flashdata('b_pokok_sub','gagal');
						}
					}else{
						$this->session->set_flashdata('b_pokok_sub', 'error_upload');
						$this->session->set_flashdata('b_pokok_sub_error', $error['error']);
						echo $error['error'];
					}
				}

				redirect("backend/sub_bahan_pokok/");
			}else{
				redirect('backend/bahan_pokok');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function hapus_sub_bahan_pokok($id_bahan_pokok_sub){
		if($this->session->userdata('admin_id')){
			if(!isset($id_bahan_pokok_sub)){
				redirect('backend/bahan_pokok');
			}
			$data['title'] = "Hapus Sub Bahan Pokok | Disperindag";
			$data['title2'] = "Hapus Sub Bahan Pokok";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));

			$data['sub_b_pokok'] = $this->back_model->daftar_sub_b_pokok_where_id($id_bahan_pokok_sub);
			if($data['sub_b_pokok']->num_rows() > 0){
				$this->load->view('backend/back_hapus_sub_b_pokok',$data);
			}else{
				redirect('backend/bahan_pokok');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function hapus_sub_bahan_pokok_ac(){
		if($this->session->userdata('admin_id')){
			if(!isset($_POST['id_bahan_pok_sub'])){
				redirect('backend/bahan_pokok');
			}else{
				$id_bahan_pok_sub = $_POST['id_bahan_pok_sub'];
				$params['id_bahan_pok_sub'] = $_POST['id_bahan_pok_sub'];
			}

			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));

			$data['sub_b_pokok'] = $this->back_model->daftar_sub_b_pokok_where_id($id_bahan_pok_sub);
			if($data['sub_b_pokok']->num_rows() > 0){

				foreach($data['sub_b_pokok']->result() as $res){}
				if($this->back_model->hapus_sub_b_pokok($params)){
					if($res->foto_sub_b_pokok != ""){
						unlink("upload/sub_bahan_pokok/".$res->foto_sub_b_pokok);
					}
					$this->session->set_flashdata('b_pokok_sub','hapus_sukses');

				}else{
					$this->session->set_flashdata('b_pokok_sub','gagal');
				}
				redirect("backend/sub_bahan_pokok/");

			}else{
				redirect('backend/bahan_pokok');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function gallery(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Gallery | Disperindag";
			$data['title2'] = "Gallery";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			if($row->level == 'Admin'){
				$data['gallery'] = $this->back_model->daftar_gallery();
				foreach($data['data_login']->result() as $row){}
				$data['level'] = $row->level;
				$this->load->view('backend/back_gallery',$data);
			}else{
				redirect('backend');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function tambah_gallery(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Tambah Gallery | Disperindag";
			$data['title2'] = "Tambah Gallery";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			if($row->level == 'Admin'){
				$data['level'] = $row->level;
				$data['katg_gallery'] = $this->back_model->daftar_kategori_gallery();
				$this->load->view('backend/back_tambah_gallery',$data);
			}else{
				redirect('backend');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function tambah_gallery_ac(){
		if($this->session->userdata('admin_id')){
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$params['publisher'] = $this->session->userdata('admin_id');
			if($row->level == 'Admin'){
				if(isset($_FILES['filename'])){
					$config['upload_path'] = './upload/gallery/';
					$config['allowed_types'] = 'jpg|jpeg|png';
					$config['max_size']	= '4048';
					$config['max_width']  = '22024';
					$config['max_height']  = '22068';

					$this->load->library('upload', $config);

					if ( $this->upload->do_upload('filename')) {
						$data = array('upload_sukses' => $this->upload->data());

						$params['text'] = $data['upload_sukses']['file_name'];
						if($this->back_model->tambah_gallery($params)){
							$this->session->set_flashdata('gallery','tambah_sukses');
							//echo "1";
						}else{
							$this->session->set_flashdata('gallery','gagal');
							//echo "0";
						}
					}else{
						$error = array('error' => $this->upload->display_errors());
						$this->session->set_flashdata('gallery', 'error_upload');
						$this->session->set_flashdata('gallery', $error['error']);
						echo $error['error'];
					}
					redirect("backend/gallery");
				}else{
					$params['text'] = 'tidak ada cover';
					if($this->back_model->tambah_gallery($params)){
						$this->session->set_flashdata('gallery','tambah_sukses');
					}else{
						$this->session->set_flashdata('gallery','gagal');
					}
				}
			}else{
				redirect('backend');
			}
			//redirect('backend/gallery');
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function katg_berita(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Kategori Berita | Disperindag";
			$data['title2'] = "Kategori Berita ";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['row'] = $this->back_model->daftar_katg_berita();
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;
			$this->load->view('backend/back_katg_berita',$data);
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function tambah_katg_berita(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Tambah Kategori Berita | Disperindag";
			$data['title2'] = "Tambah Kategori Berita";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;
			$this->load->view('backend/back_tambah_katg_berita',$data);
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function tambah_katg_berita_ac(){
		if($this->session->userdata('admin_id')){
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;

			if($this->back_model->tambah_katg_berita()){
				//echo 1
				$this->session->set_flashdata('katg_agenda','tambah_sukses');
			}else{
				//echo 0
				$this->session->set_flashdata('katg_agenda','gagal');
			}
			redirect('backend/katg_berita');
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function edit_katg_berita($id_katg){
		if($this->session->userdata('admin_id')){
			if(isset($id_katg)){
				$data['title'] = "Edit Kategori Berita | Disperindag";
				$data['title2'] = "Edit Kategori Berita";
				$data['instansi'] = $this->front_model->data_tr_instansi();
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				foreach($data['data_login']->result() as $row){}
				$data['level'] = $row->level;
				$data['row'] = $this->back_model->daftar_katg_berita_where_id($id_katg);
				if($data['row']->num_rows() > 0){
					$this->load->view('backend/back_edit_katg_berita',$data);
				}else{
					redirect('backend');
				}
			}else{
				redirect('backend');
			}

		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function edit_katg_berita_ac(){
		if($this->session->userdata('admin_id')){
			if(isset($_POST['id_kategori'])){
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				foreach($data['data_login']->result() as $row){}
				$data['level'] = $row->level;

				$data['row'] = $this->back_model->daftar_katg_berita_where_id($_POST['id_kategori']);
				if($data['row']->num_rows() > 0){
					if($this->back_model->edit_katg_berita($_POST['id_kategori'])){
						//echo 1
						$this->session->set_flashdata('katg_agenda','edit_sukses');
					}else{
						//echo 0
						$this->session->set_flashdata('katg_agenda','gagal');
					}
					redirect('backend/katg_berita');
				}else{
					redirect('backend');
				}

			}else{
				redirect('backend');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function hapus_katg_berita($id_katg){
		if($this->session->userdata('admin_id')){
			if(isset($id_katg)){
				$data['title'] = "Hapus Kategori Berita | Disperindag";
				$data['title2'] = "Hapus Kategori Berita";
				$data['instansi'] = $this->front_model->data_tr_instansi();
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				foreach($data['data_login']->result() as $row){}
				$data['level'] = $row->level;
				$data['row'] = $this->back_model->daftar_katg_berita_where_id($id_katg);
				if($data['row']->num_rows() > 0){
					$this->load->view('backend/back_hapus_katg_berita',$data);
				}else{
					redirect('backend');
				}
			}else{
				redirect('backend');
			}

		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function hapus_katg_berita_ac(){
		if($this->session->userdata('admin_id')){
			if(isset($_POST['id_kategori'])){
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				foreach($data['data_login']->result() as $row){}
				$data['level'] = $row->level;

				$data['row'] = $this->back_model->daftar_katg_berita_where_id($_POST['id_kategori']);
				if($data['row']->num_rows() > 0){
					if($this->back_model->hapus_katg_berita($_POST['id_kategori'])){
						//echo 1
						$this->session->set_flashdata('katg_agenda','hapus_sukses');
					}else{
						//echo 0
						$this->session->set_flashdata('katg_agenda','gagal');
					}
					redirect('backend/katg_berita');
				}else{
					redirect('backend');
				}

			}else{
				redirect('backend');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function berita(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Berita | Disperindag";
			$data['title2'] = "Berita";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			if($row->level == 'Admin'){
				$data['berita'] = $this->back_model->daftar_berita();
				foreach($data['data_login']->result() as $row){}
				$data['level'] = $row->level;
				$this->load->view('backend/back_berita',$data);
			}else{
				redirect('backend');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function tambah_berita(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Tambah Berita | Disperindag";
			$data['title2'] = "Tambah Berita";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			if($row->level == 'Admin'){
				$data['level'] = $row->level;
				$data['katg_berita'] = $this->back_model->daftar_kategori_berita();
				$this->load->view('backend/back_tambah_berita',$data);
			}else{
				redirect('backend');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function tambah_berita_ac(){
		if($this->session->userdata('admin_id')){
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$params['publisher'] = $this->session->userdata('admin_id');
			if($row->level == 'Admin'){
				if(isset($_FILES['filename'])){
					$config['upload_path'] = './upload/berita/';
					$config['allowed_types'] = 'jpg|jpeg|png';
					$config['max_size']	= '4048';
					$config['max_width']  = '22024';
					$config['max_height']  = '22068';

					$this->load->library('upload', $config);

					if ( $this->upload->do_upload('filename')) {
						$data = array('upload_sukses' => $this->upload->data());

						$params['text'] = $data['upload_sukses']['file_name'];
						if($this->back_model->tambah_berita($params)){
							$this->session->set_flashdata('berita','tambah_sukses');
							//echo "1";
						}else{
							$this->session->set_flashdata('berita','gagal');
							//echo "0";
						}
					}else{
						$error = array('error' => $this->upload->display_errors());
						$this->session->set_flashdata('berita', 'error_upload');
						$this->session->set_flashdata('berita', $error['error']);
						echo $error['error'];
					}
					redirect("backend/berita");
				}else{
					$params['text'] = 'tidak ada cover';
					if($this->back_model->tambah_berita($params)){
						$this->session->set_flashdata('berita','tambah_sukses');
					}else{
						$this->session->set_flashdata('berita','gagal');
					}
				}
			}else{
				redirect('backend');
			}
			$data['level'] = $row->level;
			$params['publisher'] = $row->admin_id;


			redirect('backend/berita');
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function edit_berita($id_berita){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Edit Berita | Disperindag";
			$data['title2'] = "Edit Berita";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['berita'] = $this->back_model->daftar_berita_where_id_berita($id_berita);
			if($data['berita']->num_rows() > 0){
				foreach($data['data_login']->result() as $row){}
				if($row->level == 'Admin'){
					$data['level'] = $row->level;
					$data['katg_berita'] = $this->back_model->daftar_kategori_berita();
					$this->load->view('backend/back_edit_berita',$data);
				}else{
					redirect('backend');
				}
			}else{
				redirect('backend');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function edit_berita_ac(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Edit Berita | Disperindag";
			$data['title2'] = "Edit Berita";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['berita'] = $this->back_model->daftar_berita_where_id_berita($this->input->post('id_berita'));
			if($data['berita']->num_rows() > 0){
				foreach($data['berita']->result() as $res){}
				foreach($data['data_login']->result() as $row){}
				$params['publisher'] = $this->session->userdata('admin_id');
				if($row->level == 'Admin'){
					if(isset($_FILES['filename'])){
						$config['upload_path'] = './upload/berita/';
						$config['allowed_types'] = 'jpg|jpeg|png';
						$config['max_size']	= '4048';
						$config['max_width']  = '22024';
						$config['max_height']  = '22068';

						$this->load->library('upload', $config);

						if ( $this->upload->do_upload('filename')) {
							$data = array('upload_sukses' => $this->upload->data());
							$params['text'] = $data['upload_sukses']['file_name'];
							if($this->back_model->edit_berita($params)){
								unlink("upload/berita/".$res->cover_berita);
								$this->session->set_flashdata('berita','edit_sukses');
								//echo "1";
							}else{
								$this->session->set_flashdata('berita','gagal');
								//echo "0";
							}
						}else{
							$error = array('error' => $this->upload->display_errors());
							foreach($error as $a){}
							if($a == '<p>You did not select a file to upload.</p>'){
								$params['text'] = $res->cover_berita;
								if($this->back_model->edit_berita($params)){
									$this->session->set_flashdata('berita','edit_sukses');
								}else{
									$this->session->set_flashdata('berita','gagal');
								}
							}else{
								$this->session->set_flashdata('berita', 'error_upload');
								$this->session->set_flashdata('berita_error', $error['error']);
								echo $error['error'];
							}
						}
					}else{
						$params['text'] = $res->cover_berita;
						if($this->back_model->tambah_berita($params)){
							$this->session->set_flashdata('berita','edit_sukses');
						}else{
							$this->session->set_flashdata('berita','gagal');
						}
					}
					redirect('backend/berita');
				}else{
					redirect('backend');
				}
			}else{
				redirect('backend');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function hapus_berita_ac(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Edit Berita | Disperindag";
			$data['title2'] = "Edit Berita";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['berita'] = $this->back_model->daftar_berita_where_id_berita($this->input->post('id_berita'));
			if($data['berita']->num_rows() > 0){
				foreach($data['berita']->result() as $res){}
				foreach($data['data_login']->result() as $row){}
				$params['publisher'] = $this->session->userdata('admin_id');
				if($row->level == 'Admin'){
					if($this->back_model->hapus_berita($params)){
						if($res->cover_berita != ''){
							unlink("upload/berita/".$res->cover_berita);
						}
						$this->session->set_flashdata('berita','hapus_sukses');
						//echo "1";
					}else{
						$this->session->set_flashdata('berita','gagal');
						//echo "0";
					}
				}else{
					redirect('backend');
				}
			}
			redirect('backend/berita');
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function hapus_berita($id_berita){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Edit Berita | Disperindag";
			$data['title2'] = "Edit Berita";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['berita'] = $this->back_model->daftar_berita_where_id_berita($id_berita);
			if($data['berita']->num_rows() > 0){
				foreach($data['data_login']->result() as $row){}
				if($row->level == 'Admin'){
					$data['level'] = $row->level;
					$data['katg_berita'] = $this->back_model->daftar_kategori_berita();
					$this->load->view('backend/back_hapus_berita',$data);
				}else{
					redirect('backend');
				}
			}else{
				redirect('backend');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function katg_agenda(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Kategori Agenda| Disperindag";
			$data['title2'] = "Kategori Agenda";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['row'] = $this->back_model->daftar_katg_agenda();
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;
			$this->load->view('backend/back_katg_agenda',$data);
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function hapus_katg_agenda($id_katg){
		if($this->session->userdata('admin_id')){
			if(isset($id_katg)){
				$data['title'] = "Hapus Kategori Agenda | Disperindag";
				$data['title2'] = "Hapus Kategori Agenda";
				$data['instansi'] = $this->front_model->data_tr_instansi();
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				foreach($data['data_login']->result() as $row){}
				$data['level'] = $row->level;
				$data['row'] = $this->back_model->daftar_katg_agenda_where_id($id_katg);
				if($data['row']->num_rows() > 0){
					$this->load->view('backend/back_hapus_katg_agenda',$data);
				}else{
					redirect('backend');
				}
			}else{
				redirect('backend');
			}

		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function hapus_katg_agenda_ac(){
		if($this->session->userdata('admin_id')){
			if(isset($_POST['id_kategori'])){
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				foreach($data['data_login']->result() as $row){}
				$data['level'] = $row->level;

				$data['row'] = $this->back_model->daftar_katg_agenda_where_id($_POST['id_kategori']);
				if($data['row']->num_rows() > 0){
					if($this->back_model->hapus_katg_agenda($_POST['id_kategori'])){
						//echo 1
						$this->session->set_flashdata('katg_agenda','hapus_sukses');
					}else{
						//echo 0
						$this->session->set_flashdata('katg_agenda','gagal');
					}
					redirect('backend/katg_agenda');
				}else{
					redirect('backend');
				}

			}else{
				redirect('backend');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function edit_katg_agenda($id_katg){
		if($this->session->userdata('admin_id')){
			if(isset($id_katg)){
				$data['title'] = "Edit Kategori Agenda | Disperindag";
				$data['title2'] = "Edit Kategori Agenda";
				$data['instansi'] = $this->front_model->data_tr_instansi();
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				foreach($data['data_login']->result() as $row){}
				$data['level'] = $row->level;
				$data['row'] = $this->back_model->daftar_katg_agenda_where_id($id_katg);
				if($data['row']->num_rows() > 0){
					$this->load->view('backend/back_edit_katg_agenda',$data);
				}else{
					redirect('backend');
				}
			}else{
				redirect('backend');
			}

		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function edit_katg_agenda_ac(){
		if($this->session->userdata('admin_id')){
			if(isset($_POST['id_kategori'])){
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				foreach($data['data_login']->result() as $row){}
				$data['level'] = $row->level;

				$data['row'] = $this->back_model->daftar_katg_agenda_where_id($_POST['id_kategori']);
				if($data['row']->num_rows() > 0){
					if($this->back_model->edit_katg_agenda($_POST['id_kategori'])){
						//echo 1
						$this->session->set_flashdata('katg_agenda','edit_sukses');
					}else{
						//echo 0
						$this->session->set_flashdata('katg_agenda','gagal');
					}
					redirect('backend/katg_agenda');
				}else{
					redirect('backend');
				}

			}else{
				redirect('backend');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function tambah_katg_agenda(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Tambah Kategori Agenda | Disperindag";
			$data['title2'] = "Tambah Kategori Agenda";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['row'] = $this->back_model->daftar_katg_agenda();
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;
			$this->load->view('backend/back_tambah_katg_agenda',$data);
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function tambah_katg_agenda_ac(){
		if($this->session->userdata('admin_id')){
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$data['level'] = $row->level;

			if($this->back_model->tambah_katg_agenda()){
				//echo 1
				$this->session->set_flashdata('katg_agenda','tambah_sukses');
			}else{
				//echo 0
				$this->session->set_flashdata('katg_agenda','gagal');
			}
			redirect('backend/katg_agenda');
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function agenda(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Agenda | Disperindag";
			$data['title2'] = "Agenda";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['agenda'] = $this->back_model->daftar_agenda();
			foreach($data['data_login']->result() as $row){}
			if($row->level == 'Admin'){
				$data['level'] = $row->level;
				$this->load->view('backend/back_agenda',$data);
			}else{
				redirect('backend');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function tambah_agenda(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Tambah Agenda | Disperindag";
			$data['title2'] = "Tambah Agenda";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			if($row->level == 'Admin'){
				$data['level'] = $row->level;
				$data['katg_agenda'] = $this->back_model->daftar_kategori_agenda();
				$this->load->view('backend/back_tambah_agenda',$data);
			}else{
				redirect('backend');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function tambah_agenda_ac(){
		if($this->session->userdata('admin_id')){
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$params['publisher'] = $this->session->userdata('admin_id');
			if($row->level == 'Admin'){
				if(isset($_FILES['filename'])){
					$config['upload_path'] = './upload/agenda/';
					$config['allowed_types'] = 'jpg|jpeg|png';
					$config['max_size']	= '4048';
					$config['max_width']  = '22024';
					$config['max_height']  = '22068';

					$this->load->library('upload', $config);

					if ( $this->upload->do_upload('filename')) {
						$data = array('upload_sukses' => $this->upload->data());

						$params['text'] = $data['upload_sukses']['file_name'];
						if($this->back_model->tambah_agenda($params)){
							$this->session->set_flashdata('agenda','tambah_sukses');
							//echo "1";
						}else{
							$this->session->set_flashdata('agenda','gagal');
							//echo "0";
						}
					}else{
						$error = array('error' => $this->upload->display_errors());
						$this->session->set_flashdata('agenda', 'error_upload');
						$this->session->set_flashdata('agenda_error', $error['error']);
						echo $error['error'];
					}
				}else{
					$params['text'] = 'tidak ada cover';
					if($this->back_model->tambah_agenda($params)){
						$this->session->set_flashdata('agenda','tambah_sukses');
					}else{
						$this->session->set_flashdata('agenda','gagal');
					}
				}
			}else{
				redirect('backend');
			}
			redirect('backend/agenda');
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function hapus_agenda($id_agenda){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Hapus Agenda | Disperindag";
			$data['title2'] = "Hapus Agenda";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			if($row->level == 'Admin'){
				$data['agenda'] = $this->back_model->daftar_agenda_where_id_agenda($id_agenda);
				if($data['agenda']->num_rows() > 0){
					$data['level'] = $row->level;
					$data['katg_agenda'] = $this->back_model->daftar_kategori_agenda();
					$this->load->view('backend/back_hapus_agenda',$data);
				}else{
					redirect('backend/agenda');
				}
			}else{
				redirect('backend');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function hapus_agenda_ac(){
		if($this->session->userdata('admin_id')){
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$params['publisher'] = $this->session->userdata('admin_id');
			if($row->level == 'Admin'){
				$data['agenda'] = $this->back_model->daftar_agenda_where_id_agenda($this->input->post('id_agenda'));
				if($data['agenda']->num_rows() > 0){
					foreach($data['agenda']->result() as $res){}
					if($this->back_model->hapus_agenda()){
						unlink("upload/agenda/".$res->cover_agenda);
						$this->session->set_flashdata('agenda','hapus_sukses');
						//echo "1";
					}else{
						$this->session->set_flashdata('agenda','gagal');
						//echo "0";
					}
				}else{
					redirect('backend/agenda');
				}
			}else{
				redirect('backend');
			}
			redirect('backend/agenda');
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function edit_agenda($id_agenda){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Edit Agenda | Disperindag";
			$data['title2'] = "Edit Agenda";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			if($row->level == 'Admin'){
				$data['agenda'] = $this->back_model->daftar_agenda_where_id_agenda($id_agenda);
				if($data['agenda']->num_rows() > 0){
					$data['level'] = $row->level;
					$data['katg_agenda'] = $this->back_model->daftar_kategori_agenda();
					$this->load->view('backend/back_edit_agenda',$data);
				}else{
					redirect('backend/agenda');
				}
			}else{
				redirect('backend');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function edit_agenda_ac(){
		if($this->session->userdata('admin_id')){
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			foreach($data['data_login']->result() as $row){}
			$params['publisher'] = $this->session->userdata('admin_id');
			if($row->level == 'Admin'){
				$data['agenda'] = $this->back_model->daftar_agenda_where_id_agenda($this->input->post('id_agenda'));
				if($data['agenda']->num_rows() > 0){
					foreach($data['agenda']->result() as $res){}
					if(isset($_FILES['filename'])){
						$config['upload_path'] = './upload/agenda/';
						$config['allowed_types'] = 'jpg|jpeg|png';
						$config['max_size']	= '4048';
						$config['max_width']  = '22024';
						$config['max_height']  = '22068';

						$this->load->library('upload', $config);

						if ( $this->upload->do_upload('filename')) {
							$data = array('upload_sukses' => $this->upload->data());

							$params['text'] = $data['upload_sukses']['file_name'];
							if($this->back_model->edit_agenda($params)){
								unlink("upload/agenda/".$res->cover_agenda);
								$this->session->set_flashdata('agenda','edit_sukses');
								//echo "1";
							}else{
								$this->session->set_flashdata('agenda','gagal');
								//echo "0";
							}
						}else{
							$error = array('error' => $this->upload->display_errors());
							foreach($error as $a){}
							if($a == '<p>You did not select a file to upload.</p>'){
								$params['text'] = $res->cover_agenda;
								if($this->back_model->edit_agenda($params)){
									$this->session->set_flashdata('agenda','edit_sukses');
								}else{
									$this->session->set_flashdata('agenda','gagal');
								}
							}else{
								$this->session->set_flashdata('agenda', 'error_upload');
								$this->session->set_flashdata('agenda_error', $error['error']);
								echo $error['error'];
							}
						}
					}else{
						$params['text'] = 'tidak ada cover';
						if($this->back_model->edit_agenda($params)){
							$this->session->set_flashdata('agenda','edit_sukses');
						}else{
							$this->session->set_flashdata('agenda','gagal');
						}
					}

				}else{
					redirect('backend/agenda');
				}
			}else{
				redirect('backend');
			}
			redirect('backend/agenda');
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function kategori_berita(){
		if($this->session->userdata('admin_id')){
			$data['title'] = "Kategori Berita | Disperindag";
			$data['title2'] = "Kategori Berita";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			if($row->level == 'admin'){
				$data['berita'] = $this->back_model->daftar_kategori_berita();
				foreach($data['data_login']->result() as $row){}
				$data['level'] = $row->level;
				$this->load->view('backend/back_berita',$data);
			}else{
				redirect('backend');
			}
		}
		else{
			$data['title'] = "Login Page | Disperindag";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('backend/back_login',$data);
		}
	}
	function b_pokok_where_id_pasar(){
		if($this->session->userdata('admin_id')){
			$data['row'] = $this->back_model->daftar_b_pokok_id_pasar($this->input->post('id_pasar'));
			if($data['row']->num_rows() > 0){
				echo "<option value=''>Pilih Bahan Pokok</option>";
				foreach($this->back_model->daftar_b_pokok_id_pasar($this->input->post('id_pasar'))->result() as $row){
					echo "<option value='$row->id_bahan_pok'>$row->nama_bahan_pokok</option>";
				}
			}else{
				echo "0";
			}
		}
		else{
			redirect('front/login');
		}
	}
	function sub_b_pokok_where_id_bahan_pokok(){
		if($this->session->userdata('admin_id')){
			$data['row'] = $this->back_model->daftar_sub_b_pokok($this->input->post('id_bahan_pokok'));
			if($data['row']->num_rows() > 0){
				echo "<option value=''>Pilih Sub Bahan Pokok</option>";
				foreach($this->back_model->daftar_sub_b_pokok($this->input->post('id_bahan_pokok'))->result() as $row){
					echo "<option value='$row->id_bahan_pok_sub'>$row->nama_sub_bahan_pokok</option>";
				}
			}else{
				echo "0";
			}
		}
		else{
			redirect('front/login');
		}
	}
	function setting_web(){
		if($this->session->userdata('admin_id')){
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = "Pengaturan | Aplikasi";
			$data['title2'] = "Pengaturan | Aplikasi";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			foreach($data['data_login']->result() as $row){}
			if(($row->level == "Admin") or ($row->level == "Super Admin")){
				$this->load->view('backend/back_setting_web',$data);
			}else{
				redirect('backend');
			}

		}
		else{
			redirect('front/login');
		}
	}
	function setting_web_ac(){
		if($this->session->userdata('admin_id')){
			$config['upload_path'] = './upload';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['max_size']	= '6012312481248';
			$config['max_width']  = '211241242024';
			$config['max_height']  = '171212412468';

			$this->load->library('upload', $config);

			if ( $this->upload->do_upload('logo')) {
				$data = array('upload_sukses' => $this->upload->data());
				$nama_file = $data['upload_sukses']['file_name'];
				if($this->back_model->update_setting_web($nama_file)){
					unlink("upload/".$this->input->post('logo_lama'));
					$this->session->set_flashdata('setting_web','sukses');
				}else{
					$this->session->set_flashdata('setting_web','gagal');
				}

			}else{
				$error = array('error' => $this->upload->display_errors());
				foreach($error as $a){}
				if($a = '<p>You did not select a file to upload.</p>'){
					$nama_file = 'unknown';
					if($this->back_model->update_setting_web($this->input->post('logo_lama'))){
						$this->session->set_flashdata('setting_web','sukses');
					}else{
						$this->session->set_flashdata('setting_web','gagal');
					}
				}else{
					$this->session->set_flashdata('setting_web','gagal');
				}
			}
			redirect('backend/setting_web');
		}
		else{
			redirect('front/login');
		}
	}
	function login(){
		if($this->session->userdata('admin_id')){
			redirect("front");
		}
		else{
			$user = $this->input->post('username');
			$pass = $this->input->post('password');
			if($this->back_model->data_login($user,$pass)->num_rows() > 0){
				foreach($this->back_model->data_login($user,$pass)->result() as $row){
					$admin_id = $row->admin_id;
				}
				$this->session->set_userdata('admin_id',$admin_id);
				echo "1";
			}else{
				echo "0";
			}
		}
	}
	function pengaturan_password(){
		if($this->session->userdata('admin_id')){
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['title'] = "Pengaturan Password | Disperindag";
			$data['title2'] = "Pengaturan Password";
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			if($data['data_login']->num_rows() > 0){
				foreach($data['data_login']->result() as $row){}
				$data['level'] = $row->level;
				$this->load->view('backend/back_pengaturan_password',$data);
			}else{
				redirect('front/login');
			}
		}
		else{
			redirect('front/login');
		}
	}
	function pengaturan_password_ac(){
		if($this->session->userdata('admin_id')){
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));

			$data['data_login2'] = $this->front_model->data_login($this->input->post('id_admin'));
			if($data['data_login2']->num_rows() > 0){
				$data['cek_password'] = $this->back_model->cek_password($this->input->post('old_password'));
				if($data['cek_password']->num_rows() > 0){
					if($this->input->post('new_password') == $this->input->post('re_new_password')){
						if($this->back_model->edit_pass2()){
							echo 1;
						}else{
							echo 0;
						}
					}else{
						echo 3; //ketik ulang pasword baru tidak sama
					}
				}else{
					echo 2; //password lama salah,
				}
			}else{
				redirect('front/login');
			}
		}
		else{
			redirect('front/login');
		}
	}
	function tdp(){
		if ($this->session->userdata('admin_id')) {
			$data['kecamatan'] = $this->back_model->mst_kecamatan();
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'TDP | Disperindag';
			$data['izin_usaha'] = $this->back_model->klasifikasi_iu();


			$data['siup'] = $this->back_model->data_siup();
			$data['tdp'] = $this->back_model->data_tdp();
			$data['iupr'] = $this->back_model->data_iupr();
			$data['iupp'] = $this->back_model->data_iupp();
			$data['iuts'] = $this->back_model->data_iuts();
			$data['stpw'] = $this->back_model->data_stpw();
			$data['tdg'] = $this->back_model->data_tdg();
			$data['siupmb'] = $this->back_model->data_siupmb();
			$data['tdpud'] = $this->back_model->data_tdpud();
			$data['b3'] = $this->back_model->data_b3();

			$this->load->view('backend/back_tdp',$data);
		} else {
			redirect('backend');
		}

	}
	function tambah_tdp(){
		if ($this->session->userdata('admin_id')) {
			$data['kecamatan'] = $this->back_model->mst_kecamatan();
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'TDP | Disperindag';
			$data['izin_usaha'] = $this->back_model->klasifikasi_iu();

			$this->load->view('backend/back_tambah_tdp',$data);
		} else {
			redirect('backend');
		}

	}
	function tambah_perizinan(){
		if ($this->session->userdata('admin_id')) {
			$config['upload_path'] = './upload';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['max_size']	= '6012312481248';
			$config['max_width']  = '211241242024';
			$config['max_height']  = '171212412468';

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('userfile'))
            {
                $data_form = array(
					'id'=>'',
					'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
					'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
					'izin_usaha'=>$this->input->post('izin_usaha'),
					'siup'=>$this->input->post('siup'),
					'no_sk'=>$this->input->post('no_sk'),
					'tgl_sk'=>$this->input->post('tgl_sk'),
					'npwp'=>$this->input->post('npwp'),
					'kbli'=>$this->input->post('kbli'),
					'gol_usaha'=>$this->input->post('gol_usaha'),
					'kelembagaan'=>$this->input->post('kelembagaan'),
					'brg_jasa_dag_utama'=>$this->input->post('brg_jasa_dag_utama'),
					'status_kantor'=>$this->input->post('status_kantor'),
					'masaberlaku'=>$this->input->post('masaberlaku'),
					'stok'=>$this->input->post('stok'),
					'tgl_input_stok'=>date('Ymd'),
					'foto'=>'-',
					'latitude'=>$this->input->post('latitude'),
					'longitude'=>$this->input->post('longitude')
				);
				$this->session->set_flashdata('tdp','tambah_sukses');
				$this->back_model->tambah_tdp($data_form);

                redirect('backend/tdp');
            }
            else
            {
            	$data = array('upload_data' => $this->upload->data());

				$file = array('upload_sukses' => $this->upload->data());
				$nama_file = $file['upload_sukses']['file_name'];

				$data_form = array(
					'id'=>'',
					'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
					'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
					'izin_usaha'=>$this->input->post('izin_usaha'),
					'siup'=>$this->input->post('siup'),
					'no_sk'=>$this->input->post('no_sk'),
					'tgl_sk'=>$this->input->post('tgl_sk'),
					'npwp'=>$this->input->post('npwp'),
					'kbli'=>$this->input->post('kbli'),
					'gol_usaha'=>$this->input->post('gol_usaha'),
					'kelembagaan'=>$this->input->post('kelembagaan'),
					'brg_jasa_dag_utama'=>$this->input->post('brg_jasa_dag_utama'),
					'status_kantor'=>$this->input->post('status_kantor'),
					'masaberlaku'=>$this->input->post('masaberlaku'),
					'stok'=>$this->input->post('stok'),
					'tgl_input_stok'=>date('Ymd'),
					'foto'=>$nama_file,
					'latitude'=>$this->input->post('latitude'),
					'longitude'=>$this->input->post('longitude')
				);
				$this->session->set_flashdata('tdp','tambah_sukses');
				$this->back_model->tambah_tdp($data_form);

                redirect('backend/tdp');
            }
		} else {
			redirect('backend');
		}

	}

	function logout(){
		$this->session->unset_userdata('config_search');
		$this->session->unset_userdata('admin_id');
		redirect("front");
	}
	function edit_tdp_ac(){
		if ($this->session->userdata('admin_id')) {

			$config['upload_path'] = './upload';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['max_size']	= '6012312481248';
			$config['max_width']  = '211241242024';
			$config['max_height']  = '171212412468';

			$this->load->library('upload', $config);

			if ( $this->upload->do_upload('userfile')) {
				unlink("upload/".$this->input->post('foto_lama'));
				$file = array('upload_sukses' => $this->upload->data());
				$nama_file = $file['upload_sukses']['file_name'];

				$id = $this->input->post('id');
				$data = array(
						'id'=>'',
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'izin_usaha'=>$this->input->post('izin_usaha'),
						'siup'=>$this->input->post('siup'),
						'no_sk'=>$this->input->post('no_sk'),
						'tgl_sk'=>$this->input->post('tgl_sk'),
						'npwp'=>$this->input->post('npwp'),
						'kbli'=>$this->input->post('kbli'),
						'gol_usaha'=>$this->input->post('gol_usaha'),
						'kelembagaan'=>$this->input->post('kelembagaan'),
						'brg_jasa_dag_utama'=>$this->input->post('brg_jasa_dag_utama'),
						'status_kantor'=>$this->input->post('status_kantor'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'stok'=>$this->input->post('stok'),
						'foto'=>$nama_file,
						'latitude'=>$this->input->post('latitude'),
						'longitude'=>$this->input->post('longitude')
					);
				$this->back_model->edit_tdp($id,$data);
				$this->session->set_flashdata('tdp','edit_sukses');
				redirect('backend/tdp');

			}else{
				$error = array('error' => $this->upload->display_errors());
				foreach($error as $a){}
				if($a = '<p>You did not select a file to upload.</p>'){
					$nama_file = $this->input->post('foto_lama');

					$id = $this->input->post('id');
					$data = array(
							'id'=>'',
							'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
							'kecamatan'=>$this->input->post('kecamatan'),
							'kelurahan'=>$this->input->post('kelurahan'),
							'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
							'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
							'izin_usaha'=>$this->input->post('izin_usaha'),
							'siup'=>$this->input->post('siup'),
							'no_sk'=>$this->input->post('no_sk'),
							'tgl_sk'=>$this->input->post('tgl_sk'),
							'npwp'=>$this->input->post('npwp'),
							'kbli'=>$this->input->post('kbli'),
							'gol_usaha'=>$this->input->post('gol_usaha'),
							'kelembagaan'=>$this->input->post('kelembagaan'),
							'brg_jasa_dag_utama'=>$this->input->post('brg_jasa_dag_utama'),
							'status_kantor'=>$this->input->post('status_kantor'),
							'masaberlaku'=>$this->input->post('masaberlaku'),
							'stok'=>$this->input->post('stok'),
							'foto'=>$nama_file,
							'latitude'=>$this->input->post('latitude'),
							'longitude'=>$this->input->post('longitude')
						);
					$this->back_model->edit_tdp($id,$data);
					$this->session->set_flashdata('tdp','edit_sukses');
					redirect('backend/tdp');
				}else{
					$this->session->set_flashdata('tdp','input_gagal');
					redirect('backend/tdp');
				}
			}
		} else {
			redirect('backend');
		}

	}
	function hapus_tdp($id){
		if ($this->session->userdata('admin_id')) {
			$foto = $this->back_model->data_tdp_byid($id);
			foreach ($foto->result() as $foto) {}
			unlink("upload/".$foto->foto);
			$this->back_model->delete_tdp($id);
			$this->session->set_flashdata('tdp','hapus_sukses');
			redirect('backend/tdp');
		} else {
			redirect('backend');
		}

	}

	public function tdg()
		{
			if ($this->session->userdata('admin_id')) {
				$data['kecamatan'] = $this->back_model->mst_kecamatan();
				$data['gudang'] = $this->back_model->mst_gudang();
				$data['instansi'] = $this->front_model->data_tr_instansi();
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				$data['title'] = 'TDG | Disperindag';

				$data['tdg'] = $this->back_model->data_tdg();
				$this->load->view('backend/back_tdg',$data);
			} else {
				redirect('backend');
			}

		}

	public function tambah_tdg()
	{
		if ($this->session->userdata('admin_id')) {
			$data['kecamatan'] = $this->back_model->mst_kecamatan();
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'TDG | Disperindag';
			$data['gudang'] = $this->back_model->mst_gudang();

			$this->load->view('backend/back_tambah_tdg',$data);
		} else {
			redirect('backend');
		}

	}
	function tambah_tdg_ac(){
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data = array(
					'id'=>'',
					'pemilik'=>$this->input->post('pemilik'),
					'gudang'=>$this->input->post('gudang'),
					'alamat'=>$this->input->post('alamat'),
					'provinsi'=>$this->input->post('provinsi'),
					'kota'=>$this->input->post('kota'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'stock'=>$this->input->post('stock'),
					'tgl_input'=>date('Ymd'),
					'latitude'=>$this->input->post('latitude'),
					'longitude'=>$this->input->post('longitude')
				);
			$this->session->set_flashdata('tdp','tambah_sukses');
			$this->back_model->tambah_tdg($data);
			redirect('backend/tdg');
		} else {
			redirect('backend');
		}

	}
	function edit_tdg($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['kecamatan'] = $this->back_model->mst_kecamatan();
			$data['tdg'] = $this->back_model->data_tdg_byid($id);
			$data['title'] = 'Edit Data Gudang | Disperindag';
			$this->load->view('backend/back_edit_tdg',$data);
		} else {
			redirect('backend');
		}

	}
	function edit_tdg_ac(){
		if ($this->session->userdata('admin_id')) {
			$id = $this->input->post('id');
			$data = array(
					'pemilik'=>$this->input->post('pemilik'),
					'gudang'=>$this->input->post('gudang'),
					'alamat'=>$this->input->post('alamat'),
					'provinsi'=>$this->input->post('provinsi'),
					'kota'=>$this->input->post('kota'),
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'stock'=>$this->input->post('stock'),
					'tgl_input'=>date('Ymd'),
					'latitude'=>$this->input->post('latitude'),
					'longitude'=>$this->input->post('longitude')
				);
			$this->back_model->edit_tdg($id,$data);
			$this->session->set_flashdata('tdp','edit_sukses');
			redirect('backend/tdg');
		} else {
			redirect('backend');
		}

	}

	function hapus_tdg($id){
		if ($this->session->userdata('admin_id')) {
			$this->back_model->delete_tdg($id);
			$this->session->set_flashdata('tdp','hapus_sukses');
			redirect('backend/tdg');
		} else {
			redirect('backend');
		}

	}

	public function mst_gudang()
	{
		if ($this->session->userdata('admin_id')) {
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'TDG | Disperindag';
			$data['mst_gudang'] = $this->back_model->mst_gudang();

			$this->load->view('backend/back_mst_gudang',$data);
		} else {
			redirect('backend');
		}

	}public function tambah_mst_gudang()
	{
		if ($this->session->userdata('admin_id')) {
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'TDG | Disperindag';

			$this->load->view('backend/back_tambah_mst_gudang',$data);
		} else {
			redirect('backend');
		}

	}
	function tambah_mst_gudang_ac()
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data = array(
					'kd_gudang'=>'',
					'nm_gudang'=>$this->input->post('nm_gudang')
				);
			$this->session->set_flashdata('tdp','tambah_sukses');
			$this->back_model->tambah_mst_gudang($data);
			redirect('backend/mst_gudang');
		} else {
			redirect('backend');
		}

	}

	function edit_mst_gudang($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['mst_gudang'] = $this->back_model->data_mst_gudang_byid($id);
			$data['title'] = 'Edit Data Gudang | Disperindag';
			$this->load->view('backend/back_edit_mst_gudang',$data);
		} else {
			redirect('backend');
		}

	}
	function edit_mst_gudang_ac(){
		if ($this->session->userdata('admin_id')) {
			$kd_gudang = $this->input->post('kd_gudang');
			$data = array('nm_gudang'=>$this->input->post('nm_gudang'));
			$this->back_model->edit_mst_gudang($kd_gudang,$data);
			$this->session->set_flashdata('tdp','edit_sukses');
			redirect('backend/mst_gudang');
		} else {
			redirect('backend');
		}

	}

	function hapus_mst_gudang($kd_gudang){
		if ($this->session->userdata('admin_id')) {
			$this->back_model->delete_mst_gudang($kd_gudang);
			$this->session->set_flashdata('tdp','hapus_sukses');
			redirect('backend/mst_gudang');
		} else {
			redirect('backend');
		}

	}

	public function filter_perizinan()
	{
		if ($this->session->userdata('admin_id')) {

			if(isset($_POST['btnEXCEL']))
			{
				$object = new PHPExcel();
				$object->setActiveSheetIndex(0);
				$data['table'] = $table = $this->input->post('izin_usaha');
				$kecamatan = $this->input->post('kecamatan');
				$kelurahan = $this->input->post('kelurahan');

				if ($table == 't_siup') {
					$table_columns = array("Nama Perusahaan", "Nama Penanggung Jawab", "No. Telp & Fax", "Modal Usaha", "Kelembagaan", "KBLI", "No. SIUP & Tanggal SIUP", "Masa Berlaku");

					$judul = "Data SIUP";
				} else if($table == 't_tdp'){
					$table_columns = array("Nama Perusahaan", "Nama Penanggung Jawab", "Alamat Perusahaan", "NPWP", "No. Telp & Fax", "Kegiatan Usaha Pokok", "KBLI", "Status Kantor", "No. TDP & Tanggal TDP", "Masa Berlaku");
					$judul = "Data TDP";
				} else if($table == 't_iupr'){
					$table_columns = array("Tanggal Pendaftaran", "Nama Perusahaan", "Nama Penanggung Jawab", "Alamat Perusahaan", "Jenis Usaha", "SIUP", "TDP", "No. IUPR & Tanggal IUPR");
					$judul = "Data IUPR";
				} else if($table == 't_iupp'){
					$table_columns = array("Tanggal Pendaftaran", "Nama Perusahaan", "Nama Penanggung Jawab", "Alamat Perusahaan", "Jenis Usaha", "SIUP", "TDP", "No. IUPP & Tanggal IUPP");
					$judul = "Data IUPP";
				} else if($table == 't_iuts'){
					$table_columns = array("Nama Perusahaan Pemberi", "Nama Penanggung Jawab", "Alamat Perusahaan", "NPWP", "No. Telp & Fax", "Kegiatan Usaha Pokok", "KBLI", "Status Kantor", "SIUP", "TDP", "No. IUTS & Tanggal IUTS", "Masa Berlaku");
					$judul = "Data IUTS";
				} else if($table == 't_stpw'){
					$table_columns = array("No. Pendaftaran", "Nama Perusahaan Penerima", "Nama Penanggung Jawab", "Alamat Perusahaan", "NPWP", "IUTS", "No. STPW & Tanggal STPW", "No. Telp", "Masa Berlaku");
					$judul = "Data STPW";
				} else if($table == 't_tdg'){
					$table_columns = array("Nama Perusahaan", "Nama Penanggung Jawab", "Alamat Perusahaan", "Alamat Penanggung Jawab", "Lokasi Gudang", "Luas Gudang", "Jenis Isi Gudang", "Klasifikasi Gudang", "No. TDG", "TDP", "Status", "Masa Berlaku");
					$judul = "Data TDG";
				} else if($table == 't_siupmb'){
					$table_columns = array("Nama Perusahaan", "Alamat Perusahaan", "SIUP-MB", "TDP", "Tanggal Keluar", "Tanggal Berakhir", "Instansi Yang Mengeluarkan", "Keterangan");
					$judul = "Data SIUP-MB";
				} else if($table == 't_tdpud'){
					$table_columns = array("Nama Perusahaan", "Nama Penanggung Jawab", "Alamat Perusahaan", "NPWP", "No. Telp & Fax", "Kegiatan Usaha Pokok", "KBLI", "Status Kantor", "TDP", "No. TDPUD & Tanggal TDPUD", "Masa Berlaku");
					$judul = "Data TDPUD";
				} else{
					$table_columns = array("Nama Perusahaan", "B3", "Nama Penanggung Jawab", "Alamat Perusahaan", "No. SK", "Tanggal SK", "NPWP", "KBLI", "Gol. Usaha", "Kelembagaan", "Barang Jasa Dagangan Utama", "Status Kantor", "Masa Berlaku", "Stok", "", "", "");
					$judul = "Data B3";
				}

				$column = 0;

				$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $judul);

				foreach($table_columns as $field)
				{
					$object->getActiveSheet()->setCellValueByColumnAndRow($column, 3, $field);
					$column++;
				}


				if ($kecamatan !== "-" && $kelurahan !== "-") {
					$where = array('kecamatan'=>$kecamatan,'kelurahan'=>$kelurahan);
					$data_perizinan = $this->back_model->filter_perizinan($where,$table);
				} else if($kecamatan !== "-" && $kelurahan == "-"){
					$where = array('kecamatan'=>$kecamatan);
					$data_perizinan = $this->back_model->filter_perizinan($where,$table);
				} else {
					$data_perizinan = $this->back_model->filter_perizinan2($table);
				}

				$excel_row = 4;

				if ($table == 't_siup') {
					foreach($data_perizinan->result() as $row)
					{
						$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->nm_perusahaan);
						$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->nm_penanggungjwb);
						$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->no_telp."&".$row->no_fax);
						$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->modal_usaha);
						$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->kelembagaan);
						$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->kbli);
						$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->no_siup."&".$row->tgl_siup);
						$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->masaberlaku);
						$excel_row++;
					}

				} else if($table == 't_tdp'){
					foreach($data_perizinan->result() as $row)
					{
						$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->nm_perusahaan);
						$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->nm_penanggungjwb);
						$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->almt_perusahaan);
						$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->npwp);
						$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->no_telp."&".$row->no_fax);
						$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->no_telp."&".$row->keg_usaha_pokok);
						$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->kbli);
						$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->status_kantor);
						$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->no_tdp."&".$row->tgl_tdp);
						$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->masaberlaku);
						$excel_row++;
					}
				} else if($table == 't_iupr'){
					foreach($data_perizinan->result() as $row)
					{
						$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->tgl_pendaftaran);
						$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->nm_perusahaan);
						$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->nm_penanggungjwb);
						$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->almt_perusahaan);
						$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->jenis_usaha);
						$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->siup);
						$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->tdp);
						$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->no_iupr."&".$row->tgl_iupr);
						$excel_row++;
					}
				} else if($table == 't_iupp'){
					foreach($data_perizinan->result() as $row)
					{
						$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->tgl_pendaftaran);
						$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->nm_perusahaan);
						$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->nm_penanggungjwb);
						$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->almt_perusahaan);
						$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->jenis_usaha);
						$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->siup);
						$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->tdp);
						$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->no_iupp."&".$row->tgl_iupp);
						$excel_row++;
					}
				} else if($table == 't_iuts'){
					foreach($data_perizinan->result() as $row)
					{
						$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->nm_perusahaan);
						$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->nm_penanggungjwb);
						$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->almt_perusahaan);
						$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->npwp);
						$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->no_telp."&".$row->no_fax);
						$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->keg_usaha_pokok);
						$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->kbli);
						$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->status_kantor);
						$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->siup);
						$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->tdp);
						$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->no_iuts."&".$row->tgl_iuts);
						$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row->masaberlaku);
						$excel_row++;
					}
				} else if($table == 't_stpw'){
					foreach($data_perizinan->result() as $row)
					{
						$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->no_pendaftaran);
						$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->nm_perusahaan);
						$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->nm_penanggungjwb);
						$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->almt_perusahaan);
						$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->npwp);
						$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->iuts);
						$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->no_stpw."&".$row->tgl_stpw);
						$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->no_telp);
						$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->masaberlaku);
						$excel_row++;
					}
				} else if($table == 't_tdg'){
					foreach($data_perizinan->result() as $row)
					{
						$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->nm_perusahaan);
						$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->nm_penanggungjwb);
						$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->almt_perusahaan);
						$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->almt_penanggungjwb);
						$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->lokasi_gudang);
						$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->luas_gudang);
						$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->jenis_isi_gudang);
						$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->klasifikasi_gudang);
						$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->no_tdg);
						$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->tdp);
						$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->status);
						$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row->masaberlaku);
						$excel_row++;
					}
				} else if($table == 't_siupmb'){
					foreach($data_perizinan->result() as $row)
					{
						$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->nm_perusahaan);
						$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->almt_perusahaan);
						$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->siupmb);
						$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->tdp);
						$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->tgl_keluar);
						$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->tgl_berakhir);
						$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->ins_yg_mengeluarkan);
						$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->keterangan);
						$excel_row++;
					}
				} else if($table == 't_tdpud'){
					foreach($data_perizinan->result() as $row)
					{
						$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->nm_perusahaan);
						$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->nm_penanggungjwb);
						$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->almt_perusahaan);
						$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->npwp);
						$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->no_telp."&".$row->no_fax);
						$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->keg_usaha_pokok);
						$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->kbli);
						$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->status_kantor);
						$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->tdp);
						$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->no_tdpud."&".$row->tgl_tdpud);
						$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->masaberlaku);
						$excel_row++;
					}
				} else{
					# code...
					#B3
				}


				$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename="Data Perizinan.xls"');
				$object_writer->save('php://output');
			}

			else if (isset($_POST['btnPDF']))
			{

				$data['kec'] = $kecamatan = $this->input->post('kecamatan');
				$data['kel'] = $kelurahan = $this->input->post('kelurahan');
				$data['iu']  = $izin_usaha= $this->input->post('izin_usaha');

				$data['table'] = $table = $this->input->post('izin_usaha');

				if ($kecamatan !== "-" && $kelurahan !== "-") {
					$where = array('kecamatan'=>$kecamatan,'kelurahan'=>$kelurahan);
					$data['perizinan'] = $this->back_model->filter_perizinan($where,$table);
				} else if($kecamatan !== "-" && $kelurahan == "-"){
					$where = array('kecamatan'=>$kecamatan);
					$data['perizinan'] = $this->back_model->filter_perizinan($where,$table);
				} else {
					$data['perizinan'] = $this->back_model->filter_perizinan2($table);
				}

				$data['stpw'] = $this->back_model;
				$data['iuts'] = $this->back_model;

				$html = $this->load->view('backend/back_perizinan_pdf',$data,true);

				$pdfFilePath = "TDP.pdf";

				$this->load->library('m_pdf');

				$this->m_pdf->pdf->WriteHTML($html); // write the HTML into the PDF

				$this->m_pdf->pdf->Output($pdfFilePath, "I");

			} else
			{
				echo "Excel";
			}
		} else {
			redirect('backend');
		}

	}

	public function filter_distribusi()
	{
		if ($this->session->userdata('admin_id')) {
			if (isset($_POST['btnPDF'])) {
				$tdpud = $this->input->post('tdpud');
				$id_bapok = $this->input->post('id_bapok');

				$get_jenis_brg = $this->back_model->daftar_b_pokok_byid_bapok($id_bapok);
				foreach ($get_jenis_brg->result() as $bapok) {}

				$data['jenis_barang'] = $bapok->nama_bahan_pokok;
				$data['tgl_max'] = $tgl_max = $this->input->post('tgl_max');
				$data['tgl_min'] = $tgl_min = $this->input->post('tgl_min');
				$data['get'] = $this->back_model->data_dist_pdf($id_bapok,$tdpud,$tgl_min,$tgl_max);
				$data['pengeluaran'] = $this->back_model->data_dist_jml_pengeluaran($id_bapok,$tdpud,$tgl_min,$tgl_max);
				$data['pengadaan'] = $this->back_model->data_dist_jml_pengadaan($id_bapok,$tdpud,$tgl_min,$tgl_max);
				//kalo mau -3 bulan ==> date('Y-m-d', strtotime("-3 month",strtotime($tgl_max)));
				$data['bulan_min'] = $this->bulan($this->input->post('tgl_min'));
				$data['bulan_max'] = $this->bulan($this->input->post('tgl_max'));
				$data['tahun'] = $this->tahun($this->input->post('tgl_min'),$this->input->post('tgl_max'));

				$html = $this->load->view('backend/back_distribusi_pdf',$data,true);

				$pdfFilePath = "Laporan Distribusi.pdf";

				$this->load->library('m_pdf');

				$this->m_pdf->pdf->WriteHTML($html); // write the HTML into the PDF

				$this->m_pdf->pdf->Output($pdfFilePath, "I");

			} else {
				echo "Excel";
			}

		} else {
			redirect(base_url('backend'));
		}

	}

	public function filter_distribusi2()
	{
		if ($this->session->userdata('admin_id')) {

			$data['get'] = $this->back_model->data_dist_pdf_all();
				//kalo mau -3 bulan ==> date('Y-m-d', strtotime("-3 month",strtotime($tgl_max)));
			$data['bulan_min'] = $this->bulan($this->input->post('tgl_min'));
			$data['year'] = date('Y');

			$data['sub_query'] = $this->back_model;

			$html = $this->load->view('backend/back_distribusi_pdf_all',$data,true);

			$pdfFilePath = "Laporan Distribusi.pdf";

			$this->load->library('m_pdf');

				$this->m_pdf->pdf->WriteHTML($html); // write the HTML into the PDF

				$this->m_pdf->pdf->Output($pdfFilePath, "I");
		} else {
			redirect(base_url('backend'));
		}

	}

	public function filt_dist_bpok()
	{
		if ($this->session->userdata('admin_id')) {
			$id_bapok = $this->input->post('id_bapok');

			$get_jenis_brg = $this->back_model->daftar_b_pokok_byid_bapok($id_bapok);
			foreach ($get_jenis_brg->result() as $bapok) {}
			$data['jenis_barang'] = $bapok->nama_bahan_pokok;

			$data['tgl_min'] = $tgl_min = date('Ymd');
			$data['get'] = $this->back_model->data_dist_bpok_pdf($id_bapok);
			//kalo mau -3 bulan ==> date('Y-m-d', strtotime("-3 month",strtotime($tgl_max)));

			//$data['bulan_min'] = $this->bulan($this->input->post('tgl_min'));
			//$data['bulan_max'] = $this->bulan($this->input->post('tgl_max'));
			//$data['tahun'] = $this->tahun($this->input->post('tgl_min'),$this->input->post('tgl_max'));

			$html = $this->load->view('backend/back_dist_bpok_pdf',$data,true);

			$pdfFilePath = "Laporan Distribusi.pdf";

			$this->load->library('m_pdf');

			$this->m_pdf->pdf->WriteHTML($html); // write the HTML into the PDF

			$this->m_pdf->pdf->Output($pdfFilePath, "I");
		} else {
			redirect(base_url('backend'));
		}

	}

	public function tdg_filter()
	{
		if ($this->session->userdata('admin_id')) {
			$data['kecamatan'] = $this->back_model->mst_kecamatan();
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$data['gudang'] = $this->back_model->mst_gudang();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'TDG | Disperindag';
			$data['tdg'] = $this->back_model->filter_tdg($this->input->post('gudang'),$this->input->post('kecamatan'));
			$data['tdg2'] = $this->back_model->filter_tdg($this->input->post('gudang'),$this->input->post('kecamatan'));
			$this->load->view('backend/back_tdg_filter',$data);
		} else {
			redirect('backend');
		}

	}

	public function tambah_klasifikasi()
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Klasifikasi | Disperindag';
			$data['klasifikasi'] = $this->back_model->klasifikasi_iu();
			$this->load->view('backend/back_klasifikasi',$data);
		} else {
			redirect('backend');
		}

	}

	public function tambah_klasifikasi_ac()
	{
		if ($this->session->userdata('admin_id')) {
			$data = array(
					'id'=>'',
					'nm_klasifikasi'=>$this->input->post('nm_klasifikasi'),
					'no_siup'=>$this->input->post('no_siup')
				);
			$this->back_model->input_klasifikasi($data);
			$this->session->set_flashdata('klasifikasi','tambah_sukses');
			redirect('backend/tambah_klasifikasi');
		} else {
			redirect('backend');
		}

	}

	public function tambah_klasifikasi_edit($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Klasifikasi | Disperindag';
			$data['klasifikasi'] = $this->back_model->klasifikasi_iu_byid($id);
			$this->load->view('backend/back_klasifikasi_edit',$data);
		} else {
			redirect('backend');
		}

	}

	public function edit_klasifikasi_ac()
	{
		if ($this->session->userdata('admin_id')) {
			$data = array(
					'nm_klasifikasi'=>$this->input->post('nm_klasifikasi'),
					'no_siup'=>$this->input->post('no_siup')
				);
			$id = $this->input->post('id');
			$this->back_model->edit_klasifikasi($data,$id);
			$this->session->set_flashdata('klasifikasi','edit_sukses');
			redirect('backend/tambah_klasifikasi');
		} else {
			redirect('backend');
		}

	}
	public function hapus_klasifikasi($id)
	{
		if ($this->session->userdata('admin_id')) {
			$this->back_model->delete_klasifikasi($id);
			$this->session->set_flashdata('klasifikasi','hapus_sukses');
			redirect('backend/tambah_klasifikasi');
		} else {
			redirect('backend');
		}

	}

	function setting_sambutan()
	{
		if($this->session->userdata('admin_id'))
		{
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Klasifikasi | Disperindag';
			$data['klasifikasi'] = $this->back_model->klasifikasi_iu();
			$data['sambutan'] = $this->back_model->sambutan();
			$this->load->view('backend/back_setting_sambutan',$data);
		} else {
			redirect(base_url('backend'));
		}
	}

	function setting_sambutan_ac()
	{
		if($this->session->userdata('admin_id'))
		{
			$config['upload_path'] = './upload';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['max_size']	= '6012312481248';
			$config['max_width']  = '211241242024';
			$config['max_height']  = '171212412468';

			$this->load->library('upload', $config);

			if ( $this->upload->do_upload('foto')) {
				$data = array('upload_sukses' => $this->upload->data());
				$nama_file = $data['upload_sukses']['file_name'];

				$data = array(
					'foto'=>$this->input->post('foto'),
					'text'=>$this->input->post('text')
				);
				unlink("upload/".$this->input->post('logo_lama'));
				$this->back_model->update_sambutan($data);
			}else{
				$data = array(
					'foto'=>$this->input->post('foto_lama'),
					'text'=>$this->input->post('text')
				);
				$this->back_model->update_sambutan($data);
			}
			$this->session->set_flashdata('updated','update_sukses');
			redirect('backend/setting_sambutan');
		}else{
			redirect(base_url('backend'));
		}
	}

	function setting_struk_org()
	{
		if($this->session->userdata('admin_id'))
		{
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Klasifikasi | Disperindag';
			$data['klasifikasi'] = $this->back_model->klasifikasi_iu();
			$data['people'] = $this->back_model->get_struk_org();

			$this->load->view('backend/back_setting_org',$data);
		} else {
			redirect(base_url('backend'));
		}
	}

	function setting_struk_org_ac()
	{
		if($this->session->userdata('admin_id'))
		{
			$config['upload_path'] = './upload';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['max_size']	= '6012312481248';
			$config['max_width']  = '211241242024';
			$config['max_height']  = '171212412468';

			$this->load->library('upload', $config);

			if ( $this->upload->do_upload('foto')) {
				$data = array('upload_sukses' => $this->upload->data());
				$nama_file = $data['upload_sukses']['file_name'];

				$data = array(
					'id'=>'',
					'nama'=>$this->input->post('nama'),
					'jabatan'=>$this->input->post('jabatan'),
					'foto'=>$nama_file
				);
				$this->back_model->struk_org($data);
				$this->session->set_flashdata('inserted','Data berhasil disimpan');
				redirect(base_url('backend/setting_struk_org'));
			}else{
				$error = array('error' => $this->upload->display_errors());
				foreach($error as $x){}

				$this->session->set_flashdata('upload_failed',$x);
				redirect(base_url('backend/setting_struk_org'));
			}
		} else {
			redirect(base_url('backend'));
		}
	}

	function edit_struk_org($id)
	{
		if($this->session->userdata('admin_id'))
		{
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Klasifikasi | Disperindag';
			$data['klasifikasi'] = $this->back_model->klasifikasi_iu();
			$data['people'] = $this->back_model->get_anggota_org($id);

			$this->load->view('backend/back_edit_org',$data);
		} else {
			redirect(base_url('backend'));
		}
	}

	public function stok_bahan_pokok()
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Stok Bahan Pokok | Disperindag';

			$data['stok'] = $this->back_model->stok_bapok();
			$this->load->view('backend/back_stok_bapok',$data);
		} else {
			redirect(base_url('backend'));
		}

	}

	public function tambah_stok_bapok()
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Tambah Stok Bahan Pokok | Disperindag';
			$data['bhn_pokok'] = $this->back_model->daftar_b_pokok();

			$this->load->view('backend/back_tambah_stok_bapok',$data);
		} else {
			redirect(base_url('backend'));
		}

	}

	public function tambah_stok_bapok_ac()
	{
		if ($this->session->userdata('admin_id')) {
			$stok = $this->input->post('jml_stok');
			$bahan_pokok = $this->input->post('id_bapok');
			$tdg = $this->input->post('tdg');

			//cek ketersediaan
			$cek_stok = $this->back_model->cek_stok($bahan_pokok,$tdg);

			if ($cek_stok->num_rows() >= 1) {

				foreach ($cek_stok->result() as $data_stok) {}
				$jml_stok = $data_stok->jml_stok;
				$jml_stok_akhir = $jml_stok+$stok;
				$data = array('jml_stok'=>$jml_stok_akhir);

				$this->back_model->tambah_jml_stok($data_stok->id_bapok,$data_stok->tdg,$data);
				$this->session->set_flashdata('upload','Stok Berhasil ditambah');
				redirect(base_url('backend/tambah_stok_bapok'));
			} else {

				$data = array(
						'id'=>'',
						'id_bapok'=>$this->input->post('id_bapok'),
						'tdg'=>$this->input->post('tdg'),
						'jml_stok'=>$this->input->post('jml_stok'),
						'tgl_input'=>date('Ymd'),
					);
				$this->back_model->tambah_stok_bapok($data);
				$this->session->set_flashdata('upload','Stok Berhasil ditambah');
				redirect(base_url('backend/tambah_stok_bapok'));
			}
		} else {
			redirect(base_url('backend'));
		}

	}

	public function data_pengeluaran()
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Data Pengeluaran Bahan Pokok | Disperindag';
			$data['pengeluaran'] = $this->back_model->data_pengeluaran();
			$data['bapok'] = $this->back_model->daftar_b_pokok();

			$this->load->view('backend/data_pengeluaran',$data);
		} else {
			redirect(base_url('backend'));
		}

	}

	public function pengeluaran_bapok()
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Tambah Stok Bahan Pokok | Disperindag';
			$data['bhn_pokok'] = $this->back_model->daftar_b_pokok();

			$this->load->view('backend/back_pengeluaran_bapok',$data);
		} else {
			redirect(base_url('backend'));
		}

	}

	public function pengeluaran_bapok_ac()
	{
		if ($this->session->userdata('admin_id')) {
			$cek_stok = $this->back_model->cek_stok($this->input->post('id_bapok'),$this->input->post('tdg'));

			if ($cek_stok->num_rows() > 0) {
				foreach ($cek_stok->result() as $row) {}
				if ($this->input->post('volume') < $row->jml_stok) {

					$data = array(
						'id'=>'',
						'id_bapok'=>$this->input->post('id_bapok'),
						'no_tdg'=>$this->input->post('tdg'),
						'volume'=>$this->input->post('volume'),
						'dikeluarkan_ke'=>$this->input->post('dikeluarkan_ke'),
						'tgl_keluar'=>$this->input->post('tgl_keluar')
					);

					$this->back_model->insert_pengeluaran($data);

					$current_stock = $row->jml_stok;
					$last_stock = $current_stock - $this->input->post('volume');

					$update_stock = array('jml_stok'=>$last_stock);
					$this->back_model->update_stok($row->id,$update_stock);

					$this->session->set_flashdata('cek_stok','Sukses');
					redirect(base_url('backend/pengeluaran_bapok'));

				} else {
					$this->session->set_flashdata('cek_stok','Stok Bahan Pokok Kurang');
					redirect(base_url('backend/pengeluaran_bapok'));
				}
			} else {
				$this->session->set_flashdata('cek_stok','Bahan Pokok Tidak Ada');
				redirect(base_url('backend/pengeluaran_bapok'));
			}
		} else {
			redirect(base_url('base_url'));
		}

	}

	public function edit_pengeluaran($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Tambah Stok Bahan Pokok | Disperindag';
			$data['bhn_pokok'] = $this->back_model->daftar_b_pokok();

			$data['pengeluaran'] = $this->back_model->data_pengeluaran_byid($id);

			$this->load->view('backend/back_edit_pengeluaran',$data);
		} else {
			redirect(base_url('backend'));
		}

	}

	public function edit_pengeluaran_ac($id)
	{
		if ($this->session->userdata('admin_id')) {

			$current_output = $this->input->post('current_volume');

			$stok = $this->back_model->cek_stok($this->input->post('current_bapok'),$this->input->post('tdg'));
			foreach ($stok->result() as $row) {}

			$stockbefore = $row->jml_stok + $current_output;

			if ($this->input->post('volume') < $stockbefore) {

				$data = array(
					'no_tdg'=>$this->input->post('tdg'),
					'volume'=>$this->input->post('volume'),
					'dikeluarkan_ke'=>$this->input->post('dikeluarkan_ke'),
					'tgl_keluar'=>$this->input->post('tgl_keluar')
					);

				$last_stock = $stockbefore - $this->input->post('volume');

				$update_stock = array('jml_stok'=>$last_stock);
				$this->back_model->update_stok($row->id,$update_stock);

				$this->back_model->edit_pengeluaran($id,$data);

				$this->session->set_flashdata('sukses','Sukses');
				redirect(base_url('backend/data_pengeluaran'));

			} else {
				$this->session->set_flashdata('error','Stok Bahan Pokok Kurang');
				redirect(base_url("backend/data_pengeluaran"));
			}
		} else {
			redirect(base_url('backend'));
		}

	}

	public function hapus_pengeluaran($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data_keluar = $this->back_model->data_pengeluaran_byid($id);
			foreach ($data_keluar->result() as $data_keluar) {}

			$current_stock = $this->back_model->cek_stok($data_keluar->id_bapok,$data_keluar->tdg);
			$current_out = $data_keluar->volume;

			foreach ($current_stock->result() as $current_stock) {}

			$last_stock = $current_out + $current_stock->jml_stok;
			$data = array('jml_stok'=>$last_stock);

			$this->back_model->hapus_pengeluaran($id);
			$this->back_model->update_stok($current_stock->id,$data);

			$this->session->set_flashdata('delete','Data telah dihapus');
			redirect(base_url('backend/data_pengeluaran'));
		} else {
			redirect(base_url('backend'));
		}

	}

	public function export_pengeluaran()
	{
		if ($this->session->userdata('admin_id')) {
			if (isset($_POST['btnPDF'])) {
				$tdg = $this->input->post('tdg');

				$data['keluar1'] = $this->back_model->data_pengeluaran_pdf1($tdg);
				$data['keluar2'] = $this->back_model->data_pengeluaran_pdf2($tdg);

				$html = $this->load->view('backend/back_pengeluaran_pdf',$data,true);

				$pdfFilePath = "TDP.pdf";

				$this->load->library('m_pdf');

				$this->m_pdf->pdf->WriteHTML($html); // write the HTML into the PDF

				$this->m_pdf->pdf->Output($pdfFilePath, "I");
			} else {
				$object = new PHPExcel();
				$object->setActiveSheetIndex(0);

				$tdg = $this->input->post('tdg');

				$table_columns1 = array('No','Bahan Pokok','TDG','Dikeluarkan ke','Dikeluarkan sebanyak');

				$table_columns2 = array('No','Bahan Pokok','Total Pengeluaran');

				$column = 0;

				$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, "Data keluar bahan pokok");

				foreach($table_columns1 as $field)
				{
					$object->getActiveSheet()->setCellValueByColumnAndRow($column, 3, $field);
					$column++;

					$last_column = $column;
				}

				$excel_row = 4;

				$keluar1 = $this->back_model->data_pengeluaran_pdf1($tdg);
				$keluar2 = $this->back_model->data_pengeluaran_pdf2($tdg);

				$no=1;
				$no2=1;
				foreach($keluar1->result() as $row)
				{
					$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
					$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->nama_bahan_pokok);
					$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->no_tdg);
					$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->dikeluarkan_ke);
					$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->volume." ".$row->satuan);
					$excel_row++;
					$no++;
					$last_row = $excel_row;
				}

				$column2 = 0;

				$object->getActiveSheet()->setCellValueByColumnAndRow($column2, $last_row+2, "Total Pengeluaran");

				foreach($table_columns2 as $field)
				{
					$object->getActiveSheet()->setCellValueByColumnAndRow($column2, $last_row+3, $field);
					$column2++;
				}

				foreach($keluar2->result() as $row2)
				{
					$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row+4, $no2);
					$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row+4, $row2->nama_bahan_pokok);
					$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row+4, $row2->volume." ".$row2->satuan);
					$excel_row++;
					$no2++;
					$last_row = $excel_row;
				}

				$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename="Data Pengeluaran.xls"');
				$object_writer->save('php://output');
			}
		} else {
			redirect(base_url('backend'));
		}

	}

	public function data_dist()
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Tambah Stok Bahan Pokok | Disperindag';
			$data['title2'] = 'Data Distributor';
			$data['stok'] = $this->back_model->data_dist_stok();
			$data['bhn_pokok'] = $this->back_model->daftar_b_pokok();
			$data['pengadaan'] = $this->back_model->data_dist_pengadaan();
			$data['distribusi'] = $this->back_model->data_dist();

			$this->load->view('backend/back_data_dist',$data);
		} else {
			redirect(base_url('backend'));
		}

	}

	public function tambah_data_dist()
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Tambah Stok Bahan Pokok | Disperindag';
			$data['title2'] = 'Tambah Data Pengadaan';
			$data['bhn_pokok'] = $this->back_model->daftar_b_pokok();

			$this->load->view('backend/back_tambah_data_dist',$data);
		} else {
			redirect(base_url('backend'));
		}

	}

	public function tambah_data_dist2()
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Tambah Data Distributor | Disperindag';
			$data['title2'] = 'Tambah Data Distributor';
			$data['bhn_pokok'] = $this->back_model->daftar_b_pokok();

			$this->load->view('backend/back_tambah_data_dist_keluar',$data);
		} else {
			redirect(base_url('backend'));
		}

	}

	public function tambah_data_dist_ac()
	{
		if ($this->session->userdata('admin_id')) {
			$id_bapok = $this->input->post('id_bapok');
			$tdpud = $this->input->post('tdpud');
			$asal = $this->input->post('asal');
			$jml_pengadaan = $this->input->post('jml_pengadaan');

			//cek keberadaan data stok
			$cek = $this->back_model->cek_data_dist_stok($id_bapok,$tdpud);
			if ($cek->num_rows() > 0) {
				foreach ($cek->result() as $get_data) {}
				$current_stock = $get_data->stok;
				$calculate_stock = $current_stock + $jml_pengadaan;

				$this->back_model->update_stok_dist($get_data->id,$calculate_stock);

				$data = array(
						'id'=>'',
						'id_bapok'=>$id_bapok,
						'tdpud'=>$tdpud,
						'asal'=>$asal,
						'jml_pengadaan'=>$jml_pengadaan,
						'tgl_input'=>date('Ymd')
					);
				$this->back_model->tambah_dist($data);
				$this->session->set_flashdata('insert','Berhasil menambah data');
				redirect(base_url('backend/tambah_data_dist'));
			} else {
				$data_stok = array(
						'id'=>'',
						'id_bapok'=>$id_bapok,
						'tdpud'=>$tdpud,
						'stok'=>$jml_pengadaan,
						'tgl_input'=>date('Ymd')
					);
				$data = array(
						'id'=>'',
						'id_bapok'=>$id_bapok,
						'tdpud'=>$tdpud,
						'asal'=>$asal,
						'jml_pengadaan'=>$jml_pengadaan,
						'tgl_input'=>date('Ymd')
					);
				$this->back_model->tambah_stok_dist($data_stok);
				$this->back_model->tambah_dist($data);
				$this->session->set_flashdata('insert','Berhasil menambah data');
				redirect(base_url('backend/tambah_data_dist'));
			}

		} else {
			redirect(base_url('backend'));
		}

	}

	public function tambah_data_dist_keluar_ac()
	{
		if ($this->session->userdata('admin_id')) {
			$id_bapok = $this->input->post('id_bapok');
			$tdpud = $this->input->post('tdpud');
			$tujuan = $this->input->post('tujuan');
			$jml_pengeluaran = $this->input->post('jml_pengeluaran');
			$ket = $this->input->post('ket');

			//cek stok
			$cek = $this->back_model->cek_data_dist_stok($id_bapok,$tdpud);
			foreach ($cek->result() as $stok) {}
			if ($jml_pengeluaran < $stok->stok) {

				$current_stock = $stok->stok;
				$calculate_stock = $current_stock - $jml_pengeluaran;

				$data = array(
						'id'=>'',
						'id_bapok'=>$id_bapok,
						'tdpud'=>$tdpud,
						'stok_awal'=>$current_stock,
						'jml_pengeluaran'=>$jml_pengeluaran,
						'stok_akhir'=>$calculate_stock,
						'tujuan'=>$tujuan,
						'ket'=>$ket,
						'tgl_input'=>date('Ymd')
					);

				$this->back_model->update_stok_dist($stok->id,$calculate_stock);

				$this->back_model->tambah_dist_keluar($data);
				$this->session->set_flashdata('insert','Success menambah data');
				redirect(base_url('backend/data_dist'));
			} else {
				$this->session->set_flashdata('empty_stock','Stok tidak memadai');
				redirect(base_url('backend/tambah_data_dist2'));
			}

		} else {
			redirect(base_url('backend'));
		}

	}


	//AJAX
	function kelurahan(){
		if($this->back_model->kelurahan($this->input->post("id_kecamatan"))->num_rows() > 1){
			echo "<option value='-'>Pilih Kelurahan Disini</option>";
			foreach($this->back_model->kelurahan($this->input->post("id_kecamatan"))->result() as $row){
				echo "<option value='$row->id_kelurahan'>$row->nm_kelurahan</option>";
			}
		}else{
			echo 'kosong';
		}
	}

	function siup(){
		if($this->back_model->klasifikasi_iu_bynm($this->input->post("iu"))->num_rows() >= 1){
			foreach($this->back_model->klasifikasi_iu_bynm($this->input->post("iu"))->result() as $row){
				echo $row->no_siup;
			}
		}else{
			echo '-';
		}
	}

	public function edit_stok_bapok($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Tambah Stok Bahan Pokok | Disperindag';
			$data['bhn_pokok'] = $this->back_model->daftar_b_pokok();

			$data['stok'] = $this->back_model->stok_bapok_byid($id);

			$this->load->view('backend/edit_stok_bapok',$data);
		} else {
			redirect(base_url('backend'));
		}

	}

	public function edit_stok_bapok_ac($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data = array(
					'tdg'=>$this->input->post('tdg'),
					'jml_stok'=>$this->input->post('jml_stok')
				);

			$this->back_model->edit_stok_bapok($id,$data);
			$this->session->set_flashdata('update','Data berhasil diupdate!');
			redirect(base_url('backend/stok_bahan_pokok'));
		} else {
			redirect(base_url('backend'));
		}

	}

	public function edit_data_dist($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Data Distribusi | Disperindag';
			$data['title2'] = 'Edit Data Distribusi';
			$data['bhn_pokok'] = $this->back_model->daftar_b_pokok();

			$data['dist'] = $this->back_model->data_dist_byid($id);

			$this->load->view('backend/edit_data_dist',$data);
		} else {
			redirect(base_url('backend'));
		}

	}

	public function edit_data_dist_ac($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data = array(
					'id_bapok'=>$this->input->post('id_bapok'),
					'tdpud'=>$this->input->post('tdpud'),
					'sumber'=>$this->input->post('sumber'),
					'volume'=>$this->input->post('volume'),
					'dikirim_ke'=>$this->input->post('dikirim_ke')
				);
			$this->back_model->update_data_dist($id,$data);
			$this->session->set_flashdata('update','Data berhasil diupdate!');
			redirect(base_url('backend/data_dist'));
		} else {
			redirect(base_url('backend'));
		}
	}

	public function bulan($date)
	{
		$new_date = substr($date, 5,2);
		if ($new_date == "01") {
			return "Januari";
		} else if ($new_date == "02") {
			return "Februari";
		}
		else if ($new_date == "03") {
			return "Maret";
		}
		else if ($new_date == "04") {
			return "April";
		}
		else if ($new_date == "05") {
			return "Mei";
		}
		else if ($new_date == "06") {
			return "Jeni";
		}
		else if ($new_date == "07") {
			return "Juli";
		}
		else if ($new_date == "08") {
			return "Agustus";
		}
		else if ($new_date == "09") {
			return "September";
		}
		else if ($new_date == "10") {
			return "Oktober";
		}
		else if ($new_date == "11") {
			return "November";
		} else {
			return "Desember";
		}
	}
	public function tahun($date_min,$date_max)
	{
		$new_date_min = substr($date_min, 0,4);
		$new_date_max = substr($date_max, 0,4);

		if ($new_date_min == $new_date_max) {
			return $new_date_min;
		} else {
			return $new_date_min." - ".$new_date_max;
		}

	}

	public function edit_data_dist_stok($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Edit Data | Disperindag';
			$data['title2'] = 'Edit Data Stok Distribusi';
			$data['bhn_pokok'] = $this->back_model->daftar_b_pokok();

			$data['stok_dist'] = $this->back_model->data_stok_dist_byid($id);

			$this->load->view('backend/back_edit_stok_dist',$data);
		} else {
			redirect(base_url('backend'));
		}

	}

	public function edit_data_dist_stok_ac()
	{
		if ($this->session->userdata('admin_id')) {
			$id = $this->input->post('id');
			$new_data = array(
					'id_bapok'=>$this->input->post('id_bapok'),
					'tdpud'=>$this->input->post('tdpud'),
					'stok'=>$this->input->post('stok')
				);
			$this->back_model->update_stock_dist($id,$new_data);
			$this->session->set_flashdata('update','Data telah diupdate');
			redirect(base_url('backend/data_dist'));
		} else {
			redirect(base_url('backend'));
		}

	}

	public function hapus_data_dist_stok($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data = $this->back_model->data_stok_dist_byid($id);
			foreach ($data->result() as $row) {}

			$id_bapok = $row->id_bapok;
		    $tdpud = $row->tdpud;

		    $this->back_model->delete_dist_keluar($id_bapok,$tdpud);
		    $this->back_model->delete_dist_pengadaan($id_bapok,$tdpud);
		    $this->back_model->delete_dist_stok($id_bapok,$tdpud);

		    $this->session->set_flashdata('delete','Data sudah dihapus');
		    redirect(base_url('backend/data_dist'));
		} else {
			redirect(base_url('backend'));
		}

	}
	public function edit_data_dist_pengadaan($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Edit Data | Disperindag';
			$data['title2'] = 'Edit Data Pengadaan Distribusi';
			$data['bhn_pokok'] = $this->back_model->daftar_b_pokok();

			$data['peng_dist'] = $this->back_model->data_peng_dist_byid($id);

			$this->load->view('backend/back_edit_peng_dist',$data);
		} else {
			redirect(base_url('backend'));
		}

	}

	public function edit_data_dist_pengadaan_ac()
	{
		if ($this->session->userdata('admin_id')) {
			$id_bapok = $this->input->post('id_bapok2');
			$tdpud = $this->input->post('tdpud2');
			$data_stok = $this->back_model->data_dist_stok_flter($id_bapok,$tdpud);
			$new_data = array(
					'asal'=>$this->input->post('asal'),
					'jml_pengadaan'=>$this->input->post('jml_pengadaan')
				);
			foreach ($data_stok->result() as $data_stok) {}
			$current_stock = $data_stok->stok - $this->input->post('stok_peng_lama');
			$calculate_new_stock = $current_stock + $this->input->post('jml_pengadaan');

			$new_data2 = array('stok'=>$calculate_new_stock);

			$this->back_model->update_stock_dist($data_stok->id,$new_data2);
			$this->back_model->update_dist_pengeluaran($id_bapok,$tdpud,$new_data);

			$this->session->set_flashdata('update','Data telah diupdate');
			redirect(base_url('backend/data_dist'));
		} else {
			redirect(base_url('backend'));
		}

	}

	public function hapus_data_dist_pengadaan($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data = $this->back_model->data_dist_pengadaan_byid($id);
			foreach ($data->result() as $row) {}
			$jml_pengadaan = $row->jml_pengadaan;
			$id_bapok = $row->id_bapok;
			$tdpud = $row->tdpud;

			$data_stok = $this->back_model->data_dist_stok_flter($id_bapok,$tdpud);
			foreach ($data_stok->result() as $data_stok) {}

			$last_stock = $data_stok->stok;
			$new_stock = $last_stock-$jml_pengadaan;

			$this->back_model->update_stok_dist2($id_bapok,$tdpud,$new_stock);
			$this->back_model->delete_dist_pengadaan_byid($id);

			$this->session->set_flashdata('delete','Data telah dihapus');
			redirect(base_url('backend/data_dist'));
		} else {
			redirect(base_url('backend'));
		}

	}
	public function edit_data_dist_pengeluaran($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Edit Data | Disperindag';
			$data['title2'] = 'Edit Data Pengeluaran Distribusi';
			$data['bhn_pokok'] = $this->back_model->daftar_b_pokok();

			$data['dist'] = $this->back_model->data_dist_keluar_byid($id);

			$this->load->view('backend/back_edit_dist_keluar',$data);
		} else {
			redirect(base_url('backend'));
		}

	}

	public function edit_data_dist_pengeluaran_ac()
	{
		if ($this->session->userdata('admin_id')) {
			$id = $this->input->post('id');
			$id_bapok = $this->input->post('id_bapok');
			$tdpud = $this->input->post('tdpud');
			$tujuan = $this->input->post('tujuan');
			$jml_pengeluaran_lama = $this->input->post('jml_pengeluaran_lama');
			$jml_pengeluaran = $this->input->post('jml_pengeluaran');
			$ket = $this->input->post('ket');

			$stok = $this->back_model->data_dist_stok_flter($id_bapok,$tdpud);
			foreach ($stok->result() as $stok) {}

			$current_stock = $stok->stok + $jml_pengeluaran_lama;

			echo $new_last_stock = $current_stock - $jml_pengeluaran;
			$data = array(
					'tujuan'=>$tujuan,
					'jml_pengeluaran'=>$jml_pengeluaran,
					'ket'=>$ket
				);

			$this->back_model->update_stok_dist2($id_bapok,$tdpud,$new_last_stock);
			$this->back_model->edit_data_dist_keluar($id,$data);

			$this->session->set_flashdata('update','Data telah diupdate');
			redirect(base_url('backend/data_dist'));
		} else {
			redirect(base_url('backend'));
		}

	}

	public function hapus_data_dist_pengeluaran($id)
	{
		if ($this->session->userdata('admin_id')) {
			$dist_keluar = $this->back_model->data_dist_keluar_byid($id);
			foreach ($dist_keluar->result() as $row) {}
			$stok = $this->back_model->data_stock_dist_by_bpk_tdpud($row->id_bapok,$row->tdpud);
			foreach ($stok->result() as $row2) {}

			$jml_pengeluaran = $row->jml_pengeluaran;
			$stok = $row2->stok + $jml_pengeluaran;
			$data = array('stok'=>$stok);

			$this->back_model->update_stock_dist($row2->id,$data);
			$this->back_model->delete_dist_keluar_byid($id);

			$this->session->set_flashdata('delete','Data telah dihapus');
			redirect(base_url('backend/data_dist'));
		} else {
			redirect(base_url('backend'));
		}

	}

	public function stok_gudang()
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Tambah Stok Bahan Pokok | Disperindag';
			$data['title2'] = 'Data Gudang';
			$data['bhn_pokok'] = $this->back_model->daftar_b_pokok();

			$data['stok_gudang'] = $this->back_model->get_tdg_stok();
			$data['penambahan_stok'] = $this->back_model->get_tdg_penambahan_stok();
			$data['pengiriman_gudang'] = $this->back_model->get_tdg_pengiriman();

			$this->load->view('backend/back_data_stok_tdg',$data);
		} else {
			redirect(base_url('backend'));
		}

	}

	public function filter_gudang()
	{
		if ($this->session->userdata('admin_id')) {
			$data['tgl_max'] = $tgl_max = $this->input->post('tgl_max');
			$data['tgl_min'] = $tgl_min = $this->input->post('tgl_min');
			$data['pengiriman'] = $this->back_model->data_tdg_pengiriman_pdf($tgl_min,$tgl_max);
			//$data['pengeluaran'] = $this->back_model->data_tdg_pengiriman_pdf($tgl_min,$tgl_max);
			//kalo mau -3 bulan ==> date('Y-m-d', strtotime("-3 month",strtotime($tgl_max)));
			$data['bulan_min'] = $this->bulan($this->input->post('tgl_min'));
			$data['bulan_max'] = $this->bulan($this->input->post('tgl_max'));
			$data['tahun'] = $this->tahun($this->input->post('tgl_min'),$this->input->post('tgl_max'));

			$data['sub_query'] = $this->back_model;

			$html = $this->load->view('backend/back_gudang_pdf',$data,true);

			$pdfFilePath = "Laporan Distribusi.pdf";

			$this->load->library('m_pdf');

			$this->m_pdf->pdf->WriteHTML($html); // write the HTML into the PDF

			$this->m_pdf->pdf->Output($pdfFilePath, "I");
		} else {
			redirect(base_url('backend'));
		}

	}

	public function filter_gudang2()
	{
		if ($this->session->userdata('admin_id')) {
			$tdg = $this->input->post('tdg');
			$data['data'] = $this->back_model->data_tdg_pengiriman_pdf2($tdg);
			//$data['pengeluaran'] = $this->back_model->data_tdg_pengiriman_pdf($tgl_min,$tgl_max);
			//kalo mau -3 bulan ==> date('Y-m-d', strtotime("-3 month",strtotime($tgl_max)));

			$data['sub_query'] = $this->back_model;

			$html = $this->load->view('backend/back_gudang_pdf2',$data,true);

			$pdfFilePath = "Laporan Distribusi.pdf";

			$this->load->library('m_pdf');

			$this->m_pdf->pdf->WriteHTML($html); // write the HTML into the PDF

			$this->m_pdf->pdf->Output($pdfFilePath, "I");

		} else {
			redirect(base_url('backend'));
		}

	}

	public function tambah_data_stok_tdg()
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Tambah Stok Bahan Pokok | Disperindag';
			$data['title2'] = 'Tambah Stok Gudang';
			$data['bhn_pokok'] = $this->back_model->daftar_b_pokok();

			$this->load->view('backend/back_tambah_stok_tdg',$data);
		} else {
			redirect(base_url('backend'));
		}

	}
	public function tambah_data_stok_tdg_ac()
	{
		if ($this->session->userdata('admin_id')) {
			$id_bapok = $this->input->post('id_bapok');
			$tdg = $this->input->post('tdg');
			$asal = $this->input->post('asal');
			$stok = $this->input->post('stok');

			//cek data stok tdg
			$cek = $this->back_model->cek_stok_tdg($id_bapok,$tdg);
			if ($cek->num_rows() > 0) {
				foreach ($cek->result() as $last_data) {}
				$last_stock = $last_data->stok;
				$new_stock = $last_stock + $stok;

				$data_history = array(
						'id'=>'',
						'id_bapok'=>$id_bapok,
						'tdg'=>$tdg,
						'asal'=>$asal,
						'jml_stok'=>$stok,
						'tgl_input'=>date('Ymd')
					);

				$update_data = array('stok'=>$new_stock);
				$this->back_model->update_stok_tdg($id_bapok,$tdg,$update_data);
				$this->back_model->insert_tdg_penambahan_stok($data_history);
				$this->session->set_flashdata('update','Stok berhasil ditambah');
				redirect(base_url('backend/stok_gudang'));
			} else {
				$new_data = array(
						'id'=>'',
						'id_bapok'=>$id_bapok,
						'tdg'=>$tdg,
						'stok'=>$stok,
						'tgl_input'=>date('Ymd')
					);

				$data_history = array(
						'id'=>'',
						'id_bapok'=>$id_bapok,
						'tdg'=>$tdg,
						'asal'=>$asal,
						'jml_stok'=>$stok,
						'tgl_input'=>date('Ymd')
					);
				$this->back_model->insert_stok_tdg($new_data);
				$this->back_model->insert_tdg_penambahan_stok($data_history);
				$this->session->set_flashdata('insert','Stok berhasil ditambah');
				redirect(base_url('backend/stok_gudang'));
			}

		} else {
			redirect(base_url('backend'));
		}

	}

	public function tambah_data_pengiriman_tdg()
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Tambah Data Pengiriman | Disperindag';
			$data['title2'] = 'Tambah Data Pengiriman';
			$data['bhn_pokok'] = $this->back_model->daftar_b_pokok();

			$this->load->view('backend/back_tambah_data_pengiriman_tdg',$data);
		} else {
			redirect(base_url('backend'));
		}

	}
	public function tambah_data_pengiriman_tdg_ac()
	{
		if ($this->session->userdata('admin_id')) {
			$id_bapok = $this->input->post('id_bapok');
			$tdg = $this->input->post('tdg');
			$tujuan = $this->input->post('tujuan');
			$jml_pengiriman = $this->input->post('jml_pengiriman');
			$ket = $this->input->post('ket');

			//cek stok
			$cek_stok = $this->back_model->cek_stok_tdg($id_bapok,$tdg);
			if ($cek_stok->num_rows() > 0) {
				foreach ($cek_stok->result() as $stok) {}
				if ($stok->stok > 0) {
					$last_stock = $stok->stok;
					$calculate_stock = $last_stock - $jml_pengiriman;

					$update_stok = array('stok'=>$calculate_stock);
					$this->back_model->update_stok_tdg($id_bapok,$tdg,$update_stok);
					$data_pengiriman = array(
							'id'=>'',
							'id_bapok'=>$id_bapok,
							'tdg'=>$tdg,
							'jml_pengiriman'=>$jml_pengiriman,
							'tujuan'=>$tujuan,
							'stok_awal'=>$last_stock,
							'stok_akhir'=>$calculate_stock,
							'ket'=>$ket,
							'tgl_input'=>date('Ymd')
						);

					$this->back_model->insert_data_pengiriman_tdg($data_pengiriman);

					$this->session->set_flashdata('insert','Data telah ditambah');
					redirect(base_url('backend/stok_gudang'));
				} else {
					$this->session->set_flashdata('failed','Stok tidak memadai');
					redirect(base_url('backend/stok_gudang'));
				}

			} else {
				$this->session->set_flashdata('failed','Bahan pokok tidak ditemukan');
				redirect(base_url('backend/stok_gudang'));
			}

		} else {
			redirect(base_url('backend'));
		}

	}

	public function edit_stok_gudang($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Tambah Data Pengiriman | Disperindag';
			$data['title2'] = 'Tambah Data Pengiriman';
			$data['bhn_pokok'] = $this->back_model->daftar_b_pokok();

			$data['stok'] = $this->back_model->get_tdg_stok_byid($id);

			$this->load->view('backend/back_edit_stok_gudang',$data);
		} else {
			redirect(base_url('backend'));
		}

	}

	public function edit_stok_gudang_ac()
	{
		if ($this->session->userdata('admin_id')) {
			$id_bapok = $this->input->post('id_bapok2');
			$tdg = $this->input->post('tdg2');
			$stok = $this->input->post('stok');

			$data = array('stok'=>$stok);
			$this->back_model->update_stok_tdg($id_bapok,$tdg,$data);

			$this->session->set_flashdata('update','Stok telah diupdate');
			redirect(base_url('backend/stok_gudang'));
		} else {
			redirect(base_url('backend'));
		}

	}

	public function hapus_data_gudang($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data = $this->back_model->get_tdg_stok_byid($id);
			foreach ($data->result() as $row) {}
			$id_bapok = $row->id_bapok;
			$tdg = $row->tdg;

			$this->back_model->delete_stok_tdg($id);
			$this->back_model->delete_penambahan_stok_tdg($id_bapok,$tdg);
			$this->back_model->delete_pengiriman_tdg($id_bapok,$tdg);

			$this->session->set_flashdata('delete','Data telah dihapus');
			redirect(base_url('backend/stok_gudang'));
		} else {
			redirect(base_url('backend'));
		}

	}
	public function edit_tambah_stok_gudang($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Gudang | Disperindag';
			$data['title2'] = 'Edit Data';
			$data['bhn_pokok'] = $this->back_model->daftar_b_pokok();

			$data['data'] = $this->back_model->get_data_penambahan_stok_gudang($id);

			$this->load->view('backend/back_edit_tambah_stok_gudang',$data);
		} else {
			redirect(base_url('backend'));
		}

	}

	public function edit_tambah_stok_gudang_ac()
	{
		if ($this->session->userdata('admin_id')) {
			$id = $this->input->post('id');
			$id_bapok = $this->input->post('id_bapok2');
			$tdg = $this->input->post('tdg2');
			$asal = $this->input->post('asal');
			$jml_stok = $this->input->post('jml_stok');
			$jml_stok_lama = $this->input->post('jml_stok_lama');

			//cek stok
			$data_stok = $this->back_model->cek_stok_tdg($id_bapok,$tdg);
			foreach ($data_stok->result() as $row) {}
			$stock = $row->stok - $jml_stok_lama;
			$calculate_stock = $stock + $jml_stok;

			$update_stok = array('stok'=>$calculate_stock);
			$update_data = array('asal'=>$asal,'jml_stok'=>$jml_stok);

			$this->back_model->update_stok_tdg($id_bapok,$tdg,$update_stok);
			$this->back_model->update_tambah_stok_gudang($id,$update_data);

			$this->session->set_flashdata('update','Data telah diupdate');
			redirect(base_url('backend/stok_gudang'));
		} else {
			redirect(base_url('backend'));
		}

	}
	public function hapus_tambah_stok_gudang($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data = $this->back_model->get_tdg_penambahan_stok_byid($id);
			foreach ($data->result() as $row) {}

			$id_bapok = $row->id_bapok;
			$tdg = $row->tdg;

			$data_stok = $this->back_model->get_tdg_stok_bybapok_tdg($id_bapok,$tdg);
			foreach ($data_stok->result() as $row2) {}

			$stok = $row2->stok - $row->jml_stok;
			$update_stok = array('stok'=>$stok);

			$this->back_model->update_stok_tdg($id_bapok,$tdg,$update_stok);
			$this->back_model->delete_tdg_penambahan_stok($id);

			$this->session->set_flashdata('delete','Data telah dihapus');
			redirect(base_url('backend/stok_gudang'));
		} else {
			redirect('backend');
		}

	}

	public function edit_data_tdg_pengiriman($id)
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Gudang | Disperindag';
			$data['title2'] = 'Edit Data';
			$data['bhn_pokok'] = $this->back_model->daftar_b_pokok();

			$data['data'] = $this->back_model->get_data_pengiriman_tdg($id);

			$this->load->view('backend/back_edit_data_pengiriman_gudang',$data);
		} else {
			redirect('backend');
		}

	}

	public function edit_data_tdg_pengiriman_ac()
	{
		if ($this->session->userdata('admin_id')) {
			$id = $this->input->post('id');
			$id_bapok = $this->input->post('id_bapok2');
			$tdg = $this->input->post('tdg2');
			$tujuan = $this->input->post('tujuan');
			$jml_pengiriman = $this->input->post('jml_pengiriman');
			$jml_pengiriman_lama = $this->input->post('jml_pengiriman_lama');
			$ket = $this->input->post('ket');
		} else {
			redirect(base_url('backend'));
		}

	}

	public function hapus_data_tdg_pengiriman($id)
	{
		$data = $this->back_model->get_data_pengiriman_tdg($id);
		foreach ($data->result() as $row) {}
		$id_bapok = $row->id_bapok;
		$tdg = $row->tdg;

		$stok = $this->back_model->cek_stok_tdg($id_bapok,$tdg);
		foreach ($stok->result() as $row2) {}

		$last_stock = $row2->stok;
		$calculate_stock = $last_stock + $row->jml_pengiriman;

		$update_stock = array('stok'=>$calculate_stock);
		$this->back_model->update_stok_tdg($id_bapok,$tdg,$update_stock);
		$this->back_model->delete_tdg_pengiriman($id);

		$this->session->set_flashdata('delete','Data telah dihapus');
		redirect(base_url('backend/stok_gudang'));
	}

	public function data_b3()
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Data B3 | Disperindag';
			$data['title2'] = 'Data B3';

			$data['stok'] = $this->back_model->get_stok_b3();
			$data['hist_penambahan_stok'] = $this->back_model->get_history_penambahan_stok_b3();
			$data['b3_pengeluaran'] = $this->back_model->get_data_b3_keluar();
			$this->load->view('backend/back_data_b3',$data);
		} else {
			redirect(base_url('backend'));
		}
	}

	public function tambah_stok_b3()
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'Tambah Stok B3 | Disperindag';
			$data['title2'] = 'Tambah Stok B3';
			$data['bahan_pok'] = $this->back_model->data_b3();

			$this->load->view('backend/back_tambah_stok_b3',$data);
		} else {
			redirect(base_url('backend'));
		}
		
	}

	public function tambah_stok_b3_ac()
	{
		if ($this->session->userdata('admin_id')) {
			$id_bahan_pok = $this->input->post('id_bapok');
			$distributor = $this->input->post('distributor');
			$jumlah = $this->input->post('jml_penambahan');

			$cek_stok = $this->back_model->cek_stok_b3($id_bahan_pok,$distributor);
			if ($cek_stok->num_rows() > 0) {
				foreach ($cek_stok->result() as $row) {}
				$current_stock = $row->stok;
				$calculate_stock = $current_stock+$jumlah;

				$update_stok = array('stok'=>$calculate_stock);
				$this->back_model->update_stok_b3($id_bahan_pok,$distributor,$update_stok);

				$data = array(
						'no_cas'=>$id_bahan_pok,
						'distributor'=>$distributor,
						'jml_penambahan'=>$jumlah,
						'tgl_input'=>date('Ymd')
					);
				$this->back_model->insert_history_penambahan_stok_b3($data);
				$this->session->set_flashdata('insert','Stok Berhasil Ditambah');
				redirect(base_url('backend/data_b3'));
			} else {
				$data = array(
						'no_cas'=>$id_bahan_pok,
						'distributor'=>$distributor,
						'stok'=>$jumlah,
						'tgl_input'=>date('Ymd')
					);
				$data2 = array(
						'no_cas'=>$id_bahan_pok,
						'distributor'=>$distributor,
						'jml_penambahan'=>$jumlah,
						'tgl_input'=>date('Ymd')
					);
				$this->back_model->insert_stok_b3($data);
				$this->back_model->insert_history_penambahan_stok_b3($data2);
				$this->session->set_flashdata('insert','Stok Berhasil Ditambah');
				redirect(base_url('backend/data_b3'));
			}
			
		} else {
			redirect(base_url('backend'));
		}
		
	}

	public function tambah_data_pengguna_akhir()
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['title'] = 'B3 | Disperindag';
			$data['title2'] = 'Tambah Data Pengguna Akhir B3';
			$data['bahan_pok'] = $this->back_model->data_b3();

			$this->load->view('backend/back_tambah_pengguna_akhir_b3',$data);
		} else {
			redirect(base_url('backend'));
		}
		
	}

	public function tambah_data_pengguna_akhir_ac()
	{
		if ($this->session->userdata('admin_id')) {
			$no_cas = $this->input->post('id_bapok');
			$nm_distributor = $this->input->post('nm_distributor');
			$jml_pengeluaran = $this->input->post('jml_pengeluaran');
			$nm_pengguna_akhir = $this->input->post('nm_pengguna_akhir');
			$no_rekomendasi = $this->input->post('no_rekomendasi');
			
			$cek_stok = $this->back_model->cek_stok_b3($no_cas,$nm_distributor);

			if ($cek_stok->num_rows() > 0) {
				
				foreach ($cek_stok->result() as $row) {}
				if ($row->stok > $jml_pengeluaran) {
					$current_stock = $row->stok;
					$calculate_stock = $current_stock-$jml_pengeluaran;

					$update_stok = array('stok'=>$calculate_stock);
					$this->back_model->update_stok_b3($no_cas,$nm_distributor,$update_stok);

					$data = array(
							'nm_distributor'=>$nm_distributor,
							'no_cas'=>$no_cas,
							'stok_awal'=>$current_stock,
							'jml_pengeluaran'=>$jml_pengeluaran,
							'stok_akhir'=>$calculate_stock,
							'nm_pengguna_akhir'=>$nm_pengguna_akhir,
							'no_rekomendasi'=>$no_rekomendasi,
							'tgl_input'=>date('Ymd')
						);
					$this->back_model->insert_b3_keluar($data);

					$this->session->set_flashdata('insert','Berhasil menambah data pengguna akhir B3');
					redirect(base_url('backend/data_b3'));
					
				} else {
					
					$this->session->set_flashdata('failed','Stok Bahan Pokok Tidak Memadai');
					redirect(base_url('backend/data_b3'));
					
				}
			} else {
				
				$this->session->set_flashdata('failed','Bahan Pokok Tidak Ada');
				redirect(base_url('backend/data_b3'));
				
			}
			
		} else {
			redirect(base_url('backend'));
		}
		
	}

	public function hapus_stok_b3($id)
	{
		if ($this->session->userdata('admin_id')) {
			$d = $this->back_model->get_stok_b3_byid($id);
			foreach ($d->result() as $row) {}
			$no_cas = $row->no_cas;
			$nm_distributor = $row->distributor;
			
			$this->back_model->delete_data_stok_b3($id);
			$this->back_model->delete_data_penambahan_b3($no_cas,$nm_distributor);
			$this->back_model->delete_data_keluar_b3($no_cas,$nm_distributor);

			$this->session->set_flashdata('delete','Data B3 telah dihapus');
			redirect(base_url('backend/data_b3'));
			
		} else {
			redirect(base_url('backend'));
		}
	}

	public function hapus_penambahan_b3($id)
	{
		if ($this->session->userdata('admin_id')) {
			$d = $this->back_model->get_penambahan_b3_byid($id);
			foreach ($d->result() as $row) {}
			$no_cas = $row->no_cas;
			$nm_distributor = $row->distributor;
			$jml_penambahan = $row->jml_penambahan;
			$s = $this->back_model->get_stok_b3_by_nocas($no_cas,$nm_distributor);
			foreach ($s->result() as $row2) {}
			$current_stock = $row2->stok;
			$calculate_stock = $current_stock - $jml_penambahan;

			$update_stok = array('stok'=>$calculate_stock);
			$this->back_model->update_stok_b3($no_cas,$nm_distributor,$update_stok);

			$this->back_model->delete_penambahan_b3($id);
			$this->session->set_flashdata('delete','Data penambahan B3 telah dihapus');
			redirect(base_url('backend/data_b3'));
		} else {
			redirect(base_url('backend'));
		}
	}

	public function hapus_data_b3_keluar($id)
	{
		if ($this->session->userdata('admin_id')) {
			$d = $this->back_model->get_keluar_b3_byid($id);
			foreach ($d->result() as $row) {}
			$no_cas = $row->no_cas;
			$nm_distributor = $row->nm_distributor;
			$jml_keluar = $row->jml_pengeluaran;
			$s = $this->back_model->get_stok_b3_by_nocas($no_cas,$nm_distributor);
			foreach ($s->result() as $row2) {}
			$current_stock = $row2->stok;
			echo $calculate_stock = $current_stock + $jml_keluar;

			$update_stok = array('stok'=>$calculate_stock);
			$this->back_model->update_stok_b3($no_cas,$nm_distributor,$update_stok);

			$this->back_model->delete_keluar_b3($id);
			$this->session->set_flashdata('delete','Data keluar B3 telah dihapus');
			redirect(base_url('backend/data_b3'));
		} else {
			redirect(base_url('backend'));
		}
	}
}

?>
