<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('front_model');
		$this->load->model('back_model');
	}
	function b_pokok_where_id_pasar(){
		if($this->session->userdata('admin_id')){
			$data['event'] = $this->back_model->daftar_event_sub_where_event_id($this->input->post('event_id'));			
			if($data['event']->num_rows() > 0){
				echo "<option value=''>Pilih Jenis Participant</option>";
				foreach($this->back_model->daftar_event_jenis_participant_where_id($this->input->post('event_id'))->result() as $ev_prc){
					echo "<option value='$ev_prc->id_event_participant' style='padding:10px; font-size:14px;'>$ev_prc->name_event_participant</option>";
				}
			}else{
				echo "0";
			}
		}	
		else{
			redirect('front/login');
		}
	}
	function index(){
		$file = './upload/file_excel/text.xlsx';
 
		//load the excel library
		$this->load->library("Excel/Classes/PHPExcel");
		 
		//read file from path
		$objPHPExcel = PHPExcel_IOFactory::load($file);
		 
		//get only the Cell Collection
		$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
		 
		//extract to a PHP readable array format
		foreach ($cell_collection as $cell) {
			$column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
			$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
			$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
		 
			//header will/should be in row 1 only. of course this can be modified to suit your need.
			if ($row == 2) {
				$header[$row][$column] = $data_value;
			} else {
				$arr_data[$row][$column] = $data_value;
			}
			//echo $column;
			//echo "<br>";
			//echo $row;
			//echo $arr_data[$row][$column];
		}
		 
		//send the data in an array format
		$data['header'] = $header;
		$data['values'] = $arr_data;
		
		echo $data['values'][4]['A'];
		
		
	}
}

?>