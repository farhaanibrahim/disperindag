<?php
/**
* 
*/
class Perizinan extends CI_Controller
{
	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('front_model');
		$this->load->model('back_model');
	}

	function tambah_perizinan()
	{
		if ($this->session->userdata('admin_id')) {
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));

			$config['upload_path'] = './upload';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['max_size']	= '6012312481248';
			$config['max_width']  = '211241242024';
			$config['max_height']  = '171212412468';

			$this->load->library('upload', $config);

			if (isset($_POST['btnSiup'])) {
				$table = $this->input->post('table');
				
				if ( ! $this->upload->do_upload('userfile'))
				{
					$data_form = array(
						'id'=>'',
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'no_telp'=>$this->input->post('no_telp'),
						'no_fax'=>$this->input->post('no_fax'),
						'email'=>$this->input->post('email'),
						'modal_usaha'=>$this->input->post('modal_usaha'),
						'kelembagaan'=>$this->input->post('kelembagaan'),
						'kbli'=>$this->input->post('kbli'),
						'barang_dagangan'=>$this->input->post('barang_dagangan'),
						'no_siup'=>$this->input->post('no_sk'),
						'tgl_siup'=>$this->input->post('tgl_sk'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'foto'=>''
						);
					$this->session->set_flashdata('tdp','tambah_sukses');
					$this->back_model->tambah_perizinan($data_form,$table);

					redirect('backend/tdp');
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());

					$file = array('upload_sukses' => $this->upload->data());
					$nama_file = $file['upload_sukses']['file_name'];

					$data_form = array(
						'id'=>'',
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'no_telp'=>$this->input->post('no_telp'),
						'no_fax'=>$this->input->post('no_fax'),
						'email'=>$this->input->post('email'),
						'modal_usaha'=>$this->input->post('modal_usaha'),
						'kelembagaan'=>$this->input->post('kelembagaan'),
						'kbli'=>$this->input->post('kbli'),
						'barang_dagangan'=>$this->input->post('barang_dagangan'),
						'no_siup'=>$this->input->post('no_sk'),
						'tgl_siup'=>$this->input->post('tgl_sk'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'foto'=>$nama_file
						);
					$this->session->set_flashdata('tdp','tambah_sukses');
					$this->back_model->tambah_perizinan($data_form,$table);

					redirect('backend/tdp');
				}

			} else if (isset($_POST['btnTdp'])) {
				$table = $this->input->post('table');
				
				if ( ! $this->upload->do_upload('userfile'))
				{
					$data_form = array(
						'id'=>'',
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'npwp'=>$this->input->post('npwp'),
						'no_telp'=>$this->input->post('no_telp'),
						'no_fax'=>$this->input->post('no_fax'),
						'email'=>$this->input->post('email'),
						'keg_usaha_pokok'=>$this->input->post('keg_usaha_pokok'),
						'kbli'=>$this->input->post('kbli'),
						'status_kantor'=>$this->input->post('status_kantor'),
						'no_tdp'=>$this->input->post('no_sk'),
						'tgl_tdp'=>$this->input->post('tgl_sk'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'foto'=>''
						);
					$this->session->set_flashdata('tdp','tambah_sukses');
					$this->back_model->tambah_perizinan($data_form,$table);

					redirect('backend/tdp');
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());

					$file = array('upload_sukses' => $this->upload->data());
					$nama_file = $file['upload_sukses']['file_name'];

					$data_form = array(
						'id'=>'',
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'npwp'=>$this->input->post('npwp'),
						'no_telp'=>$this->input->post('no_telp'),
						'no_fax'=>$this->input->post('no_fax'),
						'email'=>$this->input->post('email'),
						'keg_usaha_pokok'=>$this->input->post('keg_usaha_pokok'),
						'kbli'=>$this->input->post('kbli'),
						'status_kantor'=>$this->input->post('status_kantor'),
						'no_tdp'=>$this->input->post('no_sk'),
						'tgl_tdp'=>$this->input->post('tgl_sk'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'foto'=>$nama_file
						);
					$this->session->set_flashdata('tdp','tambah_sukses');
					$this->back_model->tambah_perizinan($data_form,$table);

					redirect('backend/tdp');
				}
			} else if (isset($_POST['btnIupr'])) {
				
				$table = $this->input->post('table');
				
				if ( ! $this->upload->do_upload('userfile'))
				{
					$data_form = array(
						'id'=>'',
						'tgl_pendaftaran'=>$this->input->post('tgl_pendaftaran'),
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'jenis_usaha'=>$this->input->post('jenis_usaha'),
						'siup'=>$this->input->post('siup'),
						'tdp'=>$this->input->post('tdp'),
						'no_iupr'=>$this->input->post('no_sk'),
						'tgl_iupr'=>$this->input->post('tgl_sk'),
						'no_rekomendasi'=>$this->input->post('no_rekomendasi'),
						'foto'=>''
						);
					$this->session->set_flashdata('tdp','tambah_sukses');
					$this->back_model->tambah_perizinan($data_form,$table);

					redirect('backend/tdp');
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());

					$file = array('upload_sukses' => $this->upload->data());
					$nama_file = $file['upload_sukses']['file_name'];

					$data_form = array(
						'id'=>'',
						'tgl_pendaftaran'=>$this->input->post('tgl_pendaftaran'),
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'jenis_usaha'=>$this->input->post('jenis_usaha'),
						'siup'=>$this->input->post('siup'),
						'no_iupr'=>$this->input->post('no_sk'),
						'tgl_iupr'=>$this->input->post('tgl_sk'),
						'foto'=>$nama_file
						);
					$this->session->set_flashdata('tdp','tambah_sukses');
					$this->back_model->tambah_perizinan($data_form,$table);

					redirect('backend/tdp');
				}

			} else if (isset($_POST['btnIupp'])) {
				
				$table = $this->input->post('table');
				
				if ( ! $this->upload->do_upload('userfile'))
				{
					$data_form = array(
						'id'=>'',
						'tgl_pendaftaran'=>$this->input->post('tgl_pendaftaran'),
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'jenis_usaha'=>$this->input->post('jenis_usaha'),
						'siup'=>$this->input->post('siup'),
						'tdp'=>$this->input->post('tdp'),
						'no_iupp'=>$this->input->post('no_sk'),
						'tgl_iupp'=>$this->input->post('tgl_sk'),
						'no_rekomendasi'=>$this->input->post('no_rekomendasi'),
						'no_rencana'=>$this->input->post('no_rencana'),
						'foto'=>''
						);
					$this->session->set_flashdata('tdp','tambah_sukses');
					$this->back_model->tambah_perizinan($data_form,$table);

					redirect('backend/tdp');
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());

					$file = array('upload_sukses' => $this->upload->data());
					$nama_file = $file['upload_sukses']['file_name'];

					$data_form = array(
						'id'=>'',
						'tgl_pendaftaran'=>$this->input->post('tgl_pendaftaran'),
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'jenis_usaha'=>$this->input->post('jenis_usaha'),
						'siup'=>$this->input->post('siup'),
						'no_iupp'=>$this->input->post('no_sk'),
						'tgl_iupp'=>$this->input->post('tgl_sk'),
						'no_rekomendasi'=>$this->input->post('no_rekomendasi'),
						'no_rencana'=>$this->input->post('no_rencana'),
						'foto'=>$nama_file
						);
					$this->session->set_flashdata('tdp','tambah_sukses');
					$this->back_model->tambah_perizinan($data_form,$table);

					redirect('backend/tdp');
				}

			} else if (isset($_POST['btnIuts'])) {
				
				$table = $this->input->post('table');
				
				if ( ! $this->upload->do_upload('userfile'))
				{
					$data_form = array(
						'id'=>'',
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'npwp'=>$this->input->post('npwp'),
						'no_telp'=>$this->input->post('no_telp'),
						'no_fax'=>$this->input->post('no_fax'),
						'email'=>$this->input->post('email'),
						'keg_usaha_pokok'=>$this->input->post('keg_usaha_pokok'),
						'kbli'=>$this->input->post('kbli'),
						'status_kantor'=>$this->input->post('status_kantor'),
						'siup'=>$this->input->post('siup'),
						'tdp'=>$this->input->post('tdp'),
						'no_iuts'=>$this->input->post('no_sk'),
						'tgl_iuts'=>$this->input->post('tgl_sk'),
						'no_rekomendasi'=>$this->input->post('no_rekomendasi'),
						'no_rencana'=>$this->input->post('no_rencana'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'foto'=>''
						);
					$this->session->set_flashdata('tdp','tambah_sukses');
					$this->back_model->tambah_perizinan($data_form,$table);

					redirect('backend/tdp');
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());

					$file = array('upload_sukses' => $this->upload->data());
					$nama_file = $file['upload_sukses']['file_name'];

					$data_form = array(
						'id'=>'',
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'npwp'=>$this->input->post('npwp'),
						'no_telp'=>$this->input->post('no_telp'),
						'no_fax'=>$this->input->post('no_fax'),
						'email'=>$this->input->post('email'),
						'keg_usaha_pokok'=>$this->input->post('keg_usaha_pokok'),
						'kbli'=>$this->input->post('kbli'),
						'status_kantor'=>$this->input->post('status_kantor'),
						'siup'=>$this->input->post('siup'),
						'no_iuts'=>$this->input->post('no_sk'),
						'tgl_iuts'=>$this->input->post('tgl_sk'),
						'no_rekomendasi'=>$this->input->post('no_rekomendasi'),
						'no_rencana'=>$this->input->post('no_rencana'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'foto'=>$nama_file
						);
					$this->session->set_flashdata('tdp','tambah_sukses');
					$this->back_model->tambah_perizinan($data_form,$table);

					redirect('backend/tdp');
				}

			} else if (isset($_POST['btnStpw'])) {
				
				$table = $this->input->post('table');
				
				if ( ! $this->upload->do_upload('userfile'))
				{
					$data_form = array(
						'id'=>'',
						'no_pendaftaran'=>$this->input->post('no_pendaftaran'),
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'npwp'=>$this->input->post('npwp'),
						'iuts'=>$this->input->post('iuts'),
						'no_stpw'=>$this->input->post('no_sk'),
						'tgl_stpw'=>$this->input->post('tgl_sk'),
						'no_rekomendasi'=>$this->input->post('no_rekomendasi'),
						'no_telp'=>$this->input->post('no_telp'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'foto'=>''
						);
					$this->session->set_flashdata('tdp','tambah_sukses');
					$this->back_model->tambah_perizinan($data_form,$table);

					redirect('backend/tdp');
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());

					$file = array('upload_sukses' => $this->upload->data());
					$nama_file = $file['upload_sukses']['file_name'];

					$data_form = array(
						'id'=>'',
						'no_pendaftaran'=>$this->input->post('no_pendaftaran'),
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'npwp'=>$this->input->post('npwp'),
						'iuts'=>$this->input->post('iuts'),
						'no_stpw'=>$this->input->post('no_sk'),
						'tgl_stpw'=>$this->input->post('tgl_sk'),
						'no_telp'=>$this->input->post('no_telp'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'foto'=>$nama_file
						);
					$this->session->set_flashdata('tdp','tambah_sukses');
					$this->back_model->tambah_perizinan($data_form,$table);

					redirect('backend/tdp');
				}

			} else if (isset($_POST['btnTdg'])) {
				
				$table = $this->input->post('table');
				
				if ( ! $this->upload->do_upload('userfile'))
				{
					$data_form = array(
						'id'=>'',
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'almt_penanggungjwb'=>$this->input->post('almt_penanggungjwb'),
						'almt_gudang'=>$this->input->post('almt_gudang'),
						'no_telp'=>$this->input->post('no_telp'),
						'no_fax'=>$this->input->post('no_fax'),
						'email'=>$this->input->post('email'),
						'jenis_isi_gudang'=>$this->input->post('jenis_isi_gudang'),
						'klasifikasi_gudang'=>$this->input->post('klasifikasi_gudang'),
						'luas_gudang'=>$this->input->post('luas_gudang'),
						'kapasitas_gudang'=>$this->input->post('kapasitas_gudang'),
						'kelengkapan_gudang'=>$this->input->post('kelengkapan_gudang'),
						'no_tdg'=>$this->input->post('no_tdg'),
						'tgl_tdg'=>$this->input->post('tgl_tdg'),
						'tdp'=>$this->input->post('tdp'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'longitude'=>$this->input->post('longitude'),
						'latitude'=>$this->input->post('latitude'),
						'foto'=>''
						);
					$this->session->set_flashdata('tdp','tambah_sukses');
					$this->back_model->tambah_perizinan($data_form,$table);

					redirect('backend/tdp');
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());

					$file = array('upload_sukses' => $this->upload->data());
					$nama_file = $file['upload_sukses']['file_name'];

					$data_form = array(
						'id'=>'',
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'almt_penanggungjwb'=>$this->input->post('almt_penanggungjwb'),
						'almt_gudang'=>$this->input->post('almt_gudang'),
						'no_telp'=>$this->input->post('no_telp'),
						'no_fax'=>$this->input->post('no_fax'),
						'email'=>$this->input->post('email'),
						'jenis_isi_gudang'=>$this->input->post('jenis_isi_gudang'),
						'klasifikasi_gudang'=>$this->input->post('klasifikasi_gudang'),
						'luas_gudang'=>$this->input->post('luas_gudang'),
						'kapasitas_gudang'=>$this->input->post('kapasitas_gudang'),
						'kelengkapan_gudang'=>$this->input->post('kelengkapan_gudang'),
						'no_tdg'=>$this->input->post('no_tdg'),
						'tgl_tdg'=>$this->input->post('tgl_tdg'),
						'tdp'=>$this->input->post('tdp'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'longitude'=>$this->input->post('longitude'),
						'latitude'=>$this->input->post('latitude'),
						'foto'=>$nama_file
						);
					$this->session->set_flashdata('tdp','tambah_sukses');
					$this->back_model->tambah_perizinan($data_form,$table);

					redirect('backend/tdp');
				}

			} else if (isset($_POST['btnSiupmb'])) {
				$table = $this->input->post('table');
				
				if ( ! $this->upload->do_upload('userfile'))
				{
					$data_form = array(
						'id'=>'',
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'siupmb'=>$this->input->post('siupmb'),
						'tdp'=>$this->input->post('tdp'),
						'tgl_keluar'=>$this->input->post('tgl_keluar'),
						'tgl_berakhir'=>$this->input->post('tgl_berakhir'),
						'ins_yg_mengeluarkan'=>$this->input->post('ins_yg_mengeluarkan'),
						'keterangan'=>$this->input->post('keterangan'),
						'foto'=>''
						);
					$this->session->set_flashdata('tdp','tambah_sukses');
					$this->back_model->tambah_perizinan($data_form,$table);

					redirect('backend/tdp');
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());

					$file = array('upload_sukses' => $this->upload->data());
					$nama_file = $file['upload_sukses']['file_name'];

					$data_form = array(
						'id'=>'',
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'siupmb'=>$this->input->post('siupmb'),
						'tdp'=>$this->input->post('tdp'),
						'tgl_keluar'=>$this->input->post('tgl_keluar'),
						'tgl_berakhir'=>$this->input->post('tgl_berakhir'),
						'ins_yg_mengeluarkan'=>$this->input->post('ins_yg_mengeluarkan'),
						'keterangan'=>$this->input->post('keterangan'),
						'foto'=>$nama_file
						);
					$this->session->set_flashdata('tdp','tambah_sukses');
					$this->back_model->tambah_perizinan($data_form,$table);

					redirect('backend/tdp');
				}
			} else if (isset($_POST['btnTdpud'])) {
				
				$table = $this->input->post('table');
				
				if ( ! $this->upload->do_upload('userfile'))
				{
					$data_form = array(
						'id'=>'',
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'npwp'=>$this->input->post('npwp'),
						'no_telp'=>$this->input->post('no_telp'),
						'no_fax'=>$this->input->post('no_fax'),
						'email'=>$this->input->post('email'),
						'keg_usaha_pokok'=>$this->input->post('keg_usaha_pokok'),
						'kbli'=>$this->input->post('kbli'),
						'status_kantor'=>$this->input->post('status_kantor'),
						'tdp'=>$this->input->post('tdp'),
						'no_tdpud'=>$this->input->post('no_sk'),
						'tgl_tdpud'=>$this->input->post('tgl_sk'),
						'jenis_tdpud'=>$this->input->post('jenis_tdpud'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'foto'=>''
						);
					$this->session->set_flashdata('tdp','tambah_sukses');
					$this->back_model->tambah_perizinan($data_form,$table);

					redirect('backend/tdp');
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());

					$file = array('upload_sukses' => $this->upload->data());
					$nama_file = $file['upload_sukses']['file_name'];

					$data_form = array(
						'id'=>'',
						'nm_perusahaan'=>$this->input->post('nm_perusahaan'),
						'nm_penanggungjwb'=>$this->input->post('nm_penanggungjwb'),
						'almt_perusahaan'=>$this->input->post('almt_perusahaan'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'npwp'=>$this->input->post('npwp'),
						'no_telp'=>$this->input->post('no_telp'),
						'no_fax'=>$this->input->post('no_fax'),
						'email'=>$this->input->post('email'),
						'keg_usaha_pokok'=>$this->input->post('keg_usaha_pokok'),
						'kbli'=>$this->input->post('kbli'),
						'status_kantor'=>$this->input->post('status_kantor'),
						'tdp'=>$this->input->post('tdp'),
						'no_tdpud'=>$this->input->post('no_sk'),
						'tgl_tdpud'=>$this->input->post('tgl_sk'),
						'jenis_tdpud'=>$this->input->post('jenis_tdpud'),
						'masaberlaku'=>$this->input->post('masaberlaku'),
						'foto'=>$nama_file
						);
					$this->session->set_flashdata('tdp','tambah_sukses');
					$this->back_model->tambah_perizinan($data_form,$table);

					redirect('backend/tdp');
				}

			} else {
				
				$table = $this->input->post('table');
				
				$data_form = array(
						'id'=>'',
						'no_cas'=>$this->input->post('no_cas'),
						'pos_trf_hs'=>$this->input->post('pos_trf_hs'),
						'kecamatan'=>$this->input->post('kecamatan'),
						'kelurahan'=>$this->input->post('kelurahan'),
						'almt_penyimpanan'=>$this->input->post('almt_penyimpanan'),
						'uraian_brg'=>$this->input->post('uraian_brg'),
						'tata_niaga_impor'=>$this->input->post('tata_niaga_impor'),
						'kep_lain_tdk_utk_pgn'=>$this->input->post('kep_lain_tdk_utk_pgn'),
						'lab'=>$this->input->post('lab'),
						'satuan'=>$this->input->post('satuan')
						);
				$this->session->set_flashdata('tdp','tambah_sukses');
				$this->back_model->tambah_perizinan($data_form,$table);

				redirect('backend/tdp');

			}
		} else {
			redirect(base_url('backend'));
		}
		
	}
}	