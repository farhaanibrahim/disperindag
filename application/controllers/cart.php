<?php 
/**
* 
*/
class Cart extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('back_model');
		$this->load->library('cart');
	}

	public function input_bapok()
	{
		$id_bapok = $this->input->post("id_bapok");
		$tdg = $this->input->post('tdg');
		$sumber = $this->input->post("sumber");
		$volume = $this->input->post("volume");
		$dikeluarkan_ke = $this->input->post("dikeluarkan_ke");
		$tgl_keluar = $this->input->post("tgl_keluar");
		
		$data['cek_barang'] = $this->back_model->cek_stok($id_bapok,$tdg);
		if($data['cek_barang']->num_rows() > 0){
			
			foreach($data['cek_barang']->result() as $row){}
			 if($volume <= $row->jml_stok){
				//stok barang memenuhi
				$data = array(
						'id'      => 'sku_123ABC',
						'qty'     => 100,
						'price'   => 0,
						'name'    => 'Cabai',
						'options' => array('tdg' => '12345', 'sumber'=>'Pasar A', 'dikeluarkan_ke' => 'Pulau B')
					);
				$this->cart->insert($data);
				if($this->cart->insert($data)){
					$this->session->set_flashdata('cek_barang','Berhasil memasukan bahan pokok ke dalam Cart');
				}else{
					$this->session->set_flashdata('cek_barang','Gagal memasukan bahan pokok ke dalam Cart');
				}  
			}else{
				//stok barang tidak memenuhi
				//$this->session->set_flashdata('cek_barang','Stok bapok tidak cukup');
				
			}
		redirect("backend/pengeluaran_bapok");
	}
}
}