<?php

include "koneksi.php";
$awal = $_GET['awal'];
$akhir = $awal + 4;
$query = "SELECT * FROM t_transaksi_b_pokok INNER JOIN t_bahan_pokok ON t_transaksi_b_pokok.id_b_pokok=t_bahan_pokok.id_bahan_pok INNER JOIN t_pasar ON t_transaksi_b_pokok.id_pasar=t_pasar.id_pasar INNER JOIN t_bahan_pokok_sub ON t_transaksi_b_pokok.id_sub_b_pokok=t_bahan_pokok_sub.id_bahan_pok_sub ORDER BY id_tr_pokok DESC LIMIT $awal,$akhir";
$result = $conn->query($query);
	
$outp = "";
while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
    if ($outp != "") {$outp .= ",";}
    $outp .= '{"nama_pasar":"'. $rs["nama_pasar"]. '",';
    $outp .= '"id_tr_pokok":"'. $rs["id_tr_pokok"]. '",';
    $outp .= '"nama_bahan_pokok":"'. $rs["nama_bahan_pokok"]. '",';
    $outp .= '"nama_sub_bahan_pokok":"'. $rs["nama_sub_bahan_pokok"]. '",';
    $outp .= '"kategori":"'. $rs["kategori"]. '",';
    if($rs["foto"] == null){
	$outp .= '"foto_pasar":"null",';	
	}else{
	$outp .= '"foto_pasar":"'. $rs["foto"]. '",';	
	}
	if($rs["foto_b_pokok"] == null){
	$outp .= '"foto_b_pokok":"http://disperindag.kotabogor.go.id/upload/belumtersedia.jpg",';
	$outp .= '"foto_b_pokok2":"belumtersedia.jpg",';
	}else{
	$outp .= '"foto_b_pokok":"http://disperindag.kotabogor.go.id/upload/bahan_pokok/'. $rs["foto_b_pokok"]. '",';
	$outp .= '"foto_b_pokok2":"bahan_pokok/'. $rs["foto_b_pokok"]. '",';
	}
    $outp .= '"satuan":"'. $rs["satuan"]. '",';
    //$outp .= '"satuan_sub":"'. $rs["satuan_sub"]. '",';
    $outp .= '"alamat_pasar":"'. $rs["alamat"]. '",';
    $outp .= '"harga":"'. number_format( $rs["harga"], 0 , ',' , '.' ). '"}'; 
}
//$outp ='{"records":['.$outp.']}';
$outp ='{"num_rows":'.mysqli_num_rows($result).', "records":['.$outp.']}';
$conn->close();

echo($outp);

?>