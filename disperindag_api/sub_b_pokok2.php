<?php
include "koneksi.php";
$query = "SELECT * FROM t_transaksi_b_pokok INNER JOIN t_bahan_pokok ON t_transaksi_b_pokok.id_b_pokok=t_bahan_pokok.id_bahan_pok INNER JOIN t_pasar ON t_transaksi_b_pokok.id_pasar=t_pasar.id_pasar INNER JOIN t_bahan_pokok_sub ON t_transaksi_b_pokok.id_sub_b_pokok=t_bahan_pokok_sub.id_bahan_pok_sub";
$result = $conn->query($query);


$outp = "";
while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
    if ($outp != "") {$outp .= ",";}
    $outp .= '{"nama_pasar":"'. $rs["nama_pasar"]. '",';
    $outp .= '"nama_bahan_pokok":"'. $rs["nama_bahan_pokok"]. '",';
    $outp .= '"nama_sub_bahan_pokok":"'. $rs["nama_sub_bahan_pokok"]. '",';
    $outp .= '"kategori":"'. $rs["kategori"]. '",';
    if($rs["foto"] == null){
	$outp .= '"foto_pasar":"null",';	
	}else{
	$outp .= '"foto_pasar":"'. $rs["foto"]. '",';	
	}
	if($rs["foto_b_pokok"] == null){
	$outp .= '"foto_b_pokok":"null",';
	}else{
	$outp .= '"foto_b_pokok":"'. $rs["foto_b_pokok"]. '",';
	}
    $outp .= '"satuan":"'. $rs["satuan"]. '",';
    $outp .= '"satuan_sub":"'. $rs["satuan_sub"]. '",';
    $outp .= '"alamat_pasar":"'. $rs["alamat"]. '",';
    $outp .= '"harga":"'. $rs["harga"]. '"}'; 
}
$outp ='{"status":"sukses","records":['.$outp.']}';
$conn->close();

echo($outp);

?>