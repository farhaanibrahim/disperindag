<?php

include "koneksi.php";
$awal = $_GET['awal'];
$akhir = $awal + 4;
$query = "SELECT * FROM t_berita INNER JOIN t_kategori_berita ON t_berita.kategori=t_kategori_berita.id_kategori INNER JOIN t_admin ON t_berita.publisher=t_admin.admin_id ORDER BY id_berita DESC LIMIT $awal,$akhir";
$result = $conn->query($query);


$outp = "";
while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
    if ($outp != "") {$outp .= ",";}
    $outp .= '{"id_berita":"'. $rs["id_berita"]. '",';
    $outp .= '"judul":"'. $rs["judul"]. '",';
    $outp .= '"isi_berita":"'. strip_tags($rs["isi_berita"]). '",';
    $outp .= '"tanggal_posting":"'. $rs["tanggal_posting"]. '",';
    $outp .= '"publish_status":"'. $rs["publish_status"]. '",';
	 if($rs["cover_berita"] == null){
	$outp .= '"cover_berita":"null",';	
	}else{
	$outp .= '"cover_berita":"'. $rs["cover_berita"]. '",';	
	}
    $outp .= '"nama_user":"'. $rs["nama_user"]. '"}'; 
}
$outp ='{"num_rows":'.mysqli_num_rows($result).', "records":['.$outp.']}';
$conn->close();

echo($outp);

?>