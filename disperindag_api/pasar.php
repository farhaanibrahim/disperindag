<?php
include "koneksi.php";
$query = "SELECT * FROM t_pasar ORDER BY nama_pasar ASC";
$result = $conn->query($query);


$outp = "";
while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
    if ($outp != "") {$outp .= ",";}
    $outp .= '{"nama_pasar":"'. $rs["nama_pasar"]. '",';
    $outp .= '"id_pasar":"'. $rs["id_pasar"]. '",';
    $outp .= '"lat":"'. $rs["lat"]. '",';
    $outp .= '"lon":"'. $rs["lon"]. '",';
	
	if($rs["foto"] == null){
	$outp .= '"foto_pasar":"http://disperindag.kotabogor.go.id/upload/belumtersedia.jpg",';	
	}else{
	$outp .= '"foto_pasar":"http://disperindag.kotabogor.go.id/upload/pasar/'. $rs["foto"]. '",';	
	}
    
	$outp .= '"alamat":"'. $rs["alamat"]. '"}'; 
}
$outp ='{"records":['.$outp.']}';
$conn->close();

echo($outp);

?>