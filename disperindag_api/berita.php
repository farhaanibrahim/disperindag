<?php

include "koneksi.php";
$query = "SELECT * FROM t_berita INNER JOIN t_kategori_berita ON t_berita.kategori=t_kategori_berita.id_kategori INNER JOIN t_admin ON t_berita.publisher=t_admin.admin_id";
$result = $conn->query($query);


$outp = "";
while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
    if ($outp != "") {$outp .= ",";}
    $outp .= '{"id_berita":"'. $rs["id_berita"]. '",';
    $outp .= '"judul":"'. $rs["judul"]. '",';
    $outp .= '"isi_berita":"'. strip_tags($rs["isi_berita"]). '",';
    $outp .= '"tanggal_posting":"'. $rs["tanggal_posting"]. '",';
    $outp .= '"publish_status":"'. $rs["publish_status"]. '",';
	 if($rs["cover_berita"] == null){
	$outp .= '"cover_berita":"null",';	
	}else{
	$outp .= '"cover_berita":"'. $rs["cover_berita"]. '",';	
	}
    $outp .= '"nama_user":"'. $rs["nama_user"]. '"}'; 
}
$outp ='{"records":['.$outp.']}';
$conn->close();

echo($outp);

?>